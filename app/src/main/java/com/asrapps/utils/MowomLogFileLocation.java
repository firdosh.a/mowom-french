package com.asrapps.utils;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

/**
 * Created by richa on 25/11/16.
 */

public class MowomLogFileLocation {

    private static String file = "NerrukarLocation.txt";
    private static String filePath = Environment.getExternalStorageDirectory()
            + File.separator + "NerrukarLocation" + File.separator;

    public static void writeToLog(String data) {
        try {

            File mFolder = new File(filePath);
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            File f = new File(filePath, file);
            if (!f.exists()) {
                f.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(f, true);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(data);
            osw.flush();
            osw.close();
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeToLog(Exception data) {
        try {
            File f = new File(filePath, file);
            FileOutputStream fOut = new FileOutputStream(f, true);

            PrintStream ps = new PrintStream(fOut, false);
            data.printStackTrace(ps);
            fOut.flush();
            fOut.close();
            ps.close();
        } catch (Exception e) {

        }
    }

}


