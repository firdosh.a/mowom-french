package com.asrapps.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    //TABLE FOR LOGIN USER DATA
    public static final String TABLE_LOGIN_USER = "tbl_login_user";
    public static final String UID = "UID";
    public static final String USERNAME = "userName";
    public static final String PASSWORD = "password";

    public static final String IsReport = "is_report";
    public static final String IsReview = "is_review";
    public static final String IsSignature = "is_signature";

    public static final String EMP_NAME = "emp_name";
    public static final String EMP_PHONE = "phone";
    public static final String EMP_USER_ID = "user_id";
    public static final String USER_IMAGE_SD_CARD = "profileImage";
    public static final String USER_IMAGE_PARTENER = "partenerLogo";

    //TABLE FOR LOCATION FOR STARTED JOB
    public static final String TABLE_LOCATION = "tbl_location";
    public static final String UNIQUE_ID = "id";
    public static final String EMP_ID = "emp_id";
    public static final String TASK_ID = "task_id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ET_ID = "et_Id";

    //TABLE FOR LOCATION FOR ADD SALES TASK
    public static final String TABLE_LOCATION_ADD_SALES = "tbl_location_add_sales";
    public static final String SALES_UNIQUE_ID = "sales_id";
    public static final String SALES_EMP_ID = "sales_emp_id";
    public static final String SALES_TRANS_ID = "sales_trans_id";
    public static final String SALES_LATITUDE = "sales_latitude";
    public static final String SALES_LONGITUDE = "sales_longitude";
    public static final String SALES_ET_ID = "saled_et_Id";


    // TABLE INFORMATTION FOR PHOTOS TAKEN
    public static final String TABLE_PHOTO_TAKEN_SALES = "tbl_photo_taken_sales";
    public static final String PHOTO_ID_SALES = "p_id";
    public static final String PHOTO_USER_ID_SALES = "user_id";
    public static final String PHOTO_USER_ID_SALES_tx = "user_id_tx";
    public static final String PHOTO_IMAGE_SALES = "taken_image";
    public static final String PHOTO_TAKEN_TIME_SALES = "taken_time";


    // TABLE INFORMATTION FOR PHOTOS TAKEN
    public static final String TABLE_PHOTO_TAKEN = "tbl_photo_taken";
    public static final String PHOTO_ID = "p_id";
    public static final String PHOTO_TASK_ID = "task_id";
//    public static final String PHOTO_IMAGE = "taken_image";
    public static final String PHOTO_TAKEN_TIME = "taken_time";
    public static final String PHOTO_PATH = "path_pic";


    // TABLE INFORMATTION FOR SAVED ITEMS
    public static final String TABLE_SAVED_ITEMS = "tbl_saved_items";
    public static final String SAVED_ID = "p_id";
    public static final String SAVED_TASK_ID = "task_id";
    public static final String SAVED_ERROR = "issue";
    public static final String SAVED_START_TIME = "start_time";
    public static final String SAVED_TOTAL_TIME_SPEND = "total_time_spend";
    public static final String SAVED_FINISHED_TIME = "finished_time";
    public static final String SAVED_TASK_REPORT = "task_report";
    public static final String SAVED_RATING = "rating";
    public static final String SAVED_COMMENTS = "comments";
    public static final String SAVED_SIGNATURE = "signature";


    public static final String SAVED_PROBLEM_REPORTED = "problem_reported";
    public static final String SAVED_DIAGNOSIS_DONE = "diagnosis_done";
    public static final String SAVED_RESOLUTION = "resolution";
    public static final String SAVED_NOTES_SUPPORT = "notes_support";


    public static final String SAVED_DELIVERY_STATUS = "deilvery_status";
    public static final String SAVED_NOTES_DELIVERY = "notes_delevery";

    // DATABASE INFORMATION
    static final String DATA_BASE_NAME = "movom.db";
    static final int DATA_BASE_VERSION = 5;

    //TABLE CREATION FOR LOGIN USER
    private static final String CREATE_LOGIN_USER = "create table "
            + TABLE_LOGIN_USER + "( " + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + USERNAME + " TEXT NOT NULL , "
            + PASSWORD + " TEXT , "

            + IsReport + " TEXT , "
            + IsReview + " TEXT , "
            + IsSignature + " TEXT , "

            + EMP_NAME + " TEXT , "
            + EMP_PHONE + " TEXT , "
            + USER_IMAGE_SD_CARD + " TEXT , "
            + USER_IMAGE_PARTENER + " TEXT , "
            + EMP_USER_ID + " TEXT );";

    //    //TABLE CREATION FOR START TIME COUNTER TASK
    private static final String CREATE_TABLE_LOCATION = "create table "
            + TABLE_LOCATION + "(" + UNIQUE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EMP_ID + " TEXT, "
            + TASK_ID + " TEXT, "
            + LATITUDE + " TEXT, "
            + LONGITUDE + " TEXT, "
            + ET_ID + " TEXT);";

    //    //TABLE CREATION FOR START TIME COUNTER TASK
    private static final String CREATE_TABLE_LOCATION_SALES = "create table "
            + TABLE_LOCATION_ADD_SALES + "(" + SALES_UNIQUE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SALES_EMP_ID + " TEXT, "
            + SALES_TRANS_ID + " TEXT, "
            + SALES_LATITUDE + " TEXT, "
            + SALES_LONGITUDE + " TEXT, "
            + SALES_ET_ID + " TEXT);";

    // TABLE CREATION STATEMENT FOR PHOTOS
    private static final String CREATE_PHOTO_TABLE = "create table "
            + TABLE_PHOTO_TAKEN + "(" + PHOTO_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PHOTO_TASK_ID
//            + " TEXT NOT NULL ," + PHOTO_IMAGE
            + " BLOB ," + PHOTO_TAKEN_TIME
            + " TEXT NOT NULL,"+PHOTO_PATH+" TEXT NOT NULL);";

    private static final String CREATE_PHOTO_TABLE_SALES = "create table "
            + TABLE_PHOTO_TAKEN_SALES + "(" + PHOTO_ID_SALES
            + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PHOTO_USER_ID_SALES + " TEXT NOT NULL ,"
            + PHOTO_USER_ID_SALES_tx + " TEXT NOT NULL ,"
            + PHOTO_IMAGE_SALES + " BLOB ," + PHOTO_TAKEN_TIME_SALES
            + " TEXT NOT NULL);";

    // TABLE CREATION STATEMENT FOR SAVED DETAILS
    private static final String CREATE_SAVED_TABLE = "create table "
            + TABLE_SAVED_ITEMS + "("
            + SAVED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SAVED_TASK_ID + " TEXT,"
            + SAVED_ERROR + " TEXT,"
            + SAVED_TOTAL_TIME_SPEND + " TEXT ,"
            + SAVED_START_TIME + " TEXT ,"
            + SAVED_FINISHED_TIME + " TEXT ,"
            + SAVED_TASK_REPORT + " TEXT ,"
            + SAVED_RATING + " TEXT ,"
            + SAVED_COMMENTS + " TEXT ,"
            + SAVED_SIGNATURE + " BLOB ,"
            + SAVED_PROBLEM_REPORTED + " TEXT ,"
            + SAVED_DIAGNOSIS_DONE + " TEXT ,"
            + SAVED_RESOLUTION + " TEXT ,"
            + SAVED_NOTES_SUPPORT + " TEXT ,"
            + SAVED_DELIVERY_STATUS + " TEXT ,"
            + SAVED_NOTES_DELIVERY + " TEXT );";


    //TABLE INFORMATION FOR PENDING TASK LIST
    public static final String TABLE_PENDING_TASK = "tbl_pending_task";

    public static final String PENDING_TASK_USER_ID = "user_id";
    public static final String PENDING_task_type = "PENDING_task_type";
    public static final String PENDING_taskId = "PENDING_taskId";
    public static final String PENDING_duration = "PENDING_duration";

    public static final String PENDING_is_google_sheet = "PENDING_is_google_sheet";
    public static final String PENDING_Google_sheet_url = "PENDING_Google_sheet_url";

    public static final String PENDING_issue = "PENDING_issue";
    public static final String PENDING_date_time = "PENDING_date_time";
    public static final String PENDING_phone = "PENDING_phone";
    public static final String PENDING_address = "PENDING_address";
    public static final String PENDING_org_name = "PENDING_org_name";
    public static final String PENDING_org_ph_num = "PENDING_org_ph_num";
    public static final String PENDING_latitude = "PENDING_latitude";
    public static final String PENDING_longitude = "PENDING_longitude";
    public static final String PENDING_time = "PENDING_time";
    public static final String PENDING_personName = "PENDING_personName";
    public static final String PENDING_supervisorName = "PENDING_supervisorName";
    public static final String PENDING_tasktype = "PENDING_tasktype";
    public static final String PENDING_additionalInfo = "PENDING_additionalInfo";
    public static final String PENDING_startTime = "PENDING_startTime";
    public static final String PENDING_stopTime = "PENDING_stopTime";
    public static final String PENDING_totalTimeSpend = "PENDING_totalTimeSpend";
    public static final String PENDING_taskReport = "PENDING_taskReport";
    public static final String PENDING_consumerFeedback = "PENDING_consumerFeedback";
    public static final String PENDING_rating = "PENDING_rating";
    public static final String PENDING_signatureImage = "PENDING_signatureImage";
    public static final String PENDING_signatureString = "PENDING_signatureString";
    public static final String PENDING_reason = "PENDING_reason";
    public static final String PENDING_problem_reported = "PENDING_problem_reported";
    public static final String PENDING_diagnosis_done = "PENDING_diagnosis_done";
    public static final String PENDING_resolution = "PENDING_resolution";
    public static final String PENDING_notes_support = "PENDING_notes_support";
    public static final String PENDING_deilvery_status = "PENDING_deilvery_status";
    public static final String PENDING_notes_delevery = "PENDING_notes_delevery";
    public static final String PENDING_Is_Recuring = "PENDING_Is_Recuring";
    public static final String PENDING_recurring_type = "PENDING_recurring_type";
    public static final String PENDING_recurring_end_date = "PENDING_recurring_end_date";
    public static final String PENDING_ET_ID = "PENDING_ET_ID";


    // TABLE CREATION STATEMENT FOR PENDING TASK LISTS
    private static final String CREATE_PENDING_TABLE = "create table "
            + TABLE_PENDING_TASK + "("
            + PENDING_task_type + " TEXT, "
            + PENDING_taskId + " TEXT, "
            + PENDING_duration + " TEXT, "
            + PENDING_is_google_sheet + " TEXT, "
            + PENDING_Google_sheet_url + " TEXT, "
            + PENDING_issue + " TEXT, "
            + PENDING_date_time + " TEXT, "
            + PENDING_phone + " TEXT, "
            + PENDING_address + " TEXT, "
            + PENDING_org_name + " TEXT, "
            + PENDING_org_ph_num + " TEXT, "
            + PENDING_latitude + " TEXT, "
            + PENDING_longitude + " TEXT, "
            + PENDING_time + " TEXT, "
            + PENDING_personName + " TEXT, "
            + PENDING_supervisorName + " TEXT, "
            + PENDING_tasktype + " TEXT, "
            + PENDING_additionalInfo + " TEXT, "
            + PENDING_startTime + " TEXT, "
            + PENDING_stopTime + " TEXT, "
            + PENDING_totalTimeSpend + " TEXT, "
            + PENDING_taskReport + " TEXT, "
            + PENDING_consumerFeedback + " TEXT, "
            + PENDING_rating + " TEXT, "
            + PENDING_signatureImage + " BLOB , "
            + PENDING_signatureString + " TEXT, "
            + PENDING_reason + " TEXT, "
            + PENDING_problem_reported + " TEXT, "
            + PENDING_diagnosis_done + " TEXT, "
            + PENDING_resolution + " TEXT, "
            + PENDING_notes_support + " TEXT, "
            + PENDING_deilvery_status + " TEXT, "
            + PENDING_notes_delevery + " TEXT, "
            + PENDING_Is_Recuring + " TEXT, "
            + PENDING_recurring_type + " TEXT, "
            + PENDING_recurring_end_date + " TEXT, "
            + PENDING_ET_ID + " TEXT, "
            + PENDING_TASK_USER_ID + " TEXT);";


    public static final String TABLE_SALES_TASK = "tbl_sales_task";

    public static final String SALES_TASK_ID = "task_id";
    public static final String SALES_TASK_ID_TR = "task_id_tr";
    public static final String SALES_client_name_new = "SALES_client_name_new";
    public static final String SALES_client_name = "SALES_client_name";
    public static final String SALES_client_address = "SALES_client_address";
    public static final String SALES_contact_person = "SALES_contact_person";
    //Add new arraylist field
    public static final String SALES_CLIENT_ADDRESS_ARRAY = "Sales_client_address_array";
    public static final String SALES_tel_number = "tel_number";
    public static final String SALES_client_email = "client_email";
    public static final String SALES_latitude = "latitude";
    public static final String SALES_longtitude = "longtitude";
    public static final String SALES_client_id = "client_id";
    public static final String SALES_category_id = "category_id";
    public static final String SALES_next_steps = "next_steps";
    public static final String SALES_notes = "notes";
    public static final String SALES_product = "product";
    public static final String SALES_detail = "detail";
    public static final String SALES_department = "department";
    public static final String SALES_emp_id = "emp_id";
    public static final String SALES_company_id = "company_id";
    public static final String SALES_task_name = "task_name";
    public static final String SALES_additional_instruction = "additional_instruction";
    public static final String SALES_tasktype = "tasktype";
    public static final String SALES_starttime = "strattime";
    public static final String SALES_endtime = "endtime";
    public static final String SALES_supervisor_id = "supervisor_id";


    private static final String CREATE_SALES_TASK_TABLE = "create table "
            + TABLE_SALES_TASK + "("
            + SALES_TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SALES_client_name_new + " TEXT, "
            + SALES_TASK_ID_TR + " TEXT, "
            + SALES_client_name + " TEXT, "
            + SALES_client_address + " TEXT, "
            + SALES_contact_person + " TEXT, "
            + SALES_tel_number + " TEXT, "
            + SALES_client_email + " TEXT, "
            + SALES_latitude + " TEXT, "
            + SALES_longtitude + " TEXT, "
            + SALES_client_id + " TEXT, "
            + SALES_category_id + " TEXT, "
            + SALES_next_steps + " TEXT, "
            + SALES_notes + " TEXT, "
            + SALES_product + " TEXT, "
            + SALES_detail + " TEXT, "
            + SALES_department + " TEXT, "
            + SALES_emp_id + " TEXT, "
            + SALES_company_id + " TEXT, "
            + SALES_task_name + " TEXT, "
            + SALES_starttime + " TEXT, "
            + SALES_endtime + " TEXT, "
            + SALES_supervisor_id + " TEXT, "

            + SALES_additional_instruction + " TEXT, "
            + SALES_tasktype + " TEXT);";


    public static final String TABLE_SURVEY_TASK = "tbl_survey_task";

    public static final String SURVEY_TASK_ID = "task_id";
    public static final String SURVEY_client_name_new = "SURVEY_client_name_new";
    public static final String SURVEY_client_name = "SURVEY_client_name";
    public static final String SURVEY_client_address = "SURVEY_client_address";
    public static final String SURVEY_tel_number = "tel_number";
    public static final String SURVEY_latitude = "latitude";
    public static final String SURVEY_longtitude = "longtitude";
    public static final String SURVEY_client_id = "client_id";
    public static final String SURVEY_category_id = "category_id";
    public static final String SURVEY_emp_id = "emp_id";
    public static final String SURVEY_notes = "notes";
    public static final String SURVEY_usertx = "usertx";
    public static final String SURVEY_companyId = "companyId";
    public static final String SURVEY_supervisor_id = "supervisor_id";


    private static final String CREATE_SURVEY_TASK_TABLE = "create table "
            + TABLE_SURVEY_TASK + "("
            + SURVEY_TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SURVEY_client_name_new + " TEXT, "
            + SURVEY_client_name + " TEXT, "
            + SURVEY_client_address + " TEXT, "
            + SURVEY_tel_number + " TEXT, "
            + SURVEY_latitude + " TEXT, "
            + SURVEY_longtitude + " TEXT, "
            + SURVEY_client_id + " TEXT, "
            + SURVEY_category_id + " TEXT, "
            + SURVEY_emp_id + " TEXT, "
            + SURVEY_usertx + " TEXT, "
            + SURVEY_companyId + " TEXT, "
            + SURVEY_supervisor_id + " TEXT, "
            + SURVEY_notes + " TEXT);";


    public static final String TABLE_SURVEY_CLIENT_TASK = "tbl_survey_client_task";

    public static final String SURVEY_CLIENT_CLIENT_ID = "task_id";
    public static final String SURVEY_CLIENT_client_name = "client_name";
    public static final String SURVEY_CLIENT_CLIENT_address = "address";
    public static final String SURVEY_CLIENT_client_contact = "client_contact";

    private static final String CREATE_SURVEY_CLIENT_TABLE = "create table "
            + TABLE_SURVEY_CLIENT_TASK + "("
            + SURVEY_CLIENT_CLIENT_ID + " TEXT, "
            + SURVEY_CLIENT_CLIENT_address + " TEXT, "
            + SURVEY_CLIENT_client_contact + " TEXT, "
            + SURVEY_CLIENT_client_name + " TEXT);";


    public static final String TABLE_SALES_CLIENT_TASK = "tbl_sales_client_task";

    public static final String SALES_CLIENT_CLIENT_ID = "task_id";
    public static final String SALES_CLIENT_client_name = "client_name";
    public static final String SALES_CLIENT_CLIENT_address = "SALES_CLIENT_CLIENT_address";
    public static final String SALES_CLIENT_client_contact = "SALES_CLIENT_client_contact";
    public static final String SALES_CLIENT_CLIENT_email = "SALES_CLIENT_CLIENT_email";
    public static final String SALES_CLIENT_client_phone = "SALES_CLIENT_client_phone";


    private static final String CREATE_SALES_CLIENT_TABLE = "create table "
            + TABLE_SALES_CLIENT_TASK + "("
            + SALES_CLIENT_CLIENT_ID + " TEXT, "
            + SALES_CLIENT_CLIENT_address + " TEXT, "
            + SALES_CLIENT_client_contact + " TEXT, "
            + SALES_CLIENT_CLIENT_email + " TEXT, "
            + SALES_CLIENT_client_phone + " TEXT, "
            + SALES_CLIENT_client_name + " TEXT);";

    //CREATE ADD SALES TASK FOR FILL DETAIL AUTOMATICALLY
    public static final String TABLE_ADD_SALES_TASK = "ADD_SALES_TASK";
    public static final String SALES_CLIENT_USER_ID = "SALES_CLIENT_USER_ID";
    public static final String SALES_CLIENT_ID = "SALES_CLIENT_ID";
    public static final String SALES_CLIENT_NAME = "SALES_CLIENT_NAME";
    public static final String SALES_CONTACT_NAME = "SALES_CONTACT_NAME";
    public static final String SALES_CLIENT_ADDRESS = "SALES_CLIENT_ADDRESS";
    public static final String SALES_CLIENT_PHONE = "SALES_CLIENT_PHONE";
    public static final String SALES_CLIENT_EMAIL = "SALES_CLIENT_EMAIL";
    public static final String SALES_CLIENT_START_TIME = "SALES_CLIENT_START_TIME";
    public static final String SALES_CLIENT_END_TIME = "SALES_CLIENT_END_TIME";
    public static final String SALES_CLIENT_PRODUCT = "SALES_CLIENT_PRODUCT";
    public static final String SALES_CLIENT_MEETING_NOTE = "SALES_CLIENT_MEETING_NOTE";
    public static final String SALES_CLIENT_NEXT_STEP = "SALES_CLIENT_NEXT_STEP";
    public static final String SALES_CLIENT_DETAIL = "SALES_CLIENT_DETAIL";
    public static final String SALES_CLIENT_TASK_ID_TX = "SALES_CLIENT_TASK_ID_TX";
    public static final String SALES_CLIENT_TASK_ID = "SALES_CLIENT_TASK_ID";


    private static final String CREATE_ADD_SALES_TASK = "create table "
            + TABLE_ADD_SALES_TASK + "("
            + SALES_CLIENT_USER_ID + " TEXT, "
            + SALES_CLIENT_ID + " TEXT, "
            + SALES_CLIENT_NAME + " TEXT, "
            + SALES_CONTACT_NAME + " TEXT, "
            + SALES_CLIENT_ADDRESS + " TEXT, "
            + SALES_CLIENT_PHONE + " TEXT, "
            + SALES_CLIENT_EMAIL + " TEXT, "
            + SALES_CLIENT_START_TIME + " TEXT, "
            + SALES_CLIENT_END_TIME + " TEXT, "
            + SALES_CLIENT_PRODUCT + " TEXT, "
            + SALES_CLIENT_MEETING_NOTE + " TEXT, "
            + SALES_CLIENT_NEXT_STEP + " TEXT, "
            + SALES_CLIENT_DETAIL + " TEXT, "
            + SALES_CLIENT_TASK_ID_TX + " TEXT, "
            + SALES_CLIENT_TASK_ID + " TEXT);";

    public static final String TABLE_CATEGORY = "tbl_category";

    public static final String CATEGORY_ID = "cat_id";
    public static final String CATEGORY_name = "cat_name";

    private static final String CREATE_CATEGORY_TABLE = "create table "
            + TABLE_CATEGORY + "("
            + CATEGORY_ID + " TEXT, "
            + CATEGORY_name + " TEXT);";


    public DbHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_LOCATION);
        db.execSQL(CREATE_TABLE_LOCATION_SALES);
        db.execSQL(CREATE_LOGIN_USER);
        db.execSQL(CREATE_PHOTO_TABLE);
        db.execSQL(CREATE_PHOTO_TABLE_SALES);
        db.execSQL(CREATE_SAVED_TABLE);

        db.execSQL(CREATE_PENDING_TABLE);
        db.execSQL(CREATE_SALES_TASK_TABLE);
        db.execSQL(CREATE_SURVEY_TASK_TABLE);

        db.execSQL(CREATE_SURVEY_CLIENT_TABLE);
        db.execSQL(CREATE_SALES_CLIENT_TABLE);
        db.execSQL(CREATE_ADD_SALES_TASK);
        db.execSQL(CREATE_CATEGORY_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION_ADD_SALES);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTO_TAKEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTO_TAKEN_SALES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVED_ITEMS);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PENDING_TASK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALES_TASK);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_TASK);

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY_CLIENT_TASK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALES_CLIENT_TASK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADD_SALES_TASK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);

        onCreate(db);
    }
}
