package com.asrapps.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.model.AddSalesLocation;
import com.asrapps.mowom.model.CategoryData;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.ClientData;
import com.asrapps.mowom.model.LocationOffline;
import com.asrapps.mowom.model.LocationOfflineGroup;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.model.PhotoModel;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.asrapps.mowom.model.SalesTask;
import com.asrapps.mowom.model.SurveyTask;
import com.asrapps.utils.MowomLogFileLocation;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.asrapps.dbhelper.DbHelper.EMP_USER_ID;
import static com.asrapps.dbhelper.DbHelper.PENDING_task_type;
import static com.asrapps.dbhelper.DbHelper.PHOTO_ID;
import static com.asrapps.dbhelper.DbHelper.TABLE_ADD_SALES_TASK;
import static com.asrapps.dbhelper.DbHelper.TABLE_LOCATION;
import static com.asrapps.dbhelper.DbHelper.TABLE_LOCATION_ADD_SALES;
import static com.asrapps.dbhelper.DbHelper.TABLE_LOGIN_USER;

public class SQLController {

    private DbHelper dbhelper;
    private Context ourcontext;
    private SQLiteDatabase database;

    public SQLController(Context c) {
        ourcontext = c;
    }

    public SQLController open() throws SQLException {
        dbhelper = new DbHelper(ourcontext);
        database = dbhelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbhelper.close();
    }

    public void AddUserLogin(Login login) {

        try {
          ContentValues cv = new ContentValues();
            cv.put(DbHelper.USERNAME, login.emp_email);
            cv.put(DbHelper.PASSWORD, login.password);
            cv.put(DbHelper.EMP_NAME, login.emp_name);
            cv.put(DbHelper.EMP_PHONE, login.phone);

            cv.put(DbHelper.PASSWORD, login.password);
            cv.put(DbHelper.EMP_NAME, login.emp_name);
            cv.put(DbHelper.EMP_PHONE, login.phone);

            cv.put(DbHelper.IsReport, login.is_report);
            cv.put(DbHelper.IsReview, login.is_review);
            cv.put(DbHelper.IsSignature, login.is_signature);

            cv.put(EMP_USER_ID, login.emp_id);
            cv.put(DbHelper.USER_IMAGE_SD_CARD, login.profileImageSdCard);
            cv.put(DbHelper.USER_IMAGE_PARTENER, login.client_logo);
            database.insert(TABLE_LOGIN_USER, null, cv);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
        System.out.println("...................Inserted..!!!");
    }

    public void DeleteUserById() {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_LOGIN_USER);
        database.close();
    }

    public boolean loginById(Login login) {
        database = dbhelper.getReadableDatabase();
//        database.execSQL("delete from " + DbHelper.TABLE_LOGIN_USER + " WHERE " + DbHelper.EMP_USER_ID + " = " + login.emp_id);
        database.execSQL("delete from " + DbHelper.TABLE_LOGIN_USER);
        database.close();
        return false;
    }

    public ArrayList<Login> GetUserLogin() {
        ArrayList<Login> login = new ArrayList<Login>();
        try {
            login.clear();
            String selectQuery = "SELECT * FROM " + TABLE_LOGIN_USER;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Login model = new Login();
                    model.emp_id = cursor.getString(cursor.getColumnIndex(EMP_USER_ID));
                    model.emp_email = cursor.getString(cursor.getColumnIndex(DbHelper.USERNAME));
                    model.password = cursor.getString(cursor.getColumnIndex(DbHelper.PASSWORD));

                    model.is_report = cursor.getString(cursor.getColumnIndex(DbHelper.IsReport));
                    model.is_review = cursor.getString(cursor.getColumnIndex(DbHelper.IsReview));
                    model.is_signature = cursor.getString(cursor.getColumnIndex(DbHelper.IsSignature));

                    model.emp_name = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_NAME));
                    model.phone = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_PHONE));
                    model.profileImageSdCard = cursor.getString(cursor.getColumnIndex(DbHelper.USER_IMAGE_SD_CARD));
                    model.client_logo = cursor.getString(cursor.getColumnIndex(DbHelper.USER_IMAGE_PARTENER));
                    login.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return login;
        } catch (Exception e) {
            Log.e("Login User", "" + e);
        } finally {
            database.close();
        }
        return login;
    }


    public ArrayList<Login> GetUserLoginByUserId(String userId) {
        ArrayList<Login> login = new ArrayList<Login>();
        try {
            login.clear();
            String selectQuery = "SELECT  * FROM " + TABLE_LOGIN_USER + " WHERE " + EMP_USER_ID
                    + " = " + userId;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Login model = new Login();
                    model.emp_id = cursor.getString(cursor.getColumnIndex(EMP_USER_ID));
                    model.emp_email = cursor.getString(cursor.getColumnIndex(DbHelper.USERNAME));
                    model.password = cursor.getString(cursor.getColumnIndex(DbHelper.PASSWORD));

                    model.is_report = cursor.getString(cursor.getColumnIndex(DbHelper.IsReport));
                    model.is_review = cursor.getString(cursor.getColumnIndex(DbHelper.IsReview));
                    model.is_signature = cursor.getString(cursor.getColumnIndex(DbHelper.IsSignature));

                    model.emp_name = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_NAME));
                    model.phone = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_PHONE));
                    model.profileImageSdCard = cursor.getString(cursor.getColumnIndex(DbHelper.USER_IMAGE_SD_CARD));
                    model.client_logo = cursor.getString(cursor.getColumnIndex(DbHelper.USER_IMAGE_PARTENER));
                    login.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return login;
        } catch (Exception e) {
            Log.e("Login User", "" + e);
        } finally {
            database.close();
        }
        return login;
    }

    public ArrayList<Login> getLastUSer() {
        ArrayList<Login> login = new ArrayList<Login>();
        try {
            login.clear();
            //SELECT * FROM tablename ORDER BY column DESC LIMIT 1;
            //SELECT * FROM    TABLE  WHERE   ID = (SELECT MAX(ID)  FROM TABLE);
            String selectQuery = "SELECT  * FROM " + TABLE_LOGIN_USER + " WHERE " + DbHelper.UID + " = " + "(SELECT MAX(" + DbHelper.UID
                    + ") FROM " + TABLE_LOGIN_USER + ")";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    Login model = new Login();
                    model.emp_id = cursor.getString(cursor.getColumnIndex(EMP_USER_ID));
                    model.emp_email = cursor.getString(cursor.getColumnIndex(DbHelper.USERNAME));
                    model.password = cursor.getString(cursor.getColumnIndex(DbHelper.PASSWORD));

                    model.is_report = cursor.getString(cursor.getColumnIndex(DbHelper.IsReport));
                    model.is_review = cursor.getString(cursor.getColumnIndex(DbHelper.IsReview));
                    model.is_signature = cursor.getString(cursor.getColumnIndex(DbHelper.IsSignature));

                    model.emp_name = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_NAME));
                    model.phone = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_PHONE));
                    model.profileImageSdCard = cursor.getString(cursor.getColumnIndex(DbHelper.USER_IMAGE_SD_CARD));
                    model.client_logo = cursor.getString(cursor.getColumnIndex(DbHelper.USER_IMAGE_PARTENER));
                    login.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return login;
        } catch (Exception e) {
            Log.e("Login User", "" + e);
        } finally {
            database.close();
        }
        return login;
    }


    public boolean addImageSdCard(Login mLogin) {
        ArrayList<Login> login = new ArrayList<Login>();

        String selectQuery = "SELECT  * FROM " + TABLE_LOGIN_USER + " WHERE " + EMP_USER_ID
                + " = " + mLogin.emp_id;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {

            ContentValues values = new ContentValues();
            values.put(DbHelper.USER_IMAGE_SD_CARD, mLogin.profileImageSdCard);
            values.put(DbHelper.USER_IMAGE_PARTENER, mLogin.client_logo);

            String where = EMP_USER_ID + " = ?";
            String[] whereArgs = new String[]{mLogin.emp_id + ""};
            int upd = database.update(TABLE_LOGIN_USER, values, where, whereArgs);

            Log.v("Mowom", "data updated= " + upd);

            return true;
        }
        cursor.close();
        database.close();

        return false;

    }


    public void InsertToPhoto(String taskId, byte[] image, String taken_time,String path) {
        ContentValues cv = new ContentValues();
        cv.put(DbHelper.PHOTO_TASK_ID, taskId);
//        cv.put(DbHelper.PHOTO_IMAGE, image);
        cv.put(DbHelper.PHOTO_TAKEN_TIME, taken_time);
        cv.put(DbHelper.PHOTO_PATH, path);
        long res = database.insert(DbHelper.TABLE_PHOTO_TAKEN, null, cv);
        Log.v("ttt","res  = "+res);
    }

    public void InsertToPhotoSales(String userId, byte[] image, String taken_time, String ux) {
        ContentValues cv = new ContentValues();
        cv.put(DbHelper.PHOTO_USER_ID_SALES, userId);
//        cv.put(DbHelper.PHOTO_IMAGE, image);
        cv.put(DbHelper.PHOTO_USER_ID_SALES_tx, ux);
        cv.put(DbHelper.PHOTO_TAKEN_TIME, taken_time);
        database.insert(DbHelper.TABLE_PHOTO_TAKEN_SALES, null, cv);
    }

    public int GetRowCount(String tableName, String taskId) {
//        Cursor mCount = database.rawQuery("select * from " + tableName
//                + " WHERE " + DbHelper.PHOTO_TASK_ID + " = " + taskId, null);
//        mCount.moveToFirst();

        String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PHOTO_TAKEN
                + " WHERE " + DbHelper.PHOTO_TASK_ID + " = '" + taskId+"'";
        Cursor cursor = database.rawQuery(selectQuery, null);


        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public int GetRowCountSales(String tableName, String userId) {
        Cursor mCount = database.rawQuery("select count(*) from " + tableName
                + " WHERE " + DbHelper.PHOTO_USER_ID_SALES + " = " + userId, null);
        mCount.moveToFirst();
        int count = mCount.getInt(0);
        mCount.close();
        return count;
    }

    public int GetRowCountSalesux(String tableName, String userId, String ux) {
        Cursor mCount = database.rawQuery("select count(*) from " + tableName + " WHERE " + DbHelper.PHOTO_USER_ID_SALES + " = '" + userId + "' AND " + DbHelper.PHOTO_USER_ID_SALES_tx + " = '" + ux + "'", null);
        mCount.moveToFirst();
        int count = mCount.getInt(0);
        mCount.close();
        return count;
    }

//    public void DeleteAPhoto(byte[] image) {
//        database.execSQL("delete from " + DbHelper.TABLE_PHOTO_TAKEN + " WHERE " + DbHelper.PHOTO_IMAGE + " = " + image);
//        database.close();
//    }
//
//    public void DeleteAPhotoSales(byte[] image) {
//        database.execSQL("delete from " + DbHelper.TABLE_PHOTO_TAKEN_SALES + " WHERE " + DbHelper.PHOTO_IMAGE + " = " + image);
//        database.close();
//    }

    public void DeleteOnePhoto(String id) {
        database.execSQL("delete from " + DbHelper.TABLE_PHOTO_TAKEN + " WHERE " + DbHelper.PHOTO_ID + " = " + id);
        database.close();
        System.out.println(".....................Deleted");
    }

    public void DeleteOnePhotoSales(String id) {
        database.execSQL("delete from " + DbHelper.TABLE_PHOTO_TAKEN_SALES + " WHERE " + DbHelper.PHOTO_ID_SALES + " = " + id);
        database.close();
        System.out.println(".....................Deleted");
    }


    public void DeleteAll(String tableName) {
        database.execSQL("delete from " + tableName);
        database.close();
    }

    public void DeleteAllux(String tableName, String tx) {
        database.execSQL("delete from " + tableName + " WHERE " + DbHelper.PHOTO_USER_ID_SALES_tx + "='" + tx + "'");
        database.close();
    }

    public void DeleteAlltask(String tableName, String tx) {
        database.execSQL("delete from " + tableName + " WHERE " + DbHelper.PHOTO_TASK_ID + "='" + tx + "'");
        database.close();
    }


    public ArrayList<PhotoModel> GetAllPhotos(String taskId) {
        ArrayList<PhotoModel> photo_list = new ArrayList<PhotoModel>();
        try {
            photo_list.clear();
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PHOTO_TAKEN
                    + " WHERE " + DbHelper.PHOTO_TASK_ID + " = '" + taskId+"'";
            Cursor cursor = database.rawQuery(selectQuery, null);
            int rrr=cursor.getCount();
            if (cursor.moveToFirst()) {
                do {
                    PhotoModel photo = new PhotoModel();
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TASK_ID)));
                    photo.setPhotoId(cursor.getInt(cursor.getColumnIndex(DbHelper.PHOTO_ID))+"");
//                    photo.setTakenPhoto(cursor.getBlob(cursor.getColumnIndex(DbHelper.PHOTO_IMAGE)));
                    photo.setTakenDate(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TAKEN_TIME)));
                    photo.setPhotoPath(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_PATH)));
                    photo_list.add(photo);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return photo_list;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("all_photo", "" + e);
        } finally {
            database.close();
        }
        return photo_list;
    }

    public ArrayList<PhotoSalesModel> GetAllPhotosSales(String userId) {
        ArrayList<PhotoSalesModel> photo_list = new ArrayList<PhotoSalesModel>();
        try {
            photo_list.clear();
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PHOTO_TAKEN_SALES
                    + " WHERE " + DbHelper.PHOTO_USER_ID_SALES + " = " + userId;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    PhotoSalesModel photo = new PhotoSalesModel();
                    photo.setPhotoId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_ID_SALES)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_USER_ID_SALES)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_USER_ID_SALES_tx)));
                    photo.setTakenPhoto(cursor.getBlob(cursor.getColumnIndex(DbHelper.PHOTO_IMAGE_SALES)));
                    photo.setTakenDate(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TAKEN_TIME_SALES)));
                    photo_list.add(photo);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return photo_list;
        } catch (Exception e) {
            Log.e("all_photo", "" + e);
        } finally {
            database.close();
        }
        return photo_list;
    }

    public ArrayList<PhotoSalesModel> GetAllPhotosSalesTx(String userId, String tx) {
        ArrayList<PhotoSalesModel> photo_list = new ArrayList<PhotoSalesModel>();
        try {
            photo_list.clear();
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PHOTO_TAKEN_SALES
                    + " WHERE " + DbHelper.PHOTO_USER_ID_SALES + " = '" + userId + "' AND " + DbHelper.PHOTO_USER_ID_SALES_tx + " = '" + tx + "'";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    PhotoSalesModel photo = new PhotoSalesModel();

                    photo.setPhotoId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_ID_SALES)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_USER_ID_SALES)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_USER_ID_SALES_tx)));
                    photo.setTakenPhoto(cursor.getBlob(cursor.getColumnIndex(DbHelper.PHOTO_IMAGE_SALES)));
                    photo.setTakenDate(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TAKEN_TIME_SALES)));
                    photo_list.add(photo);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return photo_list;
        } catch (Exception e) {
            Log.e("all_photo", "" + e);
        } finally {
            database.close();
        }
        return photo_list;
    }

    public PhotoSalesModel GetLastPhotosSales() {
        PhotoSalesModel photo = new PhotoSalesModel();
        try {
            //photo_list.clear();
            //SELECT * FROM tablename ORDER BY column DESC LIMIT 1;
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PHOTO_TAKEN_SALES
                    + " ORDER BY " + DbHelper.PHOTO_USER_ID_SALES + " DESC LIMIT 1 ";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    //PhotoModel photo = new PhotoModel();
                    photo.setPhotoId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_ID_SALES)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_USER_ID_SALES)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_USER_ID_SALES_tx)));
                    photo.setTakenPhoto(cursor.getBlob(cursor.getColumnIndex(DbHelper.PHOTO_IMAGE_SALES)));
                    photo.setTakenDate(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TAKEN_TIME_SALES)));

                } while (cursor.moveToNext());
            }
            cursor.close();

            return photo;
        } catch (Exception e) {
            Log.e("all_photo", "" + e);
        } finally {
            database.close();
        }
        return photo;
    }


    public PhotoModel GetLastPhotos() {
        PhotoModel photo = new PhotoModel();
        try {
            //photo_list.clear();
            //SELECT * FROM tablename ORDER BY column DESC LIMIT 1;
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PHOTO_TAKEN
                    + " ORDER BY " + DbHelper.PHOTO_TASK_ID + " DESC LIMIT 1 ";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    //PhotoModel photo = new PhotoModel();
                    photo.setPhotoId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_ID)));
                    photo.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TASK_ID)));
//                    photo.setTakenPhoto(cursor.getBlob(2));
                    photo.setPhotoPath(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_PATH)));
                    photo.setTakenDate(cursor.getString(cursor.getColumnIndex(DbHelper.PHOTO_TAKEN_TIME)));

                } while (cursor.moveToNext());
            }
            cursor.close();

            return photo;
        } catch (Exception e) {
            Log.e("all_photo", "" + e);
        } finally {
            database.close();
        }
        return photo;
    }

    public void InsertToSaveData(String taskId, String issue,
                                 String total_time_spend, String start_time, String finished_time,
                                 String task_report, String rating, String comments, byte[] signature) {
        try {
            ContentValues cv = new ContentValues();
            cv.put(DbHelper.SAVED_TASK_ID, taskId);
            cv.put(DbHelper.SAVED_ERROR, issue);
            cv.put(DbHelper.SAVED_TOTAL_TIME_SPEND, total_time_spend);
            cv.put(DbHelper.SAVED_START_TIME, start_time);
            cv.put(DbHelper.SAVED_FINISHED_TIME, finished_time);
            cv.put(DbHelper.SAVED_TASK_REPORT, task_report);
            cv.put(DbHelper.SAVED_RATING, rating);
            cv.put(DbHelper.SAVED_COMMENTS, comments);
            cv.put(DbHelper.SAVED_SIGNATURE, signature);
            database.insert(DbHelper.TABLE_SAVED_ITEMS, null, cv);
        } catch (Exception e) {
            Log.e("Insert All data saved", "" + e);
        } finally {
            database.close();
        }

//        database.close();
        System.out.println("...................Inserted..!!!");
    }

    public void UpdatePendingTask(String fieldName, String updateValue,
                                  String taskId) throws SQLException {
        try {
            ContentValues values = new ContentValues();
            values.put(fieldName, updateValue);
            database.update(DbHelper.TABLE_PENDING_TASK, values, DbHelper.PENDING_taskId + " = " + taskId, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        System.out.println("Updated..!!!");
    }


    public void UpdateSavedTable(String fieldName, String updateValue,
                                 String taskId) throws SQLException {

        ContentValues values = new ContentValues();
        values.put(fieldName, updateValue);
        database.update(DbHelper.TABLE_SAVED_ITEMS, values, DbHelper.SAVED_TASK_ID + " = " + taskId, null);
        database.close();
        System.out.println("Updated..!!!");
    }

    public void UpdateSignature(byte[] signature, String taskId)
            throws SQLException {
        ContentValues values = new ContentValues();
        values.put(DbHelper.SAVED_SIGNATURE, signature);
        database.update(DbHelper.TABLE_SAVED_ITEMS, values, DbHelper.SAVED_TASK_ID + " = " + taskId, null);
        database.close();
        System.out.println("Updated..!!!");
    }


    public void UpdateSignatures(byte[] signature, String taskId)
            throws SQLException {
        String updateQuery = "UPDATE " + DbHelper.TABLE_SAVED_ITEMS + " SET "
                + DbHelper.SAVED_SIGNATURE + " = \"" + signature + "\" WHERE "
                + DbHelper.SAVED_TASK_ID + " = \"" + taskId + "\"";

        Cursor mCursor = database.rawQuery(updateQuery, null);
        mCursor.close();
        database.close();
        System.out.println("Updated..!!!");
    }


    public PendingTask GetSavedItems(String taskId) {
        PendingTask task = new PendingTask();
        try {
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_SAVED_ITEMS
                    + " WHERE " + DbHelper.SAVED_TASK_ID + " = " + taskId;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    task.setTaskId(cursor.getString(1));
                    System.out.println("TaskId==> " + cursor.getString(1));

                    task.setIssue(cursor.getString(2));
                    System.out.println("Issue==> " + cursor.getString(2));

                    task.setTotalTimeSpend(cursor.getString(3));
                    System.out.println("TotalTimeSpend==> " + cursor.getString(3));

                    task.setStartTime(cursor.getString(4));
                    System.out.println("StartTime==> " + cursor.getString(4));

                    task.setStopTime(cursor.getString(5));
                    System.out.println("StopTime==> " + cursor.getString(5));

                    task.setTaskReport(cursor.getString(6));
                    System.out.println("TaskReport==> " + cursor.getString(6));

                    task.setRating(cursor.getString(7));
                    System.out.println("Rating==> " + cursor.getString(7));

                    task.setConsumerFeedback(cursor.getString(8));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(8));

                    task.setSignatureImage(cursor.getBlob(9));
                    System.out.println("SignatureImage==> " + cursor.getBlob(9));

                    task.setProblem_reported(cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_PROBLEM_REPORTED)));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_PROBLEM_REPORTED)));

                    task.setDiagnosis_done(cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_DIAGNOSIS_DONE)));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_DIAGNOSIS_DONE)));

                    task.setResolution(cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_RESOLUTION)));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_RESOLUTION)));

                    task.setNotes_support(cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_NOTES_SUPPORT)));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_NOTES_SUPPORT)));

                    task.setDeilvery_status(cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_DELIVERY_STATUS)));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_DELIVERY_STATUS)));

                    task.setNotes_delevery(cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_NOTES_DELIVERY)));
                    System.out.println("ConsumerFeedback==> " + cursor.getString(cursor.getColumnIndex(DbHelper.SAVED_NOTES_DELIVERY)));


                } while (cursor.moveToNext());
            }
            cursor.close();

            return task;
        } catch (Exception e) {
            Log.e("all_photo", "" + e);
        } finally {
            database.close();
        }
        return task;
    }


    public void addPendingTasList(PendingTask pendingTask) {

        if (!pendingTaskById(pendingTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.PENDING_TASK_USER_ID, ConstantFunction.getstatus(ourcontext, "UserId") + "");
            cv.put(PENDING_task_type, "1");
            cv.put(DbHelper.PENDING_taskId, pendingTask.getTaskId() + "");
            cv.put(DbHelper.PENDING_issue, pendingTask.getIssue() + "");
            cv.put(DbHelper.PENDING_is_google_sheet, pendingTask.getIs_google_sheet() + "");
            cv.put(DbHelper.PENDING_Google_sheet_url, pendingTask.getGoogle_sheet_url() + "");
            cv.put(DbHelper.PENDING_date_time, pendingTask.getDate_time() + "");
            cv.put(DbHelper.PENDING_phone, pendingTask.getPhone() + "");
            cv.put(DbHelper.PENDING_address, pendingTask.getAddress() + "");
            cv.put(DbHelper.PENDING_org_name, pendingTask.getOrg_name() + "");
            cv.put(DbHelper.PENDING_org_ph_num, pendingTask.getOrg_ph_num() + "");
            cv.put(DbHelper.PENDING_latitude, pendingTask.getLatitude() + "");
            cv.put(DbHelper.PENDING_longitude, pendingTask.getLongitude() + "");
            cv.put(DbHelper.PENDING_time, pendingTask.getTime() + "");
            cv.put(DbHelper.PENDING_personName, pendingTask.getPersonName() + "");
            cv.put(DbHelper.PENDING_supervisorName, pendingTask.getSupervisorName() + "");
            cv.put(DbHelper.PENDING_tasktype, pendingTask.getTasktype() + "");
            cv.put(DbHelper.PENDING_additionalInfo, pendingTask.getAdditionalInfo() + "");
            cv.put(DbHelper.PENDING_startTime, pendingTask.getStartTime() + "");
            cv.put(DbHelper.PENDING_stopTime, pendingTask.getStopTime() + "");
            cv.put(DbHelper.PENDING_totalTimeSpend, pendingTask.getTotalTimeSpend() + "");
            cv.put(DbHelper.PENDING_taskReport, pendingTask.getTaskReport() + "");
            cv.put(DbHelper.PENDING_consumerFeedback, pendingTask.getConsumerFeedback() + "");
            cv.put(DbHelper.PENDING_rating, pendingTask.getRating() + "");
            cv.put(DbHelper.PENDING_signatureImage, pendingTask.getSignatureImage() + "");
            cv.put(DbHelper.PENDING_signatureString, pendingTask.getSignatureString() + "");
            cv.put(DbHelper.PENDING_reason, pendingTask.getReason() + "");
            cv.put(DbHelper.PENDING_problem_reported, pendingTask.getProblem_reported() + "");
            cv.put(DbHelper.PENDING_diagnosis_done, pendingTask.getDiagnosis_done() + "");
            cv.put(DbHelper.PENDING_resolution, pendingTask.getResolution() + "");
            cv.put(DbHelper.PENDING_notes_support, pendingTask.getNotes_support() + "");
            cv.put(DbHelper.PENDING_deilvery_status, pendingTask.getDeilvery_status() + "");
            cv.put(DbHelper.PENDING_notes_delevery, pendingTask.getNotes_delevery() + "");
            cv.put(DbHelper.PENDING_Is_Recuring, pendingTask.getis_recurring() + "");
            cv.put(DbHelper.PENDING_recurring_type, pendingTask.getRecurring_type() + "");
            cv.put(DbHelper.PENDING_recurring_end_date, pendingTask.getRecurring_end_date() + "");
            cv.put(DbHelper.PENDING_ET_ID, pendingTask.getEt_id() + "");
            cv.put(DbHelper.PENDING_duration, pendingTask.getDuration() + "");

            database.insert(DbHelper.TABLE_PENDING_TASK, null, cv);
            database.close();
            Log.v("Pending Task list", pendingTask.getOrg_name());
        }

    }

    public void addSpecialTasList(PendingTask pendingTask) {

        if (!pendingTaskById(pendingTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.PENDING_TASK_USER_ID, ConstantFunction.getstatus(ourcontext, "UserId") + "");
            cv.put(PENDING_task_type, "2");
            cv.put(DbHelper.PENDING_taskId, pendingTask.getTaskId() + "");
            cv.put(DbHelper.PENDING_issue, pendingTask.getIssue() + "");

            cv.put(DbHelper.PENDING_is_google_sheet, pendingTask.getIs_google_sheet() + "");
            cv.put(DbHelper.PENDING_Google_sheet_url, pendingTask.getGoogle_sheet_url() + "");

            cv.put(DbHelper.PENDING_date_time, pendingTask.getDate_time() + "");
            cv.put(DbHelper.PENDING_phone, pendingTask.getPhone() + "");
            cv.put(DbHelper.PENDING_address, pendingTask.getAddress() + "");
            cv.put(DbHelper.PENDING_org_name, pendingTask.getOrg_name() + "");
            cv.put(DbHelper.PENDING_org_ph_num, pendingTask.getOrg_ph_num() + "");
            cv.put(DbHelper.PENDING_latitude, pendingTask.getLatitude() + "");
            cv.put(DbHelper.PENDING_longitude, pendingTask.getLongitude() + "");
            cv.put(DbHelper.PENDING_time, pendingTask.getTime() + "");
            cv.put(DbHelper.PENDING_personName, pendingTask.getPersonName() + "");
            cv.put(DbHelper.PENDING_supervisorName, pendingTask.getSupervisorName() + "");
            cv.put(DbHelper.PENDING_tasktype, pendingTask.getTasktype() + "");
            cv.put(DbHelper.PENDING_additionalInfo, pendingTask.getAdditionalInfo() + "");
            cv.put(DbHelper.PENDING_startTime, pendingTask.getStartTime() + "");
            cv.put(DbHelper.PENDING_stopTime, pendingTask.getStopTime() + "");
            cv.put(DbHelper.PENDING_totalTimeSpend, pendingTask.getTotalTimeSpend() + "");
            cv.put(DbHelper.PENDING_taskReport, pendingTask.getTaskReport() + "");
            cv.put(DbHelper.PENDING_consumerFeedback, pendingTask.getConsumerFeedback() + "");
            cv.put(DbHelper.PENDING_rating, pendingTask.getRating() + "");
            cv.put(DbHelper.PENDING_signatureImage, pendingTask.getSignatureImage() + "");
            cv.put(DbHelper.PENDING_signatureString, pendingTask.getSignatureString() + "");
            cv.put(DbHelper.PENDING_reason, pendingTask.getReason() + "");
            cv.put(DbHelper.PENDING_problem_reported, pendingTask.getProblem_reported() + "");
            cv.put(DbHelper.PENDING_diagnosis_done, pendingTask.getDiagnosis_done() + "");
            cv.put(DbHelper.PENDING_resolution, pendingTask.getResolution() + "");
            cv.put(DbHelper.PENDING_notes_support, pendingTask.getNotes_support() + "");
            cv.put(DbHelper.PENDING_deilvery_status, pendingTask.getDeilvery_status() + "");
            cv.put(DbHelper.PENDING_notes_delevery, pendingTask.getNotes_delevery() + "");
            cv.put(DbHelper.PENDING_Is_Recuring, pendingTask.getis_recurring() + "");
            cv.put(DbHelper.PENDING_recurring_type, pendingTask.getRecurring_type() + "");
            cv.put(DbHelper.PENDING_recurring_end_date, pendingTask.getRecurring_end_date() + "");
            cv.put(DbHelper.PENDING_ET_ID, pendingTask.getEt_id() + "");
            cv.put(DbHelper.PENDING_duration, pendingTask.getDuration() + "");


            database.insert(DbHelper.TABLE_PENDING_TASK, null, cv);
            database.close();
            Log.v("Special Task list", pendingTask.getOrg_name());
        }

    }

    public void taskDelete(String task_id, String et_id) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_PENDING_TASK + " WHERE " + DbHelper.PENDING_taskId + " = '" + task_id
                + "' AND " + DbHelper.PENDING_ET_ID + " = '" + et_id + "'");
        database.close();
    }


    public void taskDeleteByUser(String user_id, String type) {
        database = dbhelper.getReadableDatabase();


        int res = database.delete(DbHelper.TABLE_PENDING_TASK, DbHelper.PENDING_TASK_USER_ID + " = ? AND " + DbHelper.PENDING_task_type + " = ?",
                new String[]{user_id + "", type});

        Log.v("TTT", "deleteexpirerecord " + res);


//        database.execSQL("delete from " + DbHelper.TABLE_PENDING_TASK + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " = '" + user_id + "' AND " + DbHelper.PENDING_task_type + " = '" + PENDING_task_type + "'");
        database.close();
    }


    public boolean pendingTaskById(PendingTask pendingTask) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_PENDING_TASK + " WHERE " + DbHelper.PENDING_taskId + " = '" + pendingTask.getTaskId() + "' AND " + DbHelper.PENDING_ET_ID + " ='" + pendingTask.getEt_id() + "';");
        database.close();

//		String selectQuery = "SELECT * FROM " + DbHelper.TABLE_PENDING_TASK + " WHERE " + DbHelper.PENDING_taskId
//				+ " ='" + pendingTask.getTaskId() + "';";
//
//
//		Cursor cursor = database.rawQuery(selectQuery, null);
//
//		// looping through all rows and adding to list
//		if (cursor.getCount() > 0) {
//
//			ContentValues cv = new ContentValues();
//			cv.put(DbHelper.PENDING_TASK_USER_ID, ConstantFunction.getstatus(ourcontext,"UserId")+"");
//			cv.put(DbHelper.PENDING_task_type, "1");
//			cv.put(DbHelper.PENDING_taskId, pendingTask.getTaskId() + "");
//			cv.put(DbHelper.PENDING_issue , pendingTask.getIssue() + "");
//			cv.put(DbHelper.PENDING_date_time , pendingTask.getDate_time() + "");
//			cv.put(DbHelper.PENDING_phone, pendingTask.getPhone() + "");
//			cv.put(DbHelper.PENDING_address , pendingTask.getAddress() + "");
//			cv.put(DbHelper.PENDING_org_name , pendingTask.getOrg_name() + "");
//			cv.put(DbHelper.PENDING_org_ph_num , pendingTask.getOrg_ph_num() + "");
//			cv.put(DbHelper.PENDING_latitude , pendingTask.getLatitude() + "");
//			cv.put(DbHelper.PENDING_longitude , pendingTask.getLongitude() + "");
//			cv.put(DbHelper.PENDING_time , pendingTask.getTime() + "");
//			cv.put(DbHelper.PENDING_personName , pendingTask.getPersonName() + "");
//			cv.put(DbHelper.PENDING_supervisorName , pendingTask.getSupervisorName() + "");
//			cv.put(DbHelper.PENDING_tasktype , pendingTask.getTasktype() + "");
//			cv.put(DbHelper.PENDING_additionalInfo , pendingTask.getAdditionalInfo() + "");
//			cv.put(DbHelper.PENDING_startTime , pendingTask.getStartTime() + "");
//			cv.put(DbHelper.PENDING_stopTime , pendingTask.getStopTime() + "");
//			cv.put(DbHelper.PENDING_totalTimeSpend , pendingTask.getTotalTimeSpend() + "");
//			cv.put(DbHelper.PENDING_taskReport, pendingTask.getTaskReport() + "");
//			cv.put(DbHelper.PENDING_consumerFeedback, pendingTask.getConsumerFeedback() + "");
//			cv.put(DbHelper.PENDING_rating , pendingTask.getRating() + "");
//			cv.put(DbHelper.PENDING_signatureImage , pendingTask.getSignatureImage() + "");
//			cv.put(DbHelper.PENDING_signatureString , pendingTask.getSignatureString() + "");
//			cv.put(DbHelper.PENDING_reason , pendingTask.getReason() + "");
//			cv.put(DbHelper.PENDING_problem_reported , pendingTask.getProblem_reported() + "");
//			cv.put(DbHelper.PENDING_diagnosis_done, pendingTask.getDiagnosis_done() + "");
//			cv.put(DbHelper.PENDING_resolution , pendingTask.getResolution() + "");
//			cv.put(DbHelper.PENDING_notes_support , pendingTask.getNotes_support() + "");
//			cv.put(DbHelper.PENDING_deilvery_status, pendingTask.getDeilvery_status() + "");
//			cv.put(DbHelper.PENDING_notes_delevery , pendingTask.getNotes_delevery() + "");
//
//			// updating row
//			int upd = database.update(DbHelper.TABLE_PENDING_TASK, cv, DbHelper.PENDING_taskId + " = ?",
//					new String[]{String.valueOf(pendingTask.getTaskId())});
//
//			Log.v("TTT", "client upd = " + upd);
//
//			return true;
//		}
//		cursor.close();
//		database.close();
        return false;
    }

    public boolean pendingTaskByIdUpdate(PendingTask pendingTask) {
        database = dbhelper.getReadableDatabase();
//		database.execSQL("delete from " + DbHelper.TABLE_PENDING_TASK +" WHERE " +DbHelper.PENDING_taskId+" = "+pendingTask.getTaskId());
//		database.close();
        try {
            String selectQuery = "SELECT * FROM " + DbHelper.TABLE_PENDING_TASK + " WHERE " + DbHelper.PENDING_taskId
                    + " ='" + pendingTask.getTaskId() + "';";


            Cursor cursor = database.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0) {

                ContentValues cv = new ContentValues();
                cv.put(DbHelper.PENDING_TASK_USER_ID, ConstantFunction.getstatus(ourcontext, "UserId") + "");
                cv.put(DbHelper.PENDING_taskId, pendingTask.getTaskId() + "");
                cv.put(DbHelper.PENDING_issue, pendingTask.getIssue() + "");

                cv.put(DbHelper.PENDING_is_google_sheet, pendingTask.getIs_google_sheet() + "");
                cv.put(DbHelper.PENDING_Google_sheet_url, pendingTask.getGoogle_sheet_url() + "");

                cv.put(DbHelper.PENDING_date_time, pendingTask.getDate_time() + "");
                cv.put(DbHelper.PENDING_phone, pendingTask.getPhone() + "");
                cv.put(DbHelper.PENDING_address, pendingTask.getAddress() + "");
                cv.put(DbHelper.PENDING_org_name, pendingTask.getOrg_name() + "");
                cv.put(DbHelper.PENDING_org_ph_num, pendingTask.getOrg_ph_num() + "");
                cv.put(DbHelper.PENDING_latitude, pendingTask.getLatitude() + "");
                cv.put(DbHelper.PENDING_longitude, pendingTask.getLongitude() + "");
                cv.put(DbHelper.PENDING_time, pendingTask.getTime() + "");
                cv.put(DbHelper.PENDING_personName, pendingTask.getPersonName() + "");
                cv.put(DbHelper.PENDING_supervisorName, pendingTask.getSupervisorName() + "");
                cv.put(DbHelper.PENDING_tasktype, pendingTask.getTasktype() + "");
                cv.put(DbHelper.PENDING_additionalInfo, pendingTask.getAdditionalInfo() + "");
                cv.put(DbHelper.PENDING_startTime, pendingTask.getStartTime() + "");
                cv.put(DbHelper.PENDING_stopTime, pendingTask.getStopTime() + "");
                cv.put(DbHelper.PENDING_totalTimeSpend, pendingTask.getTotalTimeSpend() + "");
                cv.put(DbHelper.PENDING_taskReport, pendingTask.getTaskReport() + "");
                cv.put(DbHelper.PENDING_consumerFeedback, pendingTask.getConsumerFeedback() + "");
                cv.put(DbHelper.PENDING_rating, pendingTask.getRating() + "");
                cv.put(DbHelper.PENDING_signatureImage, pendingTask.getSignatureImage());
                cv.put(DbHelper.PENDING_signatureString, pendingTask.getSignatureString());
                cv.put(DbHelper.PENDING_reason, pendingTask.getReason() + "");
                cv.put(DbHelper.PENDING_problem_reported, pendingTask.getProblem_reported() + "");
                cv.put(DbHelper.PENDING_diagnosis_done, pendingTask.getDiagnosis_done() + "");
                cv.put(DbHelper.PENDING_resolution, pendingTask.getResolution() + "");
                cv.put(DbHelper.PENDING_notes_support, pendingTask.getNotes_support() + "");
                cv.put(DbHelper.PENDING_deilvery_status, pendingTask.getDeilvery_status() + "");
                cv.put(DbHelper.PENDING_notes_delevery, pendingTask.getNotes_delevery() + "");
                cv.put(DbHelper.PENDING_Is_Recuring, pendingTask.getis_recurring() + "");
                cv.put(DbHelper.PENDING_recurring_type, pendingTask.getRecurring_type() + "");
                cv.put(DbHelper.PENDING_recurring_end_date, pendingTask.getRecurring_end_date() + "");
                cv.put(DbHelper.PENDING_ET_ID, pendingTask.getEt_id() + "");
                cv.put(DbHelper.PENDING_duration, pendingTask.getDuration() + "");


                // updating row
                int upd = database.update(DbHelper.TABLE_PENDING_TASK, cv, DbHelper.PENDING_taskId + " = ?",
                        new String[]{String.valueOf(pendingTask.getTaskId())});

                Log.v("TTT", "client upd = " + upd);

                return true;
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return false;
    }

    public ArrayList<PendingTask> getPendingAllTask(String userId) {
        ArrayList<PendingTask> pendingTasks = new ArrayList<PendingTask>();
        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PENDING_TASK
                    + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "';";

            database = dbhelper.getReadableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    PendingTask model = new PendingTask();


//					model.set DbHelper.PENDING_task_type, "1");
                    model.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskId)));
                    model.setIssue(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_issue)));
                    model.setIs_google_sheet(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_is_google_sheet)));
                    model.setGoogle_sheet_url(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Google_sheet_url)));
                    model.setDate_time(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_date_time)));
                    model.setPhone(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_phone)));
                    model.setAddress(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_address)));
                    if (model.getAddress() != null && !model.getAddress().equals("null")) {
                        JSONArray arrClientAddress = new JSONArray(model.getAddress() + "");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }
                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            model.setClientAddress(new ArrayList<ClientAddress>());
                            model.getClientAddress().addAll(arrListClientAddress);
                        }
                    }

                    model.setOrg_name(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_name)));
                    model.setOrg_ph_num(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_ph_num)));
                    model.setLatitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_latitude)));
                    model.setLongitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_longitude)));
                    model.setTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_time)));
                    model.setPersonName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_personName)));
                    model.setSupervisorName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_supervisorName)));
                    model.setTasktype(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_tasktype)));
                    model.setAdditionalInfo(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_additionalInfo)));
                    model.setStartTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_startTime)));
                    model.setStopTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_stopTime)));
                    model.setTotalTimeSpend(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_totalTimeSpend)));
                    model.setTaskReport(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskReport)));
                    model.setConsumerFeedback(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_consumerFeedback)));
                    model.setRating(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_rating)));
                    model.setSignatureImage(cursor.getBlob(cursor.getColumnIndex(DbHelper.PENDING_signatureImage)));
                    model.setSignatureString(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_signatureString)));
                    model.setReason(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_reason)));
                    model.setProblem_reported(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_problem_reported)));
                    model.setDiagnosis_done(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_diagnosis_done)));
                    model.setResolution(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_resolution)));
                    model.setNotes_support(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_support)));
                    model.setDeilvery_status(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_deilvery_status)));
                    model.setNotes_delevery(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_delevery)));
                    model.setIs_recurring(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Is_Recuring)));
                    model.setRecurring_type(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_type)));
                    model.setRecurring_end_date(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_end_date)));
                    model.setEt_id(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_ET_ID)));
                    model.setDuration(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_duration)));

                    pendingTasks.add(model);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return pendingTasks;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return pendingTasks;
    }


    public ArrayList<PendingTask> getPendingTask(String userId, String taskType) {
        ArrayList<PendingTask> pendingTasks = new ArrayList<PendingTask>();
        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PENDING_TASK
                    + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND " + PENDING_task_type + " ='" + taskType + "';";

            database = dbhelper.getReadableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    PendingTask model = new PendingTask();


//					model.set DbHelper.PENDING_task_type, "1");
                    model.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskId)));
                    model.setIssue(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_issue)));

                    model.setIs_google_sheet(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_is_google_sheet)));
                    model.setGoogle_sheet_url(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Google_sheet_url)));

                    model.setDate_time(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_date_time)));
                    model.setPhone(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_phone)));
                    model.setAddress(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_address)));

                    if (model.getAddress() != null && !model.getAddress().equals("null")) {
                        JSONArray arrClientAddress = new JSONArray(model.getAddress() + "");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }
                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            model.setClientAddress(new ArrayList<ClientAddress>());
                            model.getClientAddress().addAll(arrListClientAddress);
                        }
                    }


                    model.setOrg_name(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_name)));
                    model.setOrg_ph_num(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_ph_num)));
                    model.setLatitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_latitude)));
                    model.setLongitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_longitude)));
                    model.setTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_time)));
                    model.setPersonName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_personName)));
                    model.setSupervisorName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_supervisorName)));
                    model.setTasktype(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_tasktype)));
                    model.setAdditionalInfo(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_additionalInfo)));
                    model.setStartTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_startTime)));
                    model.setStopTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_stopTime)));
                    model.setTotalTimeSpend(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_totalTimeSpend)));
                    model.setTaskReport(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskReport)));
                    model.setConsumerFeedback(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_consumerFeedback)));
                    model.setRating(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_rating)));
                    model.setSignatureImage(cursor.getBlob(cursor.getColumnIndex(DbHelper.PENDING_signatureImage)));
                    model.setSignatureString(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_signatureString)));
                    model.setReason(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_reason)));
                    model.setProblem_reported(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_problem_reported)));
                    model.setDiagnosis_done(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_diagnosis_done)));
                    model.setResolution(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_resolution)));
                    model.setNotes_support(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_support)));
                    model.setDeilvery_status(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_deilvery_status)));
                    model.setNotes_delevery(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_delevery)));
                    model.setIs_recurring(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Is_Recuring)));
                    model.setRecurring_type(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_type)));
                    model.setRecurring_end_date(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_end_date)));
                    model.setEt_id(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_ET_ID)));
                    model.setDuration(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_duration)));

                    pendingTasks.add(model);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return pendingTasks;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return pendingTasks;
    }

    public PendingTask getPendingByIdTask(String userId, String taskId) {
        PendingTask model = new PendingTask();
        database = dbhelper.getReadableDatabase();

        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PENDING_TASK
                    + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND " + DbHelper.PENDING_taskId + " ='" + taskId + "';";


            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
//					model.set DbHelper.PENDING_task_type, "1");
                    model.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskId)));
                    model.setIssue(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_issue)));

                    model.setIs_google_sheet(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_is_google_sheet)));
                    model.setGoogle_sheet_url(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Google_sheet_url)));

                    model.setDate_time(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_date_time)));
                    model.setPhone(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_phone)));
                    model.setAddress(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_address)));
                    if (model.getAddress() != null && !model.getAddress().equals("null")) {
                        JSONArray arrClientAddress = new JSONArray(model.getAddress() + "");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }
                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            model.setClientAddress(new ArrayList<ClientAddress>());
                            model.getClientAddress().addAll(arrListClientAddress);
                        }

                    }
                    model.setOrg_name(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_name)));
                    model.setOrg_ph_num(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_ph_num)));
                    model.setLatitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_latitude)));
                    model.setLongitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_longitude)));
                    model.setTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_time)));
                    model.setPersonName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_personName)));
                    model.setSupervisorName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_supervisorName)));
                    model.setTasktype(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_tasktype)));
                    model.setAdditionalInfo(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_additionalInfo)));
                    model.setStartTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_startTime)));
                    model.setStopTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_stopTime)));
                    model.setTotalTimeSpend(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_totalTimeSpend)));
                    model.setTaskReport(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskReport)));
                    model.setConsumerFeedback(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_consumerFeedback)));
                    model.setRating(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_rating)));
                    model.setSignatureImage(cursor.getBlob(cursor.getColumnIndex(DbHelper.PENDING_signatureImage)));
                    model.setSignatureString(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_signatureString)));
                    model.setReason(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_reason)));
                    model.setProblem_reported(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_problem_reported)));
                    model.setDiagnosis_done(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_diagnosis_done)));
                    model.setResolution(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_resolution)));
                    model.setNotes_support(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_support)));
                    model.setDeilvery_status(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_deilvery_status)));
                    model.setNotes_delevery(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_delevery)));
                    model.setIs_recurring(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Is_Recuring)));
                    model.setRecurring_type(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_type)));
                    model.setRecurring_end_date(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_end_date)));
                    model.setEt_id(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_ET_ID)));
                    model.setDuration(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_duration)));

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return model;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return null;
    }

    public int getPendingListCount(String userId, String taskType) {
        ArrayList<PendingTask> pendingTasks = new ArrayList<PendingTask>();
        int count = 0;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PENDING_TASK
                + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND " + PENDING_task_type + " ='" + taskType + "';";

        database = dbhelper.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        Log.v("Mowom", "Pending Task List Count = " + cursor.getCount());
        count = cursor.getCount();
        cursor.close();
        database.close();

        return count;
    }

    public int getSpecialListCount(String userId, String taskType) {
        ArrayList<PendingTask> pendingTasks = new ArrayList<PendingTask>();
        int count = 0;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PENDING_TASK
                + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND " + PENDING_task_type + " ='" + taskType + "';";

        database = dbhelper.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        Log.v("Mowom", "Pending Task List Count = " + cursor.getCount());
        count = cursor.getCount();
        cursor.close();
        database.close();

        return count;
    }


    public void addSalesTask(SalesTask salesTask) {

        if (!SalesTaskById(salesTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.SALES_client_name_new, salesTask.SALES_client_name_new);
            cv.put(DbHelper.SALES_client_name, salesTask.SALES_client_name);
            cv.put(DbHelper.SALES_client_address, salesTask.SALES_client_address);
            cv.put(DbHelper.SALES_contact_person, salesTask.SALES_contact_person);
            cv.put(DbHelper.SALES_tel_number, salesTask.tel_number);
            cv.put(DbHelper.SALES_client_email, salesTask.client_email);
            cv.put(DbHelper.SALES_latitude, salesTask.latitude);
            cv.put(DbHelper.SALES_longtitude, salesTask.longtitude);
            cv.put(DbHelper.SALES_client_id, salesTask.client_id);
            cv.put(DbHelper.SALES_category_id, salesTask.category_id);
            cv.put(DbHelper.SALES_next_steps, salesTask.next_steps);
            cv.put(DbHelper.SALES_notes, salesTask.notes);
            cv.put(DbHelper.SALES_product, salesTask.product);
            cv.put(DbHelper.SALES_detail, salesTask.detail);
            cv.put(DbHelper.SALES_department, salesTask.department);
            cv.put(DbHelper.SALES_emp_id, salesTask.emp_id);
            cv.put(DbHelper.SALES_company_id, salesTask.company_id);
            cv.put(DbHelper.SALES_task_name, salesTask.task_name);
            cv.put(DbHelper.SALES_additional_instruction, salesTask.additional_instruction);
            cv.put(DbHelper.SALES_tasktype, salesTask.tasktype);
            cv.put(DbHelper.SALES_starttime, salesTask.startTime);
            cv.put(DbHelper.SALES_endtime, salesTask.endTime);
            cv.put(DbHelper.SALES_TASK_ID_TR, salesTask.task_id_Tx);
            cv.put(DbHelper.SALES_supervisor_id, salesTask.supervisor_id);

            database.insert(DbHelper.TABLE_SALES_TASK, null, cv);
            database.close();
        }

    }

    public ArrayList<SalesTask> getSalesTaskAll() {
        ArrayList<SalesTask> modelList = new ArrayList<>();
        database = dbhelper.getReadableDatabase();

        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_SALES_TASK;
//					+ " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND "+DbHelper.PENDING_taskId + " ='" + taskId+"';";

            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    SalesTask obj = new SalesTask();

                    obj.task_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_TASK_ID));
                    obj.SALES_client_name_new = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_client_name_new));
                    obj.SALES_client_name = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_client_name));
                    obj.SALES_client_address = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_client_address));
                    obj.SALES_contact_person = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_contact_person));
                    obj.tel_number = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_tel_number));
                    obj.client_email = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_client_email));
                    obj.latitude = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_latitude));
                    obj.longtitude = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_longtitude));
                    obj.client_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_client_id));
                    obj.category_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_category_id));
                    obj.next_steps = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_next_steps));
                    obj.notes = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_notes));
                    obj.product = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_product));
                    obj.detail = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_detail));
                    obj.department = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_department));
                    obj.emp_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_emp_id));
                    obj.company_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_company_id));
                    obj.task_name = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_task_name));
                    obj.additional_instruction = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_additional_instruction));
                    obj.tasktype = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_tasktype));

                    obj.task_id_Tx = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_TASK_ID_TR));

                    obj.startTime = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_starttime));
                    obj.endTime = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_endtime));
                    obj.supervisor_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_supervisor_id));

                    modelList.add(obj);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return modelList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return modelList;
    }


    public boolean SalesTaskById(SalesTask pendingTask) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_SALES_TASK + " WHERE " + DbHelper.SALES_TASK_ID + " = " + pendingTask.task_id);
        database.close();
        return false;
    }


    public void addSurveyTask(SurveyTask salesTask) {

        if (!SurveyTaskById(salesTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.SURVEY_client_name_new, salesTask.SURVEY_client_name_new);
            cv.put(DbHelper.SURVEY_client_name, salesTask.SURVEY_client_name);
            cv.put(DbHelper.SURVEY_client_address, salesTask.SURVEY_client_address);
            cv.put(DbHelper.SURVEY_tel_number, salesTask.tel_number);
            cv.put(DbHelper.SURVEY_latitude, salesTask.latitude);
            cv.put(DbHelper.SURVEY_longtitude, salesTask.longtitude);
            cv.put(DbHelper.SURVEY_client_id, salesTask.client_id);
            cv.put(DbHelper.SURVEY_category_id, salesTask.category_id);
            cv.put(DbHelper.SURVEY_emp_id, salesTask.emp_id);
            cv.put(DbHelper.SURVEY_notes, salesTask.notes);
            cv.put(DbHelper.SURVEY_usertx, salesTask.userTx);
            cv.put(DbHelper.SURVEY_companyId, salesTask.company_id);
            cv.put(DbHelper.SURVEY_supervisor_id, salesTask.supervisor_id);

            database.insert(DbHelper.TABLE_SURVEY_TASK, null, cv);
            database.close();
        }

    }

    public ArrayList<SurveyTask> getSurveyTaskAll() {
        ArrayList<SurveyTask> modelList = new ArrayList<>();
        database = dbhelper.getReadableDatabase();

        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_SURVEY_TASK;
//					+ " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND "+DbHelper.PENDING_taskId + " ='" + taskId+"';";

            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    SurveyTask obj = new SurveyTask();

                    obj.task_id = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_TASK_ID));
                    obj.SURVEY_client_name_new = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_client_name_new));
                    obj.SURVEY_client_name = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_client_name));
                    obj.SURVEY_client_address = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_client_address));
                    obj.tel_number = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_tel_number));
                    obj.latitude = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_latitude));
                    obj.longtitude = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_longtitude));
                    obj.client_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_client_id));
                    obj.category_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_category_id));
                    obj.emp_id = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_emp_id));
                    obj.notes = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_notes));
                    obj.userTx = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_usertx));
                    obj.company_id = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_companyId));
                    obj.supervisor_id = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_supervisor_id));

                    modelList.add(obj);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return modelList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return modelList;
    }


    public boolean SurveyTaskById(SurveyTask pendingTask) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_SURVEY_TASK + " WHERE " + DbHelper.SURVEY_TASK_ID + " = " + pendingTask.task_id);
        database.close();
        return false;
    }


    public void addSalesClientTask(ClientData salesTask) {

        if (!SalesClientById(salesTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.SALES_CLIENT_CLIENT_ID, salesTask.clientId);
            cv.put(DbHelper.SALES_CLIENT_client_name, salesTask.clientName);

            cv.put(DbHelper.SALES_CLIENT_CLIENT_address, salesTask.address);
            cv.put(DbHelper.SALES_CLIENT_client_phone, salesTask.phone_number);

            cv.put(DbHelper.SALES_CLIENT_client_contact, salesTask.contact_person);
            cv.put(DbHelper.SALES_CLIENT_CLIENT_email, salesTask.clientemail);

            database.insert(DbHelper.TABLE_SALES_CLIENT_TASK, null, cv);
            database.close();
        }

    }

    public ArrayList<ClientData> getSalesClientAll() {
        ArrayList<ClientData> modelList = new ArrayList<>();
        database = dbhelper.getReadableDatabase();

        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_SALES_CLIENT_TASK;
//					+ " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND "+DbHelper.PENDING_taskId + " ='" + taskId+"';";

            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    ClientData obj = new ClientData();
                    obj.clientId = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_CLIENT_ID));
                    obj.clientName = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_client_name));
                    obj.address = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_CLIENT_address));
                    if (obj.address != null && !obj.address.equals("null")) {
                        JSONArray arrClientAddress = new JSONArray(obj.address + "");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }
                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            obj.client_address = new ArrayList<ClientAddress>();
                            obj.client_address.addAll(arrListClientAddress);
                        }
                    }

                    obj.contact_person = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_client_contact));
                    obj.clientemail = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_CLIENT_email));
                    obj.phone_number = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_client_phone));


                    modelList.add(obj);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return modelList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return modelList;
    }


    public boolean SalesClientById(ClientData pendingTask) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_SALES_CLIENT_TASK + " WHERE " + DbHelper.SALES_CLIENT_CLIENT_ID + " = " + pendingTask.clientId);
        database.close();
        return false;
    }

    public void addSurveyClientTask(ClientData salesTask) {

        if (!SurveyClientById(salesTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.SURVEY_CLIENT_CLIENT_ID, salesTask.clientId);
            cv.put(DbHelper.SURVEY_CLIENT_client_name, salesTask.clientName);

            cv.put(DbHelper.SURVEY_CLIENT_CLIENT_address, salesTask.address);
            cv.put(DbHelper.SURVEY_CLIENT_client_contact, salesTask.contact_person);


            database.insert(DbHelper.TABLE_SURVEY_CLIENT_TASK, null, cv);
            database.close();
        }

    }

    public ArrayList<ClientData> getSurveryClientAll() {
        ArrayList<ClientData> modelList = new ArrayList<>();
        database = dbhelper.getReadableDatabase();

        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_SURVEY_CLIENT_TASK;
//					+ " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND "+DbHelper.PENDING_taskId + " ='" + taskId+"';";


            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    ClientData obj = new ClientData();

                    obj.clientId = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_CLIENT_CLIENT_ID));
                    obj.clientName = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_CLIENT_client_name));

                    obj.address = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_CLIENT_CLIENT_address));
                    obj.contact_person = cursor.getString(cursor.getColumnIndex(DbHelper.SURVEY_CLIENT_client_contact));

                    modelList.add(obj);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return modelList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return modelList;
    }


    public boolean SurveyClientById(ClientData pendingTask) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_SURVEY_CLIENT_TASK + " WHERE " + DbHelper.SURVEY_CLIENT_CLIENT_ID + " = " + pendingTask.clientId);
        database.close();
        return false;
    }

    public void addCategory(CategoryData salesTask) {

        if (!CategoryDataId(salesTask)) {
            database = dbhelper.getWritableDatabase();

            ContentValues cv = new ContentValues();

            cv.put(DbHelper.CATEGORY_ID, salesTask.categoryId);
            cv.put(DbHelper.CATEGORY_name, salesTask.categoryName);


            database.insert(DbHelper.TABLE_CATEGORY, null, cv);
            database.close();
        }

    }

    public ArrayList<CategoryData> getCategoryAll() {
        ArrayList<CategoryData> modelList = new ArrayList<>();
        database = dbhelper.getReadableDatabase();

        try {
            // Select All Query
            String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_CATEGORY;
//					+ " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND "+DbHelper.PENDING_taskId + " ='" + taskId+"';";


            Cursor cursor = database.rawQuery(selectQuery, null);

            Log.v("Mowom", "Pending Task List = " + cursor.getCount());
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {

                    CategoryData obj = new CategoryData();

                    obj.categoryId = cursor.getString(cursor.getColumnIndex(DbHelper.CATEGORY_ID));
                    obj.categoryName = cursor.getString(cursor.getColumnIndex(DbHelper.CATEGORY_name));

                    modelList.add(obj);

                } while (cursor.moveToNext());
            }
            // return brand list
            cursor.close();
            return modelList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return modelList;
    }


    public boolean CategoryDataId(CategoryData pendingTask) {
        database = dbhelper.getReadableDatabase();
        database.execSQL("delete from " + DbHelper.TABLE_CATEGORY + " WHERE " + DbHelper.CATEGORY_ID + " = " + pendingTask.categoryId);
        database.close();
        return false;
    }

    public void addLoactionOffline(LocationOffline loc) {
        try {
            ContentValues cv = new ContentValues();
            cv.put(DbHelper.EMP_ID, loc.emp_id);
            cv.put(DbHelper.TASK_ID, loc.task_id);
            cv.put(DbHelper.LATITUDE, loc.latitude);
            cv.put(DbHelper.LONGITUDE, loc.longitude);
            cv.put(DbHelper.ET_ID, loc.et_id);
            database.insert(TABLE_LOCATION, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        MowomLogFileLocation.writeToLog("\n" + "MowomService :-RP$$$$$$$$--------------- -------------Offline database task_id = " + loc.task_id + "");
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$---------------- ------------Offline database latitude = " + loc.latitude + "");
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- -------------Offline database longitude = " + loc.longitude + "");


        Log.e("Offline", "loc.task_id =============== ");
        Log.e("Offline", "loc.task_id = " + loc.task_id);
        Log.e("Offline", "loc.latitude = " + loc.latitude);
        Log.e("Offline", "loc.longitude = " + loc.longitude);

        System.out.println("................... Location Inserted..!!!");
        MowomLogFileLocation.writeToLog("\n" + "MowomService :-RP$$$$$$$$---------------  -------------Offline database Location Inserted..!!!!!!! ");
    }

    public ArrayList<LocationOffline> GetLocationOffline(String taskId) {
        ArrayList<LocationOffline> loc = new ArrayList<LocationOffline>();
        try {
            loc.clear();
            String selectQuery = "SELECT * FROM " + TABLE_LOCATION + " WHERE " + DbHelper.TASK_ID + " = '" + taskId + "';";

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LocationOffline model = new LocationOffline();
                    model.unique_id = cursor.getString(cursor.getColumnIndex(DbHelper.UNIQUE_ID));
                    model.emp_id = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_ID));
                    model.task_id = cursor.getString(cursor.getColumnIndex(DbHelper.TASK_ID));
                    model.latitude = cursor.getString(cursor.getColumnIndex(DbHelper.LATITUDE));
                    model.longitude = cursor.getString(cursor.getColumnIndex(DbHelper.LONGITUDE));
                    model.et_id = cursor.getString(cursor.getColumnIndex(DbHelper.ET_ID));

                    loc.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return loc;
        } catch (Exception e) {
            Log.e("Login User", "" + e);
        } finally {
            database.close();
        }

        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- -------------Offline database Location All..!!!!!!! " + loc.size());

        return loc;
    }

    public ArrayList<LocationOfflineGroup> GetLocationOfflineAll() {
        ArrayList<LocationOfflineGroup> loc = new ArrayList<LocationOfflineGroup>();
        try {
            loc.clear();
            String selectQuery = "SELECT * FROM " + TABLE_LOCATION;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    LocationOffline model = new LocationOffline();
                    model.unique_id = cursor.getString(cursor.getColumnIndex(DbHelper.UNIQUE_ID));
                    model.emp_id = cursor.getString(cursor.getColumnIndex(DbHelper.EMP_ID));
                    model.task_id = cursor.getString(cursor.getColumnIndex(DbHelper.TASK_ID));
                    model.latitude = cursor.getString(cursor.getColumnIndex(DbHelper.LATITUDE));
                    model.longitude = cursor.getString(cursor.getColumnIndex(DbHelper.LONGITUDE));
                    model.et_id = cursor.getString(cursor.getColumnIndex(DbHelper.ET_ID));


                    if(loc.size()==0)
                    {
                        LocationOfflineGroup locationOffline=new LocationOfflineGroup();
                        locationOffline.task_id=model.task_id;
                        locationOffline.lstLoc=new ArrayList<>();
                        locationOffline.lstLoc.add(model);
                        loc.add(locationOffline);
                    }
                    else
                    {
                        boolean isExistTask=false;
                        for (int i=0;i<loc.size();i++)
                        {
                            if(model.task_id.equals(loc.get(i).task_id))
                            {
                                isExistTask=true;
                                loc.get(i).lstLoc.add(model);
                                break;
                            }
                        }

                        if(!isExistTask)
                        {
                            LocationOfflineGroup locationOffline=new LocationOfflineGroup();
                            locationOffline.task_id=model.task_id;
                            locationOffline.lstLoc=new ArrayList<>();
                            locationOffline.lstLoc.add(model);
                            loc.add(locationOffline);
                        }
                    }
                } while (cursor.moveToNext());
            }
            cursor.close();

            return loc;
        } catch (Exception e) {
            Log.e("Login User", "" + e);
        } finally {
            database.close();
        }

        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- -------------Offline database Location All..!!!!!!! " + loc.size());

        return loc;
    }


    public void deleteOfflineLocation(String uId) {
        try {
            database = dbhelper.getReadableDatabase();
            database.execSQL("delete from " + DbHelper.TABLE_LOCATION + " WHERE " + DbHelper.TASK_ID + " = " + uId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

    }

    public void insertAddSalesTask(SalesTask task) {
        try {
            if (!SalesDataById(task)) {
                database = dbhelper.getWritableDatabase();

                ContentValues cv = new ContentValues();
                if (task.client_id.length() > 0) {
                    cv.put(DbHelper.SALES_CLIENT_ID, task.client_id + "");
                    cv.put(DbHelper.SALES_CLIENT_NAME, task.SALES_client_name);
                } else {
                    cv.put(DbHelper.SALES_CLIENT_NAME, task.SALES_client_name_new);
                    cv.put(DbHelper.SALES_CLIENT_ID, task.client_id + "");
                }
                cv.put(DbHelper.SALES_CLIENT_USER_ID, task.emp_id);
                cv.put(DbHelper.SALES_CLIENT_ADDRESS, task.SALES_client_address);
                cv.put(DbHelper.SALES_CONTACT_NAME, task.SALES_contact_person);
                cv.put(DbHelper.SALES_CLIENT_PHONE, task.tel_number);
                cv.put(DbHelper.SALES_CLIENT_EMAIL, task.client_email);
                cv.put(DbHelper.SALES_CLIENT_START_TIME, task.startTime);
                cv.put(DbHelper.SALES_CLIENT_END_TIME, task.endTime);
                cv.put(DbHelper.SALES_CLIENT_PRODUCT, task.product);
                cv.put(DbHelper.SALES_CLIENT_MEETING_NOTE, task.notes);
                cv.put(DbHelper.SALES_CLIENT_NEXT_STEP, task.next_steps);
                cv.put(DbHelper.SALES_CLIENT_DETAIL, task.detail);
                cv.put(DbHelper.SALES_CLIENT_TASK_ID_TX, task.task_id_Tx);
                cv.put(DbHelper.SALES_CLIENT_TASK_ID, task.task_id);

                long res = database.insert(TABLE_ADD_SALES_TASK, null, cv);
                Log.v("insert", res + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }


//        GetAddTaskDetail(task.emp_id);
        System.out.println("................... Add task sales Inserted..!!!");
    }

    public boolean SalesDataById(SalesTask task) {
        try {
            database = dbhelper.getReadableDatabase();
            database.execSQL("delete from " + DbHelper.TABLE_ADD_SALES_TASK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
        return false;
    }

    public void UpdateSalesData(String fieldName, String updateValue,
                                String clientId) throws SQLException {
        try {
            ContentValues values = new ContentValues();
            values.put(fieldName, updateValue);
            database.update(DbHelper.TABLE_ADD_SALES_TASK, values, DbHelper.SALES_CLIENT_TASK_ID_TX + " = " + clientId, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        System.out.println("Updated..!!!");
    }

    public ArrayList<SalesTask> GetAddTaskDetail(String uId) {
        database = dbhelper.getReadableDatabase();
        ArrayList<SalesTask> salesTask = new ArrayList<SalesTask>();
        try {
            salesTask.clear();
            String selectQuery = "SELECT * FROM " + TABLE_ADD_SALES_TASK + " WHERE " + DbHelper.SALES_CLIENT_USER_ID + " = '" + uId + "'";


            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    SalesTask model = new SalesTask();
                    if (cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_ID)) != null && !cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_ID)).equals("")) {
                        model.SALES_client_name = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_NAME));
                        model.client_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_ID));
                    } else {
                        model.SALES_client_name_new = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_NAME));
                    }
                    model.emp_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_USER_ID));
                    model.SALES_contact_person = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CONTACT_NAME));
                    model.SALES_client_address = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_ADDRESS));
                    model.client_email = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_EMAIL));
                    model.tel_number = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_PHONE));
                    model.startTime = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_START_TIME));
                    model.endTime = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_END_TIME));
                    model.product = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_PRODUCT));
                    model.notes = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_MEETING_NOTE));
                    model.next_steps = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_NEXT_STEP));
                    model.detail = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_DETAIL));
                    model.task_id_Tx = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_TASK_ID_TX));
                    model.task_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_CLIENT_TASK_ID));

                    salesTask.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return salesTask;
        } catch (Exception e) {
            Log.e("Sales User", "" + e);
        } finally {
            database.close();
        }
        return salesTask;
    }

    public void SalesDataDeletedById1(String user_id) {

        int res = 0;

        try {
            database = dbhelper.getWritableDatabase();

            res = database.delete(DbHelper.TABLE_ADD_SALES_TASK, DbHelper.SALES_CLIENT_USER_ID + " = ?",
                    new String[]{user_id + ""});

            Log.v("TTT", "deleteexpirerecord " + res);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
        if (res != 1) {
            SalesDataDeletedById();
        }

    }

    public void SalesDataDeletedById() {
        try {
            database = dbhelper.getWritableDatabase();

            database.delete(DbHelper.TABLE_ADD_SALES_TASK, null, null);
//            database.execSQL("delete from " + DbHelper.TABLE_ADD_SALES_TASK);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
    }

    public void addSalesLoactionOffline(AddSalesLocation loc) {
        try {
            ContentValues cv = new ContentValues();
            cv.put(DbHelper.SALES_EMP_ID, loc.emp_id);
            cv.put(DbHelper.SALES_TRANS_ID, loc.trans_id);
            cv.put(DbHelper.SALES_LATITUDE, loc.latitude);
            cv.put(DbHelper.SALES_LONGITUDE, loc.longitude);
            cv.put(DbHelper.SALES_ET_ID, loc.et_Id);
            database.insert(TABLE_LOCATION_ADD_SALES, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        MowomLogFileLocation.writeToLog("\n" + "MowomService :-RP$$$$$$$$--------------- -------------Add sales database task_id = " + loc.trans_id + "");
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$---------------- ------------Add sales database latitude = " + loc.latitude + "");
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- -------------Add sales database longitude = " + loc.longitude + "");


        Log.e("Add sales", "loc.task_id =============== ");
        Log.e("Add sales", "loc.task_id = " + loc.trans_id);
        Log.e("Add sales", "loc.latitude = " + loc.latitude);
        Log.e("Add sales", "loc.longitude = " + loc.longitude);

        System.out.println("................... Location Inserted..!!!");
        MowomLogFileLocation.writeToLog("\n" + "MowomService :-RP$$$$$$$$---------------  -------------Add sales database Location Inserted..!!!!!!! ");
    }

    public ArrayList<AddSalesLocation> GetSalesLocation() {
        ArrayList<AddSalesLocation> loc = new ArrayList<AddSalesLocation>();
        try {
            loc.clear();
//            String selectQuery = "SELECT * FROM " + TABLE_LOCATION_ADD_SALES + " WHERE " + DbHelper.SALES_TRANS_ID + " = '" + trans_id + "';";
            String selectQuery = "SELECT * FROM " + TABLE_LOCATION_ADD_SALES;

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    AddSalesLocation model = new AddSalesLocation();
                    model.unique_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_UNIQUE_ID));
                    model.emp_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_EMP_ID));
                    model.trans_id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_TRANS_ID));
                    model.latitude = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_LATITUDE));
                    model.longitude = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_LONGITUDE));
                    model.et_Id = cursor.getString(cursor.getColumnIndex(DbHelper.SALES_ET_ID));

                    loc.add(model);
                } while (cursor.moveToNext());
            }
            cursor.close();

            return loc;
        } catch (Exception e) {
            Log.e("Login User", "" + e);
        } finally {
            database.close();
        }

        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- -------------Geting AddSales database Location All..!!!!!!! " + loc.size());

        return loc;
    }


    public void deleteSalesLocationByTransId(String uniqueId) {
        try {
            database = dbhelper.getReadableDatabase();
            database.execSQL("delete from " + DbHelper.TABLE_LOCATION_ADD_SALES + " WHERE " + DbHelper.SALES_UNIQUE_ID + " = " + uniqueId);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
    }

    public boolean deleteAddSalesAllRecord() {
        try {
            database = dbhelper.getReadableDatabase();
            database.execSQL("delete from " + DbHelper.TABLE_LOCATION_ADD_SALES);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }
        return false;
    }


    public ArrayList<PendingTask> getPendingTaskByRecuring(String userId, String taskType, int weekOrMonth) {
        ArrayList<PendingTask> pendingTasks = new ArrayList<PendingTask>();
        try {
            String selectQuery = "";
            Cursor cursor = null;

            if (weekOrMonth == 0) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                for (int i = 0; i < 7; i++) {
                    Log.i("dateTag", sdf.format(cal.getTime()));
                    String dateOfWeek = sdf.format(cal.getTime());
                    try {
                        selectQuery = "SELECT * FROM " + DbHelper.TABLE_PENDING_TASK
                                + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND " + PENDING_task_type + " ='" + taskType
                                + "' AND " + DbHelper.PENDING_date_time + " LIKE \"" + dateOfWeek + "%\";";

                        database = dbhelper.getReadableDatabase();
                        cursor = database.rawQuery(selectQuery, null);

                        Log.v("Mowom", "Pending Task List = " + cursor.getCount());
                        // looping through all rows and adding to list
                        if (cursor.moveToFirst()) {
                            do {
                                PendingTask model = new PendingTask();

//					model.set DbHelper.PENDING_task_type, "1");
                                model.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskId)));
                                model.setIssue(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_issue)));

                                model.setIs_google_sheet(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_is_google_sheet)));
                                model.setGoogle_sheet_url(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Google_sheet_url)));

                                model.setDate_time(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_date_time)));
                                model.setPhone(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_phone)));
                                model.setAddress(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_address)));
                                if (model.getAddress() != null && !model.getAddress().equals("null")) {
                                    JSONArray arrClientAddress = new JSONArray(model.getAddress() + "");
                                    ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                                    Gson gson = new Gson();
                                    for (int r = 0; r < arrClientAddress.length(); r++) {
                                        JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                                        ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                                        arrListClientAddress.add(address);
                                    }
                                    if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                                        model.setClientAddress(new ArrayList<ClientAddress>());
                                        model.getClientAddress().addAll(arrListClientAddress);
                                    }
                                }

                                model.setOrg_name(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_name)));
                                model.setOrg_ph_num(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_ph_num)));
                                model.setLatitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_latitude)));
                                model.setLongitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_longitude)));
                                model.setTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_time)));
                                model.setPersonName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_personName)));
                                model.setSupervisorName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_supervisorName)));
                                model.setTasktype(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_tasktype)));
                                model.setAdditionalInfo(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_additionalInfo)));
                                model.setStartTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_startTime)));
                                model.setStopTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_stopTime)));
                                model.setTotalTimeSpend(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_totalTimeSpend)));
                                model.setTaskReport(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskReport)));
                                model.setConsumerFeedback(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_consumerFeedback)));
                                model.setRating(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_rating)));
                                model.setSignatureImage(cursor.getBlob(cursor.getColumnIndex(DbHelper.PENDING_signatureImage)));
                                model.setSignatureString(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_signatureString)));
                                model.setReason(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_reason)));
                                model.setProblem_reported(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_problem_reported)));
                                model.setDiagnosis_done(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_diagnosis_done)));
                                model.setResolution(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_resolution)));
                                model.setNotes_support(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_support)));
                                model.setDeilvery_status(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_deilvery_status)));
                                model.setNotes_delevery(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_delevery)));
                                model.setIs_recurring(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Is_Recuring)));
                                model.setRecurring_type(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_type)));
                                model.setRecurring_end_date(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_end_date)));
                                model.setEt_id(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_ET_ID)));
                                model.setDuration(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_duration)));

                                pendingTasks.add(model);

                            } while (cursor.moveToNext());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cal.add(Calendar.DAY_OF_WEEK, 1);
                }
            } else {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_MONTH, cal.getFirstDayOfWeek());
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                int count = 31;
                if (month == 1) {
                    if ((year / 4) == 0) {
                        count = 29;
                    } else {
                        count = 28;
                    }
                }

                if (month == 0) {
                    count = 31;
                } else if (month == 2) {
                    count = 31;
                } else if (month == 3) {
                    count = 30;
                } else if (month == 4) {
                    count = 31;
                } else if (month == 5) {
                    count = 30;
                } else if (month == 6) {
                    count = 31;
                } else if (month == 7) {
                    count = 31;
                } else if (month == 8) {
                    count = 30;
                } else if (month == 9) {
                    count = 31;
                } else if (month == 10) {
                    count = 30;
                } else if (month == 11) {
                    count = 31;
                }

                for (int i = 0; i < count; i++) {
                    Log.i("dateTag", sdf.format(cal.getTime()));
                    String dateOfWeek = sdf.format(cal.getTime());
                    try {
                        selectQuery = "SELECT * FROM " + DbHelper.TABLE_PENDING_TASK
                                + " WHERE " + DbHelper.PENDING_TASK_USER_ID + " ='" + userId + "' AND " + PENDING_task_type + " ='" + taskType
                                + "' AND " + DbHelper.PENDING_date_time + " LIKE \"" + dateOfWeek + "%\";";

                        database = dbhelper.getReadableDatabase();
                        cursor = database.rawQuery(selectQuery, null);

                        Log.v("Mowom", "Pending Task List = " + cursor.getCount());
                        // looping through all rows and adding to list
                        if (cursor.moveToFirst()) {
                            do {
                                PendingTask model = new PendingTask();

//					model.set DbHelper.PENDING_task_type, "1");
                                model.setTaskId(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskId)));
                                model.setIssue(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_issue)));

                                model.setIs_google_sheet(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_is_google_sheet)));
                                model.setGoogle_sheet_url(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Google_sheet_url)));


                                model.setDate_time(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_date_time)));
                                model.setPhone(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_phone)));
                                model.setAddress(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_address)));
                                if (model.getAddress() != null && !model.getAddress().equals("null")) {
                                    JSONArray arrClientAddress = new JSONArray(model.getAddress() + "");
                                    ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                                    Gson gson = new Gson();
                                    for (int r = 0; r < arrClientAddress.length(); r++) {
                                        JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                                        ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                                        arrListClientAddress.add(address);
                                    }
                                    if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                                        model.setClientAddress(new ArrayList<ClientAddress>());
                                        model.getClientAddress().addAll(arrListClientAddress);
                                    }
                                }
                                model.setOrg_name(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_name)));
                                model.setOrg_ph_num(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_org_ph_num)));
                                model.setLatitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_latitude)));
                                model.setLongitude(cursor.getDouble(cursor.getColumnIndex(DbHelper.PENDING_longitude)));
                                model.setTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_time)));
                                model.setPersonName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_personName)));
                                model.setSupervisorName(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_supervisorName)));
                                model.setTasktype(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_tasktype)));
                                model.setAdditionalInfo(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_additionalInfo)));
                                model.setStartTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_startTime)));
                                model.setStopTime(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_stopTime)));
                                model.setTotalTimeSpend(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_totalTimeSpend)));
                                model.setTaskReport(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_taskReport)));
                                model.setConsumerFeedback(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_consumerFeedback)));
                                model.setRating(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_rating)));
                                model.setSignatureImage(cursor.getBlob(cursor.getColumnIndex(DbHelper.PENDING_signatureImage)));
                                model.setSignatureString(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_signatureString)));
                                model.setReason(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_reason)));
                                model.setProblem_reported(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_problem_reported)));
                                model.setDiagnosis_done(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_diagnosis_done)));
                                model.setResolution(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_resolution)));
                                model.setNotes_support(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_support)));
                                model.setDeilvery_status(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_deilvery_status)));
                                model.setNotes_delevery(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_notes_delevery)));
                                model.setIs_recurring(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_Is_Recuring)));
                                model.setRecurring_type(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_type)));
                                model.setRecurring_end_date(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_recurring_end_date)));
                                model.setEt_id(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_ET_ID)));
                                model.setDuration(cursor.getString(cursor.getColumnIndex(DbHelper.PENDING_duration)));

                                pendingTasks.add(model);

                            } while (cursor.moveToNext());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                }
            }


            // return brand list
            cursor.close();
            return pendingTasks;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.close();
        }

        return pendingTasks;
    }
}
