package com.asrapps.mowom;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.helper.DirectionsJSONParser;
import com.asrapps.mowom.model.ClientAddress;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapMenuDialog extends DialogFragment implements OnClickListener, OnMapReadyCallback,GoogleMap.OnMapLoadedCallback {

    private double latFrom = 0.0000, lngFrom = 0.0000;
    private GoogleMap map;
    private ImageView ivClose;
    ArrayList<ClientAddress> clientAddresses;

    private String selectedLanguage;

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        View view = inflater.inflate(R.layout.dialog_map, container, false);

        ivClose = (ImageView) view.findViewById(R.id.mapBtnClose);
        ivClose.setOnClickListener(this);

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            RelativeLayout.LayoutParams params11 = (RelativeLayout.LayoutParams) ivClose.getLayoutParams();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params11.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            }
            params11.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            ivClose.setLayoutParams(params11);
        }

        try {
            int status = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity());

            if (status == ConnectionResult.SUCCESS) {

                if (map == null) {
                    ((SupportMapFragment) getActivity()
                            .getSupportFragmentManager().findFragmentById(
                                    R.id.googleMap)).getMapAsync(this);
//                    AddMarker();
                }
//                if (map == null) {
//                    Toast toast = Toast.makeText(getActivity(),
//                            getString(R.string.unable_to_create_maps), Toast.LENGTH_SHORT);
//
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
//                }

            } else {

                int requestCode = 10;
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,
                        getActivity(), requestCode);
                dialog.show();
            }
        } catch (Exception ex) {
            Toast toast = Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mapBtnClose:
                getDialog().dismiss();
                break;

            default:
                break;
        }

    }

    private void AddMarker() {


        if (map != null) {

            MarkerOptions marker1 = new MarkerOptions().position(
                    new LatLng(latFrom, lngFrom));

            // Changing marker icon
            marker1.icon(BitmapDescriptorFactory
                    .fromResource(R.drawable.from_map_icon));

            // adding marker
            map.addMarker(marker1).showInfoWindow();


            // adding marker
            if (clientAddresses != null) {
                for (int i = 0; i < clientAddresses.size(); i++) {
                    // create marker
                    MarkerOptions marker = new MarkerOptions().position(
                            new LatLng(Double.parseDouble(clientAddresses.get(i).latitude), Double.parseDouble(clientAddresses.get(i).longitude)));

                    // Changing marker icon
                    marker.icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.to_map_icon));

                    map.addMarker(marker).showInfoWindow();
                }
            }

//            CameraPosition cameraPosition = new CameraPosition.Builder()
//                    .target(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude))).zoom(12).build();
//
//            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            if (clientAddresses != null) {
                for (int i = 0; i < clientAddresses.size(); i++) {
                    builder.include(new LatLng(Double.parseDouble(clientAddresses.get(i).latitude), Double.parseDouble(clientAddresses.get(i).longitude)));
                }
            }
            builder.include(new LatLng(latFrom, lngFrom));
            LatLngBounds bounds = builder.build();

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 10);

            if (map != null) {
                map.moveCamera(cu);
                map.animateCamera(cu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Getting URL to the Google Directions API

        if (clientAddresses != null) {
            for (int i = 0; i < clientAddresses.size(); i++) {
                String url = getDirectionsUrl(new LatLng(latFrom, lngFrom), new LatLng(Double.parseDouble(clientAddresses.get(i).latitude), Double.parseDouble(clientAddresses.get(i).longitude)));
                DownloadTask downloadTask = new DownloadTask();

                //Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (map == null) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.unable_to_create_maps), Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
//            AddMarker();
            map.setOnMapLoadedCallback(this);
        }
    }

    @Override
    public void onMapLoaded() {
        if (map == null) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.unable_to_create_maps), Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            AddMarker();
        }
    }

    // New
    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("TTT", "Exception while downloading url " + e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            try {
                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;
                MarkerOptions markerOptions = new MarkerOptions();

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(2);
                    lineOptions.color(Color.RED);
                }

                // Drawing polyline in the Google Map for the i-th route
                map.addPolyline(lineOptions);

            } catch (Exception e) {

            }
        }
    }


    // new method
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    public void setLatLong(ArrayList<ClientAddress> clientAddresses, double latFrom, double lngFrom) {
        this.clientAddresses = clientAddresses;

        this.latFrom = latFrom;
        this.lngFrom = lngFrom;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (map != null) {
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .remove(getActivity().getSupportFragmentManager()
                            .findFragmentById(R.id.googleMap)).commit();
            map = null;
        }
    }

}
