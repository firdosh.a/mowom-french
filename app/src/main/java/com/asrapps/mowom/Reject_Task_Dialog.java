package com.asrapps.mowom;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.utils.MowomLogFile;

import org.json.JSONException;
import org.json.JSONObject;

public class Reject_Task_Dialog extends DialogFragment implements
        OnClickListener, AsyncResponseListener, OnCancelListener,
        OnCheckedChangeListener {

    ImageView closeImage;
    private RadioGroup radioReasons;
    private RadioButton radioReasonButton;
    Button dialogSubmitButton;
    View view;

    RadioButton radioOver_booked, radioLocation_too_far,
            radioSkil_does_not_match, radioOther_reason;

    ConnectionDetector internetConnection;
    private ProgressHUD mProgressHUD;

    EditText reasonTextBox;

    private String task_id;

    String selectedLanguage;

    TextView textView3;

    TextView rdOver, rdLocation, rdSkill, rdOther;

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = inflater.inflate(R.layout.reject_task_reason_dialog_layout_pr,
                    container, false);
        } else {
            view = inflater.inflate(R.layout.reject_task_reason_dialog_layout,
                    container, false);
        }


        textView3 = (TextView) view.findViewById(R.id.textView3);

        rdOver = (TextView) view.findViewById(R.id.rdOver);
        rdLocation = (TextView) view.findViewById(R.id.rdLocation);
        rdSkill = (TextView) view.findViewById(R.id.rdSkill);
        rdOther = (TextView) view.findViewById(R.id.rdOther);

        reasonTextBox = (EditText) view.findViewById(R.id.reasonTextBox);
        reasonTextBox.setEnabled(false);
        closeImage = (ImageView) view.findViewById(R.id.closeImage);
        closeImage.setOnClickListener(this);

        radioReasons = (RadioGroup) view.findViewById(R.id.radioReasons);
        radioReasons.setOnCheckedChangeListener(this);

        radioOver_booked = (RadioButton) view
                .findViewById(R.id.radioOver_booked);

        radioOver_booked.setOnClickListener(this);

        radioLocation_too_far = (RadioButton) view
                .findViewById(R.id.radioLocation_too_far);
        radioLocation_too_far.setOnClickListener(this);

        radioSkil_does_not_match = (RadioButton) view
                .findViewById(R.id.radioSkil_does_not_match);
        radioSkil_does_not_match.setOnClickListener(this);

        radioOther_reason = (RadioButton) view
                .findViewById(R.id.radioOther_reason);
        radioOther_reason.setOnClickListener(this);


        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

//		if (selectedLanguage.equalsIgnoreCase("pr"))
//		{
//
//
//			RelativeLayout.LayoutParams params11 =new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params11.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//			params11.addRule(RelativeLayout.CENTER_VERTICAL);
//
//			radioOver_booked.setLayoutParams(params11);
//			radioSkil_does_not_match.setLayoutParams(params11);
//			radioLocation_too_far.setLayoutParams(params11);
//			radioOther_reason.setLayoutParams(params11);
//
//			RelativeLayout.LayoutParams params111 =  new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params111.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params111.addRule(RelativeLayout.CENTER_VERTICAL);
//			params111.addRule(RelativeLayout.LEFT_OF,R.id.radioOver_booked);
//			rdOver.setLayoutParams(params111);
//			rdOver.setGravity(Gravity.RIGHT);
//
//			RelativeLayout.LayoutParams params112 =  new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params112.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params112.addRule(RelativeLayout.CENTER_VERTICAL);
//			params112.addRule(RelativeLayout.LEFT_OF,R.id.radioSkil_does_not_match);
//			rdSkill.setLayoutParams(params112);
//			rdSkill.setGravity(Gravity.RIGHT);
//
//			RelativeLayout.LayoutParams params113 =  new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params113.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params113.addRule(RelativeLayout.CENTER_VERTICAL);
//			params113.addRule(RelativeLayout.LEFT_OF,R.id.radioLocation_too_far);
//			rdLocation.setLayoutParams(params113);
//			rdLocation.setGravity(Gravity.RIGHT);
//
//			RelativeLayout.LayoutParams params114 =  new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params114.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params114.addRule(RelativeLayout.CENTER_VERTICAL);
//			params114.addRule(RelativeLayout.LEFT_OF,R.id.radioOther_reason);
//			rdOther.setLayoutParams(params114);
//			rdOther.setGravity(Gravity.RIGHT);
//
////			Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
////			drawable.setBounds(0, 0, 57, 72);
////
////			radioOver_booked.setCompoundDrawables(null, null, drawable, null);
////			radioOver_booked.setGravity(Gravity.CENTER_VERTICAL);
////			radioOver_booked.setButtonDrawable(android.R.color.transparent);
////			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
////				//noinspection deprecation
////				radioOver_booked.setBackgroundDrawable(null);
////			} else {
////				radioOver_booked.setBackground(null);
////			}
////
////			radioLocation_too_far.setCompoundDrawables(null, null, drawable, null);
////			radioLocation_too_far.setGravity(Gravity.CENTER_VERTICAL);
////			radioLocation_too_far.setButtonDrawable(android.R.color.transparent);
////			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
////				//noinspection deprecation
////				radioLocation_too_far.setBackgroundDrawable(null);
////			} else {
////				radioLocation_too_far.setBackground(null);
////			}
////
////
////			radioSkil_does_not_match.setCompoundDrawables(null, null, drawable, null);
////			radioSkil_does_not_match.setGravity(Gravity.CENTER_VERTICAL);
////			radioSkil_does_not_match.setButtonDrawable(android.R.color.transparent);
////			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
////				//noinspection deprecation
////				radioSkil_does_not_match.setBackgroundDrawable(null);
////			} else {
////				radioSkil_does_not_match.setBackground(null);
////			}
////
////
////			radioOther_reason.setCompoundDrawables(null, null, drawable, null);
////			radioOther_reason.setGravity(Gravity.CENTER_VERTICAL);
////			radioOther_reason.setButtonDrawable(android.R.color.transparent);
////			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
////				//noinspection deprecation
////				radioOther_reason.setBackgroundDrawable(null);
////			} else {
////				radioOther_reason.setBackground(null);
////			}
//
//			reasonTextBox.setGravity(Gravity.RIGHT);
//			textView3.setGravity(Gravity.RIGHT);
//
//		}

        dialogSubmitButton = (Button) view
                .findViewById(R.id.dialogSubmitButton);
        dialogSubmitButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // safety check
        if (getDialog() == null)
            return;

        int width = WindowManager.LayoutParams.MATCH_PARENT;
        int height = WindowManager.LayoutParams.MATCH_PARENT;

        getDialog().getWindow().setLayout(width, height);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.radioOver_booked) {
            reasonTextBox.setEnabled(false);
            if (radioLocation_too_far.isChecked())
                radioLocation_too_far.setChecked(false);
            if (radioSkil_does_not_match.isChecked())
                radioSkil_does_not_match.setChecked(false);
            if (radioOther_reason.isChecked())
                radioOther_reason.setChecked(false);
        }

        if (v.getId() == R.id.radioLocation_too_far) {
            reasonTextBox.setEnabled(false);
            if (radioOver_booked.isChecked())
                radioOver_booked.setChecked(false);
            if (radioSkil_does_not_match.isChecked())
                radioSkil_does_not_match.setChecked(false);
            if (radioOther_reason.isChecked())
                radioOther_reason.setChecked(false);
        }

        if (v.getId() == R.id.radioSkil_does_not_match) {
            reasonTextBox.setEnabled(false);
            if (radioLocation_too_far.isChecked())
                radioLocation_too_far.setChecked(false);
            if (radioOver_booked.isChecked())
                radioOver_booked.setChecked(false);
            if (radioOther_reason.isChecked())
                radioOther_reason.setChecked(false);
        }

        if (v.getId() == R.id.radioOther_reason) {
            reasonTextBox.setEnabled(true);
            if (radioLocation_too_far.isChecked())
                radioLocation_too_far.setChecked(false);
            if (radioSkil_does_not_match.isChecked())
                radioSkil_does_not_match.setChecked(false);
            if (radioOver_booked.isChecked())
                radioOver_booked.setChecked(false);
        }


        switch (v.getId()) {
            case R.id.closeImage:
                getDialog().dismiss();
                break;

            case R.id.dialogSubmitButton:

                internetConnection = new ConnectionDetector(getActivity());
                if (internetConnection
                        .isConnectingToInternet(getString(R.string.check_connection))) {

                    RadioSelected();
                }

                break;

            default:
                break;
        }

    }

    private void RadioSelected() {
        int selectedId = radioReasons.getCheckedRadioButtonId();
        String reason = "";

        if (!radioOther_reason.isChecked() || !radioOver_booked.isChecked() || !radioLocation_too_far.isChecked() || !radioSkil_does_not_match.isChecked()) {


            if (radioOther_reason.isChecked()) {
                if (!isEmpty(reasonTextBox))
                    reason = reasonTextBox.getText().toString();
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.enter_the_reason),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            } else {
                if (radioOver_booked.isChecked()) {
                    reason = getResources().getString(R.string.over_booked);
                } else if (radioLocation_too_far.isChecked()) {
                    reason = getResources().getString(R.string.location_too_far);

                } else if (radioSkil_does_not_match.isChecked()) {
                    reason = getResources().getString(R.string.skil_does_not_match);

                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.select_reason), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            if (!reason.equalsIgnoreCase(""))
                RejectTask(ConstantValues.RejectTaskURL, task_id, reason);
        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.give_the_reason), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    private void RejectTask(String url, String task_id, String reason) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("task_id", task_id);
            requestObject.put("rejectreason", reason);

        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(url);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Reject_Task_Dialog Fragment :- " + ConstantValues.RejectTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {

        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
        Log.e("Response==>", response);

        if (!response.equalsIgnoreCase("")) {
            if (requestURL.equalsIgnoreCase(ConstantValues.RejectTaskURL)) {
                MowomLogFile.writeToLog("\n" + "Reject_Task_Dialog fragment :- " + ConstantValues.RejectTaskURL + " -------------" + response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {
                                Toast toast = Toast.makeText(getActivity(),
                                        getString(R.string.mission_rejected),
                                        Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();

                                getTargetFragment().onActivityResult(
                                        getTargetRequestCode(), 1,
                                        getActivity().getIntent());// To call
                                // onActivityResult
                                // in the
                                // Alert_Screen_List
                                dismiss();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again),
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }

        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        if (checkedId == R.id.radioOver_booked) {
            reasonTextBox.setEnabled(false);
            if (radioLocation_too_far.isChecked())
                radioLocation_too_far.setChecked(false);
            if (radioSkil_does_not_match.isChecked())
                radioSkil_does_not_match.setChecked(false);
            if (radioOther_reason.isChecked())
                radioOther_reason.setChecked(false);
        }

        if (checkedId == R.id.radioLocation_too_far) {
            reasonTextBox.setEnabled(false);
            if (radioOver_booked.isChecked())
                radioOver_booked.setChecked(false);
            if (radioSkil_does_not_match.isChecked())
                radioSkil_does_not_match.setChecked(false);
            if (radioOther_reason.isChecked())
                radioOther_reason.setChecked(false);
        }

        if (checkedId == R.id.radioSkil_does_not_match) {
            reasonTextBox.setEnabled(false);
            if (radioLocation_too_far.isChecked())
                radioLocation_too_far.setChecked(false);
            if (radioOver_booked.isChecked())
                radioOver_booked.setChecked(false);
            if (radioOther_reason.isChecked())
                radioOther_reason.setChecked(false);
        }

        if (checkedId == R.id.radioOther_reason) {
            reasonTextBox.setEnabled(true);
            if (radioLocation_too_far.isChecked())
                radioLocation_too_far.setChecked(false);
            if (radioSkil_does_not_match.isChecked())
                radioSkil_does_not_match.setChecked(false);
            if (radioOver_booked.isChecked())
                radioOver_booked.setChecked(false);
        }

//		switch (checkedId) {
//		case R.id.radioOver_booked:
//			radioOver_booked.setChecked(true);
//			radioLocation_too_far.setChecked(false);
//			radioSkil_does_not_match.setChecked(false);
//			radioOther_reason.setChecked(false);
//
//			reasonTextBox.setEnabled(false);
//			break;
//
//		case R.id.radioLocation_too_far:
//			radioOver_booked.setChecked(false);
//			radioLocation_too_far.setChecked(true);
//			radioSkil_does_not_match.setChecked(false);
//			radioOther_reason.setChecked(false);
//
//			reasonTextBox.setEnabled(false);
//			break;
//
//		case R.id.radioSkil_does_not_match:
//			radioOver_booked.setChecked(false);
//			radioLocation_too_far.setChecked(false);
//			radioSkil_does_not_match.setChecked(true);
//			radioOther_reason.setChecked(false);
//
//			reasonTextBox.setEnabled(false);
//			break;
//
//		case R.id.radioOther_reason:
//			radioOver_booked.setChecked(false);
//			radioLocation_too_far.setChecked(false);
//			radioSkil_does_not_match.setChecked(false);
//			radioOther_reason.setChecked(true);
//
//			reasonTextBox.setEnabled(true);
//			break;
//
//		default:
//			break;
//		}

    }

}
