package com.asrapps.mowom;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.FCM.MyInstanceIDListenerService;
import com.asrapps.mowom.FCM.RegistrationIntentService;
import com.asrapps.mowom.base.AppMainTabActivity;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.utils.MowomLogFile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import static com.asrapps.mowom.R.string.login;
import static com.asrapps.mowom.constants.ConstantValues.PendingTaskListURL;
import static com.asrapps.mowom.constants.ConstantValues.deviceToken;

public class LoginActivity extends BaseActivity implements OnClickListener,
        AsyncResponseListener, OnCancelListener {

    TextView tvForgotPwd;
    Button login_btnLogin;
    EditText login_user_email_id, login_passwordEd;

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int PERMISSION_REQUEST_CODE_CALL_PHONE = 2;
    private static final int REQUEST_WRITE_STORAGE = 112;

    SQLController sqlcon;
    private ArrayList<Login> mLogin, mLogoinUser;

    private ProgressHUD mProgressHUD;
    ConnectionDetector internetConnection;
    String selectedLanguage;
    public static String taskId = "";
    private int count_task = 0;

    BroadcastReceiver receiver;

    RadioButton rdImgEnglish, rdImgPersian;

    TextView rdEnglish, rdPersian;

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPlayServices()) {
            Intent intent = new Intent(LoginActivity.this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        sqlcon = new SQLController(LoginActivity.this);

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {

                // request phone call permission
                Log.e("value", "Permission already Granted, Now you can save image.");
                if (checkPermissionPhoneCalls()) {
                    Log.e("value", "Permission already Granted, Now you can call.");

                    if (chechPermissionForWrite()) {

                    }
                } else {
                    requestPermissionPhoneCalls();
                }
            } else {
                requestPermission();
            }
        } else {
            Log.e("value", "Not required for requesting runtime permission");
        }

//        ConstantValues.deviceToken = FirebaseInstanceId.getInstance().getToken();

//        ConstantFunction.savestatus(LoginActivity.this, "toggle", "0");

        ConstantFunction.ChangeLang(getApplicationContext(), "en");
        ConstantFunction.LoadLocale(this);
//                ConstantFunction.RestartTheApp(getApplicationContext(), 2);


        rdEnglish = (TextView) findViewById(R.id.rdEnglish);
        rdPersian = (TextView) findViewById(R.id.rdPersian);

        rdEnglish.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rdImgPersian.setChecked(false);
                rdImgEnglish.setChecked(true);
                ConstantFunction.ChangeLang(getApplicationContext(), "en");
                ConstantFunction.LoadLocale(LoginActivity.this);

                changeResources();

//                ConstantFunction.RestartTheApp(getApplicationContext(), 2);

            }
        });

//        rdPersian.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rdImgPersian.setChecked(true);
//                rdImgEnglish.setChecked(false);
//                ConstantFunction.ChangeLang(getApplicationContext(), "pr");
//                ConstantFunction.LoadLocale(LoginActivity.this);
//
//                changeResources();
//
////                ConstantFunction.RestartTheApp(getApplicationContext(), 2);
//
//
//            }
//        });

        rdPersian.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                rdImgPersian.setChecked(true);
                rdImgEnglish.setChecked(false);
                ConstantFunction.ChangeLang(getApplicationContext(), "fr");
                ConstantFunction.LoadLocale(LoginActivity.this);

                changeResources();

//                ConstantFunction.RestartTheApp(getApplicationContext(), 2);


            }
        });

        rdImgEnglish = (RadioButton) findViewById(R.id.rdImgEnglish);
        rdImgPersian = (RadioButton) findViewById(R.id.rdImgPersian);


        rdImgEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rdImgPersian.setChecked(false);
                    ConstantFunction.ChangeLang(getApplicationContext(), "en");
                    ConstantFunction.LoadLocale(LoginActivity.this);

                    changeResources();


//                ConstantFunction.RestartTheApp(getApplicationContext(), 2);
                }
            }
        });

//        rdImgPersian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    rdImgEnglish.setChecked(false);
//                    ConstantFunction.ChangeLang(getApplicationContext(), "pr");
//                    ConstantFunction.LoadLocale(LoginActivity.this);
//                    changeResources();
//
//
////                ConstantFunction.RestartTheApp(getApplicationContext(), 2);
//                }
//            }
//        });

        rdImgPersian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rdImgEnglish.setChecked(false);
                    ConstantFunction.ChangeLang(getApplicationContext(), "fr");
                    ConstantFunction.LoadLocale(LoginActivity.this);
                    changeResources();


//                ConstantFunction.RestartTheApp(getApplicationContext(), 2);
                }
            }
        });


        tvForgotPwd = (TextView) findViewById(R.id.tvForgotPwd);
        tvForgotPwd.setOnClickListener(this);

        login_btnLogin = (Button) findViewById(R.id.login_btnLogin);
        login_btnLogin.setOnClickListener(this);

        login_user_email_id = (EditText) findViewById(R.id.login_user_email_id);
        login_passwordEd = (EditText) findViewById(R.id.login_passwordEd);

        selectedLanguage = ConstantFunction.CheckLanguage(LoginActivity.this);

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            Drawable imgUser = getResources().getDrawable(R.drawable.email_id_icon);
            login_user_email_id.setCompoundDrawablesWithIntrinsicBounds(null, null, imgUser, null);

            Drawable imgPassword = getResources().getDrawable(R.drawable.password_icon);
            login_passwordEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPassword, null);


            login_user_email_id.setGravity(Gravity.RIGHT);
            login_passwordEd.setGravity(Gravity.RIGHT);

        }

        login_user_email_id.setText("");
        login_passwordEd.setText("");

        internetConnection = new ConnectionDetector(this);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                MyInstanceIDListenerService mfs = new MyInstanceIDListenerService();
                mfs.onTokenRefresh();

            }
        });

        if (checkPlayServices()) {
            Intent intent = new Intent(LoginActivity.this, RegistrationIntentService.class);
            startService(intent);
        }
        //System.setProperty("http.keepAlive", "false");
    }

    private boolean chechPermissionForWrite() {

        boolean hasPermission = (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
            return true;

        } else {
            return false;
        }

    }

    public void changeResources() {

        if (selectedLanguage != null) {
            selectedLanguage = ConstantFunction.CheckLanguage(LoginActivity.this);
        }

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }


        if (selectedLanguage.equalsIgnoreCase("pr")) {
            Drawable imgUser = getResources().getDrawable(R.drawable.email_id_icon);
            login_user_email_id.setCompoundDrawablesWithIntrinsicBounds(null, null, imgUser, null);

            Drawable imgPassword = getResources().getDrawable(R.drawable.password_icon);
            login_passwordEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPassword, null);


            login_user_email_id.setGravity(Gravity.RIGHT);
            login_passwordEd.setGravity(Gravity.RIGHT);

        } else {
            Drawable imgUser = getResources().getDrawable(R.drawable.email_id_icon);
            login_user_email_id.setCompoundDrawablesWithIntrinsicBounds(imgUser, null, null, null);

            Drawable imgPassword = getResources().getDrawable(R.drawable.password_icon);
            login_passwordEd.setCompoundDrawablesWithIntrinsicBounds(imgPassword, null, null, null);


            login_user_email_id.setGravity(Gravity.LEFT);
            login_passwordEd.setGravity(Gravity.LEFT);

        }

        Resources resources = getResources();
        login_user_email_id.setHint(resources.getString(R.string.user_name));

        login_passwordEd.setHint(resources.getString(R.string.login_user_password));
        tvForgotPwd.setText(resources.getString(R.string.login_forgot_pass));

        login_btnLogin.setText(resources.getString(login));

        ((TextView) findViewById(R.id.chooseLang)).setText(resources.getString(R.string.Choose_language));


    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }

    }

    private boolean checkPermissionPhoneCalls() {
        int result_phone_calls = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CALL_PHONE);

        if (result_phone_calls == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(LoginActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    private void requestPermissionPhoneCalls() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.CALL_PHONE)) {
            Toast.makeText(LoginActivity.this, "ALLOW TO ACCESS PHONE CALLS IN YOUR APP. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST_CODE_CALL_PHONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can save image .");
                } else {
                    Log.e("value", "Permission Denied, You cannot save image.");
                }

                // request phone call permission.
                if (checkPermissionPhoneCalls()) {
                    Log.e("value", "Permission already Granted, Now you can call.");
                } else {
                    requestPermissionPhoneCalls();
                }
                break;

            case PERMISSION_REQUEST_CODE_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can call .");
                } else {
                    Log.e("value", "Permission Denied, You cannot call.");
                }
                break;

            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //reload my activity with permission granted or use the features what required the permission

                    MowomLogFile.writeToLog("\n" + "LoginActivity" + " -------------" + "Permission Granted, Now you can store Log File.");
                } else {
                    Toast.makeText(LoginActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvForgotPwd:
                if (internetConnection.isConnectingToInternet(getString(R.string.check_connection)))
                    ForgetPassword();
                break;

            case R.id.login_btnLogin:

                ConstantFunction.LoadLocale(this);

//                if (internetConnection.isConnectingToInternet(getString(R.string.check_connection)))
                if (internetConnection.isConnectingToInternet(""))
                    Login();
                else {
                    new LoginTask(login_user_email_id.getText().toString(), login_passwordEd.getText().toString()).execute();
                }
                break;

            default:
                break;
        }
    }

    private void ForgetPassword() {
        Intent intent = new Intent(this, Forget_Password_Activity.class);
        startActivity(intent);
    }

    private void Login() {

        String deviceToken = "";
        if (ConstantValues.deviceToken != null) {
            deviceToken = ConstantValues.deviceToken;
        }

        String userName = login_user_email_id.getText().toString();
        String password = login_passwordEd.getText().toString();
        if (!deviceToken.equals("") && deviceToken != null) {
            if (!userName.equals("") && !password.equals("")) {
                if (ConstantFunction.isEmailValid(userName)) {
                    ValidateLogin(userName, password);
                } else {
                    Toast toast = Toast.makeText(this,
                            getString(R.string.please_enter_email),
                            Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            } else {
                Toast toast = Toast.makeText(this,
                        getString(R.string.please_enter_user_password),
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

        } else {
            if (checkPlayServices()) {
                Intent intent = new Intent(LoginActivity.this, RegistrationIntentService.class);
                startService(intent);
            }

            Toast toast = Toast.makeText(this,
                    getString(R.string.please_try_again),
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }


    }

    private void GoDashBoard() {
        Intent intent = new Intent(this, AppMainTabActivity.class);
//        Intent intent = new Intent(this, LogoScreenActivity.class);
        startActivity(intent);
        finish();
    }

    private void ValidateLogin(String strUserName, String strPassword) {

        JSONObject requestObject = new JSONObject();
        try {
            // email, password
            requestObject.put("email", strUserName);
            requestObject.put("password", strPassword);
            requestObject.put("deviceId", deviceToken);
            requestObject.put("device", "2");
            String selectedLanguage = ConstantFunction.CheckLanguage(LoginActivity.this);
            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }
            requestObject.put("is_app_lang",selectedLanguage );

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(this, getString(R.string.loading),
                true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.LoginURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "LoginActivity :- " + ConstantValues.LoginURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {


        Log.e("Response==>", response);

        if (!response.equalsIgnoreCase("")) {
            if (requestURL.equalsIgnoreCase(ConstantValues.LoginURL)) {
                MowomLogFile.writeToLog("\n" + "LoginActivity :- " + ConstantValues.LoginURL + " -------------" + response);
                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");
                    String uId = "";

                    if (result_code.equalsIgnoreCase("1")) {
                        try {

							/*
                             * try { InstanceID instanceID =
							 * InstanceID.getInstance(this);
							 * 
							 * String token = instanceID.getToken(
							 * "AIzaSyDv-wiRorRaZ5j9PHilCQqvnjF-wpeHxWw",
							 * GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
							 * 
							 * Log.i("TAG", "GCM Registration Token: " + token);
							 * System.out.println("GCM Registration Token: ==>"
							 * + token);
							 * 
							 * }catch (Exception e) { Log.d("TAG",
							 * "Failed to complete token refresh", e);
							 * System.out
							 * .println("Failed to complete token refresh==> " +
							 * e); }
							 */

//							ParseAnalytics
//							.trackAppOpenedInBackground(getIntent());
//							ParseInstallation.getCurrentInstallation()
//							.saveInBackground();
//							if (ParseUser.getCurrentUser() == null) {
//								ParseUser.enableAutomaticUser();
//							}
//							final ParseInstallation currentInstallation = ParseInstallation
//									.getCurrentInstallation();
//							currentInstallation
//							.saveInBackground(new SaveCallback() {
//								public void done(ParseException e) {
//									if (e == null) {
//										try{
//											System.out.println("ok");
//											ConstantValues.deviceToken = currentInstallation
//													.get("deviceToken")
//													.toString();
//											System.out
//											.println("deviceToken==> "
//													+ ConstantValues.deviceToken);
//										}catch(Exception ex)
//										{
//											System.out.println("deviceToken==> "+ ex);
//										}
//									} else {
//										System.out.println("deviceToken not ok===> ");
//									}
//								}
//							});
                            Gson gson = new Gson();
                            Login login = new Login();
                            JSONObject job2 = job.getJSONObject("data");
                            login.comp_id = job2.get("comp_id").toString();
                            login.emp_id = job2.get("emp_id").toString();

                            login.emp_name = job2.get("emp_name").toString();
                            login.emp_email = job2.get("emp_email").toString();
                            login.phone = job2.get("phone").toString();
                            login.profileimage = job2.get("profileimage").toString();
                            login.department = job2.get("department").toString();
                            login.client_logo = job2.get("client_logo").toString();
                            login.password = login_passwordEd.getText().toString();
                            if(job2.getString("partnerDetail")!=null)
                            {
                                JSONObject job3 = job2.getJSONObject("partnerDetail");
                                if(job3.getString("is_report")!=null)
                                {
                                    login.is_report=job3.getString("is_report");
                                    ConstantFunction.savestatus(LoginActivity.this,
                                            "is_report", login.is_report.toString());
                                }

                                if(job3.getString("is_review")!=null)
                                {
                                    login.is_review=job3.getString("is_review");
                                    ConstantFunction.savestatus(LoginActivity.this,
                                            "is_review", login.is_review.toString());
                                }

                                if(job3.getString("is_signature")!=null)
                                {
                                    login.is_signature=job3.getString("is_signature");
                                    ConstantFunction.savestatus(LoginActivity.this,
                                            "is_signature", login.is_signature.toString());
                                }

                            }

                            String imagePathDownload = ConstantValues.ImageURL + job2.get("profileimage").toString();
                            String partenerLogoDownload = job2.get("client_logo").toString(); //client logo for partener

                            if (imagePathDownload != null) {
                                new ImageDownloadTask(login, imagePathDownload, partenerLogoDownload).execute();
                            }

//                            sqlcon.open();
//                            sqlcon.iloginById();

                            sqlcon.open();
                            mLogoinUser = sqlcon.getLastUSer();

                            if (mLogoinUser.size() > 0) {
                                if (!mLogoinUser.get(0).emp_id.equals(login.emp_id)) {
                                    //stop service of timer
                                    stopService();

                                    sqlcon.open();
                                    sqlcon.SalesDataDeletedById1(login.emp_id);

                                }
                                sqlcon.open();
                                sqlcon.DeleteUserById();

                                ConstantFunction.savestatus(getApplicationContext(), AppConstants.task_Id,
                                        "0");

                                ConstantFunction.savestatus(getApplicationContext(), AppConstants.et_id,
                                        "0");

                            }

                            sqlcon.open();
                            sqlcon.AddUserLogin(login);
                            mSessionManager.putLoginDetail(gson.toJson(login), LoginActivity.this);

							/*
                             * ConstantFunction.savestatus(LoginActivity.this,
							 * "UserId", "1");
							 */

                            uId = job2.get("emp_id").toString();
                            ConstantFunction.savestatus(LoginActivity.this,
                                    "UserId", job2.get("emp_id").toString());

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "companyid", job2.get("companyid").toString());

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "UserHeaderName",
                                    "Hi," + " "
                                            + job2.get("emp_name").toString());

                            ConstantFunction
                                    .savestatus(LoginActivity.this, "UserName",
                                            job2.get("emp_name").toString());

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "UserPhone", job2.get("phone").toString());
                            ConstantFunction.savestatus(LoginActivity.this,
                                    "UserEmail", job2.get("emp_email")
                                            .toString());

                            ConstantFunction.savestatus(getApplicationContext(),
                                    "department", job2.get("department")
                                            .toString());

                            if (job2.get("supervisor_id") != null) {
                                ConstantFunction.savestatus(getApplicationContext(),
                                        "supervisor_id", job2.get("supervisor_id")
                                                .toString());
                            }

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "can_create_task", job2.get("can_create_task")
                                            .toString());

                            // new
                            ConstantFunction.savestatus(LoginActivity.this, "privacy", "true");
                            //startService(new Intent(this, MowomService.class));

                            String imagePath = ConstantValues.ImageURL
                                    + job2.getString("profileimage");

                            imagePath = imagePath.replace(" ", "%20");


                            ConstantFunction.savestatus(LoginActivity.this,
                                    "UserImage", (imagePath).toString());

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "client_logo", job2.get("client_logo")
                                            .toString());

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "is_privacy", job2.get("is_privacy")
                                            .toString());

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "availability", job2.get("availability")
                                            .toString());

                            String availability = job2.get("availability").toString();
                            if (availability.equals("1")) {
                                ConstantFunction.savestatus(LoginActivity.this, "toggle", "0");
                            }

                            ConstantFunction.savestatus(LoginActivity.this,
                                    "leavereason", job2.get("leavereason")
                                            .toString());
                            String leavreson = job2.get("leavereason").toString();

                            if (leavreson.equals(getResources().getString(R.string.absent))) {
                                ConstantFunction.savestatus(LoginActivity.this, "toggle", "1");
                                ConstantFunction.savestatus(LoginActivity.this, "userstatus", leavreson + "");
                            } else if (leavreson.equals(getResources().getString(R.string.sick))) {
                                ConstantFunction.savestatus(LoginActivity.this, "toggle", "2");
                                ConstantFunction.savestatus(LoginActivity.this, "userstatus", leavreson + "");
                            } else if (leavreson.equals(getResources().getString(R.string.holyday))) {
                                ConstantFunction.savestatus(LoginActivity.this, "toggle", "3");
                                ConstantFunction.savestatus(LoginActivity.this, "userstatus", leavreson + "");
                            }

//                            else {
//                                ConstantFunction.savestatus(LoginActivity.this, "toggle", "0");
//                                ConstantFunction.savestatus(LoginActivity.this, "userstatus", "");
//                            }


                            //if(!ConstantValues.deviceToken.equalsIgnoreCase(""))
                            //SaveDiveToken(ConstantValues.DeviceURL, ConstantValues.deviceToken);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //Pending and special data list and detail stored in database offline
                        GetPendingTasks(uId);


                        // ConstantFunction.showAlertDialog(LoginActivity.this,
                        // "Login success", false);
                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (status.equalsIgnoreCase("failed")) {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        Toast toast = Toast.makeText(this,
                                getString(R.string.please_usernamePwd),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        Toast toast = Toast.makeText(LoginActivity.this,
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if (mProgressHUD.isShowing()) {
                        mProgressHUD.dismiss();
                    }

                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Try again", false);
                }
            } else if (requestURL.equalsIgnoreCase(ConstantValues.DeviceURL)) {
                MowomLogFile.writeToLog("\n" + "LoginActivity :- " + ConstantValues.DeviceURL + " -------------" + response);
                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    if (status.equalsIgnoreCase("success")) {
                        try {
                            GoDashBoard();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (status.equalsIgnoreCase("failed")) {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        Toast toast = Toast.makeText(this,
                                getString(R.string.please_try_again),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (mProgressHUD.isShowing()) {
                            mProgressHUD.dismiss();
                        }
                        Toast toast = Toast.makeText(LoginActivity.this,
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if (mProgressHUD.isShowing()) {
                        mProgressHUD.dismiss();
                    }
//                    Toast toast = Toast.makeText(LoginActivity.this,
//                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();
                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Try again", false);
                }
            } else if (requestURL.equalsIgnoreCase(PendingTaskListURL)) {

                MowomLogFile.writeToLog("\n" + "LoginActivity :- " + PendingTaskListURL + " -------------" + response);
                try {
                    JSONObject job = new JSONObject(response);
                    String result_code = job.getString("code");
                    String status = job.getString("status");

                    PendingTask pendingTasks = new PendingTask(response);
                    if (result_code.equals("1")) {
                        if (pendingTasks.isSuccess()) {

                            if (pendingTasks.getPendingListTasks().size() > 0) {

                                for (int i = 0; i < pendingTasks.getPendingListTasks().size(); i++) {
                                    taskId = pendingTasks.getPendingListTasks().get(i).getTaskId();
                                    sqlcon.open();
                                    sqlcon.addPendingTasList(pendingTasks.getPendingListTasks().get(i));
                                    Log.v("pendingList", pendingTasks.getPendingListTasks().size() + "");
                                }

                            }
                        }

                        //PendingTask Detail stored in offline
                        GetSpecialTasks();
                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("18")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginActivity.this);
                            alertDialogBuilder.setMessage(getString(R.string.code_18));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(LoginActivity.this,
                                getString(R.string.please_try_again),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }

                } catch (Exception e) {
                    if (mProgressHUD.isShowing()) {
                        mProgressHUD.dismiss();
                    }
                    e.printStackTrace();
                }

            } else if (requestURL.equalsIgnoreCase(ConstantValues.SpecialTaskURL)) {

                MowomLogFile.writeToLog("\n" + "LoginActivity :- " + ConstantValues.SpecialTaskURL + " -------------" + response);
                mProgressHUD.dismiss();
                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    if (result_code.equals("1")) {
                        PendingTask pendingTasks = new PendingTask(response, true);
                        if (pendingTasks.isSuccess()) {

                            if (pendingTasks.getPendingListTasks().size() > 0) {

                                for (int i = 0; i < pendingTasks.getPendingListTasks().size(); i++) {
                                    taskId = pendingTasks.getPendingListTasks().get(i).getTaskId();
                                    sqlcon.open();
                                    sqlcon.addSpecialTasList(pendingTasks.getPendingListTasks().get(i));
                                    Log.v("pendingList", pendingTasks.getPendingListTasks().size() + "");
                                }
                            }
                        }
                        GoDashBoard();
                    }
                } catch (Exception e) {
                    if (mProgressHUD.isShowing()) {
                        mProgressHUD.dismiss();
                    }
                    e.printStackTrace();
                }
            } else {

                MowomLogFile.writeToLog("\n" + "LoginActivity :- " + " ------------- Error.");
            }
        }
    }

    private void stopService() {

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            }
        };

        if (UpdaterService.inProcess) {
            stopService(new Intent(LoginActivity.this,
                    UpdaterService.class));
            UpdaterService.inProcess = false;
            UpdaterService.firstTime = false;


            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("inProcess", false);
            editor.putBoolean("firstTime", false);
            editor.putString("currentTime", UpdaterService.currentTime + "");

            editor.commit();


            try {
                unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void SaveDiveToken(String URL, String device_id) {

        JSONObject requestObject = new JSONObject();
        try {
            // email, password
//            requestObject.put("device_id", device_id);
            requestObject.put("deviceId", deviceToken);
            requestObject.put("device", "2");
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(this, getString(R.string.loading),
                true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.DeviceURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "LoginActivity :- " + ConstantValues.DeviceURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
//				apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
//						.show();
            } else {
                Log.i("TTT", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private class LoginTask extends AsyncTask<Void, Void, Void> {
        private String userName, password;

        public LoginTask(String uName, String password) {
            this.userName = uName;
            this.password = password;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            sqlcon.open();
            mLogin = sqlcon.GetUserLogin();
            Log.v("login successfully", mLogin.size() + "");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            boolean isLogged = false;
            if (mLogin.size() > 0) {
                for (int i = 0; i < mLogin.size(); i++) {

                    if (mLogin.get(i).emp_email.equals(userName) && mLogin.get(i).password.equals(password)) {
                        Gson gson = new Gson();

                        ConstantFunction.savestatus(LoginActivity.this,
                                "UserId", mLogin.get(i).emp_id + "");
                        Log.v("userId", mLogin.get(i).emp_id);

                        mSessionManager.putLoginDetail(gson.toJson(mLogin.get(i)), getApplicationContext());
                        isLogged = true;
                        LoginActivity.this.finish();
                        startActivity(new Intent(LoginActivity.this, AppMainTabActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
//                        startActivity(new Intent(LoginActivity.this, LogoScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else {


                        Toast toast = Toast.makeText(LoginActivity.this,
                                getString(R.string.check_connection),
                                Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            } else {
                Toast toast = Toast.makeText(LoginActivity.this,
                        getString(R.string.login_first_time),
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    private void GetPendingTasks(String uId) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(LoginActivity.this, "UserId");
            //String id = "1";
            if (id != null && !id.equals("")) {
                requestObject.put("id", id);
            } else {
                requestObject.put("id", uId);
            }
            requestObject.put("list_count", count_task);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in " + e.toString());
            e.printStackTrace();
        }

//        mProgressHUD = ProgressHUD.show(LoginActivity.this, getString(R.string.loading), true,
//                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                PendingTaskListURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "LoginActivity :- " + PendingTaskListURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    private void GetSpecialTasks() {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(LoginActivity.this, "UserId");
            requestObject.put("id", id);
            requestObject.put("list_count", count_task);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in " + e.toString());
            e.printStackTrace();
        }

//        mProgressHUD = ProgressHUD.show(LoginActivity.this, getString(R.string.loading), true,
//                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.SpecialTaskURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "LoginActivity :- " + ConstantValues.SpecialTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }


    private class ImageDownloadTask extends AsyncTask<Void, Void, Void> {
        private Login mLogin;
        private String pathPartenerLogo, imagePathSdCard, partenerLogoDownload;
        private int count;

        public ImageDownloadTask(Login login, String imagePathDownload, String partenerLogoDownload) {
            this.mLogin = login;
            this.imagePathSdCard = imagePathDownload;
            this.partenerLogoDownload = partenerLogoDownload;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                if (mLogin != null && !mLogin.equals("") && partenerLogoDownload != null && !partenerLogoDownload.equals("")) {
                    URL url = new URL(partenerLogoDownload);
                    File fo = getFilesDirectory(LoginActivity.this);
                    String fileName = "/MowomApp" + System.currentTimeMillis() + ".png";

                    pathPartenerLogo = fo.toString() + fileName;

                    FileOutputStream outStream;
                    InputStream input = new BufferedInputStream(url.openStream());
                    outStream = new FileOutputStream(pathPartenerLogo);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        outStream.write(data, 0, count);
                    }
                    outStream.flush();
                    outStream.close();
                    input.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            mLogin.profileImageSdCard = path;
            mLogin.client_logo = pathPartenerLogo;
            sqlcon.open();
            sqlcon.addImageSdCard(mLogin);

            new profileImageTask(mLogin, imagePathSdCard).execute();
        }

    }

    public static File getFilesDirectory(Context context) {
        File appFilesDir = null;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            appFilesDir = getExternalFilesDir(context);
        }
        if (appFilesDir == null) {
            appFilesDir = context.getFilesDir();
        }
        return appFilesDir;
    }

    private static File getExternalFilesDir(Context context) {
        File dataDir = new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data");
        File appFilesDir = new File(new File(dataDir, context.getPackageName()), "files");
        if (!appFilesDir.exists()) {
            if (!appFilesDir.mkdirs()) {
                //L.w("Unable to create external cache directory");
                return null;
            }
            try {
                new File(appFilesDir, ".nomedia").createNewFile();
            } catch (IOException e) {
                return null;
            }
        }
        return appFilesDir;
    }

    private class profileImageTask extends AsyncTask<Void, Void, Void> {
        private Login mLogin;
        private String pathLogo, imagePathSdCard;
        private int count;

        public profileImageTask(Login mLogin, String imagePathSdCard) {
            this.mLogin = mLogin;
            this.imagePathSdCard = imagePathSdCard;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                if (mLogin != null && !mLogin.equals("") && imagePathSdCard != null && !imagePathSdCard.equals("")) {
                    URL url = new URL(imagePathSdCard);
                    File fo = getFilesDirectory(LoginActivity.this);
                    String fileName = "/MowomApp" + System.currentTimeMillis() + ".png";

                    pathLogo = fo.toString() + fileName;

                    FileOutputStream outStream;
                    InputStream input = new BufferedInputStream(url.openStream());
                    outStream = new FileOutputStream(pathLogo);
                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        outStream.write(data, 0, count);
                    }
                    outStream.flush();
                    outStream.close();
                    input.close();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mLogin.profileImageSdCard = pathLogo;
            sqlcon.open();
            sqlcon.addImageSdCard(mLogin);
        }
    }
}
