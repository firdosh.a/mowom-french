package com.asrapps.mowom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.BuildConfig;
import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.NextStepDropDownAdapter;
import com.asrapps.mowom.adapter.Photo_Grid_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.interfaces.OnDeleteImageClick;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PhotoModel;
import com.asrapps.mowom.view.HorizontalListView;
import com.asrapps.mowom.view.InternalStorageContentProvider;
import com.asrapps.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.asrapps.mowom.constants.ConstantFunction.getstatus;
import static com.asrapps.mowom.fragments.AppTab_Profile_Home.copyStream;

public class Pending_Task_Photo_Activity extends BaseFragment implements
        OnClickListener {

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1888;
    private static final int SELECT_PICTURE = 1000;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Movom";
    static File mediaStorageDir;

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String taskId, issue;

    TextView pen_tsk_srt_id_header;

    HorizontalListView photoGrid;

    Photo_Grid_Adapter adapter;
    Resources res;
    // public Pending_Task_Photo_Activity CustomListView = null;
    private ArrayList<PhotoModel> CustomListViewValuesArr = new ArrayList<PhotoModel>();

    View view;

    ImageView camaraImage2, camaraImage1, closeImageCam1, closeImageCam2;
    RelativeLayout camaraImage1Layout, camaraImage2Layout;


    private Uri fileUri;

    boolean camOneSelected, camTwoSelected, add;
    // boolean cam1ImageFilled = false, cam2ImageFilled = false;
    static String saveId = "";

    Button addImageButton, doneButton;

    SQLController sqlcon;
    private ArrayList<Login> mLogin;
    static String timeStamp = "";
    ConnectionDetector internetConnection;
    String taskNotes;
    int count = 0;

    String image1DI, image2ID;
    String selectedLanguage;

    //String id="";
    ImageView imgOne, imgTwo, imgThree, imgFour,  imgOnesep,imgTwosep,imgThreesep;


    TextView addTextView;
    LinearLayout llEng, llPersian;

    LinearLayout llFormsSupport, llFormsDelivery;

    EditText problemReportedTextView, DiagnosisDoneTextView, ResolutionEd, NoteTextView, addNotesTextBox, DeilveryStatusTextView, NoteTextViewDelivery;
    public ArrayList<String> resolutionData = new ArrayList<String>();
    public ArrayList<String> DeilveryData = new ArrayList<String>();
    TextView NoteLabel, NoteLabelDelivery;

    PopupWindow reolutionPopup, deliveryPopup;

    boolean isCamera;

    public static File fileName = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = inflater.inflate(R.layout.pending_task_photo_layout_pr,
                        container, false);
            } else {
                view = inflater.inflate(R.layout.pending_task_photo_layout,
                        container, false);
            }
            /*
             * txtHeaderUserName = (TextView) view
			 * .findViewById(R.id.txtHeaderUserName);
			 */


            llFormsSupport = (LinearLayout) view.findViewById(R.id.llFormsSupport);
            problemReportedTextView = (EditText) view.findViewById(R.id.problemReportedTextView);
            DiagnosisDoneTextView = (EditText) view.findViewById(R.id.DiagnosisDoneTextView);
            ResolutionEd = (EditText) view.findViewById(R.id.ResolutionEd);
            NoteTextView = (EditText) view.findViewById(R.id.NoteTextView);


            ResolutionEd.setFocusable(false);
            ResolutionEd.setClickable(true);

            ResolutionEd.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showResolutionPopup();
                }
            });
            NoteLabel = (TextView) view.findViewById(R.id.NoteLabel);

            NoteLabel.setVisibility(View.GONE);
            NoteTextView.setVisibility(View.GONE);

            llFormsDelivery = (LinearLayout) view.findViewById(R.id.llFormsDelivery);
            if (ConstantFunction.getstatus(getActivity().getApplicationContext(), "department").equalsIgnoreCase("delivery")) {
                llFormsDelivery.setVisibility(View.VISIBLE);
            } else {
                llFormsDelivery.setVisibility(View.GONE);
            }


            resolutionData = new ArrayList<>();
            resolutionData.add(getResources().getString(R.string.Resolved));
            resolutionData.add(getResources().getString(R.string.Partsawaited));
            resolutionData.add(getResources().getString(R.string.NotDiagnoized));
            resolutionData.add(getResources().getString(R.string.Pending));
            resolutionData.add(getResources().getString(R.string.OutofWarranty));
            resolutionData.add(getResources().getString(R.string.Others));

            DeilveryData = new ArrayList<>();
            DeilveryData.add(getResources().getString(R.string.Delivered));
            DeilveryData.add(getResources().getString(R.string.Clientnotpresent));
            DeilveryData.add(getResources().getString(R.string.Rescheduled));
            DeilveryData.add(getResources().getString(R.string.Clientcancelled));
            DeilveryData.add(getResources().getString(R.string.Others));

            if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("support")) {
                llFormsSupport.setVisibility(View.VISIBLE);
            } else {
                llFormsSupport.setVisibility(View.GONE);
            }

            DeilveryStatusTextView = (EditText) view.findViewById(R.id.DeilveryStatusTextView);
            DeilveryStatusTextView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDeliveryPopup();
                }
            });
            NoteTextViewDelivery = (EditText) view.findViewById(R.id.NoteTextViewDelivery);
            NoteLabelDelivery = (TextView) view.findViewById(R.id.NoteLabelDelivery);

            NoteLabelDelivery.setVisibility(View.GONE);
            NoteTextViewDelivery.setVisibility(View.GONE);


            DeilveryStatusTextView.setFocusable(false);
            DeilveryStatusTextView.setClickable(true);


            llEng = (LinearLayout) view.findViewById(R.id.llEng);
            llPersian = (LinearLayout) view.findViewById(R.id.llPersian);

            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
//			headerUserImage.setOnClickListener(this);

            addTextView = (TextView) view.findViewById(R.id.addTextView);


            imgOne = (ImageView) view.findViewById(R.id.imgOne);
            imgTwo = (ImageView) view.findViewById(R.id.imgTwo);
            imgThree = (ImageView) view.findViewById(R.id.imgThree);
            imgFour = (ImageView) view.findViewById(R.id.imgFour);
            imgOnesep=(ImageView)view.findViewById(R.id.imgOnesep);
            imgTwosep=(ImageView)view.findViewById(R.id.imgTwosep);
            imgThreesep=(ImageView)view.findViewById(R.id.imgThreesep);


            String is_report= getstatus(getActivity(), "is_report");
            String is_review= getstatus(getActivity(), "is_review");
            String is_signature= getstatus(getActivity(), "is_signature");

            int cntScreen=0;
            if(!is_report.equals("null") && is_report.equals("1"))
            {
                cntScreen++;
            }
            if(!is_review.equals("null") && is_review.equals("1"))
            {
                cntScreen++;
            } if(!is_signature.equals("null") && is_signature.equals("1"))
            {
                cntScreen++;
            }

            showFlowCounter(cntScreen);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            addImageButton = (Button) view.findViewById(R.id.addImageButton);
            addImageButton.setOnClickListener(this);

            doneButton = (Button) view.findViewById(R.id.doneButton);
            doneButton.setOnClickListener(this);
            // addImageButton.setEnabled(false);

            pen_tsk_srt_id_header = (TextView) view
                    .findViewById(R.id.pen_tsk_srt_id_header);

            camaraImage1 = (ImageView) view.findViewById(R.id.camaraImage1);
            camaraImage1.setOnClickListener(this);

            camaraImage2 = (ImageView) view.findViewById(R.id.camaraImage2);
            camaraImage2.setOnClickListener(this);

            closeImageCam1 = (ImageView) view.findViewById(R.id.closeImageCam1);
            closeImageCam1.setOnClickListener(this);

            closeImageCam2 = (ImageView) view.findViewById(R.id.closeImageCam2);
            closeImageCam2.setOnClickListener(this);

            closeImageCam1.setVisibility(View.GONE);
            closeImageCam2.setVisibility(View.GONE);

            camaraImage1Layout = (RelativeLayout) view
                    .findViewById(R.id.camaraImage1Layout);
            camaraImage2Layout = (RelativeLayout) view
                    .findViewById(R.id.camaraImage2Layout);

            res = getResources();
            photoGrid = (HorizontalListView) view.findViewById(R.id.photoGrid);
            photoGrid.setVisibility(View.GONE);

            sqlcon = new SQLController(getActivity());

            addNotesTextBox = (EditText) view
                    .findViewById(R.id.addNotesTextBox);

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                addTextView.setGravity(Gravity.RIGHT);
                addNotesTextBox.setGravity(Gravity.RIGHT);

//				imgOne.setImageResource(R.drawable.pr_step1_unhover);
//				imgTwo.setImageResource(R.drawable.pr_step2_hover);
//				imgThree.setImageResource(R.drawable.pr_step3_unhover);
//				imgFour.setImageResource(R.drawable.pr_step4_unhover);

                llEng.setVisibility(View.GONE);
                llPersian.setVisibility(View.VISIBLE);

                RelativeLayout.LayoutParams params11 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params11.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                params11.addRule(RelativeLayout.CENTER_VERTICAL);
                params11.addRule(RelativeLayout.RIGHT_OF, R.id.addImageButton);
                addTextView.setLayoutParams(params11);

                RelativeLayout.LayoutParams params111 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params111.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                params111.addRule(RelativeLayout.CENTER_VERTICAL);
                addImageButton.setLayoutParams(params111);
                addImageButton.setGravity(Gravity.RIGHT);


            } else {
                llEng.setVisibility(View.VISIBLE);
                llPersian.setVisibility(View.GONE);
            }

            internetConnection = new ConnectionDetector(getActivity());
            if (internetConnection
                    .isConnectingToInternet("")) {

                TruckateTable();
                SetUserDetails();
                GetFromShared();
            } else {
                TruckateTable();
                GetFromShared();
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            }

        }

        return view;

    }

    private void showFlowCounter(int cntScreen) {

        imgOne.setVisibility(View.VISIBLE);
        imgTwo.setVisibility(View.VISIBLE);
        imgThree.setVisibility(View.VISIBLE);
        imgFour.setVisibility(View.VISIBLE);
        imgOnesep.setVisibility(View.VISIBLE);
        imgTwosep.setVisibility(View.VISIBLE);
        imgThreesep.setVisibility(View.VISIBLE);

        if(cntScreen==2)
        {
            imgFour.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);

            imgTwo.setImageResource(R.drawable.step_second_hover);
            imgThree.setImageResource(R.drawable.step_three_unhover);
            imgFour.setImageResource(R.drawable.step_four__unhover);

        }
        else if(cntScreen==1)
        {
            imgFour.setVisibility(View.GONE);
            imgThree.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);
            imgTwosep.setVisibility(View.GONE);
            imgTwo.setImageResource(R.drawable.step_second_hover);
            imgThree.setImageResource(R.drawable.step_three_unhover);


        }
        else if(cntScreen==0)
        {
            imgFour.setVisibility(View.GONE);
            imgThree.setVisibility(View.GONE);
            imgTwo.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);
            imgTwosep.setVisibility(View.GONE);
            imgOnesep.setVisibility(View.GONE);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    private void showResolutionPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        reolutionPopup = new PopupWindow(view);

        int width = ResolutionEd.getWidth();
        if (width > 0) {
            reolutionPopup.setWidth(width);
        } else {
            reolutionPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        reolutionPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        reolutionPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        reolutionPopup.setOutsideTouchable(true);
        reolutionPopup.setTouchable(true);
        reolutionPopup.setFocusable(true);

        NextStepDropDownAdapter nextStepAdapter = new NextStepDropDownAdapter(getActivity(), resolutionData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(nextStepAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                problemReportedTextView.clearFocus();
                DiagnosisDoneTextView.clearFocus();
                ResolutionEd.clearFocus();
                NoteTextView.clearFocus();
                addNotesTextBox.clearFocus();
                DeilveryStatusTextView.clearFocus();
                NoteTextViewDelivery.clearFocus();


                ResolutionEd.setText(resolutionData.get(position).toString());

                if (position == resolutionData.size() - 1) {
                    NoteLabel.setVisibility(View.VISIBLE);
                    NoteTextView.setVisibility(View.VISIBLE);
                } else {
                    NoteLabel.setVisibility(View.GONE);
                    NoteTextView.setVisibility(View.GONE);
                }

                reolutionPopup.dismiss();
            }
        });

        reolutionPopup.showAsDropDown(ResolutionEd, 0, 2);
    }

    private void showDeliveryPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        deliveryPopup = new PopupWindow(view);

        int width = DeilveryStatusTextView.getWidth();
        if (width > 0) {
            deliveryPopup.setWidth(width);
        } else {
            deliveryPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        deliveryPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        deliveryPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        deliveryPopup.setOutsideTouchable(true);
        deliveryPopup.setTouchable(true);
        deliveryPopup.setFocusable(true);

        NextStepDropDownAdapter nextStepAdapter = new NextStepDropDownAdapter(getActivity(), DeilveryData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(nextStepAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                problemReportedTextView.clearFocus();
                DiagnosisDoneTextView.clearFocus();
                ResolutionEd.clearFocus();
                NoteTextView.clearFocus();
                addNotesTextBox.clearFocus();
                DeilveryStatusTextView.clearFocus();
                NoteTextViewDelivery.clearFocus();

                DeilveryStatusTextView.setText(DeilveryData.get(position).toString());

                if (position == DeilveryData.size() - 1) {
                    NoteLabelDelivery.setVisibility(View.VISIBLE);
                    NoteTextViewDelivery.setVisibility(View.VISIBLE);
                } else {
                    NoteLabelDelivery.setVisibility(View.GONE);
                    NoteTextViewDelivery.setVisibility(View.GONE);
                }

                deliveryPopup.dismiss();
            }
        });

        deliveryPopup.showAsDropDown(DeilveryStatusTextView, 0, 2);
    }

    private void SetListView() {

        CustomListViewValuesArr = new ArrayList<PhotoModel>();
        CustomListViewValuesArr = GetPhotoFromDb(taskId);
        // photoGrid.removeAll();
        // for (int i = 0; i <CustomListViewValuesArr.size(); i++) {
        // String path = CustomListViewValuesArr.get(i).getTaskId();
        // if (path.equals(taskId)) {
        // photoGrid.add(CustomListViewValuesArr.get(i),this);
        // }
        // }

        System.out.println(".................CustomListViewValuesArr.size()--> " + CustomListViewValuesArr.size());
        adapter = new Photo_Grid_Adapter(getActivity(),
                CustomListViewValuesArr, new OnDeleteImageClick() {

            @Override
            public void onClickDelete(View v, int position, PhotoModel photo) {
                DeletePhotoFromDb(photo.getPhotoId(), position);
            }

        });

        photoGrid.setAdapter(adapter);

    }

    private void GetFromShared() {

        taskId = ConstantFunction.getuser(getContext(), AppConstants.task_Id);
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + toPersianNumber(taskId) + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        } else {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + taskId + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        }

        saveId = "img_" + taskId;
    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }


    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onClick(View v) {

        sqlcon.open();
        count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);

        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.camaraImage1:
                camOneSelected = true;
                camTwoSelected = false;
                add = false;
                // captureImage(); ,
                // if(camaraImage1.getDrawable() == null)

                if (closeImageCam1.getVisibility() != View.VISIBLE) {

                    if (count < 5)
                        ShowSelectImageDialog(getActivity());
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.only_five_allowed),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                break;

            case R.id.camaraImage2:
                camOneSelected = false;
                camTwoSelected = true;
                add = false;
                // captureImage();
                // if(camaraImage2.getDrawable() == null)
                if (closeImageCam2.getVisibility() != View.VISIBLE) {

                    if (count < 5)
                        ShowSelectImageDialog(getActivity());
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.only_five_allowed),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                break;

            case R.id.addImageButton:
                camOneSelected = false;
                camTwoSelected = false;
                add = true;
                if (count < 5)
                    ShowSelectImageDialog(getActivity());
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.only_five_allowed),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;

            case R.id.doneButton:
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Done();
                break;

            case R.id.closeImageCam1:
                DeletePhoto(image1DI, true);
                break;

            case R.id.closeImageCam2:
                DeletePhoto(image2ID, false);
                break;

            default:
                break;
        }
    }

    private void Done() {

        taskNotes = addNotesTextBox.getText().toString().trim();

        if (count > 0) {
            if (taskNotes.length() != 0) {

                if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("support")) {
                    if (!problemReportedTextView.getText().toString().equals("")) {
                        UpdateTable(DbHelper.SAVED_PROBLEM_REPORTED, problemReportedTextView.getText().toString(), taskId);
                    } else {
                        Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_problem_reporting),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }

                    if (!DiagnosisDoneTextView.getText().toString().equals("")) {
                        UpdateTable(DbHelper.SAVED_DIAGNOSIS_DONE, DiagnosisDoneTextView.getText().toString(), taskId);
                    } else {
                        Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_diagnosis),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }

                    if (!ResolutionEd.getText().toString().equals("")) {
                        UpdateTable(DbHelper.SAVED_RESOLUTION, ResolutionEd.getText().toString(), taskId);
                    } else {
                        Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_resolution),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }

                    if (ResolutionEd.getText().toString().contains(getResources().getString(R.string.Others))) {
                        if (NoteTextView.getText().toString().length() > 0) {
                            UpdateTable(DbHelper.SAVED_NOTES_SUPPORT, NoteTextView.getText().toString(), taskId);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_notes),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }


                }

                if (ConstantFunction.getstatus(getActivity().getApplicationContext(), "department").equalsIgnoreCase("delivery")) {
                    if (!DeilveryStatusTextView.getText().toString().equals("")) {
                        UpdateTable(DbHelper.SAVED_DELIVERY_STATUS, DeilveryStatusTextView.getText().toString(), taskId);
                    } else {
                        Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_delivery_status),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return;
                    }

                    if (DeilveryStatusTextView.getText().toString().contains(getResources().getString(R.string.Others))) {
                        if (NoteTextViewDelivery.getText().toString().length() > 0) {
                            UpdateTable(DbHelper.SAVED_NOTES_DELIVERY, NoteTextViewDelivery.getText().toString(), taskId);
                        } else {
                            Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_notes),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            return;
                        }
                    }

                }

                UpdateTable(DbHelper.SAVED_TASK_REPORT, taskNotes, taskId);
                GetSavedItems(taskId);

                String is_review = getstatus(getActivity(), "is_review");
                String is_signature = getstatus(getActivity(), "is_signature");

               if(!is_review.equals("null") && is_review.equals("1"))
                {

                    Pending_Task_Write_Review_Fragment writeReviewFragment = new Pending_Task_Write_Review_Fragment();
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                            writeReviewFragment, true, true);


                } else if(!is_signature.equals("null") && is_signature.equals("1"))
                {
                    Pending_Task_Signature_Fragment signatureFragment = new Pending_Task_Signature_Fragment();
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                            signatureFragment, true, true);

                }else
               {
                   Pending_Task_Final_Fragment finalFragment = new Pending_Task_Final_Fragment();
                   mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                           finalFragment, true, true);
               }

            } else {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_task_report),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_add_photo), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    private void captureImage() {
//		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//		getActivity().startActivityForResult(intent,
//				CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
            } else {
                cameraIntent();
//                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                fileUri = Uri.fromFile(fileName);
//                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        } else {
//            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//            fileUri = Uri.fromFile(fileName);
//            i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//            i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            cameraIntent();
        }
    }
    private void cameraIntent() {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            fileName = new File(getActivity().getFilesDir(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(fileName);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

                fileUri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        fileName);

            } else {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(fileName);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    fileUri);
            intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getActivity().startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } catch (Exception e) {

            Log.d("CAMERA", "cannot take picture", e);
        }

    }
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                Log.v("TTT", "permission is dynamically granted");

                if (isCamera) {

                    cameraIntent();
//                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                    fileUri = Uri.fromFile(fileName);
//                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                    i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                    getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    getActivity().startActivityForResult(i,
                            SELECT_PICTURE);
                }

                break;

        }

    }

    private static File getOutputMediaFile(int type) {

        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + saveId + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getActivity();

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            getActivity();
            if (resultCode == Activity.RESULT_OK) {
                previewCapturedImage(data);
            } else {
                getActivity();
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        } else if (resultCode != Activity.RESULT_CANCELED && data != null) {
            if (requestCode == SELECT_PICTURE) {

                try {

                    Uri selectedImageUri = data.getData();
                    String[] projection = {MediaStore.MediaColumns.DATA};
                    Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                            null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    cursor.moveToFirst();

                    String selectedImagePath = cursor.getString(column_index);

                    Log.v("TTT", "selectedImagePath = " + selectedImagePath);

                    try {
                        fileName = new File(selectedImagePath);
                        ExifInterface exif = new ExifInterface(fileName.getPath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                        int angle = 0;

                        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                            angle = 90;
                        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                            angle = 180;
                        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                            angle = 270;
                        }

                        Matrix mat = new Matrix();
                        mat.postRotate(angle);

                        Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                        bmp = Bitmap.createScaledBitmap(bmp,bmp.getWidth()*50/100, bmp.getHeight()*50/100, false);
                        Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);


                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
//                        byte[] byteArray = stream.toByteArray();
                        long len=fileName.length();
                        FileOutputStream fOut;
                        try {
                            fOut = new FileOutputStream(fileName);
                            reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, fOut);
                            fOut.flush();
                            fOut.close();
                        } catch (Exception e) {}
                        long len1=fileName.length();
                        InserToPhoto(taskId,  timeStamp,fileName.getPath());

                        sqlcon.open();
                        count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);
                        System.out.println(".................count Galary--> " + count);
                        //String id="";

                        PhotoModel tempValuesArr = new PhotoModel();
                        tempValuesArr = GetLastPhotoFromDb();
                        //id=tempValuesArr.getPhotoId();

                        if (count <= 2) {
                            photoGrid.setVisibility(View.GONE);
                            camaraImage1Layout.setVisibility(View.VISIBLE);
                            camaraImage2Layout.setVisibility(View.VISIBLE);

                            //camOneSelected = false;
                            //camTwoSelected=true;


                            if (camOneSelected) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = tempValuesArr.getPhotoId();
                                //image2ID="";

                                try {
                                    int targetWidth = camaraImage1.getWidth();

                                    DisplayMetrics metrics = new DisplayMetrics();
                                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                    int height = metrics.heightPixels;
                                    targetWidth = metrics.widthPixels/2;

                                    double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                    int targetHeight = (int) (targetWidth * aspectRatio);
                                    if (targetHeight > 0 && targetWidth > 0) {
                                        Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                        if (result != reducedSizeBitmap) {
                                            // Same bitmap is returned if sizes are the same
                                            reducedSizeBitmap.recycle();
                                        }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                        camaraImage1.setImageBitmap(result);
                                    } else {
                                        camaraImage1.setImageBitmap(reducedSizeBitmap);
                                    }
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                    camaraImage1.setImageBitmap(reducedSizeBitmap);
                                }

                            }
                            if (camTwoSelected) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                //image1DI="";
                                image2ID = tempValuesArr.getPhotoId();


                                try {
                                    int targetWidth = camaraImage2.getWidth();

                                    DisplayMetrics metrics = new DisplayMetrics();
                                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                    int height = metrics.heightPixels;
                                    targetWidth = metrics.widthPixels/2;

                                    double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                    int targetHeight = (int) (targetWidth * aspectRatio);
                                    if (targetHeight > 0 && targetWidth > 0) {
                                        Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                        if (result != reducedSizeBitmap) {
                                            // Same bitmap is returned if sizes are the same
                                            reducedSizeBitmap.recycle();
                                        }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                        camaraImage2.setImageBitmap(result);
                                    } else {
                                        camaraImage2.setImageBitmap(reducedSizeBitmap);
                                    }
                                }
                                catch (Exception e)
                                {
                                    camaraImage2.setImageBitmap(reducedSizeBitmap);
                                    e.printStackTrace();
                                }

                            }

                            if (add) {
                                if (count == 1) {
                                    closeImageCam1.setVisibility(View.VISIBLE);
                                    image1DI = tempValuesArr.getPhotoId();


                                    try {
                                        int targetWidth = camaraImage1.getWidth();

                                        DisplayMetrics metrics = new DisplayMetrics();
                                        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                        int height = metrics.heightPixels;
                                        targetWidth = metrics.widthPixels/2;

                                        double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                        int targetHeight = (int) (targetWidth * aspectRatio);
                                        if (targetHeight > 0 && targetWidth > 0) {
                                            Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                            if (result != reducedSizeBitmap) {
                                                // Same bitmap is returned if sizes are the same
                                                reducedSizeBitmap.recycle();
                                            }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                            camaraImage1.setImageBitmap(result);
                                        } else {
                                            camaraImage1.setImageBitmap(reducedSizeBitmap);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                        camaraImage1.setImageBitmap(reducedSizeBitmap);
                                    }



                                } else if (count == 2) {
                                    closeImageCam2.setVisibility(View.VISIBLE);
                                    //image1DI="";
                                    image2ID = tempValuesArr.getPhotoId();

                                    try {
                                        int targetWidth = camaraImage2.getWidth();

                                        DisplayMetrics metrics = new DisplayMetrics();
                                        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                        int height = metrics.heightPixels;
                                        targetWidth = metrics.widthPixels/2;

                                        double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                        int targetHeight = (int) (targetWidth * aspectRatio);
                                        if (targetHeight > 0 && targetWidth > 0) {
                                            Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                            if (result != reducedSizeBitmap) {
                                                // Same bitmap is returned if sizes are the same
                                                reducedSizeBitmap.recycle();
                                            }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                            camaraImage2.setImageBitmap(result);
                                        } else {
                                            camaraImage2.setImageBitmap(reducedSizeBitmap);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        camaraImage2.setImageBitmap(reducedSizeBitmap);
                                        e.printStackTrace();
                                    }
                                }

                            }
                        } else {
                            photoGrid.setVisibility(View.VISIBLE);
                            camaraImage1Layout.setVisibility(View.GONE);
                            camaraImage2Layout.setVisibility(View.GONE);
                            SetListView();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.w("TAG", "-- Error in setting image");
                    } catch (OutOfMemoryError oom) {
                        oom.printStackTrace();
                        Log.w("TAG", "-- OOM Error in setting image");
                    }


                } catch (OutOfMemoryError e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                } catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Plaese Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                }

            }
        }

    }

    private void previewCapturedImage(Intent data) {
        try {
            if (fileUri != null) {
                InputStream inputStream;
                try {
                    inputStream = getActivity().getContentResolver().openInputStream(
                            data.getData());
                    if (!fileName.exists()) {
                        new File(AppConstants.tempImagePath).mkdirs();
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            fileName);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try {
                    Log.v("TTT", "imageToUploadUri = " + fileUri.toString());
//                    File fileName = new File(fileUri.toString().replace("file://", ""));
                    ExifInterface exif = new ExifInterface(fileName.getPath());
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    int angle = 0;

                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                        angle = 90;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                        angle = 180;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                        angle = 270;
                    }

                    Matrix mat = new Matrix();
                    mat.postRotate(angle);

                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                    bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth()/2, bmp.getHeight()/2, false);
                    Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

//                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                    reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
//                    byte[] byteArray = stream.toByteArray();

                    long len=fileName.length();
                    FileOutputStream fOut;
                    try {
                        fOut = new FileOutputStream(fileName);
                        reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, fOut);
                        fOut.flush();
                        fOut.close();
                    } catch (Exception e) {}
                    long len1=fileName.length();
                    InserToPhoto(taskId,  timeStamp,fileName.getPath());

                    sqlcon.open();
                    count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);


                    PhotoModel tempValuesArr = new PhotoModel();
                    tempValuesArr = GetLastPhotoFromDb();
                    System.out.println(".................count Cam--> " + count);

                    if (count <= 2) {
                        photoGrid.setVisibility(View.GONE);
                        camaraImage1Layout.setVisibility(View.VISIBLE);
                        camaraImage2Layout.setVisibility(View.VISIBLE);

                        //camOneSelected = false;
                        //camTwoSelected=true;

                        if (camOneSelected) {
                            closeImageCam1.setVisibility(View.VISIBLE);

                            image1DI = tempValuesArr.getPhotoId();
                            //image2ID="";

                            try {
                                int targetWidth = camaraImage1.getWidth();


                                DisplayMetrics metrics = new DisplayMetrics();
                                getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                int height = metrics.heightPixels;
                                targetWidth = metrics.widthPixels/2;


                                double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                int targetHeight = (int) (targetWidth * aspectRatio);
                                if (targetHeight > 0 && targetWidth > 0) {
                                    Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                    if (result != reducedSizeBitmap) {
                                        // Same bitmap is returned if sizes are the same
                                        reducedSizeBitmap.recycle();
                                    }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                    camaraImage1.setImageBitmap(result);
                                } else {
                                    camaraImage1.setImageBitmap(reducedSizeBitmap);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                camaraImage1.setImageBitmap(reducedSizeBitmap);
                            }

                        }
                        if (camTwoSelected) {
                            closeImageCam2.setVisibility(View.VISIBLE);
                            //image1DI="";
                            image2ID = tempValuesArr.getPhotoId();

                            try {
                                int targetWidth = camaraImage2.getWidth();


                                DisplayMetrics metrics = new DisplayMetrics();
                                getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                int height = metrics.heightPixels;
                                targetWidth = metrics.widthPixels/2;


                                double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                int targetHeight = (int) (targetWidth * aspectRatio);
                                if (targetHeight > 0 && targetWidth > 0) {
                                    Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                    if (result != reducedSizeBitmap) {
                                        // Same bitmap is returned if sizes are the same
                                        reducedSizeBitmap.recycle();
                                    }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                    camaraImage2.setImageBitmap(result);
                                } else {
                                    camaraImage2.setImageBitmap(reducedSizeBitmap);
                                }
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                camaraImage2.setImageBitmap(reducedSizeBitmap);
                            }

                        }
                        if (add) {

                            if (count == 1) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = tempValuesArr.getPhotoId();
                                //image2ID="";
                                try {
                                    int targetWidth = camaraImage1.getWidth();

                                    DisplayMetrics metrics = new DisplayMetrics();
                                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                    int height = metrics.heightPixels;
                                    targetWidth = metrics.widthPixels/2;



                                    double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                    int targetHeight = (int) (targetWidth * aspectRatio);
                                    if (targetHeight > 0 && targetWidth > 0) {
                                        Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                        if (result != reducedSizeBitmap) {
                                            // Same bitmap is returned if sizes are the same
                                            reducedSizeBitmap.recycle();
                                        }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                        camaraImage1.setImageBitmap(result);
                                    } else {
                                        camaraImage1.setImageBitmap(reducedSizeBitmap);
                                    }
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                    camaraImage1.setImageBitmap(reducedSizeBitmap);
                                }
                            }
                            if (count == 2) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                //image1DI="";
                                image2ID = tempValuesArr.getPhotoId();

                                try {
                                    int targetWidth = camaraImage2.getWidth();

                                    DisplayMetrics metrics = new DisplayMetrics();
                                    getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

                                    int height = metrics.heightPixels;
                                    targetWidth = metrics.widthPixels/2;


                                    double aspectRatio = (double) reducedSizeBitmap.getHeight() / (double) reducedSizeBitmap.getWidth();
                                    int targetHeight = (int) (targetWidth * aspectRatio);
                                    if (targetHeight > 0 && targetWidth > 0) {
                                        Bitmap result = Bitmap.createScaledBitmap(reducedSizeBitmap, targetWidth, targetHeight, false);
                                        if (result != reducedSizeBitmap) {
                                            // Same bitmap is returned if sizes are the same
                                            reducedSizeBitmap.recycle();
                                        }
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
                                        camaraImage2.setImageBitmap(result);
                                    } else {
                                        camaraImage2.setImageBitmap(reducedSizeBitmap);
                                    }
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                    camaraImage2.setImageBitmap(reducedSizeBitmap);
                                }
                            }

//				if(closeImageCam1.getVisibility() == View.VISIBLE){
//					closeImageCam1.setVisibility(View.VISIBLE);
//					image1DI=tempValuesArr.getPhotoId();
//					//image2ID="";
//				camaraImage1.setImageBitmap(bitmap);
//				}
//				if(closeImageCam2.getVisibility() == View.VISIBLE){
//					closeImageCam2.setVisibility(View.VISIBLE);
//					//image1DI="";
//					image2ID=tempValuesArr.getPhotoId();
//					camaraImage2.setImageBitmap(bitmap);
//				}
                        }
                    } else {
                        photoGrid.setVisibility(View.VISIBLE);
                        camaraImage1Layout.setVisibility(View.GONE);
                        camaraImage2Layout.setVisibility(View.GONE);
                        SetListView();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Log.w("TAG", "-- Error in setting image");
                } catch (OutOfMemoryError oom) {
                    oom.printStackTrace();
                    Log.w("TAG", "-- OOM Error in setting image");
                }


			
			/*
            if (count<= 2) {
				photoGrid.setVisibility(View.GONE);
				camaraImage1Layout.setVisibility(View.VISIBLE);
				camaraImage2Layout.setVisibility(View.VISIBLE);
				
				if(cam1Selected)
				camaraImage1.setImageBitmap(bitmap);
				else
					camaraImage2.setImageBitmap(bitmap);
			}
*/
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public void ShowSelectImageDialog(Context c) {

        final Context context = c;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_gallery);
        dialog.show();

        // dialog cancel button
        Button btnDialogCancel = (Button) dialog
                .findViewById(R.id.btnDialogCancel);
        btnDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // dialog Camera button
        Button btnCamera = (Button) dialog.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isCamera = true;

                captureImage();
                /*
				 * String dir = Environment
				 * .getExternalStoragePublicDirectory(Environment
				 * .DIRECTORY_PICTURES) + "/movom/"; File newdir = new
				 * File(dir); newdir.mkdirs(); String file = dir + "movom.jpg";
				 * File newfile = new File(file); try { newfile.createNewFile();
				 * } catch (IOException e) { }
				 * 
				 * Uri outputFileUri = Uri.fromFile(newfile); Intent intent =
				 * new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				 * intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
				 * getActivity().startActivityForResult(intent,
				 * CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
				 */
                dialog.dismiss();
            }
        });

        // dialog Gallery button
        Button btnGallery = (Button) dialog.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                isCamera=false;

//                Intent intent = new Intent(
//                        Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                getActivity().startActivityForResult(
//                        Intent.createChooser(intent, "Select File"), 2);
//

                isCamera = false;
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                    } else {

                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        i.setType("image/*");
                        getActivity().startActivityForResult(i,
                                SELECT_PICTURE);
                    }
                } else {

                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    getActivity().startActivityForResult(i,
                            SELECT_PICTURE);
                }

                dialog.dismiss();

            }
        });
    }

    private void DeletePhoto(String photoId, boolean selectedImage1) {
        sqlcon.open();
        sqlcon.DeleteOnePhoto(photoId);

        sqlcon.open();
        count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);

        if (selectedImage1) {
            closeImageCam1.setVisibility(View.GONE);

            camaraImage1
                    .setImageResource(R.drawable.inspection_icon_with_bg);
        } else {
            closeImageCam2.setVisibility(View.GONE);
            camaraImage2
                    .setImageResource(R.drawable.inspection_icon_with_bg);
        }

        if (closeImageCam1.getVisibility() != View.VISIBLE && closeImageCam2.getVisibility() != View.VISIBLE) {
            TruckateTable();
        }

    }

    public void TruckateTable() {
        sqlcon.open();
        sqlcon.DeleteAlltask(DbHelper.TABLE_PHOTO_TAKEN, ConstantFunction.getuser(getContext(), AppConstants.task_Id));
    }

    public void InserToPhoto(String taskId, String taken_time,String path) {
        sqlcon.open();
        sqlcon.InsertToPhoto(taskId, null, taken_time,path);
        sqlcon.open();
        sqlcon.GetAllPhotos(taskId);
    }

    public ArrayList<PhotoModel> GetPhotoFromDb(String taskId) {
        sqlcon.open();
        ArrayList<PhotoModel> tempArray = new ArrayList<PhotoModel>();
        tempArray = sqlcon.GetAllPhotos(taskId);
        return tempArray;

    }

    public PhotoModel GetLastPhotoFromDb() {
        sqlcon.open();
        PhotoModel tempArray = new PhotoModel();
        tempArray = sqlcon.GetLastPhotos();
        return tempArray;

    }

    public void DeletePhotoFromDb(String photoId, int position) {
        image1DI = "";
        image2ID = "";
        sqlcon.open();
        sqlcon.DeleteOnePhoto(photoId);
        CustomListViewValuesArr.remove(position);
        adapter.notifyDataSetChanged();
        sqlcon.open();
        count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);
        System.out.println("................................count==> " + count);
        if (count <= 2) {
            ArrayList<PhotoModel> tempPhotos = GetPhotoFromDb(taskId);
            if (tempPhotos.size() == 2) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

//                Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
//                                .getTakenPhoto(), 0,
//                        tempPhotos.get(0).getTakenPhoto().length);
                Bitmap bm= Utils.getBitmap(tempPhotos.get(0).getPhotoPath());
                camaraImage1.setImageBitmap(bm);


                Bitmap bm1= Utils.getBitmap(tempPhotos.get(1)
                        .getPhotoPath());

//                Bitmap bm1 = BitmapFactory.decodeByteArray(tempPhotos.get(1)
//                                .getTakenPhoto(), 0,
//                        tempPhotos.get(1).getTakenPhoto().length);
                camaraImage2.setImageBitmap(bm1);

                image1DI = tempPhotos.get(0).getPhotoId();
                image2ID = tempPhotos.get(1).getPhotoId();
            }

            if (tempPhotos.size() == 1) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

//                Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
//                                .getTakenPhoto(), 0,
//                        tempPhotos.get(0).getTakenPhoto().length);
                Bitmap bm= Utils.getBitmap(tempPhotos.get(0).getPhotoPath());

                camaraImage1.setImageBitmap(bm);
                image1DI = tempPhotos.get(0).getPhotoId();

                camaraImage2
                        .setImageResource(R.drawable.inspection_icon_with_bg);
            }

            if (tempPhotos.size() == 0) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                camaraImage1
                        .setImageResource(R.drawable.inspection_icon_with_bg);
                camaraImage2
                        .setImageResource(R.drawable.inspection_icon_with_bg);
                image1DI = "";
                image2ID = "";
            }


        } else {
            photoGrid.setVisibility(View.VISIBLE);
            camaraImage1Layout.setVisibility(View.GONE);
            camaraImage2Layout.setVisibility(View.GONE);
            SetListView();
        }

        if (closeImageCam1.getVisibility() != View.VISIBLE && closeImageCam2.getVisibility() != View.VISIBLE) {
            TruckateTable();
        }

    }

    private void UpdateTable(String fieldName, String updateValue, String taskId) {
        sqlcon.open();
        sqlcon.UpdateSavedTable(fieldName, updateValue, taskId);
    }

    private void GetSavedItems(String taskId) {
        sqlcon.open();
        sqlcon.GetSavedItems(taskId);
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }

        }
    }
}
