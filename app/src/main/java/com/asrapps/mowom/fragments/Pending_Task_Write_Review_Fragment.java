package com.asrapps.mowom.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.model.Login;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import static com.asrapps.mowom.constants.ConstantFunction.getstatus;

public class Pending_Task_Write_Review_Fragment extends BaseFragment implements
        OnClickListener {

    View view;
    //TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String et_id ,taskId, issue;

    TextView pen_tsk_srt_id_header;
    EditText consumerFeedBackTextView;
    Button submitButton;

    String feedBack;
    SQLController sqlcon;

    TextView givenRating;
    String strgivenRating = "0.0";
    ImageView ratingImage;

    ConnectionDetector internetConnection;
    private ArrayList<Login> mLogin;

    LinearLayout oneRatingLayout, twoRatingLayout, threeRatingLayout,
            fourRatingLayout, fiveRatingLayout, sixRatingLayout,
            sevenRatingLayout, eightRatingLayout, nineRatingLayout,
            tenRatingLayout;

    String selectedLanguage;

    ImageView imgOne, imgTwo, imgThree, imgFour,  imgOnesep,imgTwosep,imgThreesep;

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = inflater.inflate(R.layout.pending_task_write_review_layout_pr,
                        container, false);
            } else {
                view = inflater.inflate(R.layout.pending_task_write_review_layout,
                        container, false);
            }


//			llPersian=(LinearLayout)view.findViewById(R.id.llPersian);


            imgOne = (ImageView) view.findViewById(R.id.imgOne);
            imgTwo = (ImageView) view.findViewById(R.id.imgTwo);
            imgThree = (ImageView) view.findViewById(R.id.imgThree);
            imgFour = (ImageView) view.findViewById(R.id.imgFour);
            imgOnesep=(ImageView)view.findViewById(R.id.imgOnesep);
            imgTwosep=(ImageView)view.findViewById(R.id.imgTwosep);
            imgThreesep=(ImageView)view.findViewById(R.id.imgThreesep);

            String is_report= getstatus(getActivity(), "is_report");
            String is_review= getstatus(getActivity(), "is_review");
            String is_signature= getstatus(getActivity(), "is_signature");

            int cntScreen=0;
            if(!is_report.equals("null") && is_report.equals("1"))
            {
                cntScreen++;
            }
            if(!is_review.equals("null") && is_review.equals("1"))
            {
                cntScreen++;
            } if(!is_signature.equals("null") && is_signature.equals("1"))
            {
                cntScreen++;
            }

            showFlowCounter(cntScreen);

			/*txtHeaderUserName = (TextView) view
                    .findViewById(R.id.txtHeaderUserName);*/
            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
//			headerUserImage.setOnClickListener(this);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            pen_tsk_srt_id_header = (TextView) view
                    .findViewById(R.id.pen_tsk_srt_id_header);

            givenRating = (TextView) view.findViewById(R.id.givenRating);

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("0.0"));

            } else {
                givenRating.setText("0.0");
            }


            submitButton = (Button) view.findViewById(R.id.submitButton);
            submitButton.setOnClickListener(this);

            consumerFeedBackTextView = (EditText) view
                    .findViewById(R.id.consumerFeedBackTextView);
            sqlcon = new SQLController(getActivity());

            oneRatingLayout = (LinearLayout) view
                    .findViewById(R.id.oneRatingLayout);
            oneRatingLayout.setOnClickListener(this);

            twoRatingLayout = (LinearLayout) view
                    .findViewById(R.id.twoRatingLayout);
            twoRatingLayout.setOnClickListener(this);

            threeRatingLayout = (LinearLayout) view
                    .findViewById(R.id.threeRatingLayout);
            threeRatingLayout.setOnClickListener(this);

            fourRatingLayout = (LinearLayout) view
                    .findViewById(R.id.fourRatingLayout);
            fourRatingLayout.setOnClickListener(this);

            fiveRatingLayout = (LinearLayout) view
                    .findViewById(R.id.fiveRatingLayout);
            fiveRatingLayout.setOnClickListener(this);

            sixRatingLayout = (LinearLayout) view
                    .findViewById(R.id.sixRatingLayout);
            sixRatingLayout.setOnClickListener(this);

            sevenRatingLayout = (LinearLayout) view
                    .findViewById(R.id.sevenRatingLayout);
            sevenRatingLayout.setOnClickListener(this);

            eightRatingLayout = (LinearLayout) view
                    .findViewById(R.id.eightRatingLayout);
            eightRatingLayout.setOnClickListener(this);

            nineRatingLayout = (LinearLayout) view
                    .findViewById(R.id.nineRatingLayout);
            nineRatingLayout.setOnClickListener(this);

            tenRatingLayout = (LinearLayout) view
                    .findViewById(R.id.tenRatingLayout);
            tenRatingLayout.setOnClickListener(this);

            ratingImage = (ImageView) view.findViewById(R.id.ratingImage);
            ratingImage.setOnClickListener(this);

            internetConnection = new ConnectionDetector(getActivity());
            if (internetConnection
                    .isConnectingToInternet("")) {

                SetUserDetails();
                GetFromShared();
            } else {
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
                GetFromShared();
            }


//			if (selectedLanguage.equalsIgnoreCase("pr"))
//			{
//				((TextView)view.findViewById(R.id.textView)).setGravity(Gravity.RIGHT);
//				((TextView)view.findViewById(R.id.consumerFeedbackLabel)).setGravity(Gravity.RIGHT);
//
////				imgOne.setImageResource(R.drawable.pr_step1_unhover);
////				imgTwo.setImageResource(R.drawable.pr_step2_unhover);
////				imgThree.setImageResource(R.drawable.pr_step3_hover);
////				imgFour.setImageResource(R.drawable.pr_step4_unhover);
//
//				consumerFeedBackTextView.setGravity(Gravity.RIGHT);
//
//				llEng.setVisibility(View.GONE);
//				llPersian.setVisibility(View.VISIBLE);
//
//			}
//			else
//			{
//				llEng.setVisibility(View.VISIBLE);
//				llPersian.setVisibility(View.GONE);
//			}

        }
        return view;
    }

    private void showFlowCounter(int cntScreen) {

        imgOne.setVisibility(View.VISIBLE);
        imgTwo.setVisibility(View.VISIBLE);
        imgThree.setVisibility(View.VISIBLE);
        imgFour.setVisibility(View.VISIBLE);
        imgOnesep.setVisibility(View.VISIBLE);
        imgTwosep.setVisibility(View.VISIBLE);
        imgThreesep.setVisibility(View.VISIBLE);

        if(cntScreen==2)
        {
            imgFour.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);

            imgTwo.setImageResource(R.drawable.step_second_unhover);
            imgThree.setImageResource(R.drawable.step_three_hover);
            imgFour.setImageResource(R.drawable.step_four__unhover);

        }
        else if(cntScreen==1)
        {
            imgFour.setVisibility(View.GONE);
            imgThree.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);
            imgTwosep.setVisibility(View.GONE);
            imgTwo.setImageResource(R.drawable.step_second_hover);
            imgThree.setImageResource(R.drawable.step_three_unhover);


        }
    }


    private void GetFromShared() {

        taskId = ConstantFunction.getuser(getContext(), AppConstants.task_Id);
        et_id = ConstantFunction.getuser(getContext(), et_id);
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + toPersianNumber(taskId) + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        } else {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + taskId + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        }

    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {


            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.submitButton:
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Submit();
                break;

            case R.id.oneRatingLayout:
                SetRating(1);
                break;

            case R.id.twoRatingLayout:
                SetRating(2);
                break;

            case R.id.threeRatingLayout:
                SetRating(3);
                break;

            case R.id.fourRatingLayout:
                SetRating(4);
                break;

            case R.id.fiveRatingLayout:
                SetRating(5);
                break;

            case R.id.sixRatingLayout:
                SetRating(6);
                break;

            case R.id.sevenRatingLayout:
                SetRating(7);
                break;

            case R.id.eightRatingLayout:
                SetRating(8);
                break;

            case R.id.nineRatingLayout:
                SetRating(9);
                break;

            case R.id.tenRatingLayout:
                SetRating(10);
                break;

            case R.id.ratingImage:
                SetRating(0);
                break;

            default:
                break;
        }

    }

    private void Submit() {
        feedBack = consumerFeedBackTextView.getText().toString();
        if (!strgivenRating.equals("0.0")) {
            if (!feedBack.equals("")) {


                UpdateTable(DbHelper.SAVED_COMMENTS, feedBack, taskId);

                UpdateTable(DbHelper.SAVED_RATING, strgivenRating, taskId);

                GetSavedItems(taskId);

                String is_signature = getstatus(getActivity(), "is_signature");

               if(!is_signature.equals("null") && is_signature.equals("1"))
                {
                    Pending_Task_Signature_Fragment signatureFragment = new Pending_Task_Signature_Fragment();
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                            signatureFragment, true, true);

                }
                else
               {
                   Pending_Task_Final_Fragment finalFragment = new Pending_Task_Final_Fragment();
                   mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                           finalFragment, true, true);
               }

            } else {
                Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_feed_back),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(getActivity(), getString(R.string.please_enter_review_score),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void UpdateTable(String fieldName, String updateValue, String taskId) {
        sqlcon.open();
        sqlcon.UpdateSavedTable(fieldName, updateValue, taskId);
    }

    private void GetSavedItems(String taskId) {
        sqlcon.open();
        sqlcon.GetSavedItems(taskId);
    }

    private void SetRating(int selected) {
        if (selectedLanguage.equalsIgnoreCase("pr")) {
            givenRating.setText(toPersianNumber("0.0"));

        } else {
            givenRating.setText("0.0");
        }
        strgivenRating = "0.0";

        oneRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        twoRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        threeRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        fourRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        fiveRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        sixRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        sevenRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        eightRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        nineRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));
        tenRatingLayout.setBackgroundColor(getResources().getColor(
                R.color.white));

        if (selected == 10) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("5.0"));
            } else {
                givenRating.setText("5.0");
            }
            strgivenRating = "5.0";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fiveRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sixRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sevenRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
            eightRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
            nineRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_five));
            tenRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_five));
        }

        if (selected == 9) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("4.5"));
            } else {
                givenRating.setText("4.5");
            }
            strgivenRating = "4.5";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fiveRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sixRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sevenRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
            eightRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
            nineRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_five));
        }

        if (selected == 8) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("4.0"));
            } else {
                givenRating.setText("4.0");
            }
            strgivenRating = "4.0";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fiveRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sixRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sevenRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
            eightRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
        }

        if (selected == 7) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("3.5"));
            } else {
                givenRating.setText("3.5");
            }
            strgivenRating = "3.5";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fiveRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sixRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sevenRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_four));
        }

        if (selected == 6) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("3.0"));
            } else {
                givenRating.setText("3.0");
            }
            strgivenRating = "3.0";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fiveRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
            sixRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
        }

        if (selected == 5) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("2.5"));
            } else {
                givenRating.setText("2.5");
            }
            strgivenRating = "2.5";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fiveRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_three));
        }

        if (selected == 4) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("2.0"));
            } else {
                givenRating.setText("2.0");
            }
            strgivenRating = "2.0";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
            fourRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
        }

        if (selected == 3) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("1.5"));
            } else {
                givenRating.setText("1.5");
            }
            strgivenRating = "1.5";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            threeRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_two));
        }

        if (selected == 2) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("1.0"));
            } else {
                givenRating.setText("1.0");
            }
            strgivenRating = "1.0";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
            twoRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
        }

        if (selected == 1) {
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                givenRating.setText(toPersianNumber("0.5"));
            } else {
                givenRating.setText("0.5");
            }
            strgivenRating = "0.5";
            oneRatingLayout.setBackgroundColor(getResources().getColor(
                    R.color.rating_one));
        }
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out += persianNumbers[number];
            } else if (c == '٫') {
                out += '،';
            } else if (c == '.') {
                out += '.';
            } else {
                out += c;
            }

        }
        return out;
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);


                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }


}

