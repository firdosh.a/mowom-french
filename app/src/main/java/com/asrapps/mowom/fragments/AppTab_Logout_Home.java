package com.asrapps.mowom.fragments;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class AppTab_Logout_Home extends BaseFragment implements OnClickListener, AsyncResponseListener, DialogInterface.OnCancelListener {

    ImageView headerUserImage;
    Button logout_button;
    BroadcastReceiver receiver;

    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;
    private ProgressHUD mProgressHUD;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.logout_layout, container, false);


        internetConnection = new ConnectionDetector(getActivity());
        sqlcon = new SQLController(getActivity());


        // mGotoButton = (Button) view.findViewById(R.id.id_next_tab_a_button);
        // mGotoButton.setOnClickListener(listener);
        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
//		headerUserImage.setOnClickListener(this);

        logout_button = (Button) view.findViewById(R.id.logout_button);
        logout_button.setOnClickListener(this);
//        SetUserDetails();

        if (internetConnection.isConnectingToInternet("")) {
            SetUserDetails();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }
        //FinishTask();
        return view;
    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.logout_button:
//                stopService();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

                Log.v("TTT", "isStartRunning = " + preferences.getBoolean("isStartRunning", false));


                if (UpdaterService.inProcess) {

                    if (preferences.getBoolean("isStartRunning", false)) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_task_running) + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        ConstantFunction.savestatus(getActivity(),
                                "is_sales_task", "0");
                        ConstantFunction.savestatus(getActivity(),
                                "sales_task_id", "0");

                        if (internetConnection.isConnectingToInternet("")) {
                            userLogout();
                        } else {
                            ConstantFunction.Logout(getActivity());
                        }
                    }
                } else {
                    if (internetConnection.isConnectingToInternet("")) {
                        if (preferences.getBoolean("isStartRunning", false)) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.err_task_running) + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            userLogout();
                        }
                    } else {
//                    userLogout();
                        ConstantFunction.Logout(getActivity());
                    }
                }
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;
        }
    }

    private void userLogout() {


        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.LogOutURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Logout_Home :- " + ConstantValues.LogOutURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    private void stopService() {

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            }
        };

        if (UpdaterService.inProcess) {
            getActivity().stopService(new Intent(getActivity(),
                    UpdaterService.class));
            UpdaterService.inProcess = false;
            UpdaterService.firstTime = false;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("inProcess", false);
            editor.putBoolean("firstTime", false);
            editor.putString("currentTime", UpdaterService.currentTime + "");

            editor.commit();

            try {
                getActivity().unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    @Override
    public void processFinish(String requestURL, String response) {

        mProgressHUD.dismiss();
        Log.e("Response==>", response);

        if (!response.equalsIgnoreCase("")) {

            if (requestURL.equalsIgnoreCase(ConstantValues.LogOutURL)) {
                MowomLogFile.writeToLog("\n" + "AppTab_Logout_Home :- " + ConstantValues.LogOutURL + " -------------" + response);

                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    if (status.equalsIgnoreCase("success")) {

                        ConstantFunction.Logout(getActivity());

                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (status.equalsIgnoreCase("failed")) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mProgressHUD.dismiss();
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }

        }
    }

//	private void FinishTask() {
//
//		String taskStartTime = "2015-11-20 14:26:47", taskStopTime = "2015-11-20 15:46:13";
//
//		java.util.Date date1 = null;
//		java.util.Date date2 = null;
//
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
//
//		try {
//			date1 = df.parse(taskStartTime);
//			date2 = df.parse(taskStopTime);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		long diff = date2.getTime() - date1.getTime();
//
//		long timeInSeconds = diff / 1000;
//		long Hours, Mins;
//		Hours = timeInSeconds / 3600;
//		timeInSeconds = timeInSeconds - (Hours * 3600);
//		Mins = timeInSeconds / 60;
//
//		System.out.println("timeInSeconds==> " + timeInSeconds);
//		System.out.println("Hours==> " + Hours);
//		System.out.println("timeInSeconds==> " + timeInSeconds);
//		System.out.println("Mins==> " + Mins);
//
//		String takenTime = "";
//		if (Hours <= 0) {
//			if (Mins <= 1)
//				takenTime = String.valueOf(Mins) + " Min";
//			else
//				takenTime = String.valueOf(Mins) + " Mins";
//		} else {
//			if (Hours == 1)
//				takenTime = String.valueOf(Hours) + " Hour";
//			else
//				takenTime = String.valueOf(Hours) + " Hours";
//			if (Mins != 0) {
//				if (Mins <= 1)
//					takenTime += " " + String.valueOf(Mins) + " Min";
//				else
//					takenTime += " " + String.valueOf(Mins) + " Mins";
//			}
//		}
//		System.out.println("takenTime==> " + takenTime);
//
//	}
}
