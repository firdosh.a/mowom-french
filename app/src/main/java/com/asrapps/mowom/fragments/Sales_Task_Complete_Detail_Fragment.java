package com.asrapps.mowom.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.CurrentLocation;
import com.asrapps.mowom.constants.MyLeadingMarginSpan2;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.DirectionsJSONParser;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.CompletedTask;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PhotoModel;
import com.asrapps.mowom.view.HorizontalLayoutScrollListView;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;
import com.asrapps.utils.MowomLogFile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class Sales_Task_Complete_Detail_Fragment extends BaseFragment implements
        OnClickListener, AsyncResponseListener, OnCancelListener, OnMapReadyCallback {

    private GoogleMap addressGoogleMap;


    LinearLayout idLayout1, llLeadMargin;

    TextView completed_task_list_id, completed_task_list_issue,
            completed_task_list_date_time, completed_task_list_phone,
            completed_task_list_address,
            completed_task_detail_contactPersonName,
            completed_task_detail_start_endTime, totalTime,
            pending_task_detail_supervisorName, taskDetail, taskProduct, taskNote, taskNextStep, lable_id, label_comma, overlapping_issue_all;


    HorizontalLayoutScrollListView photoGrid;

    private ArrayList<Login> mLogin;
    ConnectionDetector internetConnection;
    SQLController sqlcon;

    private ProgressHUD mProgressHUD;
    CompletedTask completedTask;

    //TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String taskId, issue;

    LinearLayout contentLayout;

    ScrollView completed_task_scroll_view;

    CurrentLocation crntLocation;
    String selectedLanguage;
    View view;
    int id;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {

            internetConnection = new ConnectionDetector(getActivity());
            sqlcon = new SQLController(getActivity());

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = inflater.inflate(R.layout.sales_task_complete_detail_layout,
                        container, false);

            } else {
                view = inflater.inflate(R.layout.sales_task_complete_detail_layout_pr,
                        container, false);

            }

            crntLocation = new CurrentLocation(getActivity());
            crntLocation.GetCurrentLocation();
        /*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/
            headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
//		headerUserImage.setOnClickListener(this);

            overlapping_issue_all = (TextView) view.findViewById(R.id.overlapping_issue_all);

            llLeadMargin = (LinearLayout) view.findViewById(R.id.llLeadMargin);

            backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            taskDetail = (TextView) view
                    .findViewById(R.id.taskDetail);

            taskProduct = (TextView) view
                    .findViewById(R.id.taskProduct);
            taskNote = (TextView) view
                    .findViewById(R.id.taskNote);
            taskNextStep = (TextView) view
                    .findViewById(R.id.taskNextStep);

            lable_id = (TextView) view.findViewById(R.id.lable_id);

            completed_task_list_id = (TextView) view
                    .findViewById(R.id.completed_task_list_id);
            completed_task_list_issue = (TextView) view
                    .findViewById(R.id.completed_task_list_issue);
            completed_task_list_date_time = (TextView) view
                    .findViewById(R.id.completed_task_list_date_time);
            completed_task_list_phone = (TextView) view
                    .findViewById(R.id.completed_task_list_phone);
            completed_task_list_address = (TextView) view
                    .findViewById(R.id.completed_task_list_address);
            completed_task_detail_contactPersonName = (TextView) view
                    .findViewById(R.id.completed_task_detail_contactPersonName);
            completed_task_detail_start_endTime = (TextView) view
                    .findViewById(R.id.completed_task_detail_start_endTime);
            totalTime = (TextView) view.findViewById(R.id.totalTime);

            idLayout1 = (LinearLayout) view.findViewById(R.id.idLayout1);
            label_comma = (TextView) view
                    .findViewById(R.id.label_comma);


            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {


            }

            pending_task_detail_supervisorName = (TextView) view
                    .findViewById(R.id.pending_task_detail_supervisorName);


            photoGrid = (HorizontalLayoutScrollListView) view
                    .findViewById(R.id.photoGrid);

            contentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);
            contentLayout.setVisibility(View.GONE);

            addressGoogleMap = null;
            completed_task_scroll_view = (ScrollView) view
                    .findViewById(R.id.completed_task_scroll_view); // parent
            // scrollview in
            // xml, give
            // your
            // scrollview id
            // value
            ImageView transparentImageView = (ImageView) view
                    .findViewById(R.id.transparent_image);
            transparentImageView.setOnTouchListener(new View.OnTouchListener() {

                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            completed_task_scroll_view
                                    .requestDisallowInterceptTouchEvent(true);
                            // Disable touch on transparent view
                            return false;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            completed_task_scroll_view
                                    .requestDisallowInterceptTouchEvent(false);
                            return true;

                        case MotionEvent.ACTION_MOVE:
                            completed_task_scroll_view
                                    .requestDisallowInterceptTouchEvent(true);
                            return false;

                        default:
                            return true;
                    }
                }
            });

            SetUserDetails();
            GetFromShared();
            GetTaskDetails();

            if (internetConnection.isConnectingToInternet("")) {

            } else {
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            }


        }

        return view;

    }

    private void LoadMap() {
        try {
            int status = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity());

            if (status == ConnectionResult.SUCCESS) {
                initilizeMap();
                addressGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            } else {

                int requestCode = 10;
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,
                        getActivity(), requestCode);
                dialog.show();
            }
        } catch (Exception ex) {
//            Toast toast = Toast.makeText(getActivity(), ex.getMessage(),
//                    Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER, 0, 0);
//            toast.show();
        }
    }

    private void initilizeMap() {
        if (addressGoogleMap == null) {

            ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.completed_task_detail_AddressMap)).getMapAsync(this);

			/*
             * mMap = ((SupportMapFragment) MainActivity.fragmentManager
			 * .findFragmentById(R.id.location_map)).getMap();
			 */

//            if (addressGoogleMap == null) {
//                Toast toast = Toast.makeText(getActivity(),
//                        getString(R.string.unable_to_create_maps), Toast.LENGTH_SHORT);
//
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
        }
    }

    private void FromAddressAddMarker(double latitude, double longitude,
                                      int icon_id) {
        // addressGoogleMap.clear();
        MarkerOptions marker = new MarkerOptions().position(new LatLng(
                latitude, longitude));

        marker.icon(BitmapDescriptorFactory.fromResource(icon_id));

        if (addressGoogleMap != null) {
            addressGoogleMap.addMarker(marker);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(latitude, longitude)).zoom(5).build();

            addressGoogleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        }
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(
                    ConstantValues.latitude, ConstantValues.longitude));
            builder.include(new LatLng(
                    Double.parseDouble(completedTask.getLatitude()), Double.parseDouble(completedTask.getLongtitude())));


            LatLngBounds bounds = builder.build();

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 10);
            if (addressGoogleMap != null)
                addressGoogleMap.animateCamera(cu);
        } catch (Exception e) {
//            e.printStackTrace();
        }


//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        builder.include(new LatLng(
//                latitude, longitude));
//
//        LatLngBounds bounds = builder.build();
//
//        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        int width = displayMetrics.widthPixels;
//        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 10);
//        addressGoogleMap.animateCamera(cu);
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Sales_Task_Complete_Detail_Fragment :- " + ConstantValues.CompletedTaskDetailURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (status.equalsIgnoreCase("success")) {
                    try {
                        contentLayout.setVisibility(View.VISIBLE);
                        completedTask = new CompletedTask(
                                response);
                        SetValues(completedTask);

                        ConstantFunction.savestatus(getActivity(), AppConstants.task_Id, "0");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (status.equalsIgnoreCase("failed")) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());


                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }
        } else {

        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }
    }

    private void GetFromShared() {
        taskId = ConstantFunction.getuser(getContext(), AppConstants.task_Id);
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);
    }

    private void GetTaskDetails() {

        JSONObject requestObject = new JSONObject();
        try {

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("taskid", taskId);
            requestObject.put("empid", id);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in " + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.CompletedTaskDetailURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_Task_Complete_Detail_Fragment :- " + ConstantValues.CompletedTaskDetailURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }


    private void SetValues(CompletedTask completedTask) {
        String taskId = completedTask.getTask_id();
        String issue = completedTask.getTask_name();
        String dateTime = ConstantFunction.GetDateFormat(completedTask
                .getTask_endtime());
        String phoneNumber = completedTask.getContactnumber();
        String address = completedTask.getAddress();
        String contactPerson = getString(R.string.contact_name) + " : "
                + completedTask.getContact_name();
        String stTime = ConstantFunction.GetTimeFormat(completedTask
                .getTask_starttime());
        String edTime = ConstantFunction.GetTimeFormat(completedTask
                .getTask_endtime());


        String startEndTime = getString(R.string.str_start_time) + " " + stTime
                + " - " + getString(R.string.str_stop_time) + " " + edTime;

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {

            StringBuilder t1 = new StringBuilder();
            StringBuilder t2 = new StringBuilder();

            String startTime = completedTask
                    .getTask_starttime();
            String endTime = completedTask
                    .getTask_endtime();


            String StartTime = ConstantFunction.GetTimeFormat24(startTime);
            String StopTime = ConstantFunction.GetTimeFormat24(endTime);

            try {


                Log.v("TTT", "startTime = " + StartTime);
                Log.v("TTT", "endTime = " + StopTime);

                String st[] = StartTime.split(" ");
                String et[] = StopTime.split(" ");

                String stTime1[] = st[0].split(":");
                String etTime[] = et[0].split(":");

                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                if (selectedLanguage.equalsIgnoreCase("")) {
                    selectedLanguage = "en";
                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {

                    stTime1[0] = stTime1[0].replace(",", "").replace(" ", "");
                    String dtStart = stTime1[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SolarCalendar sc = new SolarCalendar(date);
                        String s = sc.date + "/" +
                                sc.month + "/" + sc.year;


                        Log.v("TTT", "ssssss = " + s);

                        t1.append(toPersianNumber(s));
                        t1.append(":");
                        t1.append(toPersianNumber(stTime1[1]));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t1.append(toPersianNumber(stTime1[0]));
                        t1.append(":");
                        t1.append(toPersianNumber(stTime1[1]));
                        e.printStackTrace();
                    }


                } else {

                    t1.append(stTime1[0] + "");
                    t1.append(":");
                    t1.append(stTime1[1] + "");

                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {

                    etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                    String dtStart = etTime[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SolarCalendar sc = new SolarCalendar(date);
                        String s = sc.date + "/" +
                                sc.month + "/" + sc.year;


                        Log.v("TTT", "ssssss = " + s);

                        t2.append(toPersianNumber(s));
                        t2.append(":");
                        t2.append(toPersianNumber(etTime[1]));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t2.append(toPersianNumber(etTime[0]));
                        t2.append(":");
                        t2.append(toPersianNumber(etTime[1]));
                        e.printStackTrace();
                    }


                } else {
                    t2.append(etTime[0] + "");
                    t2.append(":");
                    t2.append(etTime[1] + "");
                }


//                if(st[1].equalsIgnoreCase("am"))
//                {
//                    t1.append(" "+getResources().getString(R.string.am));
//                }
//                else
//                {
//                    t1.append(" "+getResources().getString(R.string.pm));
//                }
//
//                if(et[1].equalsIgnoreCase("am"))
//                {
//                    t2.append(" "+getResources().getString(R.string.am));
//                }
//                else
//                {
//                    t2.append(" "+getResources().getString(R.string.pm));
//                }

                StartTime = t1.toString();
                StopTime = t2.toString();

            } catch (Exception e) {
                e.printStackTrace();

                StartTime = t1.toString();
                StopTime = t2.toString();
            }

            String formatedString = "Start : %s - Stop : %s";

            startEndTime = getResources().getString(R.string.Start) + " " + StartTime + getResources().getString(R.string.stop) + " " + StopTime;
        } else {

            StringBuilder t1 = new StringBuilder();
            StringBuilder t2 = new StringBuilder();

            String startTime = completedTask
                    .getTask_starttime();
            String endTime = completedTask
                    .getTask_endtime();


            String StartTime = ConstantFunction.GetTimeFormat24(startTime);
            String StopTime = ConstantFunction.GetTimeFormat24(endTime);

//            try {
//
//
//                Log.v("TTT","startTime = "+StartTime);
//                Log.v("TTT","endTime = "+StopTime);
//
//                String st[] = StartTime.split(" ");
//                String et[] = StopTime.split(" ");
//
//                String stTime1[]=st[0].split(":");
//                String etTime[]=et[0].split(":");
//
//                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());
//
//                if (selectedLanguage.equalsIgnoreCase("")){
//                    selectedLanguage="en";
//                }
//
//
//                stTime1[0]=stTime1[0].replace(",","").replace(" ","");
//                String dtStart = stTime1[0];
//                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                try {
//                    Date date = format.parse(dtStart);
//                    System.out.println(date);
//
//                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                    String s = dateformat.format(date);
//                    System.out.println("Current Date Time : " + s);
//                    Log.v("TTT","ssssss = "+s);
//
//                    t1.append(s);
//                    t1.append(":");
//                    t1.append(stTime1[1]);
//
//                } catch (ParseException e) {
//                    // TODO Auto-generated catch block
//
//                    t1.append(stTime1[0]);
//                    t1.append(":");
//                    t1.append(stTime1[1]);
//                    e.printStackTrace();
//                }
//
//                if (!selectedLanguage.equalsIgnoreCase("pr")) {
//
//                    etTime[0]=etTime[0].replace(",","").replace(" ","");
//                    String dtStart1 = etTime[0];
//                    SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
//                    try {
//                        Date date = format1.parse(dtStart1);
//                        System.out.println(date);
//
//                        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                        String s = dateformat.format(date);
//                        System.out.println("Current Date Time : " + s);
//
//                        Log.v("TTT","ssssss = "+s);
//
//                        t2.append(s);
//                        t2.append(":");
//                        t2.append(etTime[1]);
//
//                    } catch (ParseException e) {
//                        // TODO Auto-generated catch block
//
//                        t2.append(etTime[0]);
//                        t2.append(":");
//                        t2.append(etTime[1]);
//                        e.printStackTrace();
//                    }
//
//
//                }
//
//
//
//                if(st[1].equalsIgnoreCase("am"))
//                {
//                    t1.append(" "+getResources().getString(R.string.am));
//                }
//                else
//                {
//                    t1.append(" "+getResources().getString(R.string.pm));
//                }
//
//                if(et[1].equalsIgnoreCase("am"))
//                {
//                    t2.append(" "+getResources().getString(R.string.am));
//                }
//                else
//                {
//                    t2.append(" "+getResources().getString(R.string.pm));
//                }
//
//                StartTime=t1.toString();
//                StopTime=t2.toString();
//
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//
//                StartTime=t1.toString();
//                StopTime=t2.toString();
//            }

            String formatedString = "Start : %s - Stop : %s";

            startEndTime = getResources().getString(R.string.Start) + " " + StartTime + getResources().getString(R.string.stop) + " " + StopTime;
        }


        String temptotalTime = TimeCalculation(
                completedTask.getTask_starttime(),
                completedTask.getTask_endtime());

        String strTotalTime = "";

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            strTotalTime = getString(R.string.total_time) + " "
                    + toPersianNumber(temptotalTime);
        } else {
            strTotalTime = getString(R.string.total_time) + " "
                    + temptotalTime;
        }

        String supervisorName = getString(R.string.supervisor_name) + " : "
                + completedTask.getSup_name();//
        String taskReport = completedTask.getReport();
        String consumerFeedBack = completedTask.getFeedback();
        String rating = completedTask.getRating();

        SetListImages(completedTask.getListOfImages());

        id = taskId.length();
        if (selectedLanguage.equalsIgnoreCase("pr")) {
            completed_task_list_id.setText(toPersianNumber(taskId));

            Drawable DICON = view.getResources().getDrawable(R.drawable.id_icon);
            int leftMargin = DICON.getIntrinsicWidth() + lable_id.getText().length() + id + 80;
//            Set the icon in R.id.icon
            SpannableString issueString = new SpannableString(issue.toUpperCase(Locale.getDefault()));
//            Expose the indent for the first rows
            issueString.setSpan(new MyLeadingMarginSpan2(1, 0), 0, issueString.length(), 0);

            completed_task_list_issue.setText(issueString);

            overlapping_issue_all.setText(getResources().getString(R.string.id) + " " + toPersianNumber(taskId) + " " + getResources().getString(R.string.comma) + " " + issue.toUpperCase(Locale.getDefault()));

        } else {
            completed_task_list_id.setText(taskId);

            SpannableString issueString = new SpannableString(issue.toUpperCase(Locale.getDefault()));
            completed_task_list_issue.setText(issueString);
            overlapping_issue_all.setText(getResources().getString(R.string.id) + " " + taskId + " " + getResources().getString(R.string.comma) + " " + issue.toUpperCase(Locale.getDefault()));

        }

//        completed_task_list_issue.setText(issue.toUpperCase(Locale.getDefault()));
        String task_list_date_time = dateTime;

        if (selectedLanguage.equalsIgnoreCase("pr")) {

            StringBuilder t = new StringBuilder();
            try {
                task_list_date_time = WebServiceHelper.convert12to24(task_list_date_time);

                String st[] = task_list_date_time.split(" ");

                String stTime1[] = st[1].split(":");
                String dtTime[] = st[0].split("/");

                String selectedLanguage;
                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                if (selectedLanguage.equalsIgnoreCase("")) {
                    selectedLanguage = "en";
                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {

                    st[0] = st[0].replace(",", "").replace(" ", "");
                    String dtStart = st[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SolarCalendar sc = new SolarCalendar(date);
                        String s = sc.date + "/" +
                                sc.month + "/" + sc.year;


                        Log.v("TTT", "ssssss = " + s);

                        t.append(toPersianNumber(s)).append(" ");
                        t.append(toPersianNumber(stTime1[0]));
                        t.append(":");
                        t.append(toPersianNumber(stTime1[1]));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t.append(toPersianNumber(st[0])).append(" ");
                        t.append(toPersianNumber(stTime1[0]));
                        t.append(":");
                        t.append(toPersianNumber(stTime1[1]));
                        e.printStackTrace();
                    }


                } else {
                    t.append(st[0]).append(" ");
                    t.append(stTime1[0]);
                    t.append(":");
                    t.append(stTime1[1]);

                }


//                try {
//                    if (st[2].equalsIgnoreCase("am")) {
//                        t.append(" " + getResources().getString(R.string.am));
//                    } else {
//                        t.append(" " + getResources().getString(R.string.pm));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


                task_list_date_time = t.toString();
            } catch (Exception e) {
                completed_task_list_date_time.setText(task_list_date_time);
                e.printStackTrace();
            }

            completed_task_list_date_time.setText(task_list_date_time);
        } else {

//            StringBuilder t = new StringBuilder();
//            try {
//
//                String st[] = task_list_date_time.split(" ");
//
//                String stTime1[] = st[1].split(":");
//                String dtTime[] = st[0].split("/");
//
//                String selectedLanguage;
//                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());
//
//                if (selectedLanguage.equalsIgnoreCase("")) {
//                    selectedLanguage = "en";
//                }
//
//                if (!selectedLanguage.equalsIgnoreCase("pr")) {
//
//                    st[0]=st[0].replace(",","").replace(" ","");
//                    String dtStart = st[0];
//                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                    try {
//                        Date date = format.parse(dtStart);
//                        System.out.println(date);
//
//                        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                        String s = dateformat.format(date);
//                        System.out.println("Current Date Time : " + s);
//                        Log.v("TTT","ssssss = "+s);
//
//                        t.append(s).append(" ");
//                        t.append(stTime1[0]);
//                        t.append(":");
//                        t.append(stTime1[1]);
//
//                    } catch (ParseException e) {
//                        // TODO Auto-generated catch block
//
//                        t.append(st[0]).append(" ");
//                        t.append(stTime1[0]);
//                        t.append(":");
//                        t.append(stTime1[1]);
//                        e.printStackTrace();
//                    }
//
//
//                }
//
//
//                try {
//                    if (st[2].equalsIgnoreCase("am")) {
//                        t.append(" " + getResources().getString(R.string.am));
//                    } else {
//                        t.append(" " + getResources().getString(R.string.pm));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//                task_list_date_time = t.toString();
//            } catch (Exception e) {
//                completed_task_list_date_time.setText(task_list_date_time);
//                e.printStackTrace();
//            }
            task_list_date_time = WebServiceHelper.convert12to24(task_list_date_time);

            completed_task_list_date_time.setText(task_list_date_time);
        }

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            completed_task_list_phone.setText(toPersianNumber(phoneNumber));
        } else {
            completed_task_list_phone.setText(phoneNumber);
        }

        taskDetail.setText(completedTask.getDetail());
        taskProduct.setText(completedTask.getProduct());
        taskNote.setText(completedTask.getAdditional_instruction());
        taskNextStep.setText(completedTask.getNext_steps());


        completed_task_list_address.setText(address);
        completed_task_detail_contactPersonName.setText(contactPerson);


        completed_task_detail_start_endTime.setText(startEndTime);
        totalTime.setText(strTotalTime);
        pending_task_detail_supervisorName.setText(supervisorName);


        LoadMap();

//        FromAddressAddMarker(ConstantValues.latitude, ConstantValues.longitude, R.drawable.from_map_icon);
//        FromAddressAddMarker(Double.parseDouble(completedTask.getLatitude()), Double.parseDouble(completedTask.getLongtitude()), R.drawable.to_map_icon);

        if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
            String url = getDirectionsUrl(new LatLng(ConstantValues.latitude, ConstantValues.longitude), new LatLng(Double.parseDouble(completedTask.getLatitude()), Double.parseDouble(completedTask.getLongtitude())));
            DownloadTask downloadTask = new DownloadTask();

            //Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }

//        try {
//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//            builder.include(new LatLng(
//                    ConstantValues.latitude, ConstantValues.longitude));
//            builder.include(new LatLng(
//                    Double.parseDouble(completedTask.getLatitude()), Double.parseDouble(completedTask.getLongtitude())));
//
//
//            LatLngBounds bounds = builder.build();
//
//            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//            int width = displayMetrics.widthPixels;
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 10);
//            addressGoogleMap.animateCamera(cu);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    // new method
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        addressGoogleMap = googleMap;
        if (addressGoogleMap == null) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.unable_to_create_maps), Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
            if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                FromAddressAddMarker(ConstantValues.latitude, ConstantValues.longitude, R.drawable.from_map_icon);
            }
            if (completedTask != null) {
                FromAddressAddMarker(Double.parseDouble(completedTask.getLatitude()), Double.parseDouble(completedTask.getLongtitude()), R.drawable.to_map_icon);
            }
        }
    }

    // New
    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("TTT", "Exception while downloading url " + e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            try {
                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;
                MarkerOptions markerOptions = new MarkerOptions();

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(2);
                    lineOptions.color(Color.RED);
                }

                // Drawing polyline in the Google Map for the i-th route
                if (addressGoogleMap != null)
                    addressGoogleMap.addPolyline(lineOptions);

            } catch (Exception e) {

            }
        }
    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }


    private void SetListImages(ArrayList<PhotoModel> images) {

        for (int i = 0; i < images.size(); i++) {
            photoGrid.addFromService(images.get(i), this);

        }
    }

    @SuppressLint("SimpleDateFormat")
    private String TimeCalculation(String strStartTime, String strEndTime) {

        strStartTime = ConstantFunction.GetTimeFormat(strStartTime);
        strEndTime = ConstantFunction.GetTimeFormat(strEndTime);
        java.util.Date date1 = null;
        java.util.Date date2 = null;

        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm a");

        try {
            date1 = df.parse(strStartTime);
            date2 = df.parse(strEndTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long diff = date2.getTime() - date1.getTime();

        long timeInSeconds = diff / 1000;
        long Hours, Mins;
        Hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (Hours * 3600);
        Mins = timeInSeconds / 60;

        String takenTime = "";
        if (Hours <= 0) {
            if (Mins <= 1)
                takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
            else
                takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
        } else {
            if (Hours == 1)
                takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hour);
            else
                takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hours);
            if (Mins != 0) {
                if (Mins <= 1)
                    takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
                else
                    takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
            }
        }
        return takenTime;
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);


                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }

}
