package com.asrapps.mowom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.BuildConfig;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.CategoryDropDownAdapter;
import com.asrapps.mowom.adapter.ClientDropDownAdapterSurvey;
import com.asrapps.mowom.adapter.Photo_Sales_Grid_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.CurrentLocation;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.interfaces.OnDeleteSalesImageClick;
import com.asrapps.mowom.model.CategoryData;
import com.asrapps.mowom.model.ClientData;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.asrapps.mowom.model.SurveyTask;
import com.asrapps.mowom.view.HorizontalListView;
import com.asrapps.mowom.view.InternalStorageContentProvider;
import com.asrapps.mowom.view.PlacesAutoCompleteAdapter;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.asrapps.mowom.fragments.AppTab_Profile_Home.copyStream;


public class Survey_page_Fragment extends BaseFragment implements
        OnClickListener, AsyncResponseListener, DialogInterface.OnCancelListener {

    private static final String DATEPICKER = "DatePickerDialog";

    TextView reportTxtViw;//txtHeaderUserName
    ImageView headerUserImage, backButtonImage;

    ConnectionDetector internetConnection;
    private ArrayList<Login> mLogin;

    private ProgressHUD mProgressHUD;
    Photo_Sales_Grid_Adapter adapter;
    public Survey_page_Fragment CustomListView = null;
    public ArrayList<PhotoSalesModel> CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();

    ListView report_list_view;
    int count = 0;

    ImageView fromDatePicker, toDatePicker;
    LinearLayout llClientData;
    boolean isFromDate, isToDate;

    TextView from_date_text_view, to_date_text_view, total_hours;
    RelativeLayout fromDateLayout, toDateLayout;

    LinearLayout llfrom, llTo;

    Button goButton;

    String selectedLanguage = "";

    LinearLayout llFromTo;

//	LinearLayout llEng,llPersian;

    String category_Id = "";

    AutoCompleteTextView clientNameEd;
    ClientDropDownAdapterSurvey clientDropDownAdapterSurvey;
    TextView selectCategoryEd;
    PopupWindow clientPopup, taskTypePopup, categoryPopup;
    EditText AdditionalInstructionEd, contact_no;
    public ArrayList<ClientData> clientData = new ArrayList<ClientData>();

    Button addImageButton;
    boolean camOneSelected, camTwoSelected, add;
    Resources res;
    ImageView camaraImage2, camaraImage1, closeImageCam1, closeImageCam2;
    RelativeLayout camaraImage1Layout, camaraImage2Layout;
    HorizontalListView photoGrid;
    String image1DI, image2ID;
    SQLController sqlcon;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1888;
    private static final int SELECT_PICTURE = 1000;

    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Movom";
    static File mediaStorageDir;
    private Uri fileUri;
    static String timeStamp = "";
    Button doneButton;
    static String saveId = "";
    String client_id = "";
    StringBuilder fullAddress;

    public ArrayList<String> taskTypeData = new ArrayList<String>();
    public ArrayList<CategoryData> categoryData = new ArrayList<CategoryData>();
    View view;

    CurrentLocation crntLocation;

    EditText TelPhone;

    //    BusinessCustomAutoCompleteView clientAddress;
    AutoCompleteTextView clientAddress;
    PlacesAutoCompleteAdapter placesAutoCompleteAdapter;

    String usertx;
    boolean isCamera;

    public static File fileName = null;

    TextWatcher txtwt;
    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String blockCharacterSet = "~#^|$%*!@/()-'\":;,?{}=!$^';,?×÷<>{}€£¥₩%~`¤♡♥_|《》¡¿°•○●□■◇◆♧♣▲▼▶◀↑↓←→☆★▪:-);-):-D:-(:'(:O1234567890";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    private static final int KEYBOARD_TIMEOUT = 100;
    private Handler mKeyboardHandler = new Handler();
    private Runnable mKeyboadRunnable = new Runnable() {
        @Override
        public void run() {
            boolean isOnlyOneRecord = false;
            int i;
            for (i = 0; i < clientData.size(); i++) {
                if (clientData.get(i).clientName.equals(clientNameEd.getText().toString() + "")) {
                    if (!isOnlyOneRecord) {
                        isOnlyOneRecord = true;
                    } else {
                        isOnlyOneRecord = false;
                        break;
                    }

                    break;
                } else {
                    client_id = "";
                }
            }

            if (isOnlyOneRecord) {
                if (clientData.get(i).address != null) {
                    clientAddress.setText(clientData.get(i).address);
                } else {
                    clientAddress.setText("");
                }

                if (clientData.get(i).contact_person != null) {
                    TelPhone.setText(clientData.get(i).contact_person.toString());
                } else {
                    TelPhone.setText("");
                }

                client_id = clientData.get(i).clientId;

                clientAddress.setEnabled(false);
                TelPhone.setEnabled(false);
            } else {
                clientAddress.setText("");
                TelPhone.setText("");

                clientAddress.setEnabled(true);
                TelPhone.setEnabled(true);

                client_id = "";

                if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                    if (fullAddress != null && fullAddress.length() > 0 && clientNameEd.getText().toString().length() > 0) {
                        clientAddress.setText(fullAddress + "");

                    } else {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());

                        try {
                            if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                                addresses = geocoder.getFromLocation(ConstantValues.latitude, ConstantValues.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();

                                fullAddress = new StringBuilder();
                                if (address.length() > 0) {
                                    fullAddress.append(address);
                                }


                                if (city.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(city);
                                }

                                if (state.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(state);
                                }

                                if (country.length() > 0) {

                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(country);
                                }

                                if (clientNameEd.getText().toString().length() > 0) {
                                    clientAddress.setText(fullAddress + "");
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                }
                client_id = "";

            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = inflater.inflate(R.layout.survey_layout_pr, container, false);
        } else {
            view = inflater.inflate(R.layout.survey_layout, container, false);
        }

        sqlcon = new SQLController(getActivity());

        usertx = ((long) Math.floor(Math.random() * 9000000000L) + 1000000000L) + "";

        crntLocation = new CurrentLocation(getActivity());
        crntLocation.GetCurrentLocation();
        internetConnection = new ConnectionDetector(getActivity());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

//                while (ConstantValues.latitude == 0.0 && ConstantValues.longitude == 0.0) {
                if (internetConnection
                        .isConnectingToInternet("")) {
                    if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                        Geocoder geocoder = null;
                        List<Address> addresses;
                        try {
                            geocoder = new Geocoder(getActivity(), Locale.getDefault());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                                addresses = geocoder.getFromLocation(ConstantValues.latitude, ConstantValues.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();

                                fullAddress = new StringBuilder();
                                if (address.length() > 0) {
                                    fullAddress.append(address);
                                }


                                if (city.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(city);
                                }

                                if (state.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(state);
                                }

                                if (country.length() > 0) {

                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(country);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                }
            }

//            }
        }, 2000);


        clientNameEd = (AutoCompleteTextView) view.findViewById(R.id.clientNameEd);

        clientNameEd.setFilters(new InputFilter[]{filter});


//        clientNameEd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.v("TTT","onItemClick-----------------"+clientNameEd.getText());
//
//                for(int i=0;i<clientData.size();i++)
//                {
//                    if(clientData.get(i).clientName.equals(clientNameEd.getText().toString()+""))
//                    {
//                        clientAddress.setText(clientData.get(position).address);
//                        contact_no.setText(clientData.get(position).contact_person.toString());
//                    }
//                }
//
//            }
//        });

        clientNameEd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        txtwt = new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                Log.i("REACHES AFTER", "YES");
                //clientNameEd.removeTextChangedListener(txtwt);//after this line you do the editing code


                //clientNameEd.addTextChangedListener(txtwt); // you register again for listener callbacks


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                Log.i("REACHES BEFORE", "YES");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Log.i("REACHES AFTER", "YES");
                mKeyboardHandler.removeCallbacks(mKeyboadRunnable);
                mKeyboardHandler.postDelayed(mKeyboadRunnable, KEYBOARD_TIMEOUT);

            }
        };
        clientNameEd.addTextChangedListener(txtwt);

//        clientNameEd.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                for (int i = 0; i < clientData.size(); i++) {
//                    if (clientData.get(i).clientName.equals(clientNameEd.getText().toString() + "")) {
//                        if (clientData.get(i).address != null) {
//                            clientAddress.setText(clientData.get(i).address);
//                        } else {
//                            clientAddress.setText("");
//                        }
//
//                        if (clientData.get(i).contact_person != null) {
//                            TelPhone.setText(clientData.get(i).contact_person.toString());
//                        } else {
//                            TelPhone.setText("");
//                        }
//
//                        client_id = clientData.get(i).clientId;
//
//                        clientAddress.setEnabled(false);
//                        TelPhone.setEnabled(false);
//
//
//                        break;
//                    } else {
//                        clientAddress.setText("");
//                        TelPhone.setText("");
//
//                        clientAddress.setEnabled(true);
//                        TelPhone.setEnabled(true);
//
//                        client_id = "";
//
//                        if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
//                            if(fullAddress!=null && fullAddress.length()>0 && clientNameEd.getText().toString().length()>0)
//                            {
//                                clientAddress.setText(fullAddress+"");
//
//                            }
//                            else {
//                                Geocoder geocoder;
//                                List<Address> addresses;
//                                geocoder = new Geocoder(getActivity(), Locale.getDefault());
//
//                                try {
//                                    addresses = geocoder.getFromLocation(ConstantValues.latitude, ConstantValues.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//                                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                                    String city = addresses.get(0).getLocality();
//                                    String state = addresses.get(0).getAdminArea();
//                                    String country = addresses.get(0).getCountryName();
//                                    String postalCode = addresses.get(0).getPostalCode();
//                                    String knownName = addresses.get(0).getFeatureName();
//
//                                    fullAddress = new StringBuilder();
//                                    if (address.length() > 0) {
//                                        fullAddress.append(address);
//                                    }
//
//
//                                    if (city.length() > 0) {
//                                        if (fullAddress.length() > 0) {
//
//                                            fullAddress.append(" , ");
//                                        }
//
//                                        fullAddress.append(city);
//                                    }
//
//                                    if (state.length() > 0) {
//                                        if (fullAddress.length() > 0) {
//
//                                            fullAddress.append(" , ");
//                                        }
//
//                                        fullAddress.append(state);
//                                    }
//
//                                    if (country.length() > 0) {
//
//                                        if (fullAddress.length() > 0) {
//
//                                            fullAddress.append(" , ");
//                                        }
//
//                                        fullAddress.append(country);
//                                    }
//
//                                    if(clientNameEd.getText().toString().length()>0) {
//                                        clientAddress.setText(fullAddress + "");
//                                    }
//
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//
//
//
//                        }
//
//
//
//                    }
//                }
//            }
//        });

        if (internetConnection
                .isConnectingToInternet("")) {
            //
            getclientData();

//            if (clientNameEd.getText().toString() != null) {
//                String name = clientNameEd.getText().toString();
//                for (int k = 0; k < clientData.size(); k++) {
//                    if (name.equals(clientData.get(k).clientName.toString())) {
//                        clientAddress.setText(clientData.get(k).address);
//                        contact_no.setText(clientData.get(k).contact_person.toString());
//                    }
//                }
//
//            }

        } else {

            sqlcon.open();
            clientData = sqlcon.getSurveryClientAll();

            sqlcon.open();
            categoryData = sqlcon.getCategoryAll();

            clientDropDownAdapterSurvey = new ClientDropDownAdapterSurvey(getActivity(), clientData);
            clientNameEd.setAdapter(clientDropDownAdapterSurvey);
            clientNameEd.setThreshold(0);
//            clientNameEd.showDropDown();

//            if (clientNameEd.getText().toString() != null) {
//                String name = clientNameEd.getText().toString();
//                for (int k = 0; k < clientData.size(); k++) {
//                    if (name.equals(clientData.get(k).clientName.toString())) {
//                        clientAddress.setText(clientData.get(k).address);
//                        contact_no.setText(clientData.get(k).contact_person.toString());
//                    }
//                }
//
//            }
        }

//        clientNameEd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Log.v("TTT","onItemSelected-----------------");
//                clientAddress.setText(clientData.get(position).address);
//                contact_no.setText(clientData.get(position).contact_person.toString());
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


        contact_no = (EditText) view.findViewById(R.id.telNo);
        clientAddress = (AutoCompleteTextView) view.findViewById(R.id.Address);

        placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.address_row_item);
        clientAddress.setAdapter(placesAutoCompleteAdapter);


        TelPhone = (EditText) view.findViewById(R.id.telNo);


        selectCategoryEd = (TextView) view.findViewById(R.id.selectCategoryEd);
        selectCategoryEd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                WebServiceHelper.hideKeyboard(getActivity(), v);

                showCategoryPopup();
            }
        });
        AdditionalInstructionEd = (EditText) view.findViewById(R.id.AdditionalInstructionEd);

		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/

//		llEng=(LinearLayout)view.findViewById(R.id.llEng);
//		llPersian=(LinearLayout)view.findViewById(R.id.llPersian);

        llClientData = (LinearLayout) view.findViewById(R.id.llClientData);


//        clientNameEd.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//
//                WebServiceHelper.hideKeyboard(getActivity(), v);
//                showClientPopup();
//            }
//        });


        headerUserImage = (ImageView) view
                .findViewById(R.id.headerUserImage);
//			headerUserImage.setOnClickListener(this);

        backButtonImage = (ImageView) view
                .findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

        addImageButton = (Button) view.findViewById(R.id.addImageButton);
        addImageButton.setOnClickListener(this);

        doneButton = (Button) view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(this);
        // addImageButton.setEnabled(false);


        camaraImage1 = (ImageView) view.findViewById(R.id.camaraImage1);
        camaraImage1.setOnClickListener(this);

        camaraImage2 = (ImageView) view.findViewById(R.id.camaraImage2);
        camaraImage2.setOnClickListener(this);

        closeImageCam1 = (ImageView) view.findViewById(R.id.closeImageCam1);
        closeImageCam1.setOnClickListener(this);

        closeImageCam2 = (ImageView) view.findViewById(R.id.closeImageCam2);
        closeImageCam2.setOnClickListener(this);

        closeImageCam1.setVisibility(View.GONE);
        closeImageCam2.setVisibility(View.GONE);

        camaraImage1Layout = (RelativeLayout) view
                .findViewById(R.id.camaraImage1Layout);
        camaraImage2Layout = (RelativeLayout) view
                .findViewById(R.id.camaraImage2Layout);

        res = getResources();
        photoGrid = (HorizontalListView) view.findViewById(R.id.photoGrid);
        photoGrid.setVisibility(View.GONE);


        internetConnection = new ConnectionDetector(getActivity());
        if (internetConnection
                .isConnectingToInternet("")) {

            saveId = "img_" + ConstantFunction.getstatus(getActivity(), "UserId");

            TruckateTable();
            SetUserDetails();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            TruckateTable();
        }

        return view;
    }


    private void showCategoryPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        categoryPopup = new PopupWindow(view);

        int width = selectCategoryEd.getWidth();
        if (width > 0) {
            categoryPopup.setWidth(width);
        } else {
            categoryPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        categoryPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        categoryPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        categoryPopup.setOutsideTouchable(true);
        categoryPopup.setTouchable(true);
        categoryPopup.setFocusable(true);

        CategoryDropDownAdapter nextStepAdapter = new CategoryDropDownAdapter(getActivity(), categoryData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(nextStepAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (categoryData.get(position).categoryName != null) {
                    selectCategoryEd.setText(categoryData.get(position).categoryName.toString());
                }
                category_Id = categoryData.get(position).categoryId;

                categoryPopup.dismiss();
            }
        });

        categoryPopup.showAsDropDown(selectCategoryEd, 0, 2);
    }

    private void getcategoryData() {


        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        JSONObject requestObject = new JSONObject();
        try {

            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }


        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ListOfCCategory);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Survey_page_Fragment :- " + ConstantValues.ListOfCCategory + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                mProgressHUD.dismiss();
                Log.e("Response==>", response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {

                                JSONArray job2 = job.getJSONArray("data");

                                categoryData = new ArrayList<>();
                                for (int i = 0; i < job2.length(); i++) {

                                    CategoryData obj = new CategoryData();
                                    obj.categoryId = ((JSONObject) job2.get(i)).get("id").toString();
                                    obj.categoryName = ((JSONObject) job2.get(i)).get("name").toString();

                                    categoryData.add(obj);

                                    sqlcon.open();
                                    sqlcon.addCategory(obj);

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }


                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                } else {

                }

            }
        };
        wsHelper.execute();
    }

    private void getclientData() {

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        JSONObject requestObject = new JSONObject();
        try {

            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }


        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ListOfSurveyClient);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Survey_page_Fragment :- " + ConstantValues.ListOfSurveyClient + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                mProgressHUD.dismiss();

                getcategoryData();

                Log.e("Response==>", response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {

                                JSONArray job2 = job.getJSONArray("data");

                                clientData = new ArrayList<>();
                                for (int i = 0; i < job2.length(); i++) {

                                    ClientData obj = new ClientData();
                                    obj.clientId = ((JSONObject) job2.get(i)).get("id").toString();
                                    obj.clientName = ((JSONObject) job2.get(i)).get("client_name").toString();
                                    if (((JSONObject) job2.get(i)).has("address"))
                                        obj.address = ((JSONObject) job2.get(i)).get("address").toString();
                                    obj.contact_person = ((JSONObject) job2.get(i)).get("phone_number").toString();

                                    clientData.add(obj);

                                    sqlcon.open();
                                    sqlcon.addSurveyClientTask(obj);
                                }

//                                ClientData obj = new ClientData();
//                                obj.clientId = "-1";
//                                obj.clientName = "Add New";
//
//                                sqlcon.open();
//                                sqlcon.addSurveyClientTask(obj);
//
//                                clientData.add(obj);

                                clientDropDownAdapterSurvey = new ClientDropDownAdapterSurvey(getActivity(), clientData);
                                clientNameEd.setAdapter(clientDropDownAdapterSurvey);
                                clientNameEd.setThreshold(0);
//                                clientNameEd.showDropDown();

                                String name = clientNameEd.getText().toString();

                                if (clientData != null && name != null) {
                                    for (int i = 0; i < clientData.size(); i++) {
                                        if (clientData.get(i).clientName != null) {
                                            if (name.equals(clientData.get(i).clientName)) {
                                                if (clientData.get(i).address != null) {
                                                    clientAddress.setText(clientData.get(i).address);
                                                } else {
                                                    clientAddress.setText("");
                                                }

                                                if (clientData.get(i).contact_person != null) {
                                                    TelPhone.setText(clientData.get(i).contact_person.toString());
                                                } else {
                                                    TelPhone.setText("");
                                                }

                                                clientAddress.setEnabled(false);
                                                TelPhone.setEnabled(false);

                                                break;

                                            } else {
                                                clientAddress.setEnabled(true);
                                                TelPhone.setEnabled(true);
                                            }
                                        }

                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {

                            clientData = new ArrayList<>();

                            ClientData obj = new ClientData();
                            obj.clientId = "-1";
                            obj.clientName = "Add New";

                            clientData.add(obj);

//                            Toast toast = Toast.makeText(getActivity(),
//                                    getString(R.string.please_try_again),
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            clientData = new ArrayList<>();

                            ClientData obj = new ClientData();
                            obj.clientId = "-1";
                            obj.clientName = "Add New";

                            clientData.add(obj);
                        }


                    } catch (Exception e) {
//                        Toast toast = Toast.makeText(getActivity(),
//                                getString(R.string.please_try_again),
//                                Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();

//                        clientData = new ArrayList<>();
//
//                        ClientData obj = new ClientData();
//                        obj.clientId = "-1";
//                        obj.clientName = "Add New";
//
//                        clientData.add(obj);

                    }
                } else {

                }

            }
        };
        wsHelper.execute();
    }


    private void showClientPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        clientPopup = new PopupWindow(view);

        int width = clientNameEd.getWidth();
        if (width > 0) {
            clientPopup.setWidth(width);
        } else {
            clientPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        clientPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        clientPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        clientPopup.setOutsideTouchable(true);
        clientPopup.setTouchable(true);
        clientPopup.setFocusable(true);

        ClientDropDownAdapterSurvey clientAdapter = new ClientDropDownAdapterSurvey(getActivity(), clientData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(clientAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (clientData.get(position).clientName != null) {
                    clientNameEd.setText(clientData.get(position).clientName);
                }

                if (clientData.get(position).address != null) {
                    clientAddress.setText(clientData.get(position).address.toString());
                } else {
                    clientAddress.setText("");
                }

                if (clientData.get(position).contact_person != null) {
                    TelPhone.setText(clientData.get(position).contact_person.toString());
                } else {
                    TelPhone.setText("");
                }
//
                clientAddress.setEnabled(false);
                TelPhone.setEnabled(false);

//                if (clientData.get(position).clientId.equals("-1")) {
//                    llClientData.setVisibility(View.VISIBLE);
//                } else {
//                    llClientData.setVisibility(View.VISIBLE);
//
                client_id = clientData.get(position).clientId;
//                }

                clientPopup.dismiss();
            }
        });

        clientPopup.showAsDropDown(clientNameEd, 0, 2);

    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */
        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void SetListView() {

        CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();
        CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));
        // photoGrid.removeAll();
        // for (int i = 0; i <CustomListViewValuesArr.size(); i++) {
        // String path = CustomListViewValuesArr.get(i).getTaskId();
        // if (path.equals(taskId)) {
        // photoGrid.add(CustomListViewValuesArr.get(i),this);
        // }
        // }

        System.out.println(".................CustomListViewValuesArr.size()--> " + CustomListViewValuesArr.size());
        adapter = new Photo_Sales_Grid_Adapter(getActivity(),
                CustomListViewValuesArr, new OnDeleteSalesImageClick() {

            public void onClickDelete(View v, int position,
                                      PhotoSalesModel photo) {
                // TODO Auto-generated method stub

                DeletePhotoFromDb(photo.getPhotoId(), position);

            }
        });

        photoGrid.setAdapter(adapter);

    }

    public void DeletePhotoFromDb(String photoId, int position) {
        image1DI = "";
        image2ID = "";
        sqlcon.open();
        sqlcon.DeleteOnePhotoSales(photoId);
        CustomListViewValuesArr.remove(position);
        adapter.notifyDataSetChanged();
        sqlcon.open();
        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);
        System.out.println("................................count==> " + count);
        if (count <= 2) {
            ArrayList<PhotoSalesModel> tempPhotos = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));
            if (tempPhotos.size() == 2) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
                                .getTakenPhoto(), 0,
                        tempPhotos.get(0).getTakenPhoto().length);
                camaraImage1.setImageBitmap(bm);
                Bitmap bm1 = BitmapFactory.decodeByteArray(tempPhotos.get(1)
                                .getTakenPhoto(), 0,
                        tempPhotos.get(1).getTakenPhoto().length);
                camaraImage2.setImageBitmap(bm1);

                image1DI = tempPhotos.get(0).getPhotoId();
                image2ID = tempPhotos.get(1).getPhotoId();
            }

            if (tempPhotos.size() == 1) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
                                .getTakenPhoto(), 0,
                        tempPhotos.get(0).getTakenPhoto().length);
                camaraImage1.setImageBitmap(bm);
                image1DI = tempPhotos.get(0).getPhotoId();

                camaraImage2
                        .setImageResource(R.drawable.inspection_icon_with_bg);
            }

            if (tempPhotos.size() == 0) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                camaraImage1
                        .setImageResource(R.drawable.inspection_icon_with_bg);
                camaraImage2
                        .setImageResource(R.drawable.inspection_icon_with_bg);
                image1DI = "";
                image2ID = "";
            }


        } else {
            photoGrid.setVisibility(View.VISIBLE);
            camaraImage1Layout.setVisibility(View.GONE);
            camaraImage2Layout.setVisibility(View.GONE);
            SetListView();
        }

        if (closeImageCam1.getVisibility() != View.VISIBLE && closeImageCam2.getVisibility() != View.VISIBLE) {
            TruckateTable();
        }

    }

    public void TruckateTable() {
        sqlcon.open();
        sqlcon.DeleteAllux(DbHelper.TABLE_PHOTO_TAKEN_SALES, usertx);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getActivity();

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            getActivity();
            if (resultCode == Activity.RESULT_OK) {
                previewCapturedImage(data);
            } else {
                getActivity();
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        } else if (resultCode != Activity.RESULT_CANCELED && data != null) {
            if (requestCode == SELECT_PICTURE) {
                try {

                    Uri selectedImageUri = data.getData();
                    String[] projection = {MediaStore.MediaColumns.DATA};
                    Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                            null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    cursor.moveToFirst();

                    String selectedImagePath = cursor.getString(column_index);

                    Log.v("TTT", "selectedImagePath = " + selectedImagePath);

                    try {
                        fileName = new File(selectedImagePath);
                        ExifInterface exif = new ExifInterface(fileName.getPath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                        int angle = 0;

                        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                            angle = 90;
                        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                            angle = 180;
                        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                            angle = 270;
                        }

                        Matrix mat = new Matrix();
                        mat.postRotate(angle);

                        Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                        bmp = Bitmap.createScaledBitmap(bmp,bmp.getWidth()/2, bmp.getHeight()/2, false);
                        Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);


                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
                        byte[] byteArray = stream.toByteArray();

                        InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp);

                        sqlcon.open();
                        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);
                        System.out.println(".................count Galary--> " + count);
                        //String id="";

                        PhotoSalesModel tempValuesArr = new PhotoSalesModel();
                        tempValuesArr = GetLastPhotoFromDb();
                        //id=tempValuesArr.getPhotoId();

                        if (count <= 2) {
                            photoGrid.setVisibility(View.GONE);
                            camaraImage1Layout.setVisibility(View.VISIBLE);
                            camaraImage2Layout.setVisibility(View.VISIBLE);

                            //camOneSelected = false;
                            //camTwoSelected=true;


                            if (camOneSelected) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = tempValuesArr.getPhotoId();
                                //image2ID="";
                                camaraImage1.setImageBitmap(reducedSizeBitmap);
                            }
                            if (camTwoSelected) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                //image1DI="";
                                image2ID = tempValuesArr.getPhotoId();
                                camaraImage2.setImageBitmap(reducedSizeBitmap);
                            }

                            if (add) {
                                if (count == 1) {
                                    closeImageCam1.setVisibility(View.VISIBLE);
                                    image1DI = tempValuesArr.getPhotoId();
                                    //image2ID="";
                                    camaraImage1.setImageBitmap(reducedSizeBitmap);
                                } else if (count == 2) {
                                    closeImageCam2.setVisibility(View.VISIBLE);
                                    //image1DI="";
                                    image2ID = tempValuesArr.getPhotoId();
                                    camaraImage2.setImageBitmap(reducedSizeBitmap);
                                }

                            }
                        } else {
                            photoGrid.setVisibility(View.VISIBLE);
                            camaraImage1Layout.setVisibility(View.GONE);
                            camaraImage2Layout.setVisibility(View.GONE);
                            SetListView();

                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.w("TAG", "-- Error in setting image");
                    } catch (OutOfMemoryError oom) {
                        oom.printStackTrace();
                        Log.w("TAG", "-- OOM Error in setting image");
                    }


                } catch (OutOfMemoryError e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                } catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Plaese Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                }
//                timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
//                        Locale.getDefault()).format(new Date());
//
//                Uri selectedImage = data.getData();
//                String[] filePath = {MediaStore.Images.Media.DATA};
//
//                Cursor c = getActivity().getContentResolver().query(
//                        selectedImage, filePath, null, null, null);
//                c.moveToFirst();
//                int columnIndex = c.getColumnIndex(filePath[0]);
//                String picturePath = c.getString(columnIndex);
//                c.close();
//
//                Bitmap scaleBitmap = (BitmapFactory.decodeFile(picturePath));
//                final Bitmap bitmap = Bitmap.createScaledBitmap(scaleBitmap,
//                        200, 200, false);
//                Log.d("path", picturePath + "");
//
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                byte[] byteArray = stream.toByteArray();
//
//                InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp);
//
//                sqlcon.open();
//                count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);
//                System.out.println(".................count Galary--> " + count);
//                //String id="";
//
//                PhotoSalesModel tempValuesArr = new PhotoSalesModel();
//                tempValuesArr = GetLastPhotoFromDb();
//                //id=tempValuesArr.getPhotoId();
//
//                if (count <= 2) {
//                    photoGrid.setVisibility(View.GONE);
//                    camaraImage1Layout.setVisibility(View.VISIBLE);
//                    camaraImage2Layout.setVisibility(View.VISIBLE);
//
//                    //camOneSelected = false;
//                    //camTwoSelected=true;
//
//
//                    if (camOneSelected) {
//                        closeImageCam1.setVisibility(View.VISIBLE);
//                        image1DI = tempValuesArr.getPhotoId();
//                        //image2ID="";
//                        camaraImage1.setImageBitmap(bitmap);
//                    }
//                    if (camTwoSelected) {
//                        closeImageCam2.setVisibility(View.VISIBLE);
//                        //image1DI="";
//                        image2ID = tempValuesArr.getPhotoId();
//                        camaraImage2.setImageBitmap(bitmap);
//                    }
//
//                    if (add) {
//                        if (count==1) {
//                            closeImageCam1.setVisibility(View.VISIBLE);
//                            image1DI = tempValuesArr.getPhotoId();
//                            //image2ID="";
//                            camaraImage1.setImageBitmap(bitmap);
//                        } else if (count==2) {
//                            closeImageCam2.setVisibility(View.VISIBLE);
//                            //image1DI="";
//                            image2ID = tempValuesArr.getPhotoId();
//                            camaraImage2.setImageBitmap(bitmap);
//                        }
//
////                        if (closeImageCam1.getVisibility() != View.VISIBLE) {
////                            closeImageCam1.setVisibility(View.VISIBLE);
////                            image1DI = tempValuesArr.getPhotoId();
////                            //image2ID="";
////                            camaraImage1.setImageBitmap(bitmap);
////                        } else if (closeImageCam2.getVisibility() != View.VISIBLE) {
////                            closeImageCam2.setVisibility(View.VISIBLE);
////                            //image1DI="";
////                            image2ID = tempValuesArr.getPhotoId();
////                            camaraImage2.setImageBitmap(bitmap);
////                        }
//                    }
//                } else {
//                    photoGrid.setVisibility(View.VISIBLE);
//                    camaraImage1Layout.setVisibility(View.GONE);
//                    camaraImage2Layout.setVisibility(View.GONE);
//                    SetListView();
//
//                }

				/*
                 * profImage = ConstantFunction.BitMapToString(ConstantFunction
				 * .Shrinkmethod(picturePath, ivProfilePic.getWidth(),
				 * ivProfilePic.getHeight())); mask image
				 * ivProfilePic.setImageBitmap(bitmap);
				 */
            }
        }

    }

    public PhotoSalesModel GetLastPhotoFromDb() {
        sqlcon.open();
        PhotoSalesModel tempArray = new PhotoSalesModel();
        tempArray = sqlcon.GetLastPhotosSales();
        return tempArray;

    }


    private void previewCapturedImage(Intent data) {
        try {
            if (fileUri != null) {
                InputStream inputStream;
                try {
                    inputStream = getActivity().getContentResolver().openInputStream(
                            data.getData());
                    if (!fileName.exists()) {
                        new File(AppConstants.tempImagePath).mkdirs();
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            fileName);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try {
                    Log.v("TTT", "imageToUploadUri = " + fileUri.toString());
//                    File fileName = new File(fileUri.toString().replace("file://", ""));
                    ExifInterface exif = new ExifInterface(fileName.getPath());
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    int angle = 0;

                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                        angle = 90;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                        angle = 180;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                        angle = 270;
                    }

                    Matrix mat = new Matrix();
                    mat.postRotate(angle);

                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                    bmp = Bitmap.createScaledBitmap(bmp,bmp.getWidth()/2, bmp.getHeight()/2, false);
                    Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
                    byte[] byteArray = stream.toByteArray();

                    InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp);

                    sqlcon.open();
                    count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);


                    PhotoSalesModel tempValuesArr = new PhotoSalesModel();
                    tempValuesArr = GetLastPhotoFromDb();
                    System.out.println(".................count Cam--> " + count);

                    System.out.println(".................count Cam--> " + count);

                    if (count <= 2) {
                        photoGrid.setVisibility(View.GONE);
                        camaraImage1Layout.setVisibility(View.VISIBLE);
                        camaraImage2Layout.setVisibility(View.VISIBLE);

                        //camOneSelected = false;
                        //camTwoSelected=true;

                        if (camOneSelected) {
                            closeImageCam1.setVisibility(View.VISIBLE);

                            image1DI = tempValuesArr.getPhotoId();
                            //image2ID="";
                            camaraImage1.setImageBitmap(reducedSizeBitmap);
                        }
                        if (camTwoSelected) {
                            closeImageCam2.setVisibility(View.VISIBLE);
                            //image1DI="";
                            image2ID = tempValuesArr.getPhotoId();
                            camaraImage2.setImageBitmap(reducedSizeBitmap);
                        }
                        if (add) {

                            if (count == 1) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = tempValuesArr.getPhotoId();
                                //image2ID="";
                                camaraImage1.setImageBitmap(reducedSizeBitmap);
                            }
                            if (count == 2) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                //image1DI="";
                                image2ID = tempValuesArr.getPhotoId();
                                camaraImage2.setImageBitmap(reducedSizeBitmap);
                            }

//				if(closeImageCam1.getVisibility() == View.VISIBLE){
//					closeImageCam1.setVisibility(View.VISIBLE);
//					image1DI=tempValuesArr.getPhotoId();
//					//image2ID="";
//				camaraImage1.setImageBitmap(bitmap);
//				}
//				if(closeImageCam2.getVisibility() == View.VISIBLE){
//					closeImageCam2.setVisibility(View.VISIBLE);
//					//image1DI="";
//					image2ID=tempValuesArr.getPhotoId();
//					camaraImage2.setImageBitmap(bitmap);
//				}
                        }
                    } else {
                        photoGrid.setVisibility(View.VISIBLE);
                        camaraImage1Layout.setVisibility(View.GONE);
                        camaraImage2Layout.setVisibility(View.GONE);
                        SetListView();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Log.w("TAG", "-- Error in setting image");
                } catch (OutOfMemoryError oom) {
                    oom.printStackTrace();
                    Log.w("TAG", "-- OOM Error in setting image");
                }



			/*
            if (count<= 2) {
				photoGrid.setVisibility(View.GONE);
				camaraImage1Layout.setVisibility(View.VISIBLE);
				camaraImage2Layout.setVisibility(View.VISIBLE);

				if(cam1Selected)
				camaraImage1.setImageBitmap(bitmap);
				else
					camaraImage2.setImageBitmap(bitmap);
			}
*/
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

//        try {
//
//            // bimatp factory
//            BitmapFactory.Options options = new BitmapFactory.Options();
//
//            // downsizing image as it throws OutOfMemory Exception for larger
//            // images
//            options.inSampleSize = 8;
//
//            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
//                    options);
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//            byte[] byteArray = stream.toByteArray();
//
//            InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp);
//
//            sqlcon.open();
//            count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);
//
//
//            PhotoSalesModel tempValuesArr = new PhotoSalesModel();
//            tempValuesArr = GetLastPhotoFromDb();
//            System.out.println(".................count Cam--> " + count);
//
//            if (count <= 2) {
//                photoGrid.setVisibility(View.GONE);
//                camaraImage1Layout.setVisibility(View.VISIBLE);
//                camaraImage2Layout.setVisibility(View.VISIBLE);
//
//                //camOneSelected = false;
//                //camTwoSelected=true;
//
//                if (camOneSelected) {
//                    closeImageCam1.setVisibility(View.VISIBLE);
//
//                    image1DI = tempValuesArr.getPhotoId();
//                    //image2ID="";
//                    camaraImage1.setImageBitmap(bitmap);
//                }
//                if (camTwoSelected) {
//                    closeImageCam2.setVisibility(View.VISIBLE);
//                    //image1DI="";
//                    image2ID = tempValuesArr.getPhotoId();
//                    camaraImage2.setImageBitmap(bitmap);
//                }
//                if (add) {
//                    if (count==1) {
//                        closeImageCam1.setVisibility(View.VISIBLE);
//                        image1DI = tempValuesArr.getPhotoId();
//                        //image2ID="";
//                        camaraImage1.setImageBitmap(bitmap);
//                    }
//                    if (count==2) {
//                        closeImageCam2.setVisibility(View.VISIBLE);
//                        //image1DI="";
//                        image2ID = tempValuesArr.getPhotoId();
//                        camaraImage2.setImageBitmap(bitmap);
//                    }
//
////                    if (closeImageCam1.getVisibility() == View.VISIBLE) {
////                        closeImageCam1.setVisibility(View.VISIBLE);
////                        image1DI = tempValuesArr.getPhotoId();
////                        //image2ID="";
////                        camaraImage1.setImageBitmap(bitmap);
////                    }
////                    if (closeImageCam2.getVisibility() == View.VISIBLE) {
////                        closeImageCam2.setVisibility(View.VISIBLE);
////                        //image1DI="";
////                        image2ID = tempValuesArr.getPhotoId();
////                        camaraImage2.setImageBitmap(bitmap);
////                    }
//                }
//            } else {
//                photoGrid.setVisibility(View.VISIBLE);
//                camaraImage1Layout.setVisibility(View.GONE);
//                camaraImage2Layout.setVisibility(View.GONE);
//                SetListView();
//            }
//
//			/*
//            if (count<= 2) {
//				photoGrid.setVisibility(View.GONE);
//				camaraImage1Layout.setVisibility(View.VISIBLE);
//				camaraImage2Layout.setVisibility(View.VISIBLE);
//
//				if(cam1Selected)
//				camaraImage1.setImageBitmap(bitmap);
//				else
//					camaraImage2.setImageBitmap(bitmap);
//			}
//*/
//
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
    }

    public void InserToPhoto(String taskId, byte[] image, String taken_time) {
        sqlcon.open();
        sqlcon.InsertToPhotoSales(taskId, image, taken_time, usertx);
    }

    public ArrayList<PhotoSalesModel> GetPhotoFromDb(String taskId) {
        sqlcon.open();
        ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
        tempArray = sqlcon.GetAllPhotosSalesTx(taskId, usertx);
        return tempArray;

    }

    private void DeletePhoto(String photoId, boolean selectedImage1) {
        sqlcon.open();
        sqlcon.DeleteOnePhotoSales(photoId);

        sqlcon.open();
        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);

        if (selectedImage1) {
            closeImageCam1.setVisibility(View.GONE);

            camaraImage1
                    .setImageResource(R.drawable.inspection_icon_with_bg);
        } else {
            closeImageCam2.setVisibility(View.GONE);
            camaraImage2
                    .setImageResource(R.drawable.inspection_icon_with_bg);
        }

        if (closeImageCam1.getVisibility() != View.VISIBLE && closeImageCam2.getVisibility() != View.VISIBLE) {
            TruckateTable();
        }

    }


    @Override
    public void onClick(View v) {

        sqlcon.open();
        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), usertx);

        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.camaraImage1:
                camOneSelected = true;
                camTwoSelected = false;
                add = false;
                // captureImage(); ,
                // if(camaraImage1.getDrawable() == null)

                if (closeImageCam1.getVisibility() != View.VISIBLE) {

                    if (count < 5)
                        ShowSelectImageDialog(getActivity());
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.only_five_allowed),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                break;

            case R.id.camaraImage2:
                camOneSelected = false;
                camTwoSelected = true;
                add = false;
                // captureImage();
                // if(camaraImage2.getDrawable() == null)
                if (closeImageCam2.getVisibility() != View.VISIBLE) {

                    if (count < 5)
                        ShowSelectImageDialog(getActivity());
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.only_five_allowed),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                break;

            case R.id.addImageButton:
                camOneSelected = false;
                camTwoSelected = false;
                add = true;
                if (count < 5)
                    ShowSelectImageDialog(getActivity());
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.only_five_allowed),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;

            case R.id.doneButton:
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Done();
                break;

            case R.id.closeImageCam1:
                DeletePhoto(image1DI, true);
                break;

            case R.id.closeImageCam2:
                DeletePhoto(image2ID, false);
                break;

            default:
                break;
        }
    }

    private void Done() {

        for (int i = 0; i < clientData.size(); i++) {
            if (clientData.get(i).clientName.equals(clientNameEd.getText().toString() + "")) {

                client_id = clientData.get(i).clientId;


                break;
            } else {

                client_id = "";
            }
        }

        if (client_id.length() == 0) {

            if (clientNameEd.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_name),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (clientAddress.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_address),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (TelPhone.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_tele),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (TelPhone.getText().toString().length() < 10 || TelPhone.getText().toString().length() > 16) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_tele_valid),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            }


        }


        if (selectCategoryEd.getText().toString().length() <= 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.enter_category),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;

        } else if (AdditionalInstructionEd.getText().toString().length() <= 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_enter_notes),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;

        }

        CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));

        if (CustomListViewValuesArr == null || CustomListViewValuesArr.size() == 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_add_photo),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;
        }

        if (internetConnection
                .isConnectingToInternet("")) {

            sendSurveyData();
        } else {
            sqlcon.open();
            SurveyTask obj = new SurveyTask();

            obj.supervisor_id = ConstantFunction.getstatus(getActivity(), "supervisor_id") + "";

            if (client_id.length() == 0) {

                obj.SURVEY_client_name_new = "0";

                obj.SURVEY_client_name = clientNameEd.getText().toString() + "";
                obj.SURVEY_client_address = clientAddress.getText().toString() + "";
                obj.tel_number = TelPhone.getText().toString() + "";
                obj.latitude = ConstantValues.latitude + "";
                obj.longtitude = ConstantValues.longitude + "";
                obj.client_id = client_id + "";


            } else {
                obj.SURVEY_client_name_new = "1";
                obj.client_id = client_id + "";

            }

            obj.category_id = category_Id + "";

            obj.notes = AdditionalInstructionEd.getText().toString() + "";
            obj.emp_id = ConstantFunction.getstatus(getActivity(), "UserId");

            obj.company_id = ConstantFunction.getstatus(getActivity(), "companyid");

            obj.userTx = usertx;

            sqlcon.addSurveyTask(obj);

            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.suc_uploaded),
                    Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            mActivity.ClearAllPages();
            // mActivity.popFragments();
            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                    new AppTab_Dashboard_Home(), true, true);

        }
    }

    private void sendSurveyData() {

        JSONObject requestObject = new JSONObject();
        try {
            if (client_id.length() == 0) {
                requestObject.put("client_name", clientNameEd.getText().toString() + "");
                requestObject.put("client_address", clientAddress.getText().toString() + "");
                requestObject.put("phone_number", TelPhone.getText().toString() + "");
                requestObject.put("latitude", ConstantValues.latitude + "");
                requestObject.put("longitude", ConstantValues.longitude + "");
                requestObject.put("client_id", "0");

            } else {
                requestObject.put("client_id", client_id + "");

            }
            requestObject.put("category_id", category_Id + "");

            requestObject.put("supervisor_id", ConstantFunction.getstatus(getActivity(), "supervisor_id"));
            requestObject.put("emp_id", ConstantFunction.getstatus(getActivity(), "UserId"));
            requestObject.put("notes", AdditionalInstructionEd.getText().toString() + "");
            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));

            String encodedString = "";

            String userId = ConstantFunction.getstatus(getActivity(), "UserId");
            CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));

            for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
                String strImage = GetStringFromBytes(CustomListViewValuesArr
                        .get(i).getTakenPhoto());
                encodedString += new StringBuilder(String.valueOf(strImage))
                        .append(",").toString();
            }

            requestObject.put("imagecount", CustomListViewValuesArr.size() + "");
            requestObject.put("photo", encodedString);


        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.SurveyURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        Log.v("survey existing client ", "Response =" + requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Survey_page_Fragment :- " + ConstantValues.SurveyURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    public String GetStringFromBytes(byte[] image) {
        String byteSyting = "";
        Bitmap bm = null;
        bm = BitmapFactory.decodeByteArray(image, 0, image.length);
        byteSyting = ConstantFunction.BitMapToString(bm);
        return byteSyting;
    }

    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Survey_page_Fragment :- " + ConstantValues.SurveyURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

//                if (status.equalsIgnoreCase("success")) {
                if (result_code.equalsIgnoreCase("1")) {
                    try {

                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.survey_suc_uploaded),
                                Toast.LENGTH_SHORT);

                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        mActivity.ClearAllPages();
                        // mActivity.popFragments();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                new AppTab_Dashboard_Home(), true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("18")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_18));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (status.equalsIgnoreCase("failed")) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            } catch (Exception e) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }
        } else {

        }

    }


    public void ShowSelectImageDialog(Context c) {

        final Context context = c;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_gallery);
        dialog.show();

        // dialog cancel button
        Button btnDialogCancel = (Button) dialog
                .findViewById(R.id.btnDialogCancel);
        btnDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // dialog Camera button
        Button btnCamera = (Button) dialog.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isCamera = true;

                captureImage();
                /*
                 * String dir = Environment
				 * .getExternalStoragePublicDirectory(Environment
				 * .DIRECTORY_PICTURES) + "/movom/"; File newdir = new
				 * File(dir); newdir.mkdirs(); String file = dir + "movom.jpg";
				 * File newfile = new File(file); try { newfile.createNewFile();
				 * } catch (IOException e) { }
				 *
				 * Uri outputFileUri = Uri.fromFile(newfile); Intent intent =
				 * new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				 * intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
				 * getActivity().startActivityForResult(intent,
				 * CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
				 */
                dialog.dismiss();
            }
        });

        // dialog Gallery button
        Button btnGallery = (Button) dialog.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                isCamera=false;

//                Intent intent = new Intent(
//                        Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                getActivity().startActivityForResult(
//                        Intent.createChooser(intent, "Select File"), 2);
//

                isCamera = false;
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                    } else {

                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        i.setType("image/*");
                        getActivity().startActivityForResult(i,
                                SELECT_PICTURE);
                    }
                } else {

                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    getActivity().startActivityForResult(i,
                            SELECT_PICTURE);
                }

                dialog.dismiss();

            }
        });
    }

    private void captureImage() {
//		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//		getActivity().startActivityForResult(intent,
//				CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
            } else {
                cameraIntent();
//                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                fileUri = Uri.fromFile(fileName);
//                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        } else {
            cameraIntent();
//            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//            fileUri = Uri.fromFile(fileName);
//            i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//            i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        }
    }

    private void cameraIntent() {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            fileName = new File(getActivity().getFilesDir(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(fileName);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

                fileUri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        fileName);

            } else {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(fileName);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    fileUri);
            intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getActivity().startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } catch (Exception e) {

            Log.d("CAMERA", "cannot take picture", e);
        }

    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + saveId + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }


    //
//	private void GetReport(String url, String startDate, String endDate) {
//
//		JSONObject requestObject = new JSONObject();
//		try {
//			String id = ConstantFunction.getuser(getActivity(), "UserId");
//			requestObject.put("id", id);
//			requestObject.put("starttime", startDate);
//			requestObject.put("endtime", endDate);
//
//		} catch (JSONException e) {
//			Log.d("TTT","Exception while signing in"+ e.toString());
//			e.printStackTrace();
//		}
//
//		mProgressHUD = ProgressHUD.show(getActivity(),
//				getString(R.string.loading), true, false, this);
//
//		// Call web service
//		WebServiceHelper wsHelper = new WebServiceHelper(url);
//		wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
//				"application/json");
//		wsHelper.setRequestBody(requestObject.toString());
//		wsHelper.delegate = this;
//		wsHelper.execute();
//	}
//
//	@Override
//	public void onCancel(DialogInterface arg0) {
//		// TODO Auto-generated method stub
//		mProgressHUD.dismiss();
//	}
//
//	@Override
//	public void processFinish(String requestURL, String response) {
//		// TODO Auto-generated method stub
//
//		mProgressHUD.dismiss();
//
//
//		Log.e("Response==>", response);
//		if (!response.equalsIgnoreCase("")) {
//			try {
//				Report report = new Report(response);
//				if (report.isSuccess()) {
//
//
//				} else {
//					Toast toast = Toast.makeText(getActivity(),
//							getString(R.string.please_try_again),
//							Toast.LENGTH_SHORT);
//					toast.setGravity(Gravity.CENTER, 0, 0);
//					toast.show();
//				}
//			} catch (Exception e) {
//				Toast toast = Toast.makeText(getActivity(),
//						getString(R.string.please_try_again),
//						Toast.LENGTH_SHORT);
//				toast.setGravity(Gravity.CENTER, 0, 0);
//				toast.show();
//			}
//		} else {
//			Toast toast = Toast.makeText(getActivity(),
//					getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//		}
//
//	}
//
    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }
//
//
//	private static String[] engNumbers = new String[]{ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//	public static String toEngNumber(String text) {
//		if (text.length() == 0) {
//			return "";
//		}
//		StringBuilder out = new StringBuilder();
//		int length = text.length();
//		for (int i = 0; i < length; i++) {
//			String c = text.charAt(i)+"";
//
//			if (c.equals("۰")) {
//				int number = 0;
//				out.append(engNumbers[number]);
//			} else if (c.equals("۱")) {
//				int number = 1;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۲")) {
//				int number = 2;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۳")) {
//				int number = 3;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۴")) {
//				int number = 4;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۵")) {
//				int number = 5;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۶")) {
//				int number = 6;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۷")) {
//				int number = 7;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۸")) {
//				int number = 8;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۹")) {
//				int number = 9;
//				out.append(engNumbers[number]);
//			} else {
//				out.append(c);
//			}
//
//
//		}
//		return out.toString()+"";
//	}
//
//	private void ShowToast() {
//		Toast toast = Toast.makeText(getActivity(),
//				getString(R.string.please_check_date), Toast.LENGTH_SHORT);
//		toast.setGravity(Gravity.CENTER, 0, 0);
//		toast.show();
//	}
//
//	private void ShowPopUp() {
//
//		FragmentManager fragManager = getActivity().getSupportFragmentManager();
//		FragmentTransaction fragTransaction = fragManager.beginTransaction();
//		Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();
//
//		dialog.setCancelable(true);
//		dialog.show(fragTransaction, "Dialog");
//
//		// Show popup
//		// GetService(ConstantValues.RejectTaskURL);
//	}

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                Log.v("TTT", "permission is dynamically granted");

                if (isCamera) {
                    cameraIntent();
//                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                    fileUri = Uri.fromFile(fileName);
//                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                    i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                    getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    getActivity().startActivityForResult(i,
                            SELECT_PICTURE);
                }

                break;

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }


}
