package com.asrapps.mowom.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.adapter.Report_Page_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.Report;
import com.asrapps.utils.MowomLogFile;
import com.materialdatetimepicker.date.DatePickerDialogMaterial;
import com.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Report_page_Fragment extends BaseFragment implements
        AsyncResponseListener, OnCancelListener, OnClickListener {

    private static final String DATEPICKER = "DatePickerDialog";

    TextView reportTxtViw;//txtHeaderUserName
    ImageView headerUserImage, backButtonImage;
    RelativeLayout rlTotalTime;

    private ProgressHUD mProgressHUD;
    Report_Page_Adapter adapter;
    public Report_page_Fragment CustomListView = null;
    public ArrayList<Report> CustomListViewValuesArr = new ArrayList<Report>();

    ListView report_list_view;
    int count = 0;

    ImageView fromDatePicker, toDatePicker;
    boolean isFromDate, isToDate;

    TextView from_date_text_view, to_date_text_view, total_hours;
    RelativeLayout fromDateLayout, toDateLayout;

    LinearLayout llfrom, llTo;

    Button goButton;

    String selectedLanguage = "";

    LinearLayout llFromTo;

    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;


//	LinearLayout llEng,llPersian;

    View view;
    String dd = "", mm = "", yy = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = inflater.inflate(R.layout.report_layout_pr, container, false);
        } else {
            view = inflater.inflate(R.layout.report_layout, container, false);
        }


		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/

//		llEng=(LinearLayout)view.findViewById(R.id.llEng);
//		llPersian=(LinearLayout)view.findViewById(R.id.llPersian);

        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
        //		headerUserImage.setOnClickListener(this);

        llFromTo = (LinearLayout) view.findViewById(R.id.llFromTo);

        backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

        goButton = (Button) view.findViewById(R.id.goButton);
        goButton.setOnClickListener(this);

        fromDatePicker = (ImageView) view.findViewById(R.id.fromDatePicker);
        fromDatePicker.setOnClickListener(this);

        toDatePicker = (ImageView) view.findViewById(R.id.toDatePicker);
        toDatePicker.setOnClickListener(this);

        llfrom = (LinearLayout) view.findViewById(R.id.llfrom);
        llTo = (LinearLayout) view.findViewById(R.id.llTo);

        report_list_view = (ListView) view.findViewById(R.id.report_list_view);
        rlTotalTime = (RelativeLayout) view.findViewById(R.id.rlTotalTime);

        reportTxtViw = (TextView) view.findViewById(R.id.reportTxtViw);
        reportTxtViw.setVisibility(View.GONE);

        total_hours = (TextView) view.findViewById(R.id.total_hours);

        from_date_text_view = (TextView) view
                .findViewById(R.id.from_date_text_view);
        // from_date_text_view.setEnabled(false);
        from_date_text_view.setOnClickListener(this);

        to_date_text_view = (TextView) view
                .findViewById(R.id.to_date_text_view);
        // to_date_text_view.setEnabled(false);
        to_date_text_view.setOnClickListener(this);

        fromDateLayout = (RelativeLayout) view
                .findViewById(R.id.fromDateLayout);
        fromDateLayout.setOnClickListener(this);

        toDateLayout = (RelativeLayout) view.findViewById(R.id.toDateLayout);
        toDateLayout.setOnClickListener(this);

        sqlcon = new SQLController(getActivity());
        internetConnection = new ConnectionDetector(getActivity());
        if (internetConnection
                .isConnectingToInternet(getString(R.string.check_connection))) {

            reportTxtViw.setVisibility(View.GONE);
            report_list_view.setVisibility(View.VISIBLE);
            rlTotalTime.setVisibility(View.VISIBLE);

            SetUserDetails();
            // SetListView();
            GetReport(ConstantValues.ReportURL, "", "");

        } else {

            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            reportTxtViw.setVisibility(View.VISIBLE);
            report_list_view.setVisibility(View.GONE);
            rlTotalTime.setVisibility(View.GONE);
        }

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

//		if (selectedLanguage.equalsIgnoreCase("pr"))
//		{
//
//			llEng.setVisibility(View.GONE);
//			llPersian.setVisibility(View.VISIBLE);
//
//			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params.bottomMargin=3;
//			params.topMargin=3;
//			params.leftMargin=3;
//			params.rightMargin=3;
//
//			RelativeLayout.LayoutParams params111 =  new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params111.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params111.addRule(RelativeLayout.CENTER_VERTICAL);
//			goButton.setLayoutParams(params111);
//			goButton.setGravity(Gravity.RIGHT);
//
//			RelativeLayout.LayoutParams params11 =new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params11.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//			params11.addRule(RelativeLayout.CENTER_VERTICAL);
//			params11.addRule(RelativeLayout.RIGHT_OF,R.id.goButton);
//			llFromTo.setLayoutParams(params11);
//
//			params.addRule(RelativeLayout.RIGHT_OF, R.id.from_date_text_view);
//			llfrom.setLayoutParams(params); //causes layout update
//
//			RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//			params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//			params1.addRule(RelativeLayout.RIGHT_OF, R.id.to_date_text_view);
//			llTo.setLayoutParams(params1); //causes layout update
//
//			from_date_text_view.setGravity(Gravity.RIGHT);
//			to_date_text_view.setGravity(Gravity.RIGHT);
//
//
//
//
//		}
//		else
//		{
//			llEng.setVisibility(View.VISIBLE);
//			llPersian.setVisibility(View.GONE);
//
//		}


        return view;
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    // private void SetListView() {
    // CustomListViewValuesArr = new ArrayList<Report>();
    //
    // for (int i = 0; i < 3; i++) {
    // Report listItems = new Report();
    // listItems.setDateTime(getString(R.string.temp_date_and_time));
    // listItems.setJobId(getString(R.string.temp_task_id));
    // listItems.setClientName(getString(R.string.temp_client_name));
    // listItems.setTimeSpend(getString(R.string.temp_time_spend));
    // CustomListViewValuesArr.add(listItems);
    // listItems = new Report();
    // }
    // adapter = new Report_Page_Adapter(getActivity(),
    // CustomListViewValuesArr);
    // if (CustomListViewValuesArr.size() > 0)
    // report_list_view.setAdapter(adapter);
    //
    // }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {


            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.fromDatePicker: {

                isFromDate = true;
                isToDate = false;
                showDatePicker();
            }
            break;

            case R.id.toDatePicker: {

                isToDate = true;
                isFromDate = false;
                showDatePicker();
            }
            break;

            case R.id.fromDateLayout: {

                isFromDate = true;
                isToDate = false;

                try {

//				Calendar cal = Calendar.getInstance(TimeZone.getDefault());
//				DatePickerDialog datePicker = new DatePickerDialog(getActivity(),
//						ondate,
//						cal.get(Calendar.YEAR),
//						cal.get(Calendar.MONTH),
//						cal.get(Calendar.DAY_OF_MONTH));
//
//				datePicker.setCancelable(false);
//				datePicker.setTitle("Select the date");
//				datePicker.show();
                    showDatePicker();
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
            break;

            case R.id.to_date_text_view: {

                isToDate = true;
                isFromDate = false;
                showDatePicker();
            }
            break;

            case R.id.goButton:
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String strFrom = from_date_text_view.getText().toString();
                String strTo = to_date_text_view.getText().toString();
                if (!strFrom.equalsIgnoreCase("") && !strTo.equalsIgnoreCase(""))
                    CheckDate(strFrom, strTo);
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.date_can_not_be_empty),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;

            default:
                break;
        }
    }

    private void GetReport(String url, String startDate, String endDate) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("id", id);
            if (startDate.equals("") && endDate.equals("")) {
                requestObject.put("fromDate", startDate);
                requestObject.put("toDate", endDate);
            } else {
                requestObject.put("starttime", startDate);
                requestObject.put("endtime", endDate);
            }

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(url);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Report_page_Fragment :- " + ConstantValues.ReportURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();


        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Report_page_Fragment :- " + ConstantValues.ReportURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {

                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (result_code.equals("1")) {
                    Report report = new Report(response);
                    if (report.isSuccess()) {
                        adapter = new Report_Page_Adapter(getActivity(),
                                report.getReportList(), selectedLanguage);
                        report_list_view.setAdapter(null);
                        if (report.getReportList().size() > 0) {

                            reportTxtViw.setVisibility(View.GONE);
                            report_list_view.setVisibility(View.VISIBLE);
                            rlTotalTime.setVisibility(View.VISIBLE);

                            report_list_view.setAdapter(adapter);

                            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                            if (selectedLanguage.equalsIgnoreCase("")) {
                                selectedLanguage = "en";
                            }

                            if (selectedLanguage.equalsIgnoreCase("pr")) {
                                total_hours.setText(toPersianNumber(String.valueOf(report.getHours())) + " " + getString(R.string.hours) +
                                        " " + toPersianNumber(String.valueOf(report.getMinutes())) + " " + getString(R.string.mins));
                            } else {
                                total_hours.setText(String.valueOf(report.getHours()) + " " + getString(R.string.hours) +
                                        " " + String.valueOf(report.getMinutes()) + " " + getString(R.string.mins));
                            }
                        } else {
                            reportTxtViw.setVisibility(View.VISIBLE);
                            report_list_view.setVisibility(View.GONE);
                            rlTotalTime.setVisibility(View.GONE);
                        }
                    }
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }


    private static String[] engNumbers = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public static String toEngNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            String c = text.charAt(i) + "";

            if (c.equals("۰")) {
                int number = 0;
                out.append(engNumbers[number]);
            } else if (c.equals("۱")) {
                int number = 1;
                out.append(engNumbers[number]);
            } else if (c.equals("۲")) {
                int number = 2;
                out.append(engNumbers[number]);
            } else if (c.equals("۳")) {
                int number = 3;
                out.append(engNumbers[number]);
            } else if (c.equals("۴")) {
                int number = 4;
                out.append(engNumbers[number]);
            } else if (c.equals("۵")) {
                int number = 5;
                out.append(engNumbers[number]);
            } else if (c.equals("۶")) {
                int number = 6;
                out.append(engNumbers[number]);
            } else if (c.equals("۷")) {
                int number = 7;
                out.append(engNumbers[number]);
            } else if (c.equals("۸")) {
                int number = 8;
                out.append(engNumbers[number]);
            } else if (c.equals("۹")) {
                int number = 9;
                out.append(engNumbers[number]);
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

//    private void showDatePicker() {
//        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());
//
//        if (selectedLanguage.equalsIgnoreCase("")) {
//            selectedLanguage = "en";
//        }
//
//        if (selectedLanguage.equalsIgnoreCase("pr")) {
//
//            final Dialog dialog = new Dialog(getActivity());
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.persian_datepicker);
//            dialog.setCancelable(true);
//
//            DisplayMetrics metrics = getResources().getDisplayMetrics();
//            int width = metrics.widthPixels - 50;
//            dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//            final PersianDatePicker mDatePicker;
//
//            mDatePicker = (PersianDatePicker) dialog.findViewById(R.id.datePicker);
//
//            dd = mDatePicker.getDisplayPersianDate().getPersianDay() + "";
//            mm = mDatePicker.getDisplayPersianDate().getPersianMonth() + "";
//            yy = mDatePicker.getDisplayPersianDate().getPersianYear() + "";
//
//            Log.v("TTT", "mDatePicker.getDisplayPersianDate().getPersianMonth() = " + mDatePicker.getDisplayPersianDate().getPersianMonthName());
//            mDatePicker.setOnDateChangedListener(new PersianDatePicker.OnDateChangedListener() {
//                @Override
//                public void onDateChanged(int year, int monthOfYear, int dayOfMonth) {
//                    Log.v("TTT", "-----------" + year + " " + dayOfMonth + " " + monthOfYear);
//                    if (dayOfMonth > 9) {
//                        dd = dayOfMonth + "";
//                    } else {
//                        dd = "0" + dayOfMonth + "";
//                    }
//
//                    if (year > 9) {
//                        yy = year + "";
//                    } else {
//                        yy = "0" + year + "";
//                    }
//
//                    if (monthOfYear > 9) {
//                        mm = (monthOfYear) + "";
//                    } else {
//                        mm = "0" + (monthOfYear) + "";
//                    }
//                }
//            });
//
//            final LinearLayout btnCancel = (LinearLayout) dialog.findViewById(R.id.btnCancel);
//            btnCancel.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    dialog.dismiss();
//                    return false;
//                }
//            });
//
//            LinearLayout btnSet = (LinearLayout) dialog.findViewById(R.id.btnSet);
//            btnSet.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    StringBuilder sb = new StringBuilder();
//
//                    int yy1 = Integer.parseInt(yy);
//                    int mm1 = Integer.parseInt(mm);
//                    int dd1 = Integer.parseInt(dd);
//
//                    Log.v("TTT", "month name = " + PersianCalendarConstants.persianMonthNames[mm1]);
//
//                    sb.append(yy1 + "");
//                    sb.append("-");
//
//                    if (mm1 <= 9) {
//                        sb.append("0" + (mm1));
//                    } else {
//                        sb.append((mm1) + "");
//                    }
//
//                    sb.append("-");
//
//                    if (dd1 <= 9) {
//                        sb.append("0" + dd1);
//                    } else {
//                        sb.append(dd1 + "");
//                    }
//
//                    Log.v("TTT", "set = " + dd1 + " " + mm1 + " " + yy1);
//
//                    if (isFromDate) {
//                        from_date_text_view.setText(sb);
//
//
//
//                    }
//
//                    if (isToDate) {
//                        to_date_text_view.setText(sb);
//                    }
//
//                    dialog.dismiss();
//
//                    return false;
//                }
//
//            });
//
//            dialog.show();
//
//        } else {
//
//            final Dialog dialog = new Dialog(getActivity());
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.eng_datepicker);
//            dialog.setCancelable(true);
//
//            DisplayMetrics metrics = getResources().getDisplayMetrics();
//            int width = metrics.widthPixels - 50;
//            dialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//            final DatePicker mDatePicker;
//
//            final Calendar mCalendar;
//
//            Calendar cal = Calendar.getInstance();
//
//            int year = cal.get(Calendar.YEAR);
//            final int month = cal.get(Calendar.MONTH);
//            int day = cal.get(Calendar.DAY_OF_MONTH);
//
//
//            dd = day + "";
//            mm = (cal.get(Calendar.MONTH) + 1) + "";
//            yy = year + "";
//
//            mDatePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
//            mDatePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
//                @Override
//                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                    Log.v("TTT", "-----------" + year + " " + dayOfMonth + " " + monthOfYear);
//                    if (dayOfMonth > 9) {
//                        dd = dayOfMonth + "";
//                    } else {
//                        dd = "0" + dayOfMonth + "";
//                    }
//
//                    if (year > 9) {
//                        yy = year + "";
//                    } else {
//                        yy = "0" + year + "";
//                    }
//
//                    if (monthOfYear >= 9) {
//                        mm = (monthOfYear + 1) + "";
//                    } else {
//                        mm = "0" + (monthOfYear + 1) + "";
//                    }
//                }
//
//            });
//
//            final LinearLayout btnCancel = (LinearLayout) dialog.findViewById(R.id.btnCancel);
//            btnCancel.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    dialog.dismiss();
//                    return false;
//                }
//            });
//
//            LinearLayout btnSet = (LinearLayout) dialog.findViewById(R.id.btnSet);
//            btnSet.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    Calendar calCurrent = Calendar.getInstance();
//
//                    Calendar calCheck = Calendar.getInstance();
//                    if (yy.length() > 0) {
//                        calCheck.set(Calendar.YEAR, Integer.parseInt(yy));
//                    }
//
//                    if (mm.length() > 0) {
//                        calCheck.set(Calendar.MONTH, Integer.parseInt(mm) - 1);
//                    }
//
//                    if (dd.length() > 0) {
//                        calCheck.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dd));
//                    }
//
//                    StringBuilder sb = new StringBuilder();
//
//                    int yy1 = calCheck.get(Calendar.YEAR);
//                    int mm1 = calCheck.get(Calendar.MONTH);
//                    int dd1 = calCheck.get(Calendar.DAY_OF_MONTH);
//
//                    sb.append(yy1 + "");
//                    sb.append("-");
//
//                    if (mm1 < 9) {
//                        sb.append("0" + (mm1 + 1));
//                    } else {
//                        sb.append((mm1 + 1) + "");
//                    }
//
//                    sb.append("-");
//
//                    if (dd1 <= 9) {
//                        sb.append("0" + dd1);
//                    } else {
//                        sb.append(dd1 + "");
//                    }
//
//                    Log.v("TTT", "set = " + dd1 + " " + mm1 + " " + yy1);
//
//                    if (isFromDate) {
//                        from_date_text_view.setText(sb);
//
//                    }
//
//                    if (isToDate) {
//                        to_date_text_view.setText(sb);
//                    }
//
//                    PersianCalendarConvert sc = new PersianCalendarConvert();
//                    String s= sc.day  + "/" +
//                            sc.month + "/" + sc.year;
//
//                    Log.v("TTT","ssss = "+s);
//
//                    dialog.dismiss();
//
//                    return false;
//                }
//
//            });
//
//            dialog.show();
//
//        }
//    }

    private void showDatePicker() {
        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            Calendar calender = Calendar.getInstance();
            Context context = getActivity();
            PersianCalendar now = new PersianCalendar();

//            now.setPersianDate(calender.get(Calendar.YEAR),calender.get(Calendar.MONTH)+1,calender.get(Calendar.DAY_OF_MONTH));

            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

                            if (isFromDate) {
                                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                                if (selectedLanguage.equalsIgnoreCase("")) {
                                    selectedLanguage = "en";
                                }

                                if (selectedLanguage.equalsIgnoreCase("pr")) {
                                    from_date_text_view.setText(toPersianNumber(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year)));

                                } else {
                                    from_date_text_view.setText(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year));
                                }
                            }

                            if (isToDate) {
                                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                                if (selectedLanguage.equalsIgnoreCase("")) {
                                    selectedLanguage = "en";
                                }

                                if (selectedLanguage.equalsIgnoreCase("pr")) {
                                    to_date_text_view.setText(toPersianNumber(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year)));

                                } else {
                                    to_date_text_view.setText(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year));
                                }
                            }

                        }
                    },
                    now.getPersianYear(),
                    now.getPersianMonth(),
                    now.getPersianDay()
            );
            PersianCalendar nowMin = new PersianCalendar();
            dpd.setYearRange(nowMin.getPersianYear() - 50, nowMin.getPersianYear() + 50);
            dpd.setThemeDark(false);

//			PersianCalendar nowMin = new PersianCalendar();
//			Calendar calenderMin = Calendar.getInstance();
//			nowMin.setPersianDate(calender.get(Calendar.YEAR)-30,1,1);
//
//			PersianCalendar nowMax = new PersianCalendar();
//			nowMax.setPersianDate(calender.get(Calendar.YEAR)+30,1,1);
//
//			dpd.setMinDate(nowMin);
//			dpd.setMaxDate(nowMax);


            Log.v("TTT", "max year  = " + dpd.getMaxYear());
            Log.v("TTT", "min year  = " + dpd.getMinYear());


            dpd.show(getFragmentManager(), DATEPICKER);

        } else {


            Calendar calender = Calendar.getInstance();
            DatePickerDialogMaterial dpd = DatePickerDialogMaterial.newInstance(
                    new DatePickerDialogMaterial.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePickerDialogMaterial view, int year, int monthOfYear, int dayOfMonth) {

                            monthOfYear = monthOfYear + 1;
                            if (isFromDate)

                            {
                                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                                if (selectedLanguage.equalsIgnoreCase("")) {
                                    selectedLanguage = "en";
                                }

                                if (selectedLanguage.equalsIgnoreCase("pr")) {
                                    from_date_text_view.setText(toPersianNumber(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year)));

                                } else {
                                    from_date_text_view.setText(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year));
                                }
                            }
                            if (isToDate) {
                                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                                if (selectedLanguage.equalsIgnoreCase("")) {
                                    selectedLanguage = "en";
                                }

                                if (selectedLanguage.equalsIgnoreCase("pr")) {
                                    to_date_text_view.setText(toPersianNumber(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year)));

                                } else {
                                    to_date_text_view.setText(String.valueOf(dayOfMonth) + "-"
                                            + String.valueOf(monthOfYear) + "-"
                                            + String.valueOf(year));
                                }
                            }

                        }
                    },
                    calender.get(Calendar.YEAR),
                    calender.get(Calendar.MONTH),
                    calender.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setThemeDark(false);
            dpd.show(getFragmentManager(), DATEPICKER);


//			DatePickerFragment date = new DatePickerFragment();
//			/**
//			 * Set Up Current Date Into dialog
//			 */
//			Calendar calender = Calendar.getInstance();
//			Bundle args = new Bundle();
//			args.putInt("year", calender.get(Calendar.YEAR));
//			args.putInt("month", calender.get(Calendar.MONTH));
//			args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
//			date.setArguments(args);
//			/**
//			 * Set Call back to capture selected date
//			 */
//			date.setCallBack(ondate);
//			date.show(getChildFragmentManager(), "Date Picker");
        }
    }

    OnDateSetListener ondate = new OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {

            monthOfYear = monthOfYear + 1;
            if (isFromDate)

            {
                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                if (selectedLanguage.equalsIgnoreCase("")) {
                    selectedLanguage = "en";
                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {
                    from_date_text_view.setText(toPersianNumber(String.valueOf(dayOfMonth) + "-"
                            + String.valueOf(monthOfYear) + "-"
                            + String.valueOf(year)));

                } else {
                    from_date_text_view.setText(String.valueOf(dayOfMonth) + "-"
                            + String.valueOf(monthOfYear) + "-"
                            + String.valueOf(year));
                }
            }
            if (isToDate) {
                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                if (selectedLanguage.equalsIgnoreCase("")) {
                    selectedLanguage = "en";
                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {
                    to_date_text_view.setText(toPersianNumber(String.valueOf(dayOfMonth) + "-"
                            + String.valueOf(monthOfYear) + "-"
                            + String.valueOf(year)));

                } else {
                    to_date_text_view.setText(String.valueOf(dayOfMonth) + "-"
                            + String.valueOf(monthOfYear) + "-"
                            + String.valueOf(year));
                }
            }

        }
    };

    private void CheckDate(String strFrom, String srtTo) {


        String fromDateStr;
        String toDateStr;
        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {

            fromDateStr = ConstantFunction.GetDateAndTime(toEngNumber(strFrom));
            toDateStr = ConstantFunction.GetDateAndTime(toEngNumber(srtTo));
        } else {
            fromDateStr = ConstantFunction.GetDateAndTime(strFrom);
            toDateStr = ConstantFunction.GetDateAndTime(srtTo);
        }


        Date fromDate = ConstantFunction
                .StringToDate("yyyy-MM-dd", fromDateStr);
        Date toDate = ConstantFunction.StringToDate("yyyy-MM-dd", toDateStr);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",
                Locale.getDefault());
        Date currentTime = ConstantFunction.StringToDate("yyyy-MM-dd",
                sdf.format(c.getTime()));

        if (!toDate.before(fromDate)) {
            if (!currentTime.before(fromDate) || currentTime.equals(fromDate)) {
                if (currentTime.after(toDate) || currentTime.equals(toDate)) {
                    GetReport(ConstantValues.ReportURL, ConstantFunction.DateToString("yyyy-MM-dd", fromDate), ConstantFunction.DateToString("yyyy-MM-dd", toDate));
                } else {
                    ShowToast();
                }

            } else {
                ShowToast();
            }
        } else {
            ShowToast();
        }

    }

    private void ShowToast() {
        Toast toast = Toast.makeText(getActivity(),
                getString(R.string.please_check_date), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }

        }
    }
}
