package com.asrapps.mowom.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.adapter.Completed_Task_List_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.customSwipeRefresh.SwipyRefreshLayout;
import com.asrapps.mowom.customSwipeRefresh.SwipyRefreshLayoutDirection;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.interfaces.OnDoneClick;
import com.asrapps.mowom.model.Alerts;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class Completed_Task_Fragment extends BaseFragment implements
        AsyncResponseListener, OnCancelListener, OnClickListener {

    TextView noCompletedTaskTxtViw;//txtHeaderUserName
    ImageView headerUserImage, backButtonImage;

    private ProgressHUD mProgressHUD;
    Completed_Task_List_Adapter adapter;
    PendingTask pendingTasks, pendingTasksall;
    public Pending_Task_List_Fragment CustomListView = null;
    public ArrayList<PendingTask> CustomListViewValuesArr = new ArrayList<PendingTask>();

    ListView completed_task_list_listView;
    int count = 0;

    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;

    SwipyRefreshLayout swipyrefreshlayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.completed_task_list_layout,
                container, false);

        internetConnection = new ConnectionDetector(getActivity());
        sqlcon = new SQLController(getActivity());

        pendingTasksall = new PendingTask();
        /*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {

                Log.v("TTT", "swipe refresh");

                if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
//					swipyrefreshlayout.setRefreshing(true);


//					if(pendingTasksall!=null && pendingTasksall.getPendingListTasks()!=null) {
//						count = pendingTasks.getPendingListTasks().size();
//					}

                    GetCompletedTasks();

                }

            }
        });


        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
//		headerUserImage.setOnClickListener(this);

        completed_task_list_listView = (ListView) view
                .findViewById(R.id.completed_task_list_listView);
        backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

        noCompletedTaskTxtViw = (TextView) view
                .findViewById(R.id.noCompletedTaskTxtViw);
        noCompletedTaskTxtViw.setVisibility(View.GONE);

        internetConnection = new ConnectionDetector(getActivity());
        if (internetConnection
                .isConnectingToInternet(getString(R.string.check_connection))) {

            SetUserDetails();
            count = 0;
            GetCompletedTasks();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            noCompletedTaskTxtViw.setVisibility(View.VISIBLE);
            completed_task_list_listView.setVisibility(View.GONE);
        }

        return view;
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetCompletedTasks() {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            // String id = "1";
            requestObject.put("id", id);
            requestObject.put("list_count", count);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in " + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.CompletedTaskURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Completed_Task_Fragment :- " + ConstantValues.CompletedTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.backButtonImage:
                mActivity.popFragments();
                break;


            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        try {
            swipyrefreshlayout.setRefreshing(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Completed_Task_Fragment :- " + ConstantValues.CompletedTaskURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                pendingTasks = new PendingTask(response, "", "");
                if (result_code.equals("1")) {
                    if (pendingTasks.isSuccess()) {
                        ArrayList<PendingTask> temp = new ArrayList<>();
                        if (count == 0) {
                            pendingTasksall = new PendingTask();
                            count = pendingTasks.getPendingListTasks().size();
                            if (pendingTasksall.getPendingListTasks() == null) {
                                pendingTasksall.setPendingListTasks(new ArrayList<PendingTask>());
                            }
                            pendingTasksall.setPendingListTasks(pendingTasks.getPendingListTasks());

                        } else {
//                        pendingTasksall.setPendingListTasks(
                            if (pendingTasksall.getPendingListTasks() == null) {
                                pendingTasksall.setPendingListTasks(new ArrayList<PendingTask>());
                            }
                            pendingTasksall.getPendingListTasks().addAll(pendingTasks.getPendingListTasks());
                            temp = pendingTasksall.getPendingListTasks();
                        }

//                    temp.addAll(pendingTasks.getPendingListTasks());
//
//                    pendingTasksall.setPendingListTasks(temp);
                        count = pendingTasksall.getPendingListTasks().size();

                        if (adapter == null) {
                            adapter = new Completed_Task_List_Adapter(getActivity(),
                                    pendingTasksall.getPendingListTasks(), new OnDoneClick() {

                                @Override
                                public void onClickMenu(View v, int position, PendingTask pendingTask) {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onClickMap(View v, int position, ArrayList<ClientAddress> client_address) {

                                }

                                @Override
                                public void onClickCall(View v, int position, String phoneNumber) {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onClickView(View v, int position,
                                                        PendingTask pendingTask) {
                                    // TODO Auto-generated method stub
                                    if (pendingTask != null &&
                                            pendingTask.getProduct() != null && pendingTask.getProduct().toString().length() > 0 &&
//                                            pendingTask.getAdditional_instruction() != null && pendingTask.getAdditional_instruction().toString().length() > 0 &&
                                            pendingTask.getNext_steps() != null && pendingTask.getNext_steps().toString().length() > 0 &&
                                            pendingTask.getDetail() != null && pendingTask.getDetail().toString().length() > 0) {
                                        ConstantFunction.savestatus(getContext(), AppConstants.task_Id, pendingTask.getTaskId());
                                        ConstantFunction.savestatus(getContext(), AppConstants.et_id, pendingTask.getEt_id());
                                        Sales_Task_Complete_Detail_Fragment writeReviewFragment = new Sales_Task_Complete_Detail_Fragment();
                                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                                writeReviewFragment, true, true);
                                    } else {
                                        CompletedTaskDetail(pendingTask);
                                    }
                                }


                                @Override
                                public void onAcceptClick(View v, int position,
                                                          Alerts alerts) {
                                    // TODO Auto-generated method stub

                                }

                                @Override
                                public void onRejectClick(View v, int position,
                                                          Alerts alerts) {
                                    // TODO Auto-generated method stub

                                }
                            });

                            completed_task_list_listView.setAdapter(adapter);
                        } else {

                            completed_task_list_listView.setAdapter(adapter);

                            adapter.notifyDataSetChanged();


                        }


                        if (pendingTasksall.getPendingListTasks().size() > 0) {
                            noCompletedTaskTxtViw.setVisibility(View.GONE);
                            completed_task_list_listView.setVisibility(View.VISIBLE);
                        } else {
                            noCompletedTaskTxtViw.setVisibility(View.VISIBLE);
                            completed_task_list_listView.setVisibility(View.GONE);
                        }
                    }
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {

                        noCompletedTaskTxtViw.setVisibility(View.VISIBLE);
                        completed_task_list_listView.setVisibility(View.GONE);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    noCompletedTaskTxtViw.setVisibility(View.VISIBLE);
                    completed_task_list_listView.setVisibility(View.GONE);

                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {

                noCompletedTaskTxtViw.setVisibility(View.VISIBLE);
                completed_task_list_listView.setVisibility(View.GONE);

                e.printStackTrace();
                Toast toast = Toast.makeText(getActivity(), getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {

            noCompletedTaskTxtViw.setVisibility(View.VISIBLE);
            completed_task_list_listView.setVisibility(View.GONE);

            Toast toast = Toast.makeText(getActivity(), getString(R.string.please_try_again),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void CompletedTaskDetail(PendingTask pendingTask) {
        ConstantFunction.savestatus(getContext(), AppConstants.task_Id, pendingTask.getTaskId());
        ConstantFunction.savestatus(getContext(), AppConstants.et_id, pendingTask.getEt_id());
        Completed_Task_Detail_Fragment writeReviewFragment = new Completed_Task_Detail_Fragment();
        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                writeReviewFragment, true, true);
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);


                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }

        }
    }

}
