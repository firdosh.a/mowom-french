package com.asrapps.mowom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.BuildConfig;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.view.InternalStorageContentProvider;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.asrapps.mowom.constants.ConstantFunction.getScaledBitmapLessThan720Width;


public class AppTab_Profile_Home extends BaseFragment implements
        OnClickListener, AsyncResponseListener, OnCancelListener {


    Uri fileUri;
    File imgFile;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1888;

    //TextView txtHeaderUserName;
    ImageView headerUserImage;

    EditText userNameEd, passwordEd, rePasswordEd, phoneNumberEd, oldPasswordEd;
    Button btnSave;
    private ImageView ivProfilePic;
    Bitmap bitmap = null;

    private ProgressHUD mProgressHUD;

    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;

    private int REQUEST_CAMERA = 100;

    String profImage;

    ScrollView parentView;

    String strUserName, strPassword, strRePassword, strPhoneNumber, stroldPasswordEd;

    String selectedLanguage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.profile_layout, container, false);
        userNameEd = (EditText) view.findViewById(R.id.userNameEd);
        passwordEd = (EditText) view.findViewById(R.id.passwordEd);
        rePasswordEd = (EditText) view.findViewById(R.id.rePasswordEd);
        phoneNumberEd = (EditText) view.findViewById(R.id.phoneNumberEd);
        oldPasswordEd = (EditText) view.findViewById(R.id.oldPasswordEd);

        internetConnection = new ConnectionDetector(getActivity());
        sqlcon = new SQLController(getActivity());

		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/
        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
//		headerUserImage.setOnClickListener(this);

        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);

        ivProfilePic = (ImageView) view.findViewById(R.id.ivProfilePic);
        ivProfilePic.setOnClickListener(this);

        parentView = (ScrollView) view.findViewById(R.id.parentView);
        // parentView.setVisibility(View.GONE);

        if (internetConnection.isConnectingToInternet("")) {
            SetUserDetails();
            GetProfile();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();


//            String userImage = ConstantFunction.getuser(getActivity(), "UserImage");
//            if (!userImage.equalsIgnoreCase("")) {
//                Picasso.with(getActivity())
//                        .load(ConstantFunction.getuser(getActivity(), "UserImage"))
//                        .transform(new CircleTransform()).resize(100, 100)
//                        .into(ivProfilePic);
//            }

        }


        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            Drawable imgUser = getResources().getDrawable(R.drawable.name_icon);
            userNameEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgUser, null);

            Drawable imgPassword = getResources().getDrawable(R.drawable.password_icon);
            passwordEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPassword, null);

            rePasswordEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPassword, null);

            oldPasswordEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgPassword, null);

            Drawable imgPhone = getResources().getDrawable(R.drawable.phone_icon);
            phoneNumberEd.setCompoundDrawablesWithIntrinsicBounds(null, null, imgUser, null);

            userNameEd.setGravity(Gravity.RIGHT);
            passwordEd.setGravity(Gravity.RIGHT);
            rePasswordEd.setGravity(Gravity.RIGHT);
            phoneNumberEd.setGravity(Gravity.RIGHT);
            oldPasswordEd.setGravity(Gravity.RIGHT);

        }

        if (selectedLanguage.equalsIgnoreCase("en") || selectedLanguage.equalsIgnoreCase("fr")) {

        }

        return view;
    }

    private void SetUserDetails() {
    /*	txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.btnSave:
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                strUserName = userNameEd.getText().toString();
                strPassword = passwordEd.getText().toString();
                strRePassword = rePasswordEd.getText().toString();
                strPhoneNumber = phoneNumberEd.getText().toString();
                stroldPasswordEd = oldPasswordEd.getText().toString();

                if (IsValid(strUserName, strPassword, strRePassword, strPhoneNumber)) {

                    String id = ConstantFunction.getuser(getActivity(), "UserId");
                    if (internetConnection
                            .isConnectingToInternet(getString(R.string.check_connection))) {

                        ChangeProfile(id, strUserName, stroldPasswordEd, strPassword, strRePassword,
                                strPhoneNumber, profImage);
                    }
                }

                break;

            case R.id.ivProfilePic:
                showSelectImageDialog(getActivity());
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }

    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }


    private void GetProfile() {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("id", id);
        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.GetProfile);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Profile_Home :- " + ConstantValues.GetProfile + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    private void ChangeProfile(String id, String strUserName, String stroldPasswordEd,
                               String strPassword, String strRePassword, String phoneNumber,
                               String profImage) {

        JSONObject requestObject = new JSONObject();
        try {
            requestObject.put("id", id);
            requestObject.put("name", strUserName);
            requestObject.put("old_password", stroldPasswordEd);
            requestObject.put("new_password", strPassword);
            requestObject.put("phone", phoneNumber);
            requestObject.put("user_image", profImage);
        } catch (JSONException e) {
            Log.d("Exception while signing_in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ChangeProfile);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Profile_Home :- " + ConstantValues.ChangeProfile + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);

        if (!response.equalsIgnoreCase("")) {

            if (requestURL.equalsIgnoreCase(ConstantValues.GetProfile)) {
                MowomLogFile.writeToLog("\n" + "AppTab_Profile_Home :- " + ConstantValues.GetProfile + " -------------" + response);

                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    if (status.equalsIgnoreCase("success")) {
                        try {

                            JSONObject job2 = job.getJSONObject("data");
                            String userName = job2.getString("emp_name");
                            String userPhone = job2.getString("emp_number");
                            String imagePath = job2.getString("profileimage");
                            imagePath = imagePath.replace(" ", "%20");
                            /*Picasso.with(getActivity()).load(imagePath)
                                    .error(R.drawable.icon_no_preview)
									.into(ivProfilePic);*/

                            userNameEd.setText(userName);

                            phoneNumberEd.setText(userPhone);

                            /*if (!imagePath.equalsIgnoreCase(ConstantValues.ImageURL))
                                new DownloadImage().execute(imagePath);*/

                            Picasso.with(getActivity())
                                    .load(imagePath)
                                    .error(R.drawable.icon_no_preview)
                                    .into(new Target() {
                                        @Override
                                        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                                    /* Save the bitmap or do something with it here */
                                            profImage = ConstantFunction.BitMapToString(bitmap);
                                            //Set it in the ImageView
                                            ivProfilePic.setImageBitmap(bitmap);
                                        }

                                        @Override
                                        public void onBitmapFailed(Drawable arg0) {
                                            // TODO Auto-generated method stub

                                        }

                                        @Override
                                        public void onPrepareLoad(Drawable arg0) {
                                            // TODO Auto-generated method stub

                                        }
                                    });


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (status.equalsIgnoreCase("failed")) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                        ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getActivity());

                                        startActivity(new Intent(getActivity(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (result_code.equalsIgnoreCase("402")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }


                } catch (Exception e) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } else if (requestURL.equalsIgnoreCase(ConstantValues.ChangeProfile)) {
                MowomLogFile.writeToLog("\n" + "AppTab_Profile_Home :- " + ConstantValues.ChangeProfile + " -------------" + response);

                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    if (status.equalsIgnoreCase("success")) {
                        try {

                            JSONObject job2 = job.getJSONObject("data");

                            ConstantFunction.savestatus(getActivity(),
                                    "UserId", job2.get("emp_id").toString());
                            ConstantFunction.savestatus(getActivity(),
                                    "UserHeaderName",
                                    "Hi," + " "
                                            + job2.get("emp_name").toString());

                            ConstantFunction
                                    .savestatus(getActivity(), "UserName", job2
                                            .get("emp_name").toString());

                            ConstantFunction.savestatus(getActivity(),
                                    "UserPhone", job2.get("phone").toString());
                            ConstantFunction.savestatus(getActivity(),
                                    "UserEmail", job2.get("emp_email")
                                            .toString());


                            String imagePath = job2.getString("profileimage");
                            imagePath = imagePath.replace(" ", "%20");

                            ConstantFunction.savestatus(getActivity(),
                                    "UserImage", (imagePath).toString());


                            ConstantFunction.savestatus(getActivity(),
                                    "client_logo", imagePath);


                            SetUserDetails();
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.profile_updated), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            oldPasswordEd.setText("");
                            passwordEd.setText("");
                            rePasswordEd.setText("");

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (status.equalsIgnoreCase("failed")) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage("User does not Exist.");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                        ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getActivity());

                                        startActivity(new Intent(getActivity(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (result_code.equalsIgnoreCase("402")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                } catch (Exception e) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            }

        } else {
        }

    }

    public void showSelectImageDialog(Context c) {

        final Context context = c;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_gallery);
        dialog.show();

        // dialog cancel button
        Button btnDialogCancel = (Button) dialog
                .findViewById(R.id.btnDialogCancel);
        btnDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // dialog Camera button
        Button btnCamera = (Button) dialog.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
                    } else {
                        cameraIntent();
//                        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                        fileUri = Uri.fromFile(fileName);
//                        i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                        i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                        getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    }
                } else {
                    cameraIntent();
//                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                    fileUri = Uri.fromFile(fileName);
//                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                    i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                    getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                }

                dialog.dismiss();
            }
        });

        // dialog Gallery button
        Button btnGallery = (Button) dialog.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                getActivity().startActivityForResult(
                        Intent.createChooser(intent, "Select File"), 2);
                dialog.dismiss();
            }
        });
    }

    private void cameraIntent() {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            imgFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            imgFile = new File(getActivity().getFilesDir(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(imgFile);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

                fileUri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        imgFile);

            } else {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(imgFile);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    fileUri);
            intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getActivity().startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } catch (Exception e) {

            Log.d("CAMERA", "cannot take picture", e);
        }

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                Log.v("TTT", "permission is dynamically granted");

//                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                fileUri = Uri.fromFile(fileName);
//                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                cameraIntent();
                break;

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;

        getActivity();
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                try {

                    getActivity();
                    if (resultCode == Activity.RESULT_OK) {

                        try {
                            if (fileUri != null) {
                                InputStream inputStream;
                                try {
                                    inputStream = getActivity().getContentResolver().openInputStream(
                                            data.getData());
                                    if (!imgFile.exists()) {
                                        new File(AppConstants.tempImagePath).mkdirs();
                                    }
                                    FileOutputStream fileOutputStream = new FileOutputStream(
                                            imgFile);
                                    copyStream(inputStream, fileOutputStream);
                                    fileOutputStream.close();
                                    inputStream.close();
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                try {
                                    Log.v("TTT", "imageToUploadUri = " + fileUri.toString());
//                                    File fileName = new File(fileUri.toString().replace("file://", ""));
                                    ExifInterface exif = new ExifInterface(imgFile.getPath());
                                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                    int angle = 0;

                                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                                        angle = 90;
                                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                                        angle = 180;
                                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                                        angle = 270;
                                    }

                                    Matrix mat = new Matrix();
                                    mat.postRotate(angle);

                                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(imgFile), null, null);
                                    bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth()/2, bmp.getHeight()/2, false);
                                    Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

                                    ivProfilePic.setImageBitmap(reducedSizeBitmap);

                                    profImage = ConstantFunction.BitMapToString(reducedSizeBitmap);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Log.w("TAG", "-- Error in setting image");
                                } catch (OutOfMemoryError oom) {
                                    oom.printStackTrace();
                                    Log.w("TAG", "-- OOM Error in setting image");
                                }

                            }

                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                    } else {
                        getActivity();
                        if (resultCode == Activity.RESULT_CANCELED) {

                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    "Sorry! Failed to capture image",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }


//                    String path = android.os.Environment
//                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
//                            + File.separator
//                            + "movom"
//                            + File.separator
//                            + "movom.jpg";
//
//                    File fileName = new File(path.toString().replace("file://", ""));
//
//                    ExifInterface exif = new ExifInterface(fileName.getPath());
//                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
//
//                    int angle = 0;
//
//                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
//                        angle = 90;
//                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
//                        angle = 180;
//                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
//                        angle = 270;
//                    }
//
//                    Matrix mat = new Matrix();
//                    mat.postRotate(angle);
//
//                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
//                    bmp = Bitmap.createScaledBitmap(bmp, 200, 200, false);
//                    Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
//
//
//                    profImage = ConstantFunction
//                            .BitMapToString(ConstantFunction.Shrinkmethod(path,
//                                    ivProfilePic.getWidth(),
//                                    ivProfilePic.getHeight()));
//
//					/* mask image */
//                    bitmap = ConstantFunction.Shrinkmethod(path,
//                            ivProfilePic.getWidth(), ivProfilePic.getHeight());
//                    ivProfilePic.setImageBitmap(reducedSizeBitmap);
//
//                    // f.delete();
//                    OutputStream outFile = null;
//                    File file = new File(path);
//                    try {
//                        outFile = new FileOutputStream(file);
//                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100,
//                                outFile);
//                        outFile.flush();
//                        outFile.close();
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};

                Cursor c = getActivity().getContentResolver().query(
                        selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();

                try {
                    Log.v("TTT", "imageToUploadUri = " + picturePath);
                    File fileName = new File(picturePath.replace("file://", ""));
                    ExifInterface exif = new ExifInterface(fileName.getPath());
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    int angle = 0;

                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                        angle = 90;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                        angle = 180;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                        angle = 270;
                    }

                    Matrix mat = new Matrix();
                    mat.postRotate(angle);


                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                    bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth()/2, bmp.getHeight()/2, false);
                    Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

                    ivProfilePic.setImageBitmap(reducedSizeBitmap);

                    profImage = ConstantFunction.BitMapToString(ConstantFunction
                            .Shrinkmethod(picturePath, ivProfilePic.getWidth(),
                                    ivProfilePic.getHeight()));
                /* mask image */
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.w("TAG", "-- Error in setting image");
                } catch (OutOfMemoryError oom) {
                    oom.printStackTrace();
                    Log.w("TAG", "-- OOM Error in setting image");
                }
            }
        }
    }

    /**
     * to copy stream *
     */
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private boolean IsValid(String strUserName, String strPassword, String strRePassword, String strPhoneNumber) {


        strUserName = userNameEd.getText().toString();
        strPassword = passwordEd.getText().toString();
        strRePassword = rePasswordEd.getText().toString();
        strPhoneNumber = phoneNumberEd.getText().toString();

        if (strUserName.equalsIgnoreCase("")) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_enter_user_name), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return false;
        }
        if (strPhoneNumber.equalsIgnoreCase("")) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_enter_phone_number), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            return false;
        }

        if (strPhoneNumber.length() < 10 || strPhoneNumber.length() > 16) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.enter_client_tele_valid),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return false;

        }


        if (!stroldPasswordEd.equalsIgnoreCase("")) {
            if (strPassword.equalsIgnoreCase("")) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_enter_password), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return false;
            }
        }

        if (!strPassword.equalsIgnoreCase("")) {
            if (strRePassword.equalsIgnoreCase("")) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_enter_repassword), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return false;
            }
        }

        if (!strPassword.equalsIgnoreCase("") && !strRePassword.equalsIgnoreCase("")) {
            if (stroldPasswordEd.equalsIgnoreCase("")) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_enter_old_pwd), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return false;
            }
        }

        if (!strPassword.equalsIgnoreCase(strRePassword)) {
            if (!strPassword.equalsIgnoreCase(strRePassword)) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.password_mis_match), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return false;
            }
        }

				/*if(!strPassword.equalsIgnoreCase("")){
                    if(!strPassword.equalsIgnoreCase(strRePassword)){
						Toast toast = Toast.makeText(getActivity(),
								getString(R.string.password_mis_match), Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						return false;						
					}
				}
				
				if(!strRePassword.equalsIgnoreCase("")){
					if(!strPassword.equalsIgnoreCase(strRePassword)){
						Toast toast = Toast.makeText(getActivity(),
								getString(R.string.password_mis_match), Toast.LENGTH_SHORT);
						toast.setGravity(Gravity.CENTER, 0, 0);
						toast.show();
						return false;						
					}
				}*/
        return true;
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
            for (int i = 0; i < mLogin.size(); i++) {

                userNameEd.setText(mLogin.get(i).emp_name + "");
                phoneNumberEd.setText(mLogin.get(i).phone + "");

                if (mLogin.get(i).profileImageSdCard != null && !mLogin.get(i).profileImageSdCard.equals("null") && !mLogin.get(i).profileImageSdCard.equals("")) {
                    File file = new File(mLogin.get(i).profileImageSdCard);

                    if (file.exists()) {
                        myBitmap = getScaledBitmapLessThan720Width(file.getAbsolutePath());

                        try {
                            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                            int orientation = exif.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION, 1);
                            Log.d("EXIF", "Exif: " + orientation);
                            Matrix matrix = new Matrix();
                            if (orientation == 6) {
                                matrix.postRotate(90);
                            } else if (orientation == 3) {
                                matrix.postRotate(180);
                            } else if (orientation == 8) {
                                matrix.postRotate(270);
                            }
                            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(),
                                    matrix, true); // rotating bitmap
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                    ivProfilePic.setImageBitmap(myBitmap);
                }

            }
        }
    }


}