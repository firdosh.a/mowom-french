package com.asrapps.mowom.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.MapMenuDialog;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.CurrentLocation;
import com.asrapps.mowom.constants.MyLeadingMarginSpan2;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.DirectionsJSONParser;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;
import com.asrapps.utils.MowomLogFile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.asrapps.mowom.constants.AppConstants.issue;
import static com.asrapps.mowom.constants.ConstantValues.PendingTaskDetailsURL;
import static com.asrapps.mowom.constants.ConstantValues.latitude;
import static com.asrapps.mowom.constants.ConstantValues.longitude;
import static com.asrapps.mowom.constants.ConstantValues.task_list_date_time;


public class Pending_Task_Details_Fragment extends BaseFragment implements
        AsyncResponseListener, OnCancelListener, OnClickListener, OnMapReadyCallback {

    private GoogleMap addressGoogleMap;
    String et_id, taskId;
    String pending_task_id;
    PendingTask pendingTasks;
    TextView pending_task_detail_orgName, pending_task_detail_id,
            pending_task_detail_issue, pending_task_detail_address,
            pending_task_detail_date_time,pending_task_detail_duration,
            pending_task_detail_contactPersonName, pending_task_detail_phone,
            pending_task_detail_supervisorName, addtional_info, lable_id, label_comma;
    ScrollView pending_task_detail_scroll;
    LinearLayout contentsLayout, llLeadMargin;
    Button pending_task_detail_startJob_button;

    ImageView pending_task_detail_phone_image, pending_task_detail_map_image, pending_task_detail_map_direction_image;

    double lat, lng;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;
    int id;
    SharedPreferences sharedPref;


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getEtId() {
        return et_id;
    }

    public void setEtId(String et_id) {
        this.et_id = et_id;
    }

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;
    private ProgressHUD mProgressHUD;
    ConnectionDetector internetConnection;
    CurrentLocation crntLocation;

    private LatLng fromLocation;
    private LatLng toLocation;
    String selectedLanguage, getDirectionLat = "", getDirectionLong = "";

    LinearLayout idLayout1;
    View view;
    TextView pending_task_detail_issue_all;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = inflater.inflate(R.layout.pending_task_detail_layout_pr,
                    container, false);
        } else {
            view = inflater.inflate(R.layout.pending_task_detail_layout,
                    container, false);
        }

        sqlcon = new SQLController(getActivity());

        sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);

        crntLocation = new CurrentLocation(getActivity());
        crntLocation.GetCurrentLocation();

		/*
         * txtHeaderUserName = (TextView) view
		 * .findViewById(R.id.txtHeaderUserName);
		 */

        pending_task_detail_issue_all = (TextView) view.findViewById(R.id.pending_task_detail_issue_all);

        llLeadMargin = (LinearLayout) view.findViewById(R.id.llLeadMargin);

        lable_id = (TextView) view.findViewById(R.id.lable_id);

        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
        // headerUserImage.setOnClickListener(this);

        backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

        idLayout1 = (LinearLayout) view.findViewById(R.id.idLayout1);
        label_comma = (TextView) view
                .findViewById(R.id.label_comma);

        pending_task_detail_orgName = (TextView) view
                .findViewById(R.id.pending_task_detail_orgName);
        pending_task_detail_id = (TextView) view
                .findViewById(R.id.pending_task_detail_id);
        pending_task_detail_issue = (TextView) view
                .findViewById(R.id.pending_task_detail_issue);
        pending_task_detail_address = (TextView) view
                .findViewById(R.id.pending_task_detail_address);
        pending_task_detail_duration = (TextView) view
                .findViewById(R.id.pending_task_detail_duration);


        pending_task_detail_date_time = (TextView) view
                .findViewById(R.id.pending_task_detail_date_time);
        pending_task_detail_contactPersonName = (TextView) view
                .findViewById(R.id.pending_task_detail_contactPersonName);
        pending_task_detail_phone = (TextView) view
                .findViewById(R.id.pending_task_detail_phone);
        pending_task_detail_supervisorName = (TextView) view
                .findViewById(R.id.pending_task_detail_supervisorName);
        addtional_info = (TextView) view
                .findViewById(R.id.addtional_info);

        contentsLayout = (LinearLayout) view.findViewById(R.id.contentsLayout);
        pending_task_detail_startJob_button = (Button) view
                .findViewById(R.id.pending_task_detail_startJob_button);
        pending_task_detail_startJob_button.setOnClickListener(this);
//        contentsLayout.setVisibility(View.GONE);
//        pending_task_detail_startJob_button.setVisibility(View.GONE);

        pending_task_detail_phone_image = (ImageView) view
                .findViewById(R.id.pending_task_detail_phone_image);
        pending_task_detail_phone_image.setOnClickListener(this);

        pending_task_detail_map_image = (ImageView) view
                .findViewById(R.id.pending_task_detail_map_image);
        pending_task_detail_map_image.setOnClickListener(this);

        pending_task_detail_map_direction_image = (ImageView) view.findViewById(R.id.pending_task_detail_map_direction_image);
        pending_task_detail_map_direction_image.setOnClickListener(this);


        pending_task_detail_scroll = (ScrollView) view
                .findViewById(R.id.pending_task_detail_scroll); // parent
        // scrollview in
        // xml, give
        // your
        // scrollview id
        // value

        addressGoogleMap = null;

        internetConnection = new ConnectionDetector(getActivity());

        if (internetConnection
                .isConnectingToInternet("")) {

            LocationManager manager = (LocationManager) getActivity()
                    .getSystemService(Context.LOCATION_SERVICE);

            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps(getActivity());
            } else {
                SetUserDetails();
                LoadMap();
                GetPendingTasks();
            }

        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            LoadMap();

            sqlcon.open();
            pendingTasks = sqlcon.getPendingByIdTask(ConstantFunction.getstatus(getActivity(), "UserId") + "", taskId); /// pass et id

            SetView(pendingTasks);
        }

        ImageView transparentImageView = (ImageView) view
                .findViewById(R.id.transparent_image);
        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        pending_task_detail_scroll
                                .requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        pending_task_detail_scroll
                                .requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        pending_task_detail_scroll
                                .requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
        return view;
    }

    public void buildAlertMessageNoGps(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(mActivity.getString(R.string.gps_enable))
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                mActivity
                                        .startActivity(new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                // turnGPSOn();
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                dialog.cancel();
                                mActivity.finish();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private void SetView(PendingTask pendingTask) {

        if (pendingTask != null) {

            ConstantFunction.savestatus(getContext(), "pending_task_id",
                    pendingTask.getTaskId());

            ConstantFunction.savestatus(getContext(), issue,
                    pendingTask.getIssue());

            pending_task_detail_orgName.setText(pendingTask.getOrg_name()
                    .toUpperCase(Locale.getDefault()));

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }
            if (pendingTask
                    .getDuration() != null && pendingTask
                    .getDuration().length() > 0) {

                pending_task_detail_duration.setText(getString(R.string.duration) +" "+pendingTask
                        .getDuration()+" "+ getString(R.string.Mins));

            }
            else
            {
                pending_task_detail_duration.setText(getString(R.string.duration) +" 0"+" "+ getString(R.string.Mins));
            }
            id = pendingTask.getTaskId().length();
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                pending_task_detail_id.setText(toPersianNumber(pendingTask.getTaskId()));

                Drawable DICON = view.getResources().getDrawable(R.drawable.id_icon);
                int leftMargin = DICON.getIntrinsicWidth() + lable_id.getText().length() + id + 80;
                SpannableString issueString = new SpannableString(pendingTask.getIssue().toUpperCase(Locale.getDefault()));
                issueString.setSpan(new MyLeadingMarginSpan2(1, 0), 0, issueString.length(), 0);

                pending_task_detail_issue.setText(issueString);

                pending_task_detail_issue_all.setText(getResources().getString(R.string.id) + " " + pendingTask.getTaskId() + " " + getResources().getString(R.string.comma) + " " + pendingTask.getIssue().toUpperCase(Locale.getDefault()));

            } else {
                pending_task_detail_id.setText(pendingTask.getTaskId());

                SpannableString issueString = new SpannableString(pendingTask.getIssue().toUpperCase(Locale.getDefault()));
                pending_task_detail_issue.setText(issueString);
                pending_task_detail_issue_all.setText(getResources().getString(R.string.id) + " " + pendingTask.getTaskId() + " " + getResources().getString(R.string.comma) + " " + pendingTask.getIssue().toUpperCase(Locale.getDefault()));

            }

            if (pendingTask
                    .getClientAddress() != null && pendingTask
                    .getClientAddress().size() > 0 && pendingTask
                    .getClientAddress().get(0).location != null) {
                pending_task_detail_address.setText(pendingTask.getClientAddress().get(0).location);
            } else {
                pending_task_detail_address.setText("");
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                StringBuilder t1 = new StringBuilder();

                try {

                    Log.v("TTT", "startTime = " + pendingTask.getTime());
                    String dt = WebServiceHelper.convert12to24("2016-10-01 " + pendingTask.getTime());

                    dt = dt.substring(11);
                    String st[] = dt.split(" ");

                    String stTime[] = st[0].split(":");

                    selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                    if (selectedLanguage.equalsIgnoreCase("")) {
                        selectedLanguage = "en";
                    }

                    if (selectedLanguage.equalsIgnoreCase("pr")) {

                        stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                        String dtStart = stTime[0];
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = format.parse(dtStart);
                            System.out.println(date);

                            SolarCalendar sc = new SolarCalendar(date);
                            String s = sc.date + "/" +
                                    sc.month + "/" + sc.year;

                            Log.v("TTT", "ssssss = " + s);

                            t1.append(toPersianNumber(s));
                            t1.append(":");
                            t1.append(toPersianNumber(stTime[1]));

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block

                            t1.append(toPersianNumber(stTime[0]));
                            t1.append(":");
                            t1.append(toPersianNumber(stTime[1]));
                            e.printStackTrace();
                        }

                    } else {

                        t1.append(stTime[0] + "");
                        t1.append(":");
                        t1.append(stTime[1] + "");

                    }

                } catch (Exception e) {
                    e.printStackTrace();

                }

                pending_task_detail_date_time.setText(t1.toString());

            } else {
                if (pendingTask.getDate_time() != null && !pendingTask.getDate_time().equals("null")) {
                    pending_task_detail_date_time.setText(WebServiceHelper.convert12to24(pendingTask.getDate_time()));
                }

            }


            pending_task_detail_contactPersonName
                    .setText(getString(R.string.contact_name) + " : "
                            + pendingTask.getPersonName());
            sqlcon.open();
            sqlcon.UpdatePendingTask(DbHelper.PENDING_personName, pendingTask.getPersonName() + "", pendingTask.getTaskId() + "");

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                pending_task_detail_phone.setText(toPersianNumber(pendingTask.getPhone()));
            } else {
                pending_task_detail_phone.setText(pendingTask.getPhone());
            }


            if (pendingTask.getSupervisorName() != null && !pendingTask.getSupervisorName().equals("null")) {
                pending_task_detail_supervisorName
                        .setText(getString(R.string.supervisor_name) + " : "
                                + pendingTask.getSupervisorName());
            }
            contentsLayout.setVisibility(View.VISIBLE);
            pending_task_detail_startJob_button.setVisibility(View.VISIBLE);

            addtional_info.setText(pendingTask.getAdditionalInfo());


            getDirectionLat = String.valueOf(pendingTask.getClientAddress().get(0).latitude);
            getDirectionLong = String.valueOf(pendingTask.getClientAddress().get(0).longitude);

//            if (latitude != 0.0 && longitude != 0.0) {
//                FromAddressAddMarker(latitude, longitude,
//                        R.drawable.from_map_icon);
//            }
            if (pendingTask != null && pendingTask.getClientAddress() != null) {
                if (pendingTask.getClientAddress().size() > 0) {
                    FromAddressAddMarker(pendingTask.getClientAddress(), R.drawable.to_map_icon);
                }
            }


            lat = pendingTask.getLatitude();
            lng = pendingTask.getLongitude();

            ConstantValues.task_org_name = pendingTask.getOrg_name().toUpperCase(
                    Locale.getDefault());
            ConstantValues.task_list_id = pendingTask.getTaskId();
            ConstantValues.task_list_issue = pendingTask.getIssue();
            task_list_date_time = pendingTask.getDate_time();
            ConstantValues.task_list_phone = pendingTask.getPhone();
            if (pendingTask
                    .getClientAddress() != null && pendingTask
                    .getClientAddress().size() > 0 && pendingTask
                    .getClientAddress().get(0).location != null) {
                ConstantValues.task_list_address = pendingTask.getClientAddress().get(0).location;
            } else {
                ConstantValues.task_list_address = "";
            }
            ConstantValues.task_detail_contactPersonName = getString(R.string.contact_name)
                    + " : " + pendingTask.getPersonName();

            for (int r = 0; r < pendingTask.getClientAddress().size(); r++) {
                fromLocation = new LatLng(latitude, longitude);
                toLocation = new LatLng(Double.parseDouble(pendingTask.getClientAddress().get(r).latitude), Double.parseDouble(pendingTask.getClientAddress().get(r).longitude));

                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(fromLocation, toLocation);
                DownloadTask downloadTask = new DownloadTask();

                //Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }
        }
    }



    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }
    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private void LoadMap() {
        try {
            int status = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(getActivity());

            if (status == ConnectionResult.SUCCESS) {
                initilizeMap();
                if (addressGoogleMap != null)
                    addressGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            } else {

                int requestCode = 10;
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,
                        getActivity(), requestCode);
                dialog.show();
            }
        } catch (Exception ex) {
            Toast toast = Toast.makeText(getActivity(), ex.getMessage(),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void initilizeMap() {
        if (addressGoogleMap == null) {
            ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.pending_task_detail_AddressMap)).getMapAsync(this);

			/*
             * mMap = ((SupportMapFragment) MainActivity.fragmentManager
			 * .findFragmentById(R.id.location_map)).getMap();
			 */

//            if (addressGoogleMap == null) {
//                Toast toast = Toast.makeText(getActivity(),
//                        getString(R.string.unable_to_create_maps),
//                        Toast.LENGTH_SHORT);
//
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
        }
    }

    private void FromAddressAddMarker(ArrayList<ClientAddress> client_address,
                                      int icon_id) {
        // addressGoogleMap.clear();
        MarkerOptions marker1 = new MarkerOptions().position(new LatLng(
                latitude, longitude));

        marker1.icon(BitmapDescriptorFactory.fromResource(R.drawable.from_map_icon));
        if (addressGoogleMap != null) {
            addressGoogleMap.addMarker(marker1);

        }
        for (int r = 0; r < client_address.size(); r++) {
            MarkerOptions marker = new MarkerOptions().position(new LatLng(
                    Double.parseDouble(client_address.get(r).latitude), Double.parseDouble(client_address.get(r).longitude)));

            marker.icon(BitmapDescriptorFactory.fromResource(icon_id));
            if (addressGoogleMap != null) {
                addressGoogleMap.addMarker(marker);

//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target(new LatLng(latitude, longitude)).zoom(2).build();
//
//                addressGoogleMap.animateCamera(CameraUpdateFactory
//                        .newCameraPosition(cameraPosition));
            }
        }


        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(
                    latitude, longitude));
            for (int r = 0; r < client_address.size(); r++) {
                builder.include(new LatLng(
                        Double.parseDouble(client_address.get(r).latitude), Double.parseDouble(client_address.get(r).longitude)));
            }


            LatLngBounds bounds = builder.build();

            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            int width = displayMetrics.widthPixels;
            int height = displayMetrics.heightPixels;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 10);

            if (addressGoogleMap != null) {
                addressGoogleMap.moveCamera(cu);
                addressGoogleMap.animateCamera(cu);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // new method
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    private void StartTasks() {

        JSONObject requestObject = new JSONObject();
        try {
            Calendar c = Calendar.getInstance();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                    Locale.getDefault());
            String task_starttime = sdf.format(c.getTime());

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("task_id", taskId);
            requestObject.put("emp_task_id", et_id);
            requestObject.put("task_starttime", task_starttime);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

//        SharedPreferences sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString("task_id", taskId);
//        editor.commit();

        ConstantFunction.savestatus(getContext(), AppConstants.task_Id,
                taskId);
        ConstantFunction.savestatus(getContext(), AppConstants.et_id,
                et_id);

        ConstantFunction.savestatus(getContext(), "pending_task_id",
                taskId);

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.StartTaskURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Pending_Task_Details_Fragment :- " + ConstantValues.StartTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    private void GetPendingTasks() {

        JSONObject requestObject = new JSONObject();
        try {

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("id", id);
            requestObject.put("task_id", taskId);
            requestObject.put("emp_task_id", et_id);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                PendingTaskDetailsURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Pending_Task_Details_Fragment :- " + PendingTaskDetailsURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();

    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        if (!response.equalsIgnoreCase("")) {
            if (!requestURL.equalsIgnoreCase(ConstantValues.StartTaskURL)) {
                MowomLogFile.writeToLog("\n" + "Pending_Task_Details_Fragment :- " + ConstantValues.PendingTaskDetailsURL + " -------------" + response);

                try {
                    pendingTasks = new PendingTask(response, "");
                    if (pendingTasks.isSuccess()) {
                        SetView(pendingTasks.getPendingTask());

                    } else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                MowomLogFile.writeToLog("\n" + "Pending_Task_Details_Fragment :- " + ConstantValues.StartTaskURL + " -------------" + response);
                JSONObject job;
                String status = "";
                try {
                    job = new JSONObject(response);
                    status = job.getString("status");
                    String result_code = job.getString("code");

                    if (status.equalsIgnoreCase("Success")) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.task_started),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        Pending_Task_Start_Fragment detailFragment = new Pending_Task_Start_Fragment();
                        // detailFragment.setTaskId(pendingTask.getTaskId());
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                detailFragment, true, true);
                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                        ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getActivity());

                                        startActivity(new Intent(getActivity(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // {"code":1,"status":"Success","data":{"started":"1","0":"1"}}
            }

        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    public String getStartTime() {
        String taskStartTime = "";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                Locale.getDefault());
        taskStartTime = sdf.format(c.getTime());
        UpdaterService.currentTime = taskStartTime;
        return taskStartTime;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.pending_task_detail_startJob_button:

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                SharedPreferences.Editor editor2 = preferences.edit();
                editor2.putBoolean("isStartRunning", true);
                editor2.commit();


                SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                SharedPreferences.Editor editor1 = prefs1.edit();
                editor1.putString("currentTime", getStartTime() + "");
                editor1.commit();

                String task_id = ConstantFunction.getuser(getActivity(), AppConstants.task_Id);
//
//                if (!TextUtils.isEmpty(ConstantFunction.getuser(getActivity(), "pending_task_id")))
//                    pending_task_id = ConstantFunction.getuser(getActivity(), "pending_task_id");
//                else
//                    ConstantFunction.savestatus(getContext(), "pending_task_id",
//                            task_id);

//                if (!task_id.equals("0") && sharedPref.getBoolean("is_start_task", true)) {
//                    try {
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//                        alertDialogBuilder.setMessage(getString(R.string.err_start_task));
//                        alertDialogBuilder.setCancelable(false);
//
//                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int arg1) {
//                                dialog.dismiss();
//
//                                mActivity.ClearAllPages();
//                                // mActivity.popFragments();
//                                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
//                                        new AppTab_Dashboard_Home(), true, true);
//
//                            }
//                        });
//
//                        AlertDialog alertDialog = alertDialogBuilder.create();
//                        alertDialog.show();
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//
//                    }
//
//                } else {

//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                    SharedPreferences.Editor edit = sharedPref.edit();
//                    edit.putBoolean("is_start_task", true);
//                    edit.commit();

                if (!UpdaterService.inProcess) {
                    UpdaterService.firstTime = true;

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("firstTime", true);
                    editor.putString("currentTime", UpdaterService.currentTime + "");

                    editor.commit();

                    LocationManager manager = (LocationManager) getActivity()
                            .getSystemService(Context.LOCATION_SERVICE);

                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        buildAlertMessageNoGps(getActivity());
                    } else {
                        internetConnection = new ConnectionDetector(getActivity());
                        if (internetConnection
                                .isConnectingToInternet("")) {
                            StartTasks();
                        } else {

//                                SharedPreferences sharedPref1 = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor1 = sharedPref1.edit();
//                                editor1.putString("task_id", taskId);
//                                editor1.commit();

                            ConstantFunction.savestatus(getContext(), AppConstants.task_Id,
                                    taskId);

                            ConstantFunction.savestatus(getContext(), AppConstants.et_id,
                                    et_id);

                            ConstantFunction.savestatus(getContext(), "pending_task_id",
                                    taskId);

                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.task_started),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            Pending_Task_Start_Fragment detailFragment = new Pending_Task_Start_Fragment();
                            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                    detailFragment, true, true);
                        }
                    }
                } else {
                    Pending_Task_Start_Fragment detailFragment = new Pending_Task_Start_Fragment();
                    // detailFragment.setTaskId(pendingTask.getTaskId());
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                            detailFragment, true, true);
                    UpdaterService.firstTime = false;

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("firstTime", false);
                    editor.commit();

//                    }
                }

                break;

            case R.id.pending_task_detail_phone_image:
                CallThePerson(pending_task_detail_phone.getText().toString());
                break;

            case R.id.pending_task_detail_map_image:
                getMenuPopUp(pendingTasks.getPendingTask().getClientAddress());
                break;

            case R.id.pending_task_detail_map_direction_image:

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                                "&saddr=" + latitude + "," + longitude + "&daddr=" + getDirectionLat + "," + getDirectionLong + "&hl=zh&t=m&dirflg=d"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        // initilizeMap();
        // LoadMap();
    }

    private void CallThePerson(String phoneNumber) {
        Intent intentcall = new Intent();
        intentcall.setAction(Intent.ACTION_CALL);
        intentcall.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intentcall);
    }

    private void getMenuPopUp(ArrayList<ClientAddress> client_address) {
        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        MapMenuDialog dialog = new MapMenuDialog();
        if (fromLocation != null) {
            dialog.setLatLong(client_address, fromLocation.latitude, fromLocation.longitude);
        } else {
            dialog.setLatLong(client_address, 0.0, 0.0);

        }
        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        addressGoogleMap = googleMap;

        if (addressGoogleMap == null) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.unable_to_create_maps),
                    Toast.LENGTH_SHORT);

            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        } else {
//            if (latitude != 0.0 && longitude != 0.0) {
//                FromAddressAddMarker(latitude, longitude,
//                        R.drawable.from_map_icon);
//            }
            if (pendingTasks != null) {
                if (pendingTasks.getClientAddress() != null) {
//                    for (int r = 0; r < pendingTasks.getClientAddress().size(); r++) {
                    FromAddressAddMarker(pendingTasks.getClientAddress(), R.drawable.to_map_icon);
//                    }
                }

            }

        }
    }


    // New
    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("TTT", "Exception while signing in" + e.toString());

        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            try {
                ArrayList<LatLng> points = null;
                PolylineOptions lineOptions = null;
                MarkerOptions markerOptions = new MarkerOptions();

                // Traversing through all the routes
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = result.get(i);

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(2);
                    lineOptions.color(Color.RED);
                }

                // Drawing polyline in the Google Map for the i-th route
                if (addressGoogleMap != null)
                    addressGoogleMap.addPolyline(lineOptions);

            } catch (Exception e) {

            }
        }
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }
}
