package com.asrapps.mowom.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.AddSalesLocation;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.asrapps.mowom.model.SalesTask;
import com.asrapps.mowom.view.HorizontalLayoutScrollListView2;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Sales_Task_Final_Fragment extends BaseFragment implements
        OnClickListener, AsyncResponseListener, OnCancelListener {

    private ProgressHUD mProgressHUD;
    PendingTask offlineSelectedTask;


    View view;
    SQLController sqlcon;

    public ArrayList<SalesTask> arrSalesTask, allSales = new ArrayList<>();
    private ArrayList<Login> mLogin;

    ImageView customerSignature, customerSignature1;
    String taskId, issue, et_id;

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    TextView pending_task_detail_orgName,
            pending_task_list_issue,
            pending_task_list_phone, pending_task_list_address,
            pending_task_detail_contactPersonName,
            pending_task_detail_start_endTime, totalTime, taskDetail, taskProduct, taskNote, taskNextStep;


    String task_list_id, task_list_issue, task_list_phone,
            task_list_address, task_detail_contactPersonName,
            task_detail_start_endTime, strTotalTime, strTaskDetailTextView,
            strTaskProductTextView, strTaskNoteTextView, strTasknextstepTextView;

    String problem_reported;
    String diagnosis_done;
    String resolution;
    String notes_support;

    String deilvery_status;
    String notes_delevery;

    String selectedLanguage;

    String startTime, endTime;
    String strSignature;

    HorizontalLayoutScrollListView2 photoGrid;
    public Pending_Task_Photo_Activity CustomListView = null;
    private ArrayList<PhotoSalesModel> CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();

    Button taskEndButton;

    LinearLayout contentsLayoutEng, contentsLayoutPer;

    ConnectionDetector internetConnection;

    ArrayList<SalesTask> arrayList = new ArrayList<>();
    public ArrayList<AddSalesLocation> arrAddSalesTask = new ArrayList<>();


    public Sales_Task_Final_Fragment() {

    }

    public Sales_Task_Final_Fragment(ArrayList<SalesTask> arrayList) {

        this.arrayList = arrayList;

        Log.e("AAA", "Arraylist size in final page ; " + arrayList.size());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = inflater.inflate(R.layout.sales_task_final_layout_pr,
                        container, false);

            } else {
                view = inflater.inflate(R.layout.sales_task_final_layout,
                        container, false);

            }


			/*
             * txtHeaderUserName = (TextView) view
			 * .findViewById(R.id.txtHeaderUserName);
			 */

            contentsLayoutEng = (LinearLayout) view.findViewById(R.id.contentsLayoutEng);
            contentsLayoutPer = (LinearLayout) view.findViewById(R.id.contentsLayoutPer);

            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
            // headerUserImage.setOnClickListener(this);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            sqlcon = new SQLController(getActivity());
            customerSignature = (ImageView) view
                    .findViewById(R.id.customerSignature);

            customerSignature1 = (ImageView) view
                    .findViewById(R.id.customerSignature1);

            taskEndButton = (Button) view.findViewById(R.id.taskEndButton);
            taskEndButton.setOnClickListener(this);


            pending_task_detail_orgName = (TextView) view
                    .findViewById(R.id.pending_task_detail_orgName);


            pending_task_list_issue = (TextView) view
                    .findViewById(R.id.pending_task_list_issue);


            pending_task_list_phone = (TextView) view
                    .findViewById(R.id.pending_task_list_phone);


            pending_task_list_address = (TextView) view
                    .findViewById(R.id.pending_task_list_address);


            pending_task_detail_contactPersonName = (TextView) view
                    .findViewById(R.id.pending_task_detail_contactPersonName);


            pending_task_detail_start_endTime = (TextView) view
                    .findViewById(R.id.pending_task_detail_start_endTime);


            totalTime = (TextView) view.findViewById(R.id.totalTime);


            taskDetail = (TextView) view
                    .findViewById(R.id.taskDetail);

            taskProduct = (TextView) view
                    .findViewById(R.id.taskProduct);
            taskNote = (TextView) view
                    .findViewById(R.id.taskNote);
            taskNextStep = (TextView) view
                    .findViewById(R.id.taskNextStep);

            photoGrid = (HorizontalLayoutScrollListView2) view
                    .findViewById(R.id.photoGrid);


            GetFromShared();
            GetSavedItems();

            SetValues();

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

            } else {
            }

            et_id = ConstantFunction.getuser(mActivity.getApplicationContext(), AppConstants.et_id);
            sqlcon = new SQLController(getActivity());
            internetConnection = new ConnectionDetector(getActivity());
            if (internetConnection.isConnectingToInternet("")) {
                SetUserDetails();
            } else {
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            }
        }


        return view;
    }

    private void SetListView() {

        CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();
        CustomListViewValuesArr = GetPhotoFromDb(arrayList.get(0).task_id_Tx);

        for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
            String path = CustomListViewValuesArr.get(i).getTaskId();
            if (path.equals(arrayList.get(0).task_id_Tx)) {
                photoGrid.add(CustomListViewValuesArr.get(i), this);
            }
        }

        System.out.println(".................CustomListViewValuesArr.size()--> " + CustomListViewValuesArr.size());
//        adapter = new Photo_Sales_Grid_Adapter(getActivity(),
//                CustomListViewValuesArr, new OnDeleteSalesImageClick() {
//
//            public void onClickDelete(View v, int position,
//                                      PhotoSalesModel photo) {
//                // TODO Auto-generated method stub
//
//                DeletePhotoFromDb(photo.getPhotoId(), position);
//
//            }
//        });
//
//        photoGrid.setAdapter(adapter);
    }

    public ArrayList<PhotoSalesModel> GetPhotoFromDb(String taskId) {
        sqlcon.open();
        ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
        tempArray = sqlcon.GetAllPhotosSalesTx(ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
        return tempArray;

    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private void GetFromShared() {
        taskId = arrayList.get(0).task_id_Tx;
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);
    }

    private void SetValues() {


        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }


        pending_task_list_issue.setText(task_list_issue.toUpperCase(Locale
                .getDefault()));


        pending_task_list_address.setText(task_list_address);

        pending_task_detail_contactPersonName
                .setText(task_detail_contactPersonName);

        pending_task_detail_start_endTime.setText(task_detail_start_endTime);

        totalTime.setText(strTotalTime);

        taskDetail.setText(strTaskDetailTextView);

        taskProduct.setText(strTaskProductTextView);
        taskNote.setText(strTaskNoteTextView);
        taskNextStep.setText(strTasknextstepTextView);

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pending_task_list_phone.setText(toPersianNumber(task_list_phone));
        } else {
            pending_task_list_phone.setText(task_list_phone);
        }

        SetListView();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.backButtonImage:
                // mActivity.popFragments();

                break;

            case R.id.taskEndButton:

//                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                SharedPreferences.Editor edit = sharedPref.edit();
//                edit.putBoolean("is_start_task", false);
//                edit.commit();

                ConstantFunction.savestatus(getActivity(), AppConstants.task_Id,
                        "0");


                ConstantFunction.savestatus(getActivity(), "sales_Task",
                        "0");

                ConstantFunction.savestatus(getActivity(),
                        "is_sales_task", "0");
                ConstantFunction.savestatus(getActivity(),
                        "sales_task_id", "0");

                sqlcon.open();
                arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

                if (arrSalesTask.size() > 0 && arrSalesTask != null) {
                    sqlcon.open();
                    sqlcon.SalesDataDeletedById();
                }


                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                LocationManager manager = (LocationManager) getActivity()
                        .getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps(getActivity());
                } else {

                    internetConnection = new ConnectionDetector(getActivity());
                    if (internetConnection.isConnectingToInternet("")) {
                        EndMission();
                    } else {
                        sqlcon.open();
                        sqlcon.addSalesTask(arrayList.get(0));

                        try {

                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.suc_uploaded),
                                    Toast.LENGTH_SHORT);

                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                            mActivity.ClearAllPages();
                            // mActivity.popFragments();
                            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                    new AppTab_Dashboard_Home(), true, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.v("TTT", "task is updated with compelete");
                    }


                }
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }

    }

    public void buildAlertMessageNoGps(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(mActivity.getString(R.string.gps_enable))
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                mActivity
                                        .startActivity(new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                // turnGPSOn();
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                dialog.cancel();
                                mActivity.finish();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    private void EndMission() {

//        sqlcon.open();
//        if (offlineSelectedTask != null) {
//            boolean isUpdated = sqlcon.pendingTaskByIdUpdate(offlineSelectedTask);
//
//            Log.v("TTT", "task is updated with compelete");
//        }

        //Get All sales task location
        sqlcon.open();
        arrAddSalesTask = sqlcon.GetSalesLocation();


        JSONObject requestObject = new JSONObject();
        try {
            if (arrayList.get(0).client_id.length() == 0) {
                requestObject.put("client_id", "0");
                requestObject.put("client_name", arrayList.get(0).SALES_client_name_new + "");
                requestObject.put("client_address", arrayList.get(0).SALES_client_address + "");
                requestObject.put("contact_person", arrayList.get(0).SALES_contact_person + "");
                requestObject.put("tel_number", arrayList.get(0).tel_number + "");
                requestObject.put("client_email", arrayList.get(0).client_email + "");
                requestObject.put("latitude", ConstantValues.latitude + "");
                requestObject.put("longtitude", ConstantValues.longitude + "");
                requestObject.put("companyid", arrayList.get(0).company_id + "");


            } else {
                requestObject.put("client_id", arrayList.get(0).client_id + "");
            }
            requestObject.put("emp_task_id", et_id + "");
            requestObject.put("task_id", arrayList.get(0).task_id + "");
            requestObject.put("supervisor_id", arrayList.get(0).supervisor_id + "");
            requestObject.put("category_id", arrayList.get(0).category_id + "");
            requestObject.put("next_steps", arrayList.get(0).next_steps + "");
            requestObject.put("notes", arrayList.get(0).notes + "");
            requestObject.put("product", arrayList.get(0).product + "");
            requestObject.put("detail", arrayList.get(0).detail + "");
            requestObject.put("department", ConstantFunction.getstatus(getActivity(), "department"));
            requestObject.put("emp_id", ConstantFunction.getstatus(getActivity(), "UserId"));

            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));
            requestObject.put("task_name", arrayList.get(0).task_name + "");
            requestObject.put("additional_instruction", arrayList.get(0).additional_instruction + "");
            requestObject.put("tasktype", arrayList.get(0).tasktype + "");

            requestObject.put("task_start_time", arrayList.get(0).startTime + "");
            requestObject.put("task_endtime", arrayList.get(0).endTime + "");

            String encodedString = "";

            String userId = ConstantFunction.getstatus(getActivity(), "UserId");
            CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));

            for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
                String strImage = GetStringFromBytes(CustomListViewValuesArr
                        .get(i).getTakenPhoto());
                encodedString += new StringBuilder(String.valueOf(strImage))
                        .append(",").toString();
            }

            requestObject.put("dept_imagecount", CustomListViewValuesArr.size());
            requestObject.put("dept_images", encodedString);

            //send location arrary to server of add sales task...........
            if (arrayList != null && arrayList.size() > 0 && arrayList.get(0).task_id == null || arrayList.get(0).task_id.equals("0")) {
                if (arrAddSalesTask.size() > 0 && arrAddSalesTask != null) {
                    try {
                        JSONArray arrayObj = new JSONArray();
                        for (int r = 0; r < arrAddSalesTask.size(); r++) {

                            JSONObject obj = new JSONObject();

                            String uniqueId = arrAddSalesTask.get(r).unique_id;
                            obj.put("lat", arrAddSalesTask.get(r).latitude);
                            obj.put("long", arrAddSalesTask.get(r).longitude);
                            arrayObj.put(obj);

                            sqlcon.open();
                            sqlcon.deleteSalesLocationByTransId(uniqueId);

                        }

                        requestObject.put("geopoints", arrayObj);

                        Log.v("Add sales", "arrayObj = " + arrayObj);

                    } catch (JSONException e) {
                        Log.d("Exception", e.toString());
                        e.printStackTrace();
                    }
                }
            }

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.AddSalesENDTaskURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_Task_Final_Fragment :- " + ConstantValues.AddSalesENDTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();

    }

    public void GetSavedItems() {

        sqlcon.open();
        allSales = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

        task_list_id = ConstantValues.task_list_id;

        if (TextUtils.isEmpty(arrayList.get(0).task_name) && !TextUtils.isEmpty(allSales.get(0).task_name))
            arrayList.get(0).task_name = allSales.get(0).task_name;

        if (TextUtils.isEmpty(arrayList.get(0).category_Name) && !TextUtils.isEmpty(allSales.get(0).category_Name))
            arrayList.get(0).category_Name = allSales.get(0).category_Name;

        if (TextUtils.isEmpty(arrayList.get(0).tel_number) && !TextUtils.isEmpty(allSales.get(0).tel_number))
            arrayList.get(0).tel_number = allSales.get(0).tel_number;

        if (TextUtils.isEmpty(arrayList.get(0).SALES_client_address) && !TextUtils.isEmpty(allSales.get(0).SALES_client_address))
            arrayList.get(0).SALES_client_address = allSales.get(0).SALES_client_address;

        if (TextUtils.isEmpty(arrayList.get(0).SALES_contact_person) && !TextUtils.isEmpty(allSales.get(0).SALES_contact_person))
            arrayList.get(0).SALES_contact_person = allSales.get(0).SALES_contact_person;


        task_list_issue = arrayList.get(0).task_name.toUpperCase() + " (" + arrayList.get(0).category_Name + ")";

        task_list_phone = arrayList.get(0).tel_number;
        task_list_address = arrayList.get(0).SALES_client_address;
        task_detail_contactPersonName = getString(R.string.contact_name)
                + " : " + arrayList.get(0).SALES_contact_person;


        StringBuilder t1 = new StringBuilder();
        StringBuilder t2 = new StringBuilder();


        startTime = arrayList.get(0).startTime;
        endTime = arrayList.get(0).endTime;

        if (startTime == null || endTime == null || startTime.equals("") || endTime.equals("")) {
            if (allSales != null && allSales.size() > 0) {
                if (allSales.get(0).startTime != null && !allSales.get(0).startTime.equals("")) {
                    startTime = allSales.get(0).startTime;
                    arrayList.get(0).startTime = allSales.get(0).startTime;
                }

                if (allSales.get(0).endTime != null && !allSales.get(0).endTime.equals("")) {
                    endTime = allSales.get(0).endTime;
                    arrayList.get(0).endTime = allSales.get(0).endTime;
                }
            }
        }

        if (startTime != null && endTime != null && !startTime.equals("") && !endTime.equals("")) {
            String StartTime = ConstantFunction.GetSimpleTimeFormat24(startTime);
            String StopTime = ConstantFunction.GetSimpleTimeFormat24(endTime);

            try {


                Log.v("TTT", "startTime = " + StartTime);
                Log.v("TTT", "endTime = " + StopTime);

                String st[] = StartTime.split(" ");
                String et[] = StopTime.split(" ");

                String stTime[] = st[0].split(":");
                String etTime[] = et[0].split(":");

                selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                if (selectedLanguage.equalsIgnoreCase("")) {
                    selectedLanguage = "en";
                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {

                    stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                    String dtStart = stTime[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SolarCalendar sc = new SolarCalendar(date);
                        String s = sc.date + "/" +
                                sc.month + "/" + sc.year;


                        Log.v("TTT", "ssssss = " + s);

                        t1.append(toPersianNumber(s));
                        t1.append(":");
                        t1.append(toPersianNumber(stTime[1]));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t1.append(toPersianNumber(stTime[0]));
                        t1.append(":");
                        t1.append(toPersianNumber(stTime[1]));
                        e.printStackTrace();
                    }


                } else {

                    stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                    String dtStart = stTime[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                        String s = dateformat.format(date);
                        System.out.println("Current Date Time : " + s);

                        Log.v("TTT", "ssssss = " + s);

                        t1.append(s);
                        t1.append(":");
                        t1.append(stTime[1]);

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t1.append(stTime[0]);
                        t1.append(":");
                        t1.append(stTime[1]);
                        e.printStackTrace();
                    }


                }

                if (selectedLanguage.equalsIgnoreCase("pr")) {

                    etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                    String dtStart = etTime[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SolarCalendar sc = new SolarCalendar(date);
                        String s = sc.date + "/" +
                                sc.month + "/" + sc.year;

                        Log.v("TTT", "ssssss = " + s);

                        t2.append(toPersianNumber(s));
                        t2.append(":");
                        t2.append(toPersianNumber(etTime[1]));

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t2.append(toPersianNumber(etTime[0]));
                        t2.append(":");
                        t2.append(toPersianNumber(etTime[1]));
                        e.printStackTrace();
                    }


                } else {

                    etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                    String dtStart = etTime[0];
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(dtStart);
                        System.out.println(date);

                        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                        String s = dateformat.format(date);
                        System.out.println("Current Date Time : " + s);


                        Log.v("TTT", "ssssss = " + s);

                        t2.append(s);
                        t2.append(":");
                        t2.append(etTime[1]);

                    } catch (ParseException e) {
                        // TODO Auto-generated catch block

                        t2.append(etTime[0]);
                        t2.append(":");
                        t2.append(etTime[1]);
                        e.printStackTrace();
                    }


                }


//            if (st[1].equalsIgnoreCase("am")) {
//                t1.append(" " + getResources().getString(R.string.am));
//            } else {
//                t1.append(" " + getResources().getString(R.string.pm));
//            }
//
//            if (et[1].equalsIgnoreCase("am")) {
//                t2.append(" " + getResources().getString(R.string.am));
//            } else {
//                t2.append(" " + getResources().getString(R.string.pm));
//            }

                StartTime = t1.toString();
                StopTime = t2.toString();

            } catch (Exception e) {
                e.printStackTrace();

                StartTime = t1.toString();
                StopTime = t2.toString();
            }


//		startTime = pendingTask.getStartTime();
//		endTime = pendingTask.getStopTime();

            String formatedString = "Start : %s - Stop : %s";


            task_detail_start_endTime = getResources().getString(R.string.Start) + " " + StartTime + getResources().getString(R.string.stop) + " " + StopTime;


            String temptotalTime = TimeCalculation(
                    arrayList.get(0).startTime,
                    arrayList.get(0).endTime);

            strTotalTime = "";

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                strTotalTime = getString(R.string.total_time) + " "
                        + toPersianNumber(temptotalTime);
            } else {
                strTotalTime = getString(R.string.total_time) + " "
                        + temptotalTime;
            }

        }

        if (TextUtils.isEmpty(arrayList.get(0).detail) && !TextUtils.isEmpty(allSales.get(0).detail))
            arrayList.get(0).detail = allSales.get(0).detail;

        if (TextUtils.isEmpty(arrayList.get(0).product) && !TextUtils.isEmpty(allSales.get(0).product))
            arrayList.get(0).product = allSales.get(0).product;

        if (TextUtils.isEmpty(arrayList.get(0).notes) && !TextUtils.isEmpty(allSales.get(0).notes))
            arrayList.get(0).notes = allSales.get(0).notes;

        if (TextUtils.isEmpty(arrayList.get(0).next_steps) && !TextUtils.isEmpty(allSales.get(0).next_steps))
            arrayList.get(0).next_steps = allSales.get(0).next_steps;


        strTaskDetailTextView = arrayList.get(0).detail;
        strTaskProductTextView = arrayList.get(0).product;
        strTaskNoteTextView = arrayList.get(0).notes;
        strTasknextstepTextView = arrayList.get(0).next_steps;
    }

    @SuppressLint("SimpleDateFormat")
    private String TimeCalculation(String strStartTime, String strEndTime) {

        strStartTime = ConstantFunction.GetTimeFormat(strStartTime);
        strEndTime = ConstantFunction.GetTimeFormat(strEndTime);
        java.util.Date date1 = null;
        java.util.Date date2 = null;

        java.text.DateFormat df = new java.text.SimpleDateFormat("hh:mm a");

        try {
            date1 = df.parse(strStartTime);
            date2 = df.parse(strEndTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long diff = date2.getTime() - date1.getTime();

        long timeInSeconds = diff / 1000;
        long Hours, Mins;
        Hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (Hours * 3600);
        Mins = timeInSeconds / 60;

        String takenTime = "";
        if (Hours <= 0) {
            if (Mins <= 1)
                takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
            else
                takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
        } else {
            if (Hours == 1)
                takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hour);
            else
                takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hours);
            if (Mins != 0) {
                if (Mins <= 1)
                    takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
                else
                    takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
            }
        }
        return takenTime;
    }

    Bitmap getImageView(byte[] image) {
        Bitmap bm = null;
        bm = BitmapFactory.decodeByteArray(image, 0, image.length);
        strSignature = ConstantFunction.BitMapToString(bm);
        return bm;

    }

    public String GetStringFromBytes(byte[] image) {
        String byteSyting = "";
        Bitmap bm = null;
        bm = BitmapFactory.decodeByteArray(image, 0, image.length);
        byteSyting = ConstantFunction.BitMapToString(bm);
        return byteSyting;
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Sales_Task_Final_Fragment :- " + ConstantValues.AddSalesENDTaskURL + " -------------" + response);
//        writeToFile(response, getActivity());
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                ConstantFunction.savestatus(getActivity(), AppConstants.et_id,
                        "0");

                if (result_code.equalsIgnoreCase("1")) {
                    try {

                        sqlcon.open();
                        sqlcon.deleteAddSalesAllRecord();

                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.suc_uploaded),
                                Toast.LENGTH_SHORT);

                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();


                        mActivity.ClearAllPages();
                        // mActivity.popFragments();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                new AppTab_Dashboard_Home(), true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    sendFinishTaskStatus();
                } else if (result_code.equalsIgnoreCase("18")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_18));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("17")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (status.equalsIgnoreCase("failed")) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
                MowomLogFile.writeToLog(e);
//                Toast toast = Toast.makeText(getActivity(),
//                        getString(R.string.please_try_again),
//                        Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();

            }
        } else {

        }

    }

    public String getStartTime() {
        Calendar cStart = null;
        String taskStartTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        taskStartTime = sdf.format(cStart.getTime());
        return taskStartTime;
    }

    private void writeToFile(String data, Context context) {
//        ftuyftgyufcyu
//
//        File file = getFileStreamPath("test.txt");

        try {
//            if (!file.exists()) {
//                file.createNewFile();
//            }
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("Test.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void sendFinishTaskStatus() {

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("emp_work_status", "0");
            requestObject.put("start_date_time", "");

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }
        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ListOfChangeStatus);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_Task_Final_Fragment :- " + ConstantValues.ListOfChangeStatus + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                mProgressHUD.dismiss();
                Log.e("Response==>", response);
                MowomLogFile.writeToLog("\n" + "Sales_Task_Final_Fragment :- " + ConstantValues.ListOfChangeStatus + " -------------" + response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {

                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
//                            Toast toast = Toast.makeText(getActivity(),
//                                    getString(R.string.please_try_again),
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
//                            Toast toast = Toast.makeText(getActivity(),
//                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        MowomLogFile.writeToLog(e);
//                        Toast toast = Toast.makeText(getActivity(),
//                                getString(R.string.please_try_again),
//                                Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();

                    }
                } else {

                }

            }
        };
        wsHelper.execute();
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }

            }
        }
    }

}
