package com.asrapps.mowom.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.MapMenuDialog;
import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.Pending_Task_List_Adapter;
import com.asrapps.mowom.adapter.SizeDropDownAdapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.CurrentLocation;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.interfaces.OnDoneClick;
import com.asrapps.mowom.model.Alerts;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.utils.MowomLogFile;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

//import android.support.v7.app.AlertDialog;

public class Pending_Task_List_Fragment extends BaseFragment implements
        AsyncResponseListener, OnCancelListener, OnClickListener {

    ListView pending_task_list_listView;
    TextView noPendingTaskTxtViw, tvRecuringType;//txtHeaderUserName
    ImageView headerUserImage, backButtonImage;
    private ProgressHUD mProgressHUD;

    Pending_Task_List_Adapter adapter;
    public Pending_Task_List_Fragment CustomListView = null;
    Resources res;

    int count = 0;
    private LatLng fromLocation;

    SharedPreferences sharedPref;
    ConnectionDetector internetConnection;

    CurrentLocation crntLocation;

    private ArrayList<Login> mLogin;
    SQLController sqlcon;

    public static String taskId = "";
    public ArrayList<PendingTask> CustomListViewValuesArr, CustomListViewValuesArrAll = new ArrayList<PendingTask>();
    public ArrayList<PendingTask> customRecuringTypeArr = new ArrayList<PendingTask>();
    private ArrayList<String> reportString = new ArrayList<>();
    private PopupWindow recurringPopUpWindow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pending_task_list_layout,
                container, false);

        crntLocation = new CurrentLocation(getActivity());
        crntLocation.GetCurrentLocation();

        sqlcon = new SQLController(getActivity());


		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/
        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
//		headerUserImage.setOnClickListener(this);

        pending_task_list_listView = (ListView) view
                .findViewById(R.id.pending_task_list_listView);
        backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

        tvRecuringType = (TextView) view.findViewById(R.id.tvRecuringType);
        tvRecuringType.setOnClickListener(this);

        reportString.add(getString(R.string.daily));
        reportString.add(getString(R.string.weekly));
        reportString.add(getString(R.string.monthly));

        noPendingTaskTxtViw = (TextView) view
                .findViewById(R.id.noPendingTaskTxtViw);
        noPendingTaskTxtViw.setVisibility(View.GONE);
        res = getResources();
        internetConnection = new ConnectionDetector(getActivity());
//        if (internetConnection
//                .isConnectingToInternet(getString(R.string.check_connection))) {
        if (internetConnection
                .isConnectingToInternet("")) {

            SetUserDetails();
            GetPendingTasks();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            new GetPendingTaskListDb().execute();
        }
        return view;
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetPendingTasks() {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            //String id = "1";
            requestObject.put("id", id);
            requestObject.put("list_count", count);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in " + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.PendingTaskListURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Pending_Task_List_Fragment :- " + ConstantValues.PendingTaskListURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Pending_Task_List_Fragment :- " + ConstantValues.PendingTaskListURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String result_code = job.getString("code");
                String status = job.getString("status");

                PendingTask pendingTasks = new PendingTask(response);
                if (result_code.equals("1")) {
                    if (pendingTasks.isSuccess()) {
//					adapter = new Pending_Task_List_Adapter(getActivity(),
//							pendingTasks.getPendingListTasks(),
//							new OnDoneClick() {
//								@Override
//								public void onClickMap(View v, int position, double lat, double lng) {
//
//									fromLocation = new LatLng(ConstantValues.latitude, ConstantValues.longitude);
//
//									getMenuPopUp(lat, lng);
//								}
//
//								@Override
//								public void onClickCall(View v, int position, String phoneNumber) {
//									CallThePerson(phoneNumber);
//								}
//
//								@Override
//								public void onClickMenu(View v, int position, PendingTask pendingTask) {
//									MenuPage(pendingTask);
//
//								}
//
//								@Override
//								public void onClickView(View v, int position,PendingTask pendingTask) {
//								}
//
//								@Override
//								public void onAcceptClick(View v, int position,
//										Alerts alerts) {
//									// TODO Auto-generated method stub
//
//								}
//
//								@Override
//								public void onRejectClick(View v, int position,
//										Alerts alerts) {
//									// TODO Auto-generated method stub
//
//								}
//							});

                        if (pendingTasks.getPendingListTasks().size() > 0) {

                            noPendingTaskTxtViw.setVisibility(View.GONE);
                            pending_task_list_listView.setVisibility(View.VISIBLE);
//                            pending_task_list_listView.setVisibility(View.GONE);

//                        pending_task_list_listView.setAdapter(adapter);

                            sqlcon.open();
                            sqlcon.taskDeleteByUser(ConstantFunction.getstatus(getActivity(), "UserId") + "", "1");

                            for (int i = 0; i < pendingTasks.getPendingListTasks().size(); i++) {
                                taskId = pendingTasks.getPendingListTasks().get(i).getTaskId();
                                sqlcon.open();
                                sqlcon.addPendingTasList(pendingTasks.getPendingListTasks().get(i));
                                Log.v("pendingList", pendingTasks.getPendingListTasks().size() + "");
                            }

                        } else {
                            noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                            pending_task_list_listView.setVisibility(View.GONE);
                        }


//					if (pendingTasks.getPendingListTasks().size() > 0)
//						pending_task_list_listView.setAdapter(adapter);
//					else
//						noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                    }
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                            pending_task_list_listView.setVisibility(View.GONE);

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {

                        noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                        pending_task_list_listView.setVisibility(View.GONE);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                    pending_task_list_listView.setVisibility(View.GONE);

                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
                noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                pending_task_list_listView.setVisibility(View.GONE);


                Toast toast = Toast.makeText(getActivity(), getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            noPendingTaskTxtViw.setVisibility(View.VISIBLE);
            pending_task_list_listView.setVisibility(View.GONE);


            Toast toast = Toast.makeText(getActivity(), getString(R.string.please_try_again),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        new GetPendingTaskListDb().execute();

    }

    private class GetPendingTaskListDb extends AsyncTask<Void, Void, Void> {


        int i = 0;

        public GetPendingTaskListDb() {
        }

        @Override
        public void onPreExecute() {
//            super.onPreExecute();
            i = 0;

        }

        @Override
        public Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                CustomListViewValuesArrAll = sqlcon.getPendingTask(ConstantFunction.getstatus(getActivity(), "UserId") + "", "1");
                Log.v("list of pending task", CustomListViewValuesArrAll.size() + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (CustomListViewValuesArrAll.size() > 0 && CustomListViewValuesArrAll != null) {
                CustomListViewValuesArr = new ArrayList<>();

                for (int i = 0; i < CustomListViewValuesArrAll.size(); i++) {
                    CustomListViewValuesArr.add(CustomListViewValuesArrAll.get(i));
                }

                adapter = new Pending_Task_List_Adapter(getActivity(),
                        CustomListViewValuesArr,
                        new OnDoneClick() {
                            @Override
                            public void onClickMap(View v, int position, ArrayList<ClientAddress> clientAddresses) {

                                if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                                    fromLocation = new LatLng(ConstantValues.latitude, ConstantValues.longitude);
                                }

                                getMenuPopUp(clientAddresses);
                            }

                            @Override
                            public void onClickCall(View v, int position, String phoneNumber) {
                                CallThePerson(phoneNumber);
                            }

                            @Override
                            public void onClickMenu(View v, int position, PendingTask pendingTask) {
                                MenuPage(pendingTask);

                            }

                            @Override
                            public void onClickView(View v, int position, PendingTask pendingTask) {
                            }

                            @Override
                            public void onAcceptClick(View v, int position,
                                                      Alerts alerts) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onRejectClick(View v, int position,
                                                      Alerts alerts) {
                                // TODO Auto-generated method stub
                            }
                        });

                pending_task_list_listView.setAdapter(adapter);
                noPendingTaskTxtViw.setVisibility(View.GONE);
                pending_task_list_listView.setVisibility(View.VISIBLE);

            } else {
                //offline no any pending task.
                noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                pending_task_list_listView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.tvRecuringType:
                showReportDialog();
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }

    }

    private void showReportDialog() {
        LayoutInflater layoutInflater = (LayoutInflater)
                mActivity.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.report_dropdown_popup, null);

        recurringPopUpWindow = new PopupWindow(view);

        int width = tvRecuringType.getWidth();
        if (width > 0) {
            recurringPopUpWindow.setWidth(width);
        } else {
            recurringPopUpWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        recurringPopUpWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        recurringPopUpWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00000000")));
        recurringPopUpWindow.setOutsideTouchable(true);
        recurringPopUpWindow.setTouchable(true);
        recurringPopUpWindow.setFocusable(true);

        SizeDropDownAdapter sizeDropDownAdapter = new SizeDropDownAdapter(mActivity, reportString);

        final ListView list = (ListView) view.findViewById(R.id.listView);
        list.setAdapter(sizeDropDownAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    tvRecuringType.setText(getString(R.string.daily));
                    new GetPendingTaskListDb().execute();
                    recurringPopUpWindow.dismiss();
                } else {
                    try {
                        if (position == 1) {
                            tvRecuringType.setText(getString(R.string.weekly));
                            sqlcon.open();
                            customRecuringTypeArr = sqlcon.getPendingTaskByRecuring(ConstantFunction.getstatus(getActivity(), "UserId") + "", "1", 0);
                        } else if (position == 2) {
                            tvRecuringType.setText(getString(R.string.monthly));
                            sqlcon.open();
                            customRecuringTypeArr = sqlcon.getPendingTaskByRecuring(ConstantFunction.getstatus(getActivity(), "UserId") + "", "1", 1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (customRecuringTypeArr.size() > 0 && customRecuringTypeArr != null) {

                        adapter = new Pending_Task_List_Adapter(getActivity(),
                                customRecuringTypeArr,
                                new OnDoneClick() {

                                    @Override
                                    public void onClickMap(View v, int position, ArrayList<ClientAddress> client_address) {
                                        if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                                            fromLocation = new LatLng(ConstantValues.latitude, ConstantValues.longitude);
                                        }
                                        getMenuPopUp(client_address);
                                    }
//                                    @Override
//                                    public void onClickMap(View v, int position, double lat, double lng) {
//                                        if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
//                                            fromLocation = new LatLng(ConstantValues.latitude, ConstantValues.longitude);
//                                        }
//                                        getMenuPopUp(lat, lng);
//                                    }

                                    @Override
                                    public void onClickCall(View v, int position, String phoneNumber) {
                                        CallThePerson(phoneNumber);
                                    }

                                    @Override
                                    public void onClickMenu(View v, int position, PendingTask pendingTask) {
                                        MenuPage(pendingTask);

                                    }

                                    @Override
                                    public void onClickView(View v, int position, PendingTask pendingTask) {
                                    }

                                    @Override
                                    public void onAcceptClick(View v, int position,
                                                              Alerts alerts) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void onRejectClick(View v, int position,
                                                              Alerts alerts) {
                                        // TODO Auto-generated method stub

                                    }
                                });

                        pending_task_list_listView.setVisibility(View.VISIBLE);
                        noPendingTaskTxtViw.setVisibility(View.GONE);
                        pending_task_list_listView.setAdapter(adapter);

                    } else {
                        pending_task_list_listView.setVisibility(View.GONE);
                        noPendingTaskTxtViw.setVisibility(View.VISIBLE);
                    }
                    recurringPopUpWindow.dismiss();
                }
            }
        });

        recurringPopUpWindow.showAsDropDown(tvRecuringType, 0, 2);
    }

    private void getMenuPopUp(ArrayList<ClientAddress> client_address) {
        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        MapMenuDialog dialog = new MapMenuDialog();
        if (fromLocation != null) {
            dialog.setLatLong(client_address, fromLocation.latitude, fromLocation.longitude);
        } else {
            dialog.setLatLong(client_address, 0.0, 0.0);

        }
        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");
    }

    private void CallThePerson(String phoneNumber) {
        Intent intentcall = new Intent();
        intentcall.setAction(Intent.ACTION_CALL);
        intentcall.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intentcall);
    }

    private void MenuPage(PendingTask pendingTask) {
        //Toast.makeText(getActivity(), "pendingTask.getTaskId()==> "+pendingTask.getTaskId(), Toast.LENGTH_SHORT).show();

        Log.v("UpdaterService", "Menu click = " + UpdaterService.inProcess + " UpdaterService.current  = " + UpdaterService.currentTime);

        sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
        String task_id = ConstantFunction.getuser(getActivity(), AppConstants.task_Id);
        String et_id = ConstantFunction.getuser(getActivity(), AppConstants.et_id);

        if (pendingTask != null && !pendingTask.getTaskId().equals(task_id) &&
                !TextUtils.isEmpty(task_id) && !task_id.equals("0") && !pendingTask.getEt_id().equals(et_id) &&
                !TextUtils.isEmpty(et_id) && !et_id.equals("0"))// &&
        //        sharedPref.getBoolean("is_start_task", true))
        {
            try {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                alertDialogBuilder.setMessage(getString(R.string.err_start_task));
                alertDialogBuilder.setCancelable(false);

                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();

                        mActivity.ClearAllPages();
                        // mActivity.popFragments();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                new AppTab_Dashboard_Home(), true, true);

                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            } catch (Exception e) {
                e.printStackTrace();

            }

        } else {

            if (pendingTask != null && pendingTask.getTaskId().equals(task_id)) {

                UpdaterService.inProcess = true;

                Pending_Task_Start_Fragment detailFragment = new Pending_Task_Start_Fragment();
                //detailFragment.setTaskId(pendingTask.getTaskId());
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        detailFragment, true, true);
            } else {
                Pending_Task_Details_Fragment detailFragment = new Pending_Task_Details_Fragment();
                detailFragment.setTaskId(pendingTask.getTaskId());
                detailFragment.setEtId(pendingTask.getEt_id());
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        detailFragment, true, true);
            }
        }
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }
}
