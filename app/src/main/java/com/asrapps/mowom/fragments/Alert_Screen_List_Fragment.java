package com.asrapps.mowom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.Reject_Task_Dialog;
import com.asrapps.mowom.adapter.Alert_List_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.interfaces.OnDoneClick;
import com.asrapps.mowom.model.Alerts;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class Alert_Screen_List_Fragment extends BaseFragment implements
        AsyncResponseListener, OnCancelListener, OnClickListener {

    TextView noAlertsTxtViw;//txtHeaderUserName
    ImageView headerUserImage, backButtonImage;

    private ProgressHUD mProgressHUD;
    Alert_List_Adapter adapter;
    public Pending_Task_List_Fragment CustomListView = null;
    public ArrayList<Alerts> CustomListViewValuesArr = new ArrayList<Alerts>();

    ListView alert_list_listView;

    SQLController sqlcon;
    private ArrayList<Login> mLogin;

    ConnectionDetector internetConnection;

    int listPosition = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alert_screen_list_layout,
                container, false);

		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/
        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);

//headerUserImage.setOnClickListener(this);

        alert_list_listView = (ListView) view
                .findViewById(R.id.alert_list_listView);
        backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

        noAlertsTxtViw = (TextView) view.findViewById(R.id.noAlertsTxtViw);
        noAlertsTxtViw.setVisibility(View.GONE);

        sqlcon = new SQLController(getActivity());

        internetConnection = new ConnectionDetector(getActivity());
        if (internetConnection
                .isConnectingToInternet(getString(R.string.check_connection))) {

            SetUserDetails();
            GetService(ConstantValues.AlertScreenURL, "");
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();

            noAlertsTxtViw.setVisibility(View.VISIBLE);
            alert_list_listView.setVisibility(View.GONE);
        }

        return view;
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetService(String url, String task_id) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            // String id = "1";
            if (url.equalsIgnoreCase(ConstantValues.AlertScreenURL)) {
                requestObject.put("id", id);
            }

            if (url.equalsIgnoreCase(ConstantValues.AcceptMissionURL)) {
                requestObject.put("task_id", task_id);
                requestObject.put("emp_id", id);
            }

        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(url);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Alert_Screen_List_Fragment :- " + ConstantValues.AlertScreenURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {

        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        if (!response.equalsIgnoreCase("")) {

            if (requestURL.equalsIgnoreCase(ConstantValues.AlertScreenURL)) {
                MowomLogFile.writeToLog("\n" + "Alert_Screen_List_Fragment :- " + ConstantValues.AlertScreenURL + " -------------" + response);
                try {

                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    Alerts alerts = new Alerts(response);
                    if (alerts.isSuccess()) {
                        CustomListViewValuesArr = alerts.getAlertList();
                        adapter = new Alert_List_Adapter(getActivity(),
                                CustomListViewValuesArr, new OnDoneClick() {

                            @Override
                            public void onClickMenu(View v,
                                                    int position,
                                                    PendingTask pendingTask) {
                                // TODO Auto-generated method stub

                            }


                            @Override
                            public void onClickMap(View v, int position, ArrayList<ClientAddress> client_address) {

                            }

                            @Override
                            public void onClickCall(View v,
                                                    int position, String phoneNumber) {
                                // TODO Auto-generated method stub

                            }

                            @Override
                            public void onClickView(View v,
                                                    int position,
                                                    PendingTask pendingTask) {

                            }

                            @Override
                            public void onAcceptClick(View v,
                                                      int position, Alerts alerts) {
                                // TODO Auto-generated method stub
                                listPosition = position;
                                AcceptMission(alerts);
                            }

                            @Override
                            public void onRejectClick(View v,
                                                      int position, Alerts alerts) {
                                // TODO Auto-generated method stub
                                RejectMission(alerts, position);
                            }
                        });
                        if (alerts.getAlertList().size() > 0) {
                            alert_list_listView.setAdapter(adapter);
                            noAlertsTxtViw.setVisibility(View.GONE);
                            alert_list_listView.setVisibility(View.VISIBLE);
                        } else {
                            noAlertsTxtViw.setVisibility(View.VISIBLE);
                            alert_list_listView.setVisibility(View.GONE);
                        }
                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("400")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status);
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (result_code.equalsIgnoreCase("402")) {
                        try {

                            noAlertsTxtViw.setVisibility(View.VISIBLE);
                            alert_list_listView.setVisibility(View.GONE);

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status);
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {

                            noAlertsTxtViw.setVisibility(View.VISIBLE);
                            alert_list_listView.setVisibility(View.GONE);

                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                        noAlertsTxtViw.setVisibility(View.VISIBLE);
                        alert_list_listView.setVisibility(View.GONE);

                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                } catch (Exception e) {

                    noAlertsTxtViw.setVisibility(View.VISIBLE);
                    alert_list_listView.setVisibility(View.GONE);

                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Try again", false);
                }
            } else if (requestURL
                    .equalsIgnoreCase(ConstantValues.AcceptMissionURL)) {

                MowomLogFile.writeToLog("\n" + "Alert_Screen_List_Fragment :- " + ConstantValues.AcceptMissionURL + " -------------" + response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {
                                Toast toast = Toast.makeText(getActivity(),
                                        getResources().getString(R.string.mission_accepted),
                                        Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                CustomListViewValuesArr.remove(listPosition);
                                adapter.notifyDataSetChanged();
                                //mActivity.popFragments();
                                // JSONObject job2 = job.getJSONObject("data");


                                if (CustomListViewValuesArr != null && CustomListViewValuesArr.size() > 0) {
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }

                                    noAlertsTxtViw.setVisibility(View.GONE);
                                    alert_list_listView.setVisibility(View.VISIBLE);

                                } else {
                                    noAlertsTxtViw.setVisibility(View.VISIBLE);
                                    alert_list_listView.setVisibility(View.GONE);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {

                            noAlertsTxtViw.setVisibility(View.VISIBLE);
                            alert_list_listView.setVisibility(View.GONE);

                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again),
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } else if (result_code.equalsIgnoreCase("400")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status);
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                        ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getActivity());


                                        startActivity(new Intent(getActivity(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("402")) {
                            try {

                                noAlertsTxtViw.setVisibility(View.VISIBLE);
                                alert_list_listView.setVisibility(View.GONE);

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status);
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();
                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {

                                noAlertsTxtViw.setVisibility(View.VISIBLE);
                                alert_list_listView.setVisibility(View.GONE);

                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            noAlertsTxtViw.setVisibility(View.VISIBLE);
                            alert_list_listView.setVisibility(View.GONE);

                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } catch (Exception e) {

                        noAlertsTxtViw.setVisibility(View.VISIBLE);
                        alert_list_listView.setVisibility(View.GONE);

                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }

        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }

    }

    private void AcceptMission(Alerts alerts) {
        LocationManager manager = (LocationManager) getActivity()
                .getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps(getActivity());
        } else {
            GetService(ConstantValues.AcceptMissionURL, alerts.getTask_id());
        }

    }

    public void buildAlertMessageNoGps(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(mActivity.getString(R.string.gps_enable))
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                mActivity
                                        .startActivity(new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                // turnGPSOn();
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                dialog.cancel();
                                mActivity.finish();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void RejectMission(Alerts alerts, int pos) {
        listPosition = pos;
        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Reject_Task_Dialog dialog = new Reject_Task_Dialog();
        dialog.setTask_id(alerts.getTask_id());
        dialog.setTargetFragment(this, 1);
        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Stuff to do, dependent on requestCode and resultCode
        if (requestCode == 1) // 1 is an arbitrary number, can be any int
        {
            // This is the return result of your DialogFragment
            if (resultCode == 1) // 1 is an arbitrary number, can be any int
            {
                CustomListViewValuesArr.remove(listPosition);

                adapter.notifyDataSetChanged();
                if (CustomListViewValuesArr.size() == 0) {
                    noAlertsTxtViw.setVisibility(View.VISIBLE);
                    alert_list_listView.setVisibility(View.GONE);
                }
            }
        }
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }

            }
        }
    }

}
