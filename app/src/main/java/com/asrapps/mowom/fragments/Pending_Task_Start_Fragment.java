package com.asrapps.mowom.fragments;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.model.Login;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static com.asrapps.mowom.constants.ConstantFunction.getstatus;

public class Pending_Task_Start_Fragment extends BaseFragment implements
        OnClickListener {

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String et_id, taskId, issue;

    TextView pen_tsk_srt_id_header, startHourFirstDigit, startHourFirstDigit1, startHourSeconfDigit, startHourSeconfDigit1,
            startMinsFirstDigit, startMinsFirstDigit1, startMinsSecondDigit, startMinsSecondDigit1, endHoursFirstDigit, endHoursFirstDigit1,
            endHoursSecondDigit, endHoursSecondDigit1, endMinsFirstDigit, endMinsFirstDigit1, endMinsSecondDigit, endMinsSecondDigit1,
            total_hours;

    SQLController sqlcon;
    private ArrayList<Login> mLogin, mLogoinUser;

    String strStartTime, strEndTime;

    Button pen_tsk_srt_stop_button;
    LinearLayout total_time_layout;

    //private Handler customHandler = new Handler();
    int startHour = 0, runningHours = 0;
    int startMins = 0, runningMins = 0;
    int startSecs = 0, runningSecs = 0;
    View view;
    SimpleDateFormat sdf;
    String taskStartTime, taskStopTime;

    boolean isThreadRunning;
    ConnectionDetector internetConnection;

    BroadcastReceiver receiver;
    Intent serviceIntent;
    SharedPreferences sharedPref;


    String selectedLanguage;

    LinearLayout llEng, llPersian;

    LinearLayout llEngStart, llPerStart, llEngEnd, llPerEnd;

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    ImageView imgOne, imgTwo, imgThree, imgFour, imgOnesep, imgTwosep, imgThreesep;
    ImageView imgOne1, imgTwo1, imgThree1, imgFour1, imgOnesep1, imgTwosep1, imgThreesep1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.pending_task_start_layout,
                    container, false);

            sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
            /*
             * txtHeaderUserName = (TextView) view
			 * .findViewById(R.id.txtHeaderUserName);
			 */

            llEng = (LinearLayout) view.findViewById(R.id.llEng);
            llPersian = (LinearLayout) view.findViewById(R.id.llPersian);

            llEngStart = (LinearLayout) view.findViewById(R.id.llEngStart);
            llPerStart = (LinearLayout) view.findViewById(R.id.llPerStart);

            llEngEnd = (LinearLayout) view.findViewById(R.id.llEngEnd);
            llPerEnd = (LinearLayout) view.findViewById(R.id.llPerEnd);

            imgOne = (ImageView) view.findViewById(R.id.imgOne);
            imgTwo = (ImageView) view.findViewById(R.id.imgTwo);
            imgThree = (ImageView) view.findViewById(R.id.imgThree);
            imgFour = (ImageView) view.findViewById(R.id.imgFour);
            imgOnesep = (ImageView) view.findViewById(R.id.imgOnesep);
            imgTwosep = (ImageView) view.findViewById(R.id.imgTwosep);
            imgThreesep = (ImageView) view.findViewById(R.id.imgThreesep);

            imgOne1 = (ImageView) view.findViewById(R.id.imgOne1);
            imgTwo1 = (ImageView) view.findViewById(R.id.imgTwo1);
            imgThree1 = (ImageView) view.findViewById(R.id.imgThree1);
            imgFour1 = (ImageView) view.findViewById(R.id.imgFour1);
            imgOnesep1 = (ImageView) view.findViewById(R.id.imgOnesep1);
            imgTwosep1 = (ImageView) view.findViewById(R.id.imgTwosep1);
            imgThreesep1 = (ImageView) view.findViewById(R.id.imgThreesep1);

            String is_report = getstatus(getActivity(), "is_report");
            String is_review = getstatus(getActivity(), "is_review");
            String is_signature = getstatus(getActivity(), "is_signature");

            int cntScreen = 0;
            if (!is_report.equals("null") && is_report.equals("1")) {
                cntScreen++;
            }
            if (!is_review.equals("null") && is_review.equals("1")) {
                cntScreen++;
            }
            if (!is_signature.equals("null") && is_signature.equals("1")) {
                cntScreen++;
            }

            showFlowCounter(cntScreen);

            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
            //			headerUserImage.setOnClickListener(this);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            pen_tsk_srt_id_header = (TextView) view
                    .findViewById(R.id.pen_tsk_srt_id_header);

            startHourFirstDigit = (TextView) view
                    .findViewById(R.id.startHourFirstDigit);

            startHourSeconfDigit = (TextView) view
                    .findViewById(R.id.startHourSeconfDigit);

            startMinsFirstDigit = (TextView) view
                    .findViewById(R.id.startMinsFirstDigit);

            startMinsSecondDigit = (TextView) view
                    .findViewById(R.id.startMinsSecondDigit);


            startHourFirstDigit1 = (TextView) view
                    .findViewById(R.id.startHourFirstDigit1);

            startHourSeconfDigit1 = (TextView) view
                    .findViewById(R.id.startHourSeconfDigit1);

            startMinsFirstDigit1 = (TextView) view
                    .findViewById(R.id.startMinsFirstDigit1);

            startMinsSecondDigit1 = (TextView) view
                    .findViewById(R.id.startMinsSecondDigit1);


            endHoursFirstDigit = (TextView) view
                    .findViewById(R.id.endHoursFirstDigit);

            endHoursSecondDigit = (TextView) view
                    .findViewById(R.id.endHoursSecondDigit);

            endMinsFirstDigit = (TextView) view
                    .findViewById(R.id.endMinsFirstDigit);

            endMinsSecondDigit = (TextView) view
                    .findViewById(R.id.endMinsSecondDigit);


            endHoursFirstDigit1 = (TextView) view
                    .findViewById(R.id.endHoursFirstDigit1);

            endHoursSecondDigit1 = (TextView) view
                    .findViewById(R.id.endHoursSecondDigit1);

            endMinsFirstDigit1 = (TextView) view
                    .findViewById(R.id.endMinsFirstDigit1);

            endMinsSecondDigit1 = (TextView) view
                    .findViewById(R.id.endMinsSecondDigit1);

            total_hours = (TextView) view.findViewById(R.id.total_hours);

            pen_tsk_srt_stop_button = (Button) view
                    .findViewById(R.id.pen_tsk_srt_stop_button);
            //pen_tsk_srt_stop_button.setEnabled(false);
            pen_tsk_srt_stop_button.setOnClickListener(this);

            total_time_layout = (LinearLayout) view
                    .findViewById(R.id.total_time_layout);
            total_time_layout.setVisibility(View.GONE);
            sqlcon = new SQLController(getActivity());
            internetConnection = new ConnectionDetector(getActivity());
            if (internetConnection
                    .isConnectingToInternet("")) {
                if (UpdaterService.firstTime) {
                    DeleteItemsInDB();
                    GetCurrentTime();
                }

                SetUserDetails();
                GetFromShared();

                //customHandler.postDelayed(updateTimerThread, 1000);

                receiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String startTime = intent.getStringExtra("time");

                        Log.v("UpdateService", "o startTime = " + startTime);

                        String counter = intent.getStringExtra("counter");
                        SetRunningTime(startTime, counter);
                    }
                };
            } else {
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
                if (UpdaterService.firstTime) {
                    GetCurrentTime();
                }


                GetFromShared();

                //customHandler.postDelayed(updateTimerThread, 1000);

                receiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String startTime = intent.getStringExtra("time");

                        Log.v("UpdateService", "o startTime = " + startTime);
                        String counter = intent.getStringExtra("counter");
                        SetRunningTime(startTime, counter);
                    }
                };
            }

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                llEng.setVisibility(View.GONE);
                llPersian.setVisibility(View.VISIBLE);

//				imgOne.setImageResource(R.drawable.pr_step1_hover);
//				imgTwo.setImageResource(R.drawable.pr_step2_unhover);
//				imgThree.setImageResource(R.drawable.pr_step3_unhover);
//				imgFour.setImageResource(R.drawable.pr_step4_unhover);

                llEngStart.setVisibility(View.GONE);
                llPerStart.setVisibility(View.VISIBLE);

                llEngEnd.setVisibility(View.GONE);
                llPerEnd.setVisibility(View.VISIBLE);
            } else {
                llEng.setVisibility(View.VISIBLE);
                llPersian.setVisibility(View.GONE);

                llEngStart.setVisibility(View.VISIBLE);
                llPerStart.setVisibility(View.GONE);

                llEngEnd.setVisibility(View.VISIBLE);
                llPerEnd.setVisibility(View.GONE);

            }

        }
        return view;
    }

    private void showFlowCounter(int cntScreen) {

        imgOne.setVisibility(View.VISIBLE);
        imgTwo.setVisibility(View.VISIBLE);
        imgThree.setVisibility(View.VISIBLE);
        imgFour.setVisibility(View.VISIBLE);
        imgOnesep.setVisibility(View.VISIBLE);
        imgTwosep.setVisibility(View.VISIBLE);
        imgThreesep.setVisibility(View.VISIBLE);

        imgOne1.setVisibility(View.VISIBLE);
        imgTwo1.setVisibility(View.VISIBLE);
        imgThree1.setVisibility(View.VISIBLE);
        imgFour1.setVisibility(View.VISIBLE);
        imgOnesep1.setVisibility(View.VISIBLE);
        imgTwosep1.setVisibility(View.VISIBLE);
        imgThreesep1.setVisibility(View.VISIBLE);

        if (cntScreen == 2) {
            imgFour.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);

            imgFour1.setVisibility(View.GONE);
            imgThreesep1.setVisibility(View.GONE);
        } else if (cntScreen == 1) {
            imgFour.setVisibility(View.GONE);
            imgThree.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);
            imgTwosep.setVisibility(View.GONE);

            imgFour1.setVisibility(View.GONE);
            imgThree1.setVisibility(View.GONE);
            imgThreesep1.setVisibility(View.GONE);
            imgTwosep1.setVisibility(View.GONE);

        } else if (cntScreen == 0) {
            imgFour.setVisibility(View.GONE);
            imgThree.setVisibility(View.GONE);
            imgTwo.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);
            imgTwosep.setVisibility(View.GONE);
            imgOnesep.setVisibility(View.GONE);

            imgFour1.setVisibility(View.GONE);
            imgThree1.setVisibility(View.GONE);
            imgTwo1.setVisibility(View.GONE);
            imgThreesep1.setVisibility(View.GONE);
            imgTwosep1.setVisibility(View.GONE);
            imgOnesep1.setVisibility(View.GONE);

        }
    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */
        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetCurrentTime() {
        Calendar c = Calendar.getInstance();

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                Locale.getDefault());
        taskStartTime = sdf.format(c.getTime());

        if (startMins == 0 && startHour == 0) {
            runningMins = startMins = c.get(Calendar.MINUTE);
            runningHours = startHour = c.get(Calendar.HOUR_OF_DAY);

            strStartTime = startHour + ":" + runningMins;

            String StartHour = String.format(Locale.getDefault(), "%02d",
                    startHour);
            String StartMin = String.format(Locale.getDefault(), "%02d",
                    startMins);
        }
    }

    private void SetRunningTime(String startTime, String counter) {

        String runningHourOne = Character.toString(counter.charAt(0));
        String runningHourTwo = Character.toString(counter.charAt(1));
        String runningMinOne = Character.toString(counter.charAt(3));
        String runningMinTwo = Character.toString(counter.charAt(4));

        startHourFirstDigit.setText(String.valueOf(startTime.charAt(11)));
        startHourSeconfDigit.setText(String.valueOf(startTime.charAt(12)));
        startMinsFirstDigit.setText(String.valueOf(startTime.charAt(14)));
        startMinsSecondDigit.setText(String.valueOf(startTime.charAt(15)));

        startHourFirstDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(11))));
        startHourSeconfDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(12))));
        startMinsFirstDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(14))));
        startMinsSecondDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(15))));


        endHoursFirstDigit.setText(runningHourOne);
        endHoursSecondDigit.setText(runningHourTwo);
        endMinsFirstDigit.setText(runningMinOne);
        endMinsSecondDigit.setText(runningMinTwo);

        endHoursFirstDigit1.setText(toPersianNumber(runningHourOne));
        endHoursSecondDigit1.setText(toPersianNumber(runningHourTwo));
        endMinsFirstDigit1.setText(toPersianNumber(runningMinOne));
        endMinsSecondDigit1.setText(toPersianNumber(runningMinTwo));


    }

    private void GetFromShared() {

        taskId = ConstantFunction.getuser(getContext(), AppConstants.task_Id);
        et_id = ConstantFunction.getuser(getContext(), et_id);
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);
        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + toPersianNumber(taskId) + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        } else {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + taskId + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        }

        InserToSavedTable(taskId, issue, "", "", "", "", "", "", null);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:

                Pending_Task_List_Fragment detailFragment = new Pending_Task_List_Fragment();
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        detailFragment, true, true);
//                mActivity.popFragments();
                break;

            case R.id.pen_tsk_srt_stop_button:
//			SharedPreferences.Editor editor = sharedPref.edit();
//			editor.putString("task_id", "0");
//			editor.commit();

                if (isThreadRunning)
                    ShowCompletedWarning();
                else {

                    String is_report = getstatus(getActivity(), "is_report");
                    String is_review = getstatus(getActivity(), "is_review");
                    String is_signature = getstatus(getActivity(), "is_signature");

                    if (!is_report.equals("null") && is_report.equals("1")) {
                        Pending_Task_Photo_Activity takePhotoFragment = new Pending_Task_Photo_Activity();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                takePhotoFragment, true, true);
                    } else if (!is_review.equals("null") && is_review.equals("1")) {

                        Pending_Task_Write_Review_Fragment writeReviewFragment = new Pending_Task_Write_Review_Fragment();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                writeReviewFragment, true, true);


                    } else if (!is_signature.equals("null") && is_signature.equals("1")) {
                        Pending_Task_Signature_Fragment signatureFragment = new Pending_Task_Signature_Fragment();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                signatureFragment, true, true);

                    } else {
                        Pending_Task_Final_Fragment finalFragment = new Pending_Task_Final_Fragment();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                finalFragment, true, true);
                    }
                }

                break;

            default:
                break;
        }
    }


    private void ShowCompletedWarning() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.confirm));
        builder.setMessage(getString(R.string.finished_task));

        builder.setPositiveButton(getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        FinishTask();
                        dialog.dismiss();
                    }

                });

        builder.setNegativeButton(getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void FinishTask() {
        stopService();
        isThreadRunning = false;
        Calendar c = Calendar.getInstance();

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                Locale.getDefault());
        taskStopTime = sdf.format(c.getTime());
        taskStartTime = UpdaterService.currentTime;
        total_time_layout.setVisibility(View.VISIBLE);

        java.util.Date date1 = null;
        java.util.Date date2 = null;

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

        try {
            date1 = df.parse(taskStartTime);
            date2 = df.parse(taskStopTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long diff = date2.getTime() - date1.getTime();

        long timeInSeconds = diff / 1000;
        long Hours, Mins;
        Hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (Hours * 3600);
        Mins = timeInSeconds / 60;

        String takenTime = "";
        if (selectedLanguage.equalsIgnoreCase("en") || selectedLanguage.equalsIgnoreCase("fr")) {
            if (Hours <= 0) {
                if (Mins <= 1) {
                    takenTime = (String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                } else {

                    takenTime = (String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                }
            } else {
                if (Hours == 1)
                    takenTime = (String.valueOf(Hours)) + " " + getResources().getString(R.string.Hour);
                else
                    takenTime = (String.valueOf(Hours)) + " " + getResources().getString(R.string.Hours);
                if (Mins != 0) {
                    if (Mins == 1)
                        takenTime += " " + (String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                    else
                        takenTime += " " + (String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                }
            }
            total_hours.setText(takenTime);
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            if (Hours <= 0) {
                if (Mins <= 1) {
                    takenTime = toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                } else {

                    takenTime = toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                }
            } else {
                if (Hours == 1)
                    takenTime = toPersianNumber(String.valueOf(Hours)) + " " + getResources().getString(R.string.Hour);
                else
                    takenTime = toPersianNumber(String.valueOf(Hours)) + " " + getResources().getString(R.string.Hours);
                if (Mins != 0) {
                    if (Mins == 1)
                        takenTime += " " + toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                    else
                        takenTime += " " + toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                }
            }
            total_hours.setText(takenTime);
        }

        UpdateTable(DbHelper.SAVED_START_TIME, taskStartTime, taskId);
        UpdateTable(DbHelper.SAVED_FINISHED_TIME, taskStopTime, taskId);
        UpdateTable(DbHelper.SAVED_TOTAL_TIME_SPEND, takenTime, taskId);


        GetSavedItems(taskId);


        String is_report = getstatus(getActivity(), "is_report");
        String is_review = getstatus(getActivity(), "is_review");
        String is_signature = getstatus(getActivity(), "is_signature");

        if (!is_report.equals("null") && is_report.equals("1")) {
            Pending_Task_Photo_Activity takePhotoFragment = new Pending_Task_Photo_Activity();
            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                    takePhotoFragment, true, true);
        } else if (!is_review.equals("null") && is_review.equals("1")) {

            Pending_Task_Write_Review_Fragment writeReviewFragment = new Pending_Task_Write_Review_Fragment();
            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                    writeReviewFragment, true, true);


        } else if (!is_signature.equals("null") && is_signature.equals("1")) {
            Pending_Task_Signature_Fragment signatureFragment = new Pending_Task_Signature_Fragment();
            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                    signatureFragment, true, true);

        } else {
            Pending_Task_Final_Fragment finalFragment = new Pending_Task_Final_Fragment();
            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                    finalFragment, true, true);
        }

    }

	/*@Override
    public boolean onBackPressed() {
		boolean ret = true;
		if (total_time_layout.getVisibility() != View.VISIBLE) {
			ShowWarning();
			ret = true;
		} else {
			ret = false;

		}
		return ret;
	}*/

    public void InserToSavedTable(String taskId, String issue,
                                  String total_time_spend, String start_time, String finished_time,
                                  String task_report, String rating, String comments, byte[] signature) {
        sqlcon.open();
        sqlcon.InsertToSaveData(taskId, issue, total_time_spend, start_time,
                finished_time, task_report, rating, comments, signature);
    }

    public void DeleteItemsInDB() {
        sqlcon.open();
        sqlcon.DeleteAll(DbHelper.TABLE_SAVED_ITEMS);
    }

    private void UpdateTable(String fieldName, String updateValue, String taskId) {
        sqlcon.open();
        sqlcon.UpdateSavedTable(fieldName, updateValue, taskId);
    }

    private void GetSavedItems(String taskId) {
        sqlcon.open();
        sqlcon.GetSavedItems(taskId);
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((UpdaterService.firstTime) && (!UpdaterService.inProcess)) {
            isThreadRunning = true;
            startService();
            UpdaterService.firstTime = false;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", false);
            editor.putString("currentTime", UpdaterService.currentTime + "");

            editor.commit();


        }
        if (UpdaterService.inProcess) {
            isThreadRunning = true;
            startService();
            getActivity().registerReceiver(receiver, new IntentFilter(
                    UpdaterService.BROADCAST_ACTION));
        }
    }

    private void startService() {
        serviceIntent = new Intent(getActivity(),
                UpdaterService.class);
        getActivity().startService(serviceIntent);
        UpdaterService.inProcess = true;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("inProcess", true);
        editor.putString("currentTime", UpdaterService.currentTime + "");

        editor.commit();

        // registerReceiver(receiver, new IntentFilter(UpdaterService.BROADCAST_ACTION));

    }

    private void stopService() {
        if (UpdaterService.inProcess) {
            if (serviceIntent != null)
                getActivity().stopService(serviceIntent);
            UpdaterService.inProcess = false;
            UpdaterService.firstTime = false;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("inProcess", false);
            editor.putBoolean("firstTime", false);
            editor.putString("currentTime", UpdaterService.currentTime + "");

            editor.commit();


            getActivity().unregisterReceiver(receiver);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (UpdaterService.inProcess)
            getActivity().unregisterReceiver(receiver);
    }

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out += persianNumbers[number];
            } else if (c == '٫') {
                out += '،';
            } else {
                out += c;
            }

        }
        return out;
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }

}
