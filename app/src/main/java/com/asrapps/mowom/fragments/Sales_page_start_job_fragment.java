package com.asrapps.mowom.fragments;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.SalesTask;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by abcd on 9/20/16.
 */

public class Sales_page_start_job_fragment extends BaseFragment implements
        View.OnClickListener, DialogInterface.OnCancelListener, AsyncResponseListener {

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String taskId, issue;
    private ProgressHUD mProgressHUD;

    TextView pen_tsk_srt_id_header, startHourFirstDigit, startHourFirstDigit1, startHourSeconfDigit, startHourSeconfDigit1,
            startMinsFirstDigit, startMinsFirstDigit1, startMinsSecondDigit, startMinsSecondDigit1, endHoursFirstDigit, endHoursFirstDigit1,
            endHoursSecondDigit, endHoursSecondDigit1, endMinsFirstDigit, endMinsFirstDigit1, endMinsSecondDigit, endMinsSecondDigit1,
            total_hours;

    SQLController sqlcon;
    public ArrayList<SalesTask> arrSalesTask = new ArrayList<>();
    private ArrayList<Login> mLogin;

    String strStartTime, strEndTime;

    Button pen_tsk_srt_stop_button;
    LinearLayout total_time_layout;

    int startHour = 0, runningHours = 0;
    int startMins = 0, runningMins = 0;
    int startSecs = 0, runningSecs = 0;
    View view;
    SimpleDateFormat sdf;
    String taskStartTime, taskStopTime;

    boolean isThreadRunning;
    ConnectionDetector internetConnection;

    BroadcastReceiver receiver;
    Intent serviceIntent;
    SharedPreferences sharedPref;

    ImageView imgOne, imgTwo, imgThree, imgFour;

    String selectedLanguage;
    String startTime, counter;
    LinearLayout llEng, llPersian;
    ArrayList<SalesTask> arrayList = new ArrayList<>();

    LinearLayout llEngStart, llPerStart, llEngEnd, llPerEnd;
    Calendar cStart;

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};
    private Handler customHandler = new Handler();


    public Sales_page_start_job_fragment(ArrayList<SalesTask> arrayList) {
        super();
        this.arrayList = arrayList;
        Log.e("AAA", "ArrayList Size  cons:  " + arrayList.size());

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {


            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = inflater.inflate(R.layout.sales_task_start_pr,
                        container, false);

            } else {
                view = inflater.inflate(R.layout.sales_task_start,
                        container, false);
            }

            cStart = Calendar.getInstance();

           /* Bundle args = getArguments();
            if (args != null) {
                arrayList = args.getParcelableArrayList("arr");
                Log.e("AAA", "ArrayList Size  :  " + arrayList.size());

            }
*/
            sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
            /*
             * txtHeaderUserName = (TextView) view
			 * .findViewById(R.id.txtHeaderUserName);
			 */
            llEng = (LinearLayout) view.findViewById(R.id.llEng);
            llPersian = (LinearLayout) view.findViewById(R.id.llPersian);

            llEngStart = (LinearLayout) view.findViewById(R.id.llEngStart);
            llPerStart = (LinearLayout) view.findViewById(R.id.llPerStart);

            llEngEnd = (LinearLayout) view.findViewById(R.id.llEngEnd);
            llPerEnd = (LinearLayout) view.findViewById(R.id.llPerEnd);

            imgOne = (ImageView) view.findViewById(R.id.imgOne);
            imgTwo = (ImageView) view.findViewById(R.id.imgTwo);
            imgThree = (ImageView) view.findViewById(R.id.imgThree);
            imgFour = (ImageView) view.findViewById(R.id.imgFour);


            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
            //			headerUserImage.setOnClickListener(this);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            pen_tsk_srt_id_header = (TextView) view
                    .findViewById(R.id.pen_tsk_srt_id_header);

            startHourFirstDigit = (TextView) view
                    .findViewById(R.id.startHourFirstDigit);

            startHourSeconfDigit = (TextView) view
                    .findViewById(R.id.startHourSeconfDigit);

            startMinsFirstDigit = (TextView) view
                    .findViewById(R.id.startMinsFirstDigit);

            startMinsSecondDigit = (TextView) view
                    .findViewById(R.id.startMinsSecondDigit);


            startHourFirstDigit1 = (TextView) view
                    .findViewById(R.id.startHourFirstDigit1);

            startHourSeconfDigit1 = (TextView) view
                    .findViewById(R.id.startHourSeconfDigit1);

            startMinsFirstDigit1 = (TextView) view
                    .findViewById(R.id.startMinsFirstDigit1);

            startMinsSecondDigit1 = (TextView) view
                    .findViewById(R.id.startMinsSecondDigit1);


            endHoursFirstDigit = (TextView) view
                    .findViewById(R.id.endHoursFirstDigit);

            endHoursSecondDigit = (TextView) view
                    .findViewById(R.id.endHoursSecondDigit);

            endMinsFirstDigit = (TextView) view
                    .findViewById(R.id.endMinsFirstDigit);

            endMinsSecondDigit = (TextView) view
                    .findViewById(R.id.endMinsSecondDigit);


            endHoursFirstDigit1 = (TextView) view
                    .findViewById(R.id.endHoursFirstDigit1);

            endHoursSecondDigit1 = (TextView) view
                    .findViewById(R.id.endHoursSecondDigit1);

            endMinsFirstDigit1 = (TextView) view
                    .findViewById(R.id.endMinsFirstDigit1);

            endMinsSecondDigit1 = (TextView) view
                    .findViewById(R.id.endMinsSecondDigit1);

            total_hours = (TextView) view.findViewById(R.id.total_hours);

            pen_tsk_srt_stop_button = (Button) view
                    .findViewById(R.id.pen_tsk_srt_stop_button);
            //pen_tsk_srt_stop_button.setEnabled(false);
            pen_tsk_srt_stop_button.setOnClickListener(this);

            total_time_layout = (LinearLayout) view
                    .findViewById(R.id.total_time_layout);
            total_time_layout.setVisibility(View.GONE);
            sqlcon = new SQLController(getActivity());
            internetConnection = new ConnectionDetector(getActivity());

            sqlcon.open();
            arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

            if (internetConnection
                    .isConnectingToInternet("")) {
                if (UpdaterService.firstTime) {
                    DeleteItemsInDB();
                    GetCurrentTime();
                }

                SetUserDetails();
                GetFromShared();

                //customHandler.postDelayed(updateTimerThread, 1000);
                if (arrSalesTask != null && arrSalesTask.size() > 0 && arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("") && arrSalesTask.get(0).endTime != null && !arrSalesTask.get(0).endTime.equals("")) {


                } else if (arrSalesTask != null && arrSalesTask.size() > 0 && arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("")) {

                    receiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            startTime = arrSalesTask.get(0).startTime + "";
                            counter = intent.getStringExtra("counter");
                            SetRunningTime(startTime, counter);
                        }
                    };
                } else {

                    receiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            startTime = intent.getStringExtra("time");
                            counter = intent.getStringExtra("counter");
                            SetRunningTime(startTime, counter);
                        }
                    };


                    if (arrSalesTask.size() > 0 && arrSalesTask != null) {

                        if (arrSalesTask.get(0).task_id_Tx != null) {
                            sqlcon.open();
                            sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_START_TIME, getStartTime(), arrSalesTask.get(0).task_id_Tx);

                            Log.v("start Time ", startTime + "");
                        }
                    }


                }
                if (TextUtils.isEmpty(ConstantFunction.getuser(mActivity.getApplicationContext(), AppConstants.et_id)) ||
                        ConstantFunction.getuser(mActivity.getApplicationContext(), AppConstants.et_id).equals("0")) {
                    EndMission(arrayList, getStartTime());
                }

            } else {
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
                GetCurrentTime();
                GetFromShared();
                //customHandler.postDelayed(updateTimerThread, 1000);

                if (arrSalesTask != null && arrSalesTask.size() > 0 && arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("") && arrSalesTask.get(0).endTime != null && !arrSalesTask.get(0).endTime.equals("")) {


                } else if (arrSalesTask != null && arrSalesTask.size() > 0 && arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("")) {

                    receiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            startTime = arrSalesTask.get(0).startTime + "";
                            counter = intent.getStringExtra("counter");
                            SetRunningTime(startTime, counter);
                        }
                    };
                } else {
                    receiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            startTime = intent.getStringExtra("time");
                            counter = intent.getStringExtra("counter");
                            SetRunningTime(startTime, counter);
                        }
                    };

                    if (arrSalesTask.size() > 0 && arrSalesTask != null) {

                        if (arrSalesTask.get(0).task_id_Tx != null) {
                            sqlcon.open();
                            sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_START_TIME, getStartTime(), arrSalesTask.get(0).task_id_Tx);

                            Log.v("start Time ", startTime + "");
                        }
                    }


                }

            }


            if (arrSalesTask != null && arrSalesTask.size() > 0) {
                if (arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("")) {

                    startHourFirstDigit.setText(String.valueOf(arrSalesTask.get(0).startTime.charAt(11)));
                    startHourSeconfDigit.setText(String.valueOf(arrSalesTask.get(0).startTime.charAt(12)));
                    startMinsFirstDigit.setText(String.valueOf(arrSalesTask.get(0).startTime.charAt(14)));
                    startMinsSecondDigit.setText(String.valueOf(arrSalesTask.get(0).startTime.charAt(15)));

                    startHourFirstDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).startTime.charAt(11))));
                    startHourSeconfDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).startTime.charAt(12))));
                    startMinsFirstDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).startTime.charAt(14))));
                    startMinsSecondDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).startTime.charAt(15))));
                }

                if (arrSalesTask.get(0).endTime != null && !arrSalesTask.get(0).endTime.equals("")) {
                    endHoursFirstDigit.setText(String.valueOf(arrSalesTask.get(0).endTime.charAt(11)));
                    endHoursSecondDigit.setText(String.valueOf(arrSalesTask.get(0).endTime.charAt(12)));
                    endMinsFirstDigit.setText(String.valueOf(arrSalesTask.get(0).endTime.charAt(14)));
                    endMinsSecondDigit.setText(String.valueOf(arrSalesTask.get(0).endTime.charAt(15)));

                    endHoursFirstDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).endTime.charAt(11))));
                    endHoursSecondDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).endTime.charAt(12))));
                    endMinsFirstDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).endTime.charAt(14))));
                    endMinsSecondDigit1.setText(toPersianNumber(String.valueOf(arrSalesTask.get(0).endTime.charAt(15))));
                }


                if (arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("") &&
                        arrSalesTask.get(0).endTime != null && !arrSalesTask.get(0).endTime.equals("")) {

                    total_time_layout.setVisibility(View.VISIBLE);

                    java.util.Date date1 = null;
                    java.util.Date date2 = null;

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

                    try {
                        date1 = df.parse(arrSalesTask.get(0).startTime);
                        date2 = df.parse(arrSalesTask.get(0).endTime);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    long diff = date2.getTime() - date1.getTime();

                    long timeInSeconds = diff / 1000;
                    long Hours, Mins;
                    Hours = timeInSeconds / 3600;
                    timeInSeconds = timeInSeconds - (Hours * 3600);
                    Mins = timeInSeconds / 60;

                    String takenTime = "";
                    selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                    if (selectedLanguage.equalsIgnoreCase("")) {
                        selectedLanguage = "en";
                    }

                    if (selectedLanguage.equalsIgnoreCase("pr")) {
                        if (Hours <= 0) {
                            if (Mins <= 1) {
                                takenTime = toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                            } else {

                                takenTime = toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                            }
                        } else {
                            if (Hours == 1)
                                takenTime = toPersianNumber(String.valueOf(Hours)) + " " + getResources().getString(R.string.Hour);
                            else
                                takenTime = toPersianNumber(String.valueOf(Hours)) + " " + getResources().getString(R.string.Hours);
                            if (Mins != 0) {
                                if (Mins == 1)
                                    takenTime += " " + toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                                else
                                    takenTime += " " + toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                            }
                        }
                    } else {
                        if (Hours <= 0) {
                            if (Mins <= 1) {
                                takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
                            } else {

                                takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
                            }
                        } else {
                            if (Hours == 1)
                                takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hour);
                            else
                                takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hours);
                            if (Mins != 0) {
                                if (Mins == 1)
                                    takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
                                else
                                    takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
                            }
                        }
                    }
                    total_hours.setText(takenTime);

                    arrayList.get(0).startTime = taskStartTime;
                    arrayList.get(0).endTime = taskStopTime;
                    arrayList.get(0).totalTimeTaken = takenTime;

                    Log.e("AAA", "after apdating arraylist size : " + arrayList.size());

//                    Sales_page_finished_task_fragment detailFragment = new Sales_page_finished_task_fragment(arrayList);
//                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
//                            detailFragment, true, true);

                }

            }


            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                llEng.setVisibility(View.GONE);
                llPersian.setVisibility(View.VISIBLE);

//				imgOne.setImageResource(R.drawable.pr_step1_hover);
//				imgTwo.setImageResource(R.drawable.pr_step2_unhover);
//				imgThree.setImageResource(R.drawable.pr_step3_unhover);
//				imgFour.setImageResource(R.drawable.pr_step4_unhover);

                llEngStart.setVisibility(View.GONE);
                llPerStart.setVisibility(View.VISIBLE);

                llEngEnd.setVisibility(View.GONE);
                llPerEnd.setVisibility(View.VISIBLE);
            } else {
                llEng.setVisibility(View.VISIBLE);
                llPersian.setVisibility(View.GONE);

                llEngStart.setVisibility(View.VISIBLE);
                llPerStart.setVisibility(View.GONE);

                llEngEnd.setVisibility(View.VISIBLE);
                llPerEnd.setVisibility(View.GONE);


            }

            if (isMyServiceRunning(UpdaterService.class)) {
                stopService();

            }
            startService();

            sqlcon.open();
            arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

            //store data already filled

//            if (arrSalesTask.size() > 0 && arrSalesTask != null) {
//
//                if (arrSalesTask.get(0).task_id_Tx != null) {
//                    if (arrSalesTask.get(0).startTime == null && arrSalesTask.get(0).startTime.equals("")) {
//                        sqlcon.open();
//                        sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_START_TIME, startTime, arrSalesTask.get(0).task_id_Tx);
//
//                        Log.v("start Time ", startTime + "");
//                    }
//                }
//            }

        }


        return view;
    }

    private void EndMission(ArrayList<SalesTask> arrayList, String start_Time) {

        JSONObject requestObject = new JSONObject();
        try {

            if (arrayList.get(0).client_id.length() == 0) {
                requestObject.put("client_id", "0");
                requestObject.put("client_name", arrayList.get(0).SALES_client_name_new + "");
                requestObject.put("client_address", arrayList.get(0).SALES_client_address + "");
                requestObject.put("contact_person", arrayList.get(0).SALES_contact_person + "");
                requestObject.put("tel_number", arrayList.get(0).tel_number + "");
                requestObject.put("client_email", arrayList.get(0).client_email + "");
                requestObject.put("latitude", ConstantValues.latitude + "");
                requestObject.put("longtitude", ConstantValues.longitude + "");


            } else {
                requestObject.put("client_id", arrayList.get(0).client_id + "");
            }

            requestObject.put("department", ConstantFunction.getstatus(getActivity(), "department"));
            requestObject.put("emp_id", ConstantFunction.getstatus(getActivity(), "UserId"));
            requestObject.put("task_start_time", start_Time + "");
            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));
            requestObject.put("supervisor_id", arrayList.get(0).supervisor_id + "");
            requestObject.put("category_id", arrayList.get(0).category_id + "");
            requestObject.put("task_name", arrayList.get(0).task_name + "");
            requestObject.put("tasktype", arrayList.get(0).tasktype + "");

        } catch (JSONException e) {
            Log.d("TTT", "Exception while sending parameter in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.SalesTaskStartURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_Task_Final_Fragment :- " + ConstantValues.SalesTaskStartURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */
        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetCurrentTime() {
        Calendar c = Calendar.getInstance();

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                Locale.getDefault());
        taskStartTime = sdf.format(c.getTime());

        if (startMins == 0 && startHour == 0) {
            runningMins = startMins = c.get(Calendar.MINUTE);
            runningHours = startHour = c.get(Calendar.HOUR_OF_DAY);

            strStartTime = startHour + ":" + runningMins;

            String StartHour = String.format(Locale.getDefault(), "%02d",
                    startHour);
            String StartMin = String.format(Locale.getDefault(), "%02d",
                    startMins);
        }
    }

    private void SetRunningTime(String startTime, String counter) {

        String runningHourOne = Character.toString(counter.charAt(0));
        String runningHourTwo = Character.toString(counter.charAt(1));
        String runningMinOne = Character.toString(counter.charAt(3));
        String runningMinTwo = Character.toString(counter.charAt(4));

        startHourFirstDigit.setText(String.valueOf(startTime.charAt(11)));
        startHourSeconfDigit.setText(String.valueOf(startTime.charAt(12)));
        startMinsFirstDigit.setText(String.valueOf(startTime.charAt(14)));
        startMinsSecondDigit.setText(String.valueOf(startTime.charAt(15)));

        startHourFirstDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(11))));
        startHourSeconfDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(12))));
        startMinsFirstDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(14))));
        startMinsSecondDigit1.setText(toPersianNumber(String.valueOf(startTime.charAt(15))));


        endHoursFirstDigit.setText(runningHourOne);
        endHoursSecondDigit.setText(runningHourTwo);
        endMinsFirstDigit.setText(runningMinOne);
        endMinsSecondDigit.setText(runningMinTwo);

        endHoursFirstDigit1.setText(toPersianNumber(runningHourOne));
        endHoursSecondDigit1.setText(toPersianNumber(runningHourTwo));
        endMinsFirstDigit1.setText(toPersianNumber(runningMinOne));
        endMinsSecondDigit1.setText(toPersianNumber(runningMinTwo));


    }

    private void GetFromShared() {

        taskId = arrayList.get(0).task_id_Tx;
        issue = arrayList.get(0).task_name.toUpperCase() + " (" + arrayList.get(0).category_Name + ")";

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + toPersianNumber(taskId) + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        } else {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + taskId + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        }

        InserToSavedTable(taskId, issue, "", "", "", "", "", "", null);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:

                mActivity.popFragments();


                break;

            case R.id.pen_tsk_srt_stop_button:

//                Sales_page_finished_task_fragment detailFragment = new Sales_page_finished_task_fragment(arrayList);
//                // detailFragment.setTaskId(pendingTask.getTaskId());
//                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
//                        detailFragment, true, true);


                if (arrSalesTask != null && arrSalesTask.size() > 0 && arrSalesTask.get(0).startTime != null && !arrSalesTask.get(0).startTime.equals("") && arrSalesTask.get(0).endTime != null && !arrSalesTask.get(0).endTime.equals("")) {


                    arrayList.get(0).startTime = arrSalesTask.get(0).startTime;
                    arrayList.get(0).endTime = arrSalesTask.get(0).endTime;

                    Sales_page_finished_task_fragment detailFragment = new Sales_page_finished_task_fragment(arrayList);
                    // detailFragment.setTaskId(pendingTask.getTaskId());
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                            detailFragment, true, true);
                } else {

                    //set task_id "0" for not send location  23_12
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString("task_id", "0");
//                    editor.commit();

                    if (isThreadRunning)
                        ShowCompletedWarning();
                    else {
                        FragmentManager manager = getActivity().getSupportFragmentManager();
                        FragmentTransaction ft = manager.beginTransaction();
                        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        ft.replace(R.id.realtabcontent, new Sales_page_finished_task_fragment(arrayList));
                        ft.commit();
                        //  mActivity.pushFragments(ConstantValues.TAB_DASHBOARD, takePhotoFragment, true, true);
                    }

                }


                break;

            default:
                break;
        }
    }


    private void ShowCompletedWarning() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.confirm));
        builder.setMessage(getString(R.string.finished_task));

        builder.setPositiveButton(getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        FinishTask();
                        dialog.dismiss();
                    }

                });

        builder.setNegativeButton(getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                        dialog.dismiss();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public String getStartTime() {
        String taskStartTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                Locale.getDefault());
        taskStartTime = sdf.format(cStart.getTime());
        return taskStartTime;
    }

    private void FinishTask() {
        stopService();
        isThreadRunning = false;
        Calendar c = Calendar.getInstance();

        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                Locale.getDefault());
        taskStopTime = sdf.format(c.getTime());
        taskStartTime = getStartTime();
        total_time_layout.setVisibility(View.VISIBLE);

        java.util.Date date1 = null;
        java.util.Date date2 = null;

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

        try {
            date1 = df.parse(taskStartTime);
            date2 = df.parse(taskStopTime);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        long diff = date2.getTime() - date1.getTime();

        long timeInSeconds = diff / 1000;
        long Hours, Mins;
        Hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (Hours * 3600);
        Mins = timeInSeconds / 60;

        String takenTime = "";

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            if (Hours <= 0) {
                if (Mins <= 1) {
                    takenTime = toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                } else {

                    takenTime = toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                }
            } else {
                if (Hours == 1)
                    takenTime = toPersianNumber(String.valueOf(Hours)) + " " + getResources().getString(R.string.Hour);
                else
                    takenTime = toPersianNumber(String.valueOf(Hours)) + " " + getResources().getString(R.string.Hours);
                if (Mins != 0) {
                    if (Mins == 1)
                        takenTime += " " + toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Min);
                    else
                        takenTime += " " + toPersianNumber(String.valueOf(Mins)) + " " + getResources().getString(R.string.Mins);
                }
            }
        } else {
            if (Hours <= 0) {
                if (Mins <= 1) {
                    takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
                } else {

                    takenTime = String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
                }
            } else {
                if (Hours == 1)
                    takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hour);
                else
                    takenTime = String.valueOf(Hours) + " " + getResources().getString(R.string.Hours);
                if (Mins != 0) {
                    if (Mins == 1)
                        takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Min);
                    else
                        takenTime += " " + String.valueOf(Mins) + " " + getResources().getString(R.string.Mins);
                }
            }
        }
        total_hours.setText(takenTime);

        arrayList.get(0).startTime = taskStartTime;
        arrayList.get(0).endTime = taskStopTime;
        arrayList.get(0).totalTimeTaken = takenTime;

        Log.e("AAA", "after apdating arraylist size : " + arrayList.size());

        sqlcon.open();
        arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

        //store data already filled
        if (arrSalesTask.size() > 0 && arrSalesTask != null) {

            if (arrSalesTask.get(0).task_id_Tx != null) {

                arrayList.get(0).startTime = arrSalesTask.get(0).startTime;

//                if (!arrayList.get(0).startTime.equals("")) {
//                    sqlcon.open();
//                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_START_TIME, arrayList.get(0).startTime, arrSalesTask.get(0).task_id_Tx);
//                }

                if (!arrayList.get(0).endTime.equals("")) {
                    sqlcon.open();
                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_END_TIME, arrayList.get(0).endTime, arrSalesTask.get(0).task_id_Tx);
                }

                Log.v("start Time ", startTime + "");
            }
        }


//        UpdateTable(DbHelper.SAVED_START_TIME, taskStartTime, taskId);
        //   UpdateTable(DbHelper.SAVED_FINISHED_TIME, taskStopTime, taskId);
        //     UpdateTable(DbHelper.SAVED_TOTAL_TIME_SPEND, takenTime, taskId);


//        GetSavedItems(taskId);

        //  Sales_page_finished_task_fragment takePhotoFragment = new Sales_page_finished_task_fragment();
        //   mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,takePhotoFragment, true, true);


        Sales_page_finished_task_fragment detailFragment = new Sales_page_finished_task_fragment(arrayList);
        // detailFragment.setTaskId(pendingTask.getTaskId());
        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                detailFragment, true, true);

//        FragmentManager manager =getActivity(). getSupportFragmentManager();
//        FragmentTransaction ft = manager.beginTransaction();
//        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
//        ft.replace(R.id.realtabcontent, new Sales_page_finished_task_fragment(arrayList));
//        ft.commit();

    }



	/*@Override
    public boolean onBackPressed() {
		boolean ret = true;
		if (total_time_layout.getVisibility() != View.VISIBLE) {
			ShowWarning();
			ret = true;
		} else {
			ret = false;

		}
		return ret;
	}*/

    public void InserToSavedTable(String taskId, String issue,
                                  String total_time_spend, String start_time, String finished_time,
                                  String task_report, String rating, String comments, byte[] signature) {
        sqlcon.open();
        sqlcon.InsertToSaveData(taskId, issue, total_time_spend, start_time,
                finished_time, task_report, rating, comments, signature);
    }

    public void DeleteItemsInDB() {
        sqlcon.open();
        sqlcon.DeleteAll(DbHelper.TABLE_SAVED_ITEMS);
    }

    private void UpdateTable(String fieldName, String updateValue, String taskId) {
        sqlcon.open();
        sqlcon.UpdateSavedTable(fieldName, updateValue, taskId);
    }

    private void GetSavedItems(String taskId) {
        sqlcon.open();
        sqlcon.GetSavedItems(taskId);
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((UpdaterService.firstTime) && (!UpdaterService.inProcess)) {
            isThreadRunning = true;
            startService();
            UpdaterService.firstTime = false;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", false);
            editor.putString("currentTime", UpdaterService.currentTime + "");

            editor.commit();

        }
        if (UpdaterService.inProcess && receiver != null) {
            isThreadRunning = true;
            startService();
            getActivity().registerReceiver(receiver, new IntentFilter(
                    UpdaterService.BROADCAST_ACTION));
        }

    }

    private void startService() {
        serviceIntent = new Intent(getActivity(),
                UpdaterService.class);
        getActivity().startService(serviceIntent);
        UpdaterService.inProcess = true;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("inProcess", true);
        editor.putString("currentTime", UpdaterService.currentTime + "");
        editor.commit();

        // registerReceiver(receiver, new IntentFilter(UpdaterService.BROADCAST_ACTION));

    }

    private void stopService() {
        if (UpdaterService.inProcess) {
            getActivity().stopService(new Intent(getActivity(),
                    UpdaterService.class));
            UpdaterService.inProcess = false;
            UpdaterService.firstTime = false;

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("inProcess", false);
            editor.putBoolean("firstTime", false);
            editor.putString("currentTime", UpdaterService.currentTime + "");

            editor.commit();


            try {
                if (receiver != null) {
                    getActivity().unregisterReceiver(receiver);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (UpdaterService.inProcess && receiver != null)
            getActivity().unregisterReceiver(receiver);
    }

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        String out = "";
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out += persianNumbers[number];
            } else if (c == '٫') {
                out += '،';
            } else {
                out += c;
            }

        }
        return out;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {

        mProgressHUD.dismiss();
        Log.e("Response==>", response);

        if (requestURL.equalsIgnoreCase(ConstantValues.SalesTaskStartURL)) {
            MowomLogFile.writeToLog("\n" + "Sales_page_startJob_Fragment :- " + ConstantValues.SalesTaskStartURL + " -------------" + response);
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (status.equalsIgnoreCase("success")) {

                    JSONObject job2 = job.getJSONObject("data");

//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString("task_id", job2.get("task_id").toString() + "");
//                    editor.commit();

                    ConstantFunction.savestatus(getContext(), AppConstants.task_Id,
                            job2.get("task_id").toString());

                    ConstantFunction.savestatus(getContext(), AppConstants.et_id,
                            job2.get("et_id").toString());

                    ConstantFunction.savestatus(getActivity(), "sales_Task",
                            job2.get("task_id").toString());

                    arrayList.get(0).task_id = job2.get("task_id").toString();

                    if (arrayList.get(0).task_id_Tx != null) {
                        sqlcon.open();
                        sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_TASK_ID, job2.get("task_id").toString() + "", arrayList.get(0).task_id_Tx);
                    }


                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (status.equalsIgnoreCase("failed")) {
//                            Toast toast = Toast.makeText(getActivity(),
//                                    getString(R.string.please_try_again),
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();

                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }

}
