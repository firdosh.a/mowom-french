package com.asrapps.mowom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.Photo_Grid_Final_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.MyLeadingMarginSpan2;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.model.PhotoModel;
import com.asrapps.mowom.view.HorizontalLayoutScrollListView;
import com.asrapps.mowom.view.HorizontalListView;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;
import com.asrapps.utils.MowomLogFile;
import com.asrapps.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Pending_Task_Final_Fragment extends BaseFragment implements
        OnClickListener, AsyncResponseListener, OnCancelListener {

    private ProgressHUD mProgressHUD;
    PendingTask offlineSelectedTask;
    SharedPreferences sharedPref;

    View view;
    String StartTime, StopTime;
    SQLController sqlcon;

    ImageView customerSignature, customerSignature1;
    String taskId, issue;

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    TextView pending_task_detail_orgName, pending_task_list_id, pending_task_list_id1,
            pending_task_list_issue, pending_task_list_issue1, pending_task_list_date_time, pending_task_list_date_time1,
            pending_task_list_phone, pending_task_list_phone1, pending_task_list_address, pending_task_list_address1,
            pending_task_detail_contactPersonName, pending_task_detail_contactPersonName1,
            pending_task_detail_start_endTime, pending_task_detail_start_endTime1, totalTime, totalTime1, taskReportTextView, taskReportTextView1,
            consumerFeedBackTextView, consumerFeedBackTextView1, ratingValue, ratingValue1, lable_id, label_comma,
            overlapping_issue_all, overlapping_issue_all1, pending_task_detail_duration;

    LinearLayout idLayout1, llLeadMargin, consumerFeedBackLayout, consumerFeedBackTextLayout, llPhoto, llPhoto1, taskReportTextLayout, taskReportTextLayout111, taskReportLayout, taskReportLayout2;

    ImageView star_image_1, star_image_2, star_image_3, star_image_4,
            star_image_5, star_image_11, star_image_21, star_image_31, star_image_41,
            star_image_51;

    Bitmap signature;

    String et_id, task_list_id, task_list_issue, task_list_date_time, task_list_phone,
            task_list_address, task_detail_contactPersonName,
            task_detail_start_endTime, strTotalTime, strTaskReportTextView, google_sheet_url, is_google_sheet,
            strConsumerFeedBackTextView, strRatingValue = "0", task_org_name;

    String problem_reported;
    String diagnosis_done;
    String resolution;
    String notes_support;

    String deilvery_status;
    String notes_delevery;

    String selectedLanguage;

    String startTime, endTime;
    String strSignature;

    int id;

    HorizontalListView photoGrid;
    HorizontalLayoutScrollListView photoGrid1;
    public Pending_Task_Photo_Activity CustomListView = null;
    private ArrayList<PhotoModel> CustomListViewValuesArr = new ArrayList<PhotoModel>();

    Button taskEndButton;

    LinearLayout contentsLayoutEng, contentsLayoutPer;

    ConnectionDetector internetConnection;
    private ArrayList<Login> mLogin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.pending_task_final_layout,
                    container, false);

            sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);

			/*
             * txtHeaderUserName = (TextView) view
			 * .findViewById(R.id.txtHeaderUserName);
			 */


            llLeadMargin = (LinearLayout) view.findViewById(R.id.llLeadMargin);
            idLayout1 = (LinearLayout) view.findViewById(R.id.idLayout1);
            label_comma = (TextView) view
                    .findViewById(R.id.label_comma);

            lable_id = (TextView) view.findViewById(R.id.lable_id);
            contentsLayoutEng = (LinearLayout) view.findViewById(R.id.contentsLayoutEng);
            contentsLayoutPer = (LinearLayout) view.findViewById(R.id.contentsLayoutPer);

            consumerFeedBackLayout = (LinearLayout) view.findViewById(R.id.consumerFeedBackLayout);
            consumerFeedBackTextLayout = (LinearLayout) view.findViewById(R.id.consumerFeedBackTextLayout);


            pending_task_detail_duration = (TextView) view
                    .findViewById(R.id.pending_task_detail_duration);

            taskReportTextLayout = (LinearLayout) view.findViewById(R.id.taskReportTextLayout);
            taskReportTextLayout111 = (LinearLayout) view.findViewById(R.id.taskReportTextLayout111);
            taskReportLayout = (LinearLayout) view.findViewById(R.id.taskReportLayout);
            taskReportLayout2 = (LinearLayout) view.findViewById(R.id.taskReportLayout2);

            overlapping_issue_all = (TextView) view.findViewById(R.id.overlapping_issue_all);
            overlapping_issue_all1 = (TextView) view.findViewById(R.id.overlapping_issue_all1);

            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
            // headerUserImage.setOnClickListener(this);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            sqlcon = new SQLController(getActivity());
            customerSignature = (ImageView) view
                    .findViewById(R.id.customerSignature);

            customerSignature1 = (ImageView) view
                    .findViewById(R.id.customerSignature1);

            taskEndButton = (Button) view.findViewById(R.id.taskEndButton);
            taskEndButton.setOnClickListener(this);


            pending_task_list_id = (TextView) view
                    .findViewById(R.id.pending_task_list_id);

            pending_task_list_id1 = (TextView) view
                    .findViewById(R.id.pending_task_list_id1);

            pending_task_detail_orgName = (TextView) view
                    .findViewById(R.id.pending_task_detail_orgName);


            pending_task_list_issue = (TextView) view
                    .findViewById(R.id.pending_task_list_issue);

            pending_task_list_issue1 = (TextView) view
                    .findViewById(R.id.pending_task_list_issue1);

            pending_task_list_date_time = (TextView) view
                    .findViewById(R.id.pending_task_list_date_time);
            pending_task_list_date_time1 = (TextView) view
                    .findViewById(R.id.pending_task_list_date_time1);


            pending_task_list_phone = (TextView) view
                    .findViewById(R.id.pending_task_list_phone);

            pending_task_list_phone1 = (TextView) view
                    .findViewById(R.id.pending_task_list_phone1);

            pending_task_list_address = (TextView) view
                    .findViewById(R.id.pending_task_list_address);

            pending_task_list_address1 = (TextView) view
                    .findViewById(R.id.pending_task_list_address1);

            pending_task_detail_contactPersonName = (TextView) view
                    .findViewById(R.id.pending_task_detail_contactPersonName);

            pending_task_detail_contactPersonName1 = (TextView) view
                    .findViewById(R.id.pending_task_detail_contactPersonName1);


            pending_task_detail_start_endTime = (TextView) view
                    .findViewById(R.id.pending_task_detail_start_endTime);

            pending_task_detail_start_endTime1 = (TextView) view
                    .findViewById(R.id.pending_task_detail_start_endTime1);

            totalTime = (TextView) view.findViewById(R.id.totalTime);

            totalTime1 = (TextView) view.findViewById(R.id.totalTime1);

            taskReportTextView = (TextView) view
                    .findViewById(R.id.taskReportTextView);

            taskReportTextView1 = (TextView) view
                    .findViewById(R.id.taskReportTextView1);

            consumerFeedBackTextView = (TextView) view
                    .findViewById(R.id.consumerFeedBackTextView);

            consumerFeedBackTextView1 = (TextView) view
                    .findViewById(R.id.consumerFeedBackTextView1);

            ratingValue = (TextView) view.findViewById(R.id.ratingValue);
            ratingValue1 = (TextView) view.findViewById(R.id.ratingValue1);

            photoGrid = (HorizontalListView) view
                    .findViewById(R.id.photoGrid);
            llPhoto = (LinearLayout) view.findViewById(R.id.llPhoto);

            photoGrid1 = (HorizontalLayoutScrollListView) view
                    .findViewById(R.id.photoGrid1);
            llPhoto1 = (LinearLayout) view.findViewById(R.id.llPhoto1);

            star_image_1 = (ImageView) view.findViewById(R.id.star_image_1);
            star_image_2 = (ImageView) view.findViewById(R.id.star_image_2);
            star_image_3 = (ImageView) view.findViewById(R.id.star_image_3);
            star_image_4 = (ImageView) view.findViewById(R.id.star_image_4);
            star_image_5 = (ImageView) view.findViewById(R.id.star_image_5);

            star_image_11 = (ImageView) view.findViewById(R.id.star_image_11);
            star_image_21 = (ImageView) view.findViewById(R.id.star_image_21);
            star_image_31 = (ImageView) view.findViewById(R.id.star_image_31);
            star_image_41 = (ImageView) view.findViewById(R.id.star_image_41);
            star_image_51 = (ImageView) view.findViewById(R.id.star_image_51);


            GetFromShared();
            GetSavedItems(taskId);

            SetValues();

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                pending_task_detail_orgName.setGravity(Gravity.RIGHT);
                contentsLayoutEng.setVisibility(View.GONE);
                contentsLayoutPer.setVisibility(View.VISIBLE);
            } else {
                contentsLayoutEng.setVisibility(View.VISIBLE);
                contentsLayoutPer.setVisibility(View.GONE);
            }

        }
        internetConnection = new ConnectionDetector(getActivity());
        if (internetConnection.isConnectingToInternet("")) {
            SetUserDetails();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

        return view;

    }

    private void SetListView() {


        CustomListViewValuesArr = new ArrayList<PhotoModel>();
        CustomListViewValuesArr = GetPhotoFromDb(taskId);
        // photoGrid.removeAll();
        // for (int i = 0; i <CustomListViewValuesArr.size(); i++) {
        // String path = CustomListViewValuesArr.get(i).getTaskId();
        // if (path.equals(taskId)) {
        // photoGrid.add(CustomListViewValuesArr.get(i),this);
        // }
        // }

        System.out.println(".................CustomListViewValuesArr.size()--> " + CustomListViewValuesArr.size());

        Photo_Grid_Final_Adapter adapter = new Photo_Grid_Final_Adapter(getActivity(), CustomListViewValuesArr);

        photoGrid.setAdapter(adapter);

        for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
            String path = CustomListViewValuesArr.get(i).getTaskId();
            if (path.equals(taskId)) {
//                photoGrid.add(CustomListViewValuesArr.get(i), this);
                photoGrid1.add(CustomListViewValuesArr.get(i), this);
            }
        }

        if (CustomListViewValuesArr != null && CustomListViewValuesArr.size() == 0) {
            llPhoto.setVisibility(View.GONE);
            llPhoto1.setVisibility(View.GONE);
        } else {
            llPhoto.setVisibility(View.VISIBLE);
            llPhoto1.setVisibility(View.VISIBLE);
        }
    }

    public ArrayList<PhotoModel> GetPhotoFromDb(String taskId) {
        sqlcon.open();
        ArrayList<PhotoModel> tempArray = new ArrayList<PhotoModel>();
        tempArray = sqlcon.GetAllPhotos(taskId);
        return tempArray;

    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private void GetFromShared() {
        taskId = ConstantFunction.getuser(getContext(), AppConstants.task_Id);
        et_id = ConstantFunction.getuser(getContext(), AppConstants.et_id);
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);
    }

    private void SetValues() {
        pending_task_detail_orgName.setText(task_org_name.toUpperCase(Locale
                .getDefault()));


        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        id = task_list_id.length();
        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pending_task_list_id1.setText(toPersianNumber(task_list_id));

            Drawable DICON = view.getResources().getDrawable(R.drawable.id_icon);
            int leftMargin = DICON.getIntrinsicWidth() + lable_id.getText().length() + id + 80;
//            Set the icon in R.id.icon
            SpannableString issueString = new SpannableString(task_list_issue.toUpperCase(Locale.getDefault()));
//            Expose the indent for the first rows
            issueString.setSpan(new MyLeadingMarginSpan2(1, 0), 0, issueString.length(), 0);

            pending_task_list_issue.setText(issueString);

            overlapping_issue_all1.setText(getResources().getString(R.string.id) + " " + toPersianNumber(task_list_id) + " " + getResources().getString(R.string.comma) + " " + task_list_issue.toUpperCase(Locale.getDefault()));

        } else {
            pending_task_list_id.setText(task_list_id);
            SpannableString issueString = new SpannableString(task_list_issue.toUpperCase(Locale.getDefault()));
            pending_task_list_issue.setText(issueString);
            overlapping_issue_all.setText(getResources().getString(R.string.id) + " " + task_list_id + " " + getResources().getString(R.string.comma) + " " + task_list_issue.toUpperCase(Locale.getDefault()));
        }

        pending_task_list_issue1.setText(task_list_issue.toUpperCase(Locale
                .getDefault()));


        pending_task_list_date_time.setText(task_list_date_time);


        pending_task_list_date_time1.setText(task_list_date_time);

        pending_task_list_phone.setText(task_list_phone);
        pending_task_list_phone1.setText(toPersianNumber(task_list_phone));

        pending_task_list_address.setText(task_list_address);
        pending_task_list_address1.setText(task_list_address);

        pending_task_detail_contactPersonName
                .setText(task_detail_contactPersonName);
        pending_task_detail_contactPersonName1
                .setText(task_detail_contactPersonName);

        pending_task_detail_start_endTime.setText(task_detail_start_endTime);
        pending_task_detail_start_endTime1.setText(task_detail_start_endTime);

        totalTime.setText(strTotalTime);
        totalTime1.setText(strTotalTime);

        if (strTaskReportTextView != null && !strTaskReportTextView.equals("null") && strTaskReportTextView.trim().length() > 0) {
            taskReportTextView.setText(strTaskReportTextView);
            taskReportTextView1.setText(strTaskReportTextView);

            taskReportTextLayout.setVisibility(View.VISIBLE);
            taskReportTextLayout111.setVisibility(View.VISIBLE);
            taskReportLayout.setVisibility(View.VISIBLE);
            taskReportLayout2.setVisibility(View.VISIBLE);
        } else {
            taskReportTextLayout.setVisibility(View.GONE);
            taskReportTextLayout111.setVisibility(View.GONE);
            taskReportLayout.setVisibility(View.GONE);
            taskReportLayout2.setVisibility(View.GONE);
        }

        if (strConsumerFeedBackTextView != null && !strConsumerFeedBackTextView.equals("null") && strConsumerFeedBackTextView.trim().length() > 0) {
            consumerFeedBackTextView.setText(strConsumerFeedBackTextView);
            consumerFeedBackTextView1.setText(strConsumerFeedBackTextView);

            consumerFeedBackLayout.setVisibility(View.VISIBLE);
            consumerFeedBackTextLayout.setVisibility(View.VISIBLE);
        } else {
            consumerFeedBackLayout.setVisibility(View.GONE);
            consumerFeedBackTextLayout.setVisibility(View.GONE);
        }

        ratingValue.setText(strRatingValue);
        ratingValue1.setText(toPersianNumber(strRatingValue));

        customerSignature.setImageBitmap(signature);
        customerSignature1.setImageBitmap(signature);

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
        }

        if (strRatingValue.length() > 0) {
            SetRatings(Double.parseDouble(strRatingValue));
        }

        SetListView();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.backButtonImage:
                // mActivity.popFragments();

                break;

            case R.id.taskEndButton:

//                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                SharedPreferences.Editor edit = sharedPref.edit();
//                edit.putBoolean("is_start_task", false);
//                edit.commit();

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
                SharedPreferences.Editor editor2 = prefs.edit();
                editor2.putBoolean("isStartRunning", false);
                editor2.commit();

                ConstantFunction.savestatus(getActivity(), AppConstants.task_Id,
                        "0");

                ConstantFunction.savestatus(getActivity(), AppConstants.et_id,
                        "0");

                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                LocationManager manager = (LocationManager) getActivity()
                        .getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps(getActivity());
                } else {

                    internetConnection = new ConnectionDetector(getActivity());
                    if (internetConnection.isConnectingToInternet("")) {
                        EndMission();
                    } else {
                        new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();

                        sqlcon.open();
                        if (offlineSelectedTask != null) {
                            boolean isUpdated = sqlcon.pendingTaskByIdUpdate(offlineSelectedTask);

                            if (isUpdated) {
                                try {

                                    Toast toast = Toast.makeText(getActivity(),
                                            getString(R.string.suc_uploaded),
                                            Toast.LENGTH_SHORT);

                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();

                                    mActivity.ClearAllPages();
                                    // mActivity.popFragments();
                                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                            new AppTab_Dashboard_Home(), true, true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            Log.v("TTT", "task is updated with compelete");
                        }

                    }
                }
                break;

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            default:
                break;
        }

    }

    public void buildAlertMessageNoGps(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(mActivity.getString(R.string.gps_enable))
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                mActivity
                                        .startActivity(new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                // turnGPSOn();
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                dialog.cancel();
                                mActivity.finish();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    private void EndMission() {

        ConstantValues.isStartRunning = false;
        sqlcon.open();
        if (offlineSelectedTask != null) {
            boolean isUpdated = sqlcon.pendingTaskByIdUpdate(offlineSelectedTask);

            Log.v("TTT", "task is updated with compelete");
        }

        JSONObject requestObject = new JSONObject();
        try {

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("emp_task_id", et_id);
            requestObject.put("taskid", task_list_id);
            requestObject.put("task_start_time", startTime);
            requestObject.put("task_endtime", endTime);
            requestObject.put("task_report", strTaskReportTextView);
            requestObject.put("task_feedback", strConsumerFeedBackTextView);
            requestObject.put("task_ratings", strRatingValue);
            if (strSignature != null && !strSignature.equals("null")) {
                requestObject.put("client_sign", strSignature + "");
            } else {
                requestObject.put("client_sign", "");
            }


            if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("support")) {
                requestObject.put("problem_reported", problem_reported);
                requestObject.put("diagnosis_done", diagnosis_done);
                requestObject.put("resolution", resolution);
                requestObject.put("notes", notes_support);
            }

            if (ConstantFunction.getstatus(getActivity().getApplicationContext(), "department").equalsIgnoreCase("delivery")) {
                requestObject.put("deilvery_status", deilvery_status);
                requestObject.put("notes", notes_delevery);
            }

            String encodedString = "";
            for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
//                String strImage = GetStringFromBytes(CustomListViewValuesArr
//                        .get(i).getTakenPhoto());

                Bitmap reducedSizeBitmap = Utils.getBitmap(CustomListViewValuesArr
                        .get(i).getPhotoPath());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
                byte[] byteArray = stream.toByteArray();

                String strImage = GetStringFromBytes(byteArray);

                encodedString += new StringBuilder(String.valueOf(strImage))
                        .append(",").toString();
            }
            requestObject.put("imagecount", CustomListViewValuesArr.size());
            requestObject.put("task_images", encodedString);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.UpdateTaskURL.trim());
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Pending_Task_Final_Fragment :- " + ConstantValues.UpdateTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    public void GetSavedItems(String taskId) {
        sqlcon.open();
        PendingTask pendingTask = new PendingTask();
        pendingTask = sqlcon.GetSavedItems(taskId);

        sqlcon.open();
        offlineSelectedTask = sqlcon.getPendingByIdTask(ConstantFunction.getstatus(getActivity(), "UserId") + "", taskId);


        if (offlineSelectedTask
                .getDuration() != null && offlineSelectedTask
                .getDuration().length() > 0) {

            pending_task_detail_duration.setText(getString(R.string.duration) + " " + offlineSelectedTask
                    .getDuration() + " " + getString(R.string.Mins));

        } else {
            pending_task_detail_duration.setText(getString(R.string.duration) + " 0" + " " + getString(R.string.Mins));
        }

        task_org_name = offlineSelectedTask.getOrg_name().toUpperCase(
                Locale.getDefault());
        et_id = offlineSelectedTask.getEt_id();
        task_list_id = offlineSelectedTask.getTaskId();
        task_list_issue = offlineSelectedTask.getIssue();
        task_list_date_time = offlineSelectedTask.getDate_time();


        problem_reported = pendingTask.getProblem_reported();
        offlineSelectedTask.setProblem_reported(problem_reported);


        diagnosis_done = pendingTask.getDiagnosis_done();
        offlineSelectedTask.setDiagnosis_done(diagnosis_done);

        resolution = pendingTask.getResolution();
        offlineSelectedTask.setResolution(resolution);

        notes_support = pendingTask.getNotes_support();
        offlineSelectedTask.setNotes_support(notes_support);

        deilvery_status = pendingTask.getDeilvery_status();
        offlineSelectedTask.setDeilvery_status(deilvery_status);

        notes_delevery = pendingTask.getNotes_delevery();
        offlineSelectedTask.setNotes_delevery(notes_delevery);

        StringBuilder t = new StringBuilder();
        try {
            String st[] = task_list_date_time.split(" ");

            String stTime[] = st[1].split(":");
            String dtTime[] = st[0].split("/");

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {


                st[0] = st[0].replace(",", "").replace(" ", "");
                String dtStart = st[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SolarCalendar sc = new SolarCalendar(date);
                    String s = sc.date + "/" +
                            sc.month + "/" + sc.year;


                    Log.v("TTT", "ssssss = " + s);

                    t.append(toPersianNumber(s)).append(" ");
                    t.append(toPersianNumber(stTime[0]));
                    t.append(":");
                    t.append(toPersianNumber(stTime[1]));

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t.append(toPersianNumber(st[0])).append(" ");
                    t.append(toPersianNumber(stTime[0]));
                    t.append(":");
                    t.append(toPersianNumber(stTime[1]));
                    e.printStackTrace();
                }
                task_list_date_time = t.toString();

            } else {

                task_list_date_time = WebServiceHelper.convert12to24(task_list_date_time);


            }

        } catch (Exception e) {


        }

        task_list_phone = offlineSelectedTask.getOrg_ph_num();
        if (offlineSelectedTask.getClientAddress() != null
                && offlineSelectedTask.getClientAddress().size() > 0
                && offlineSelectedTask.getClientAddress().get(0).location != null) {
            task_list_address = offlineSelectedTask.getClientAddress().get(0).location;
        } else {
            task_list_address = "";
        }

        if (offlineSelectedTask.getPersonName() != null) {
            task_detail_contactPersonName = getString(R.string.contact_name)
                    + " : " + offlineSelectedTask.getPersonName();
        }

        StringBuilder t1 = new StringBuilder();
        StringBuilder t2 = new StringBuilder();


        offlineSelectedTask.setStartTime(pendingTask.getStartTime());
        startTime = pendingTask.getStartTime();

        offlineSelectedTask.setStopTime(pendingTask.getStopTime());
        endTime = pendingTask.getStopTime();


        if (startTime != null && !startTime.equals("")) {
            StartTime = ConstantFunction.GetSimpleTimeFormat24(startTime);
        }
        if (endTime != null && !endTime.equals("")) {
            StopTime = ConstantFunction.GetSimpleTimeFormat24(endTime);
        }


        try {


            Log.v("TTT", "startTime = " + StartTime);
            Log.v("TTT", "endTime = " + StopTime);

            String st[] = StartTime.split(" ");
            String et[] = StopTime.split(" ");

            String stTime[] = st[0].split(":");
            String etTime[] = et[0].split(":");

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                String dtStart = stTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SolarCalendar sc = new SolarCalendar(date);
                    String s = sc.date + "/" +
                            sc.month + "/" + sc.year;

                    Log.v("TTT", "ssssss = " + s);

                    t1.append(toPersianNumber(s));
                    t1.append(":");
                    t1.append(toPersianNumber(stTime[1]));

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t1.append(toPersianNumber(stTime[0]));
                    t1.append(":");
                    t1.append(toPersianNumber(stTime[1]));
                    e.printStackTrace();
                }


            } else {

                stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                String dtStart = stTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                    String s = dateformat.format(date);
                    System.out.println("Current Date Time : " + s);

                    Log.v("TTT", "ssssss = " + s);

                    t1.append(s);
                    t1.append(":");
                    t1.append(stTime[1]);

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t1.append(stTime[0]);
                    t1.append(":");
                    t1.append(stTime[1]);
                    e.printStackTrace();
                }


            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                String dtStart = etTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SolarCalendar sc = new SolarCalendar(date);
                    String s = sc.date + "/" +
                            sc.month + "/" + sc.year;


                    Log.v("TTT", "ssssss = " + s);

                    t2.append(toPersianNumber(s));
                    t2.append(":");
                    t2.append(toPersianNumber(etTime[1]));

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t2.append(toPersianNumber(etTime[0]));
                    t2.append(":");
                    t2.append(toPersianNumber(etTime[1]));
                    e.printStackTrace();
                }


            } else {

                etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                String dtStart = etTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                    String s = dateformat.format(date);
                    System.out.println("Current Date Time : " + s);


                    Log.v("TTT", "ssssss = " + s);

                    t2.append(s);
                    t2.append(":");
                    t2.append(etTime[1]);

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t2.append(etTime[0]);
                    t2.append(":");
                    t2.append(etTime[1]);
                    e.printStackTrace();
                }


            }


            StartTime = t1.toString();
            StopTime = t2.toString();

        } catch (Exception e) {
            e.printStackTrace();

            StartTime = t1.toString();
            StopTime = t2.toString();
        }

        String formatedString = "Start : %s - Stop : %s";

        task_detail_start_endTime = getResources().getString(R.string.Start) + "  " + StartTime + getResources().getString(R.string.stop) + " " + StopTime;

        strTotalTime = getString(R.string.total_time) + " "
                + pendingTask.getTotalTimeSpend();
        offlineSelectedTask.setTotalTimeSpend(pendingTask.getTotalTimeSpend());

        if (offlineSelectedTask != null && offlineSelectedTask.getGoogle_sheet_url() != null) {
            google_sheet_url = offlineSelectedTask.getGoogle_sheet_url();
        }

        if (offlineSelectedTask != null && offlineSelectedTask.getIs_google_sheet() != null) {
            is_google_sheet = offlineSelectedTask.getIs_google_sheet();
        }

        strTaskReportTextView = pendingTask.getTaskReport();
        if (pendingTask.getTaskReport() != null) {
            offlineSelectedTask.setTaskReport(pendingTask.getTaskReport());
        }

        strConsumerFeedBackTextView = pendingTask.getConsumerFeedback();
        if (pendingTask.getConsumerFeedback() != null) {
            offlineSelectedTask.setConsumerFeedback(pendingTask.getConsumerFeedback());
        }

        if (pendingTask.getRating() != null) {
            strRatingValue = pendingTask.getRating();
            offlineSelectedTask.setRating(pendingTask.getRating());
        }

        if (pendingTask.getSignatureImage() != null) {
            offlineSelectedTask.setSignatureImage(pendingTask.getSignatureImage());
        }

        if (pendingTask.getSignatureImage() != null && pendingTask.getSignatureImage().length > 0 && !pendingTask.getSignatureImage().equals("null")) {
            signature = getImageView(pendingTask.getSignatureImage());
            ((View) customerSignature1.getParent()).setVisibility(View.VISIBLE);
            ((View) customerSignature.getParent()).setVisibility(View.VISIBLE);
        } else {
            ((View) customerSignature1.getParent()).setVisibility(View.GONE);
            ((View) customerSignature.getParent()).setVisibility(View.GONE);
        }


        SetListView();

    }

    Bitmap getImageView(byte[] image) {
        Bitmap bm = null;
        if (image != null) {
            bm = BitmapFactory.decodeByteArray(image, 0, image.length);
            strSignature = ConstantFunction.BitMapToString(bm);
        }
        return bm;

    }

    public String GetStringFromBytes(byte[] image) {
        String byteSyting = "";
        Bitmap bm = null;
        bm = BitmapFactory.decodeByteArray(image, 0, image.length);
        byteSyting = ConstantFunction.BitMapToString(bm);
        return byteSyting;
    }

    private void SetRatings(double rating) {
        star_image_1.setImageResource(R.drawable.empty_star_icon);
        star_image_2.setImageResource(R.drawable.empty_star_icon);
        star_image_3.setImageResource(R.drawable.empty_star_icon);
        star_image_4.setImageResource(R.drawable.empty_star_icon);
        star_image_5.setImageResource(R.drawable.empty_star_icon);

        star_image_11.setImageResource(R.drawable.empty_star_icon);
        star_image_21.setImageResource(R.drawable.empty_star_icon);
        star_image_31.setImageResource(R.drawable.empty_star_icon);
        star_image_41.setImageResource(R.drawable.empty_star_icon);
        star_image_51.setImageResource(R.drawable.empty_star_icon);


        if (rating > 0 && rating <= 0.5) {
            star_image_1.setImageResource(R.drawable.half_star_icon);
            star_image_11.setImageResource(R.drawable.half_star_icon);
        }
        if (rating > 0.5 && rating <= 1.0) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_11.setImageResource(R.drawable.full_star_icon);
        }
        if (rating > 1.0 && rating <= 1.5) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.half_star_icon);

            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.half_star_icon);
        }
        if (rating > 1.5 && rating <= 2.0) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);

            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
        }
        if (rating > 2.0 && rating <= 2.5) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);
            star_image_3.setImageResource(R.drawable.half_star_icon);

            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
            star_image_31.setImageResource(R.drawable.half_star_icon);
        }
        if (rating > 2.5 && rating <= 3.0) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);
            star_image_3.setImageResource(R.drawable.full_star_icon);
            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
            star_image_31.setImageResource(R.drawable.full_star_icon);

        }
        if (rating > 3.0 && rating <= 3.5) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);
            star_image_3.setImageResource(R.drawable.full_star_icon);
            star_image_4.setImageResource(R.drawable.half_star_icon);
            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
            star_image_31.setImageResource(R.drawable.full_star_icon);
            star_image_41.setImageResource(R.drawable.half_star_icon);


        }
        if (rating > 3.5 && rating <= 4.0) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);
            star_image_3.setImageResource(R.drawable.full_star_icon);
            star_image_4.setImageResource(R.drawable.full_star_icon);

            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
            star_image_31.setImageResource(R.drawable.full_star_icon);
            star_image_41.setImageResource(R.drawable.full_star_icon);
        }
        if (rating > 4.0 && rating <= 4.5) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);
            star_image_3.setImageResource(R.drawable.full_star_icon);
            star_image_4.setImageResource(R.drawable.full_star_icon);
            star_image_5.setImageResource(R.drawable.half_star_icon);
            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
            star_image_31.setImageResource(R.drawable.full_star_icon);
            star_image_41.setImageResource(R.drawable.full_star_icon);
            star_image_51.setImageResource(R.drawable.half_star_icon);

        }
        if (rating > 4.5 && rating <= 5.0) {
            star_image_1.setImageResource(R.drawable.full_star_icon);
            star_image_2.setImageResource(R.drawable.full_star_icon);
            star_image_3.setImageResource(R.drawable.full_star_icon);
            star_image_4.setImageResource(R.drawable.full_star_icon);
            star_image_5.setImageResource(R.drawable.full_star_icon);

            star_image_11.setImageResource(R.drawable.full_star_icon);
            star_image_21.setImageResource(R.drawable.full_star_icon);
            star_image_31.setImageResource(R.drawable.full_star_icon);
            star_image_41.setImageResource(R.drawable.full_star_icon);
            star_image_51.setImageResource(R.drawable.full_star_icon);
        }

    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "Pending_Task_Final_Fragment :- " + ConstantValues.UpdateTaskURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (result_code.equalsIgnoreCase("1")) {
                    try {

                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.suc_completed),
                                Toast.LENGTH_SHORT);

                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        sqlcon.open();
                        sqlcon.taskDelete(task_list_id, et_id);

                        if (is_google_sheet != null && is_google_sheet.equals("1") && google_sheet_url != null && google_sheet_url.length() > 0) {

                            Report_Task_Details_Fragment detailFragment = new Report_Task_Details_Fragment();
                            detailFragment.setgoogleUrl(google_sheet_url);
                            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                    detailFragment, true, true);
                        } else {
                            mActivity.ClearAllPages();
                            // mActivity.popFragments();
                            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                    new AppTab_Dashboard_Home(), true, true);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (result_code.equalsIgnoreCase("18")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_18));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (result_code.equalsIgnoreCase("17")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (status.equalsIgnoreCase("failed")) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());

                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }
        } else {

        }

    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {

                if (mLogin != null && mLogin.size() > 0) {
                    if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                        Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                        ConstantFunction.savestatus(getActivity(),
                                "UserId", mLogin.get(0).emp_id + "");
                        Log.v("userId", mLogin.get(0).emp_id);

                        Picasso.with(getActivity())
                                .load(uri)
                                .transform(new CircleTransform()).resize(100, 100)
                                .into(headerUserImage);
                    }
                }
            }
        }
    }
}
