package com.asrapps.mowom.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.broadcost.MowomService;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.asrapps.mowom.R.id.activeToggleButton;

public class AppTab_Settings_Home extends BaseFragment implements OnClickListener, OnCheckedChangeListener, AsyncResponseListener, OnCancelListener {

    RadioButton radioEnglishLanguage, radioPersianLanguage;

    RelativeLayout rlMainPrivacy;
    Button btnLangSave;
    private RadioGroup radioLanguages;
    ImageView headerUserImage;
    ToggleButton tgbutton, privacyBtn;

    private RadioGroup setingsRadioInActive;
    RelativeLayout radioGroupLayout;

    String selectedLanguage;
    String active;
    String reason = "";
    View view;
    //private RadioButton radioStatusButton;
    private ProgressHUD mProgressHUD;

    private static final int REQUEST_LOCATION = 113;

    RadioButton absentBtn, sickBtn, holydayBtn;

    TextView lblEng, lblPer, lblActive, lblPrivacy;

    TextView rdPer, rdEng, rdAbsent, rdSick, rdHoliday;


    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        internetConnection = new ConnectionDetector(getActivity());
        sqlcon = new SQLController(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = inflater
                    .inflate(R.layout.settings_layout_pr, container, false);
        } else {
            view = inflater
                    .inflate(R.layout.settings_layout, container, false);
        }

        tgbutton = (ToggleButton) view.findViewById(R.id.activeToggleButton);

        rlMainPrivacy = (RelativeLayout) view.findViewById(R.id.rlMainPrivacy);
        rdPer = (TextView) view.findViewById(R.id.rdPer);
        rdEng = (TextView) view.findViewById(R.id.rdEng);

        rdAbsent = (TextView) view.findViewById(R.id.rdAbsent);
        rdSick = (TextView) view.findViewById(R.id.rdSick);
        rdHoliday = (TextView) view.findViewById(R.id.rdHoliday);

        tgbutton.setOnClickListener(this);

        radioGroupLayout = (RelativeLayout) view
                .findViewById(R.id.radioGroupLayout);

        lblEng = (TextView) view.findViewById(R.id.lblEng);
        lblPer = (TextView) view.findViewById(R.id.lblPer);
        lblActive = (TextView) view.findViewById(R.id.lblActive);
        lblPrivacy = (TextView) view.findViewById(R.id.lblPrivacy);

        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
        //		headerUserImage.setOnClickListener(this);

        radioLanguages = (RadioGroup) view.findViewById(R.id.radioLanguages);
        radioLanguages.setOnCheckedChangeListener(this);

        radioEnglishLanguage = (RadioButton) view
                .findViewById(R.id.radioEnglishLanguage);
        radioEnglishLanguage.setOnClickListener(this);

//		radioFrenchLanguage = (RadioButton) view
//				.findViewById(R.id.radioFrenchLanguage);
//		radioFrenchLanguage.setOnClickListener(this);

        radioPersianLanguage = (RadioButton) view
                .findViewById(R.id.radioPersianLanguage);
        radioPersianLanguage.setOnClickListener(this);


        btnLangSave = (Button) view.findViewById(R.id.btnLangSave);
        btnLangSave.setOnClickListener(this);

        setingsRadioInActive = (RadioGroup) view.findViewById(R.id.setingsRadioInActive);
        setingsRadioInActive.setOnCheckedChangeListener(this);

        absentBtn = (RadioButton) view.findViewById(R.id.absentBtn);
        absentBtn.setOnClickListener(this);

        sickBtn = (RadioButton) view.findViewById(R.id.sickBtn);
        sickBtn.setOnClickListener(this);

        holydayBtn = (RadioButton) view.findViewById(R.id.holydayBtn);
        holydayBtn.setOnClickListener(this);

        privacyBtn = (ToggleButton) view.findViewById(R.id.privacyToggleButton);
        //privacyBtn.setOnCheckedChangeListener(toggleButtonChangeListener);
        privacyBtn.setOnClickListener(this);

        SetSelectedLanguage();
        SetSelected();
        if (internetConnection.isConnectingToInternet("")) {
            SetUserDetails();
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }


        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        String is_privacy = ConstantFunction.getstatus(getActivity(), "is_privacy");

        if (is_privacy.equals("0")) {
            rlMainPrivacy.setVisibility(View.GONE);
        } else {
            rlMainPrivacy.setVisibility(View.VISIBLE);
        }

        String tg = ConstantFunction.getstatus(getActivity(), "toggle");

        if (tg.equals("0")) {
            absentBtn.setChecked(false);
            sickBtn.setChecked(false);
            holydayBtn.setChecked(false);
        } else if (tg.equals("1")) {
            absentBtn.setChecked(true);
            sickBtn.setChecked(false);
            holydayBtn.setChecked(false);
        } else if (tg.equals("2")) {
            absentBtn.setChecked(false);
            sickBtn.setChecked(true);
            holydayBtn.setChecked(false);
        } else if (tg.equals("3")) {
            absentBtn.setChecked(false);
            sickBtn.setChecked(false);
            holydayBtn.setChecked(true);
        }

        return view;
    }

    // Start the service
    public void startNewService() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                getActivity().startService(new Intent(getActivity(), MowomService.class));
            }
        } else {
            getActivity().startService(new Intent(getActivity(), MowomService.class));
        }

//        getActivity().startService(new Intent(getActivity(), MowomService.class));
        ConstantFunction.savestatus(getActivity(), "privacy", "true");//  getuser(getActivity(), "privacy");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_LOCATION: {
                getActivity().startService(new Intent(getActivity(), MowomService.class));
            }
        }

    }


    // Stop the service
    public void stopNewService() {

        getActivity().stopService(new Intent(getActivity(), MowomService.class));
        ConstantFunction.savestatus(getActivity(), "privacy", "false");//  getuser(getActivity(), "privacy");
    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */
        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        if (v.getId() == R.id.radioEnglishLanguage) {
//			if (radioFrenchLanguage.isChecked())
//				radioFrenchLanguage.setChecked(false);
            if (radioPersianLanguage.isChecked())
                radioPersianLanguage.setChecked(false);
        }

//		if (v.getId() == R.id.radioFrenchLanguage) {
//			if (radioEnglishLanguage.isChecked())
//				radioEnglishLanguage.setChecked(false);
//			if (radioPersianLanguage.isChecked())
//				radioPersianLanguage.setChecked(false);
//		}

        if (v.getId() == R.id.radioPersianLanguage) {
            if (radioEnglishLanguage.isChecked())
                radioEnglishLanguage.setChecked(false);
//			if (radioFrenchLanguage.isChecked())
//				radioFrenchLanguage.setChecked(false);
        }

        if (v.getId() == R.id.absentBtn) {
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (v.getId() == R.id.sickBtn) {
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (v.getId() == R.id.holydayBtn) {
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
        }


        if (absentBtn.isChecked() && !tgbutton.isChecked()) {
            ConstantFunction.savestatus(getActivity(), "toggle", "1");
        } else if (sickBtn.isChecked() && !tgbutton.isChecked()) {
            ConstantFunction.savestatus(getActivity(), "toggle", "2");
        } else if (holydayBtn.isChecked() && !tgbutton.isChecked()) {
            ConstantFunction.savestatus(getActivity(), "toggle", "3");
        } else {
            ConstantFunction.savestatus(getActivity(), "toggle", "0");
        }


        if (v.getId() == activeToggleButton) {

            if (tgbutton.isChecked()) {
                tgbutton.setText("On");
                ConstantFunction.savestatus(getActivity(), "toggle", "0");
                radioGroupLayout.setVisibility(View.GONE);
            } else {
                tgbutton.setText("Off");
                radioGroupLayout.setVisibility(View.VISIBLE);
            }
        }

        if (v.getId() == R.id.privacyToggleButton) {

            if (privacyBtn.isChecked()) {
                privacyBtn.setText("On");
                stopNewService();
                Toast.makeText(getActivity(), "Stop Service", Toast.LENGTH_SHORT).show();
            } else {
                privacyBtn.setText("Off");
                startNewService();
                Toast.makeText(getActivity(), "Start Service", Toast.LENGTH_SHORT).show();
            }
        }

        if (v.getId() == R.id.btnLangSave) {

            String reason = ConstantFunction.getuser(getActivity(), "userstatus");


            if (tgbutton.isChecked()) {
                tgbutton.setText("On");
                ConstantFunction.savestatus(getActivity(), "userstatus", "0");
                radioGroupLayout.setVisibility(View.GONE);
            } else {
                tgbutton.setText("Off");
                ConstantFunction.savestatus(getActivity(), "userstatus", "-1");
                radioGroupLayout.setVisibility(View.VISIBLE);
            }



            if (reason.equalsIgnoreCase("") || tgbutton.isChecked()) {
                RadioSelected();
//                ChangeLanguage();

            } else {
                RadioSelected();
            }
        }

        if (v.getId() == R.id.headerUserImage)
            ShowPopUp();
    }


    private void RadioSelected() {

        boolean send = false;

        if (tgbutton.isChecked()) {
            active = "1";
            reason = "";
            send = true;
        } else {

            if (!absentBtn.isChecked() || !sickBtn.isChecked() || !holydayBtn.isChecked()) {

                if (absentBtn.isChecked()) {
                    reason = getResources().getString(R.string.absent);
                    active = "2";
                    send = true;
                } else if (sickBtn.isChecked()) {
                    reason = getResources().getString(R.string.sick);
                    active = "2";
                    send = true;
                } else if (holydayBtn.isChecked()) {
                    reason = getResources().getString(R.string.holyday);
                    active = "2";
                    send = true;
                }

            } else {
                send = false;
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.select_status), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }

        }
        if (send) {

            if (internetConnection
                    .isConnectingToInternet(getString(R.string.check_connection))) {

                String selectedLanguageStr="";
                if(radioPersianLanguage.isChecked())
                {
                    selectedLanguageStr="fr";
                }
                else
                {
                    selectedLanguageStr="en";
                }
                UpdateStatus(ConstantValues.UpdateStatusURL, reason, active,selectedLanguageStr);
            }
        }


    }

    private void ChangeLanguage() {



        if (radioPersianLanguage.isChecked()) {
//				ConstantFunction.ChangeLang(getContext(), "pr");
//				ConstantFunction.Restart(getActivity(),"pr");

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getActivity().getString(R.string.need_re_start));
            builder.setCancelable(false);
            builder.setPositiveButton(getActivity().getString(R.string.yes), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    ConstantFunction.ChangeLang(getContext(), "fr");

                    if (tgbutton.isChecked()) {
                        tgbutton.setText("On");
                        ConstantFunction.savestatus(getActivity(), "userstatus", "0");
                        radioGroupLayout.setVisibility(View.GONE);
                    } else {
                        tgbutton.setText("Off");
                        ConstantFunction.savestatus(getActivity(), "userstatus", "-1");
                        radioGroupLayout.setVisibility(View.VISIBLE);
                    }


                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.already_active), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

//						new Handler().postDelayed(new Runnable() {
//							@Override
//							public void run() {

                    ConstantFunction.RestartTheApp(getActivity(), 2);
//							}
//						}, 1000);


                }

            });
            builder.setNegativeButton(getActivity().getString(R.string.no), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    // TODO Auto-generated method stub
//                    String lan = ConstantFunction.getLocale(getActivity());
//                    boolean isEnglish = lan.equalsIgnoreCase("en");
//                    radioEnglishLanguage.setChecked(isEnglish);
//                    radioPersianLanguage.setChecked(!isEnglish);
                }
            });
            builder.show();
//				Toast toast = Toast.makeText(getActivity(),
//						getString(R.string.pr_lng_changed),
//						Toast.LENGTH_SHORT);
//				// toast.setGravity(Gravity.CENTER, 0, 0);
//				toast.show();

        }

//			if (radioFrenchLanguage.isChecked()) {
////				ConstantFunction.ChangeLang(getContext(), "fr");
//				ConstantFunction.Restart(getActivity(),"fr");
////				Toast toast = Toast.makeText(getActivity(),
////						getString(R.string.fr_lng_changed),
////						Toast.LENGTH_SHORT);
////				// toast.setGravity(Gravity.CENTER, 0, 0);
////				toast.show();
//
//			}

        if (radioEnglishLanguage.isChecked()) {
//				ConstantFunction.ChangeLang(getContext(), "en");
//
//				ConstantFunction.Restart(getActivity(),"en");

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getActivity().getString(R.string.need_re_start));
            builder.setCancelable(false);
            builder.setPositiveButton(getActivity().getString(R.string.yes), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    ConstantFunction.ChangeLang(getContext(), "en");


                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), getActivity().getString(R.string.already_active), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

//						new Handler().postDelayed(new Runnable() {
//							@Override
//							public void run() {

                    ConstantFunction.RestartTheApp(getActivity(), 2);
//							}
//						}, 1000);


                }

            });
            builder.setNegativeButton(getActivity().getString(R.string.no), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    // TODO Auto-generated method
//                    String lan = ConstantFunction.getLocale(getActivity());
//                    boolean isEnglish = lan.equalsIgnoreCase("en");
//                    radioEnglishLanguage.setChecked(isEnglish);
//                    radioPersianLanguage.setChecked(!isEnglish);
                }
            });
            builder.show();


//				Toast toast = Toast.makeText(getActivity(),
//						getString(R.string.en_lng_changed),
//						Toast.LENGTH_SHORT);
//				// toast.setGravity(Gravity.CENTER, 0, 0);
//				toast.show();

        }

//		}
    }

    private void SetSelectedLanguage() {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
            radioEnglishLanguage.setChecked(true);
        }
        if (selectedLanguage.equalsIgnoreCase("fr"))
            radioPersianLanguage.setChecked(true);
//		if (selectedLanguage.equalsIgnoreCase("fr"))
//			radioFrenchLanguage.setChecked(true);
        if (selectedLanguage.equalsIgnoreCase("en"))
            radioEnglishLanguage.setChecked(true);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        // TODO Auto-generated method stub
        if (checkedId == R.id.radioEnglishLanguage) {
//			if (radioFrenchLanguage.isChecked())
//				radioFrenchLanguage.setChecked(false);
            if (radioPersianLanguage.isChecked())
                radioPersianLanguage.setChecked(false);
        }

//		if (checkedId == R.id.radioFrenchLanguage) {
//			if (radioEnglishLanguage.isChecked())
//				radioEnglishLanguage.setChecked(false);
//			if (radioPersianLanguage.isChecked())
//				radioPersianLanguage.setChecked(false);
//		}

        if (checkedId == R.id.radioPersianLanguage) {
            if (radioEnglishLanguage.isChecked())
                radioEnglishLanguage.setChecked(false);
//			if (radioFrenchLanguage.isChecked())
//				radioFrenchLanguage.setChecked(false);
        }

        if (checkedId == R.id.absentBtn) {
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (checkedId == R.id.sickBtn) {
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (checkedId == R.id.holydayBtn) {
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
        }

    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }


    private void UpdateStatus(String url, String reason, String active, String selectedLanguageStr) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("reason", reason);
            requestObject.put("active", active);
            requestObject.put("is_app_lang", selectedLanguageStr);

            Log.d("UpdateStatus ", active.toString());
        } catch (JSONException e) {
            Log.d("Exception", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(url);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Settings_Home :- " + ConstantValues.UpdateStatusURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "AppTab_Settings_Home :- " + ConstantValues.UpdateStatusURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            if (!response.equalsIgnoreCase("")) {
                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

                    if (status.equalsIgnoreCase("success")) {
                        try {

                            JSONObject job2 = job.getJSONObject("data");

                            ConstantFunction.savestatus(getActivity(),
                                    "status", reason);

                            ConstantFunction.savestatus(getActivity(),
                                    "availability", job2.get("availability")
                                            .toString());

                            String availability = job2.get("availability").toString();
                            if (availability.equals("1")) {
                                ConstantFunction.savestatus(getActivity(), "toggle", "0");
                            }

                            ConstantFunction.savestatus(getActivity(),
                                    "leavereason", job2.get("leavereason")
                                            .toString());
                            String leavreson = job2.get("leavereason").toString();

                            if (leavreson.equals(getResources().getString(R.string.absent))) {
                                ConstantFunction.savestatus(getActivity(), "toggle", "1");
                                ConstantFunction.savestatus(getActivity(), "userstatus", leavreson + "");
                            } else if (leavreson.equals(getResources().getString(R.string.sick))) {
                                ConstantFunction.savestatus(getActivity(), "toggle", "2");
                                ConstantFunction.savestatus(getActivity(), "userstatus", leavreson + "");
                            } else if (leavreson.equals(getResources().getString(R.string.holyday))) {
                                ConstantFunction.savestatus(getActivity(), "toggle", "3");
                                ConstantFunction.savestatus(getActivity(), "userstatus", leavreson + "");
                            }

                            ChangeLanguage();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (status.equalsIgnoreCase("failed")) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                        ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getActivity());


                                        startActivity(new Intent(getActivity(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }

                } catch (Exception e) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again),
                            Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }
    }

    private void SetSelected() {
        String reason = ConstantFunction.getuser(getActivity(), "userstatus");
        String toggele = ConstantFunction.getuser(getActivity(), "toggle");
        String privacy = ConstantFunction.getuser(getActivity(), "privacy");

        if (privacy.equals("true")) {
            privacyBtn.setText("Off");
            privacyBtn.setChecked(false);
        } else {
            privacyBtn.setChecked(true);
            privacyBtn.setText("On");
        }


        if (toggele.equals("1")) {
            absentBtn.setChecked(true);
        } else if (toggele.equals("2")) {
            sickBtn.setChecked(true);
        }
        if (toggele.equals("3")) {
            holydayBtn.setChecked(true);
        }


        if (reason.equalsIgnoreCase("") || reason.equalsIgnoreCase("-1")) {
            tgbutton.setChecked(false);
            tgbutton.setText("Off");
            radioGroupLayout.setVisibility(View.VISIBLE);
        } else {
            tgbutton.setText("On");
            tgbutton.setChecked(true);
            radioGroupLayout.setVisibility(View.GONE);
//            if (reason.equalsIgnoreCase(getString(R.string.absent))) {
//                absentBtn.setChecked(true);
//                sickBtn.setChecked(false);
//                holydayBtn.setChecked(false);
//            }
//
//            if (reason.equalsIgnoreCase(getString(R.string.sick))) {
//                absentBtn.setChecked(false);
//                sickBtn.setCheckif (reason.equalsIgnoreCase("") ed(true);
//                holydayBtn.setChecked(false);
//            }
//
//            if (reason.equalsIgnoreCase(getString(R.string.holyday))) {
//                absentBtn.setChecked(false);
//                sickBtn.setChecked(false);
//                holydayBtn.setChecked(true);
//            }
        }

        if (toggele.equals("0")) {
            tgbutton.setText("On");
            tgbutton.setChecked(true);
            radioGroupLayout.setVisibility(View.GONE);
        } else
        {
            tgbutton.setChecked(false);
            tgbutton.setText("Off");
            radioGroupLayout.setVisibility(View.VISIBLE);
        }
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);


                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }
}
