package com.asrapps.mowom.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.BuildConfig;
import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.NextStepDropDownAdapter;
import com.asrapps.mowom.adapter.Photo_Sales_Grid_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.OnDeleteSalesImageClick;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.asrapps.mowom.model.SalesTask;
import com.asrapps.mowom.view.HorizontalListView;
import com.asrapps.mowom.view.InternalStorageContentProvider;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.asrapps.mowom.fragments.AppTab_Profile_Home.copyStream;

public class Sales_page_finished_task_fragment extends BaseFragment implements
        OnClickListener {

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1888;
    private static final int SELECT_PICTURE = 1000;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Movom";
    static File mediaStorageDir;
    public ArrayList<String> nextStepData = new ArrayList<String>();

    // TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String taskId, issue;

    TextView pen_tsk_srt_id_header;

    HorizontalListView photoGrid;

    Photo_Sales_Grid_Adapter adapter;
    Resources res;
    // public Pending_Task_Photo_Activity CustomListView = null;
    public ArrayList<PhotoSalesModel> CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();

    View view;

    ImageView camaraImage2, camaraImage1, closeImageCam1, closeImageCam2;
    RelativeLayout camaraImage1Layout, camaraImage2Layout;


    private Uri fileUri;

    boolean camOneSelected, camTwoSelected, add;
    // boolean cam1ImageFilled = false, cam2ImageFilled = false;
    static String saveId = "";

    Button addImageButton, doneButton;

    private ArrayList<Login> mLogin;

    SQLController sqlcon;
    public ArrayList<SalesTask> arrSalesTask = new ArrayList<>();
    static String timeStamp = "";
    ConnectionDetector internetConnection;
    String taskNotes;
    int count = 0;

    String image1DI, image2ID;
    String selectedLanguage;

    //String id="";
    ImageView imgOne, imgTwo, imgThree, imgFour;

    TextView addTextView;
    LinearLayout llEng, llPersian;
    PopupWindow nextStepPopup;
    private EditText product, MeetingNote, Details;
    private TextView NextSteps;
    ArrayList<SalesTask> arrayList = new ArrayList<>();

    boolean isCamera;

    public static File fileName = null;

    public Sales_page_finished_task_fragment(ArrayList<SalesTask> arrayList) {

        this.arrayList = arrayList;

        Log.e("AAA", "Arraylist size in final page ; " + arrayList.size());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = inflater.inflate(R.layout.sales_finished_task_pr,
                        container, false);

            } else {
                view = inflater.inflate(R.layout.sales_finished_task,
                        container, false);
            }

			/*
             * txtHeaderUserName = (TextView) view
			 * .findViewById(R.id.txtHeaderUserName);
			 */
            nextStepData = new ArrayList<>();
            nextStepData.add(getResources().getString(R.string.Followup));
            nextStepData.add(getResources().getString(R.string.SendInformation));
            nextStepData.add(getResources().getString(R.string.Contactnotavailable));
            nextStepData.add(getResources().getString(R.string.NotInterested));

            product = (EditText) view.findViewById(R.id.product);
            MeetingNote = (EditText) view.findViewById(R.id.MeetingNote);
            Details = (EditText) view.findViewById(R.id.Details);
            NextSteps = (TextView) view.findViewById(R.id.NextSteps);
            NextSteps.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                    WebServiceHelper.hideKeyboard(getActivity(), v);

                    showNextStepPopup();
                }
            });

            llEng = (LinearLayout) view.findViewById(R.id.llEng);
            llPersian = (LinearLayout) view.findViewById(R.id.llPersian);

            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
//			headerUserImage.setOnClickListener(this);

            addTextView = (TextView) view.findViewById(R.id.addTextView);


            imgOne = (ImageView) view.findViewById(R.id.imgOne);
            imgTwo = (ImageView) view.findViewById(R.id.imgTwo);
            imgThree = (ImageView) view.findViewById(R.id.imgThree);
            imgFour = (ImageView) view.findViewById(R.id.imgFour);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);

            addImageButton = (Button) view.findViewById(R.id.addImageButton);
            addImageButton.setOnClickListener(this);

            doneButton = (Button) view.findViewById(R.id.doneButton);
            doneButton.setOnClickListener(this);
            // addImageButton.setEnabled(false);

            pen_tsk_srt_id_header = (TextView) view
                    .findViewById(R.id.pen_tsk_srt_id_header);

            camaraImage1 = (ImageView) view.findViewById(R.id.camaraImage1);
            camaraImage1.setOnClickListener(this);

            camaraImage2 = (ImageView) view.findViewById(R.id.camaraImage2);
            camaraImage2.setOnClickListener(this);

            closeImageCam1 = (ImageView) view.findViewById(R.id.closeImageCam1);
            closeImageCam1.setOnClickListener(this);

            closeImageCam2 = (ImageView) view.findViewById(R.id.closeImageCam2);
            closeImageCam2.setOnClickListener(this);

            closeImageCam1.setVisibility(View.GONE);
            closeImageCam2.setVisibility(View.GONE);

            camaraImage1Layout = (RelativeLayout) view
                    .findViewById(R.id.camaraImage1Layout);
            camaraImage2Layout = (RelativeLayout) view
                    .findViewById(R.id.camaraImage2Layout);

            res = getResources();
            photoGrid = (HorizontalListView) view.findViewById(R.id.photoGrid);
            photoGrid.setVisibility(View.GONE);

            sqlcon = new SQLController(getActivity());


            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                addTextView.setGravity(Gravity.RIGHT);

//				imgOne.setImageResource(R.drawable.pr_step1_unhover);
//				imgTwo.setImageResource(R.drawable.pr_step2_hover);
//				imgThree.setImageResource(R.drawable.pr_step3_unhover);
//				imgFour.setImageResource(R.drawable.pr_step4_unhover);

//                llEng.setVisibility(View.GONE);
//                llPersian.setVisibility(View.VISIBLE);

//                RelativeLayout.LayoutParams params11 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                params11.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//                params11.addRule(RelativeLayout.CENTER_VERTICAL);
//                params11.addRule(RelativeLayout.RIGHT_OF, R.id.addImageButton);
//                addTextView.setLayoutParams(params11);
//
//                RelativeLayout.LayoutParams params111 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                params111.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//                params111.addRule(RelativeLayout.CENTER_VERTICAL);
//                addImageButton.setLayoutParams(params111);
//                addImageButton.setGravity(Gravity.RIGHT);


            } else {
//                llEng.setVisibility(View.VISIBLE);
//                llPersian.setVisibility(View.GONE);
            }

//            setImageView();

            sqlcon.open();
            arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));



            internetConnection = new ConnectionDetector(getActivity());
            if (internetConnection
                    .isConnectingToInternet("")) {

                CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();

                if(arrSalesTask!=null && arrSalesTask.size()>0 && arrSalesTask.get(0).task_id_Tx!=null) {
                    CustomListViewValuesArr = GetImageFromDb(arrSalesTask.get(0).task_id_Tx);

                    count = CustomListViewValuesArr.size();
                }

                if(CustomListViewValuesArr!=null && CustomListViewValuesArr.size()>0)
                {

                }
                else {
                    TruckateTable();
                }

                SetUserDetails();
                GetFromShared();
            } else {

                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();

                CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();



                if(arrSalesTask!=null && arrSalesTask.size()>0 && arrSalesTask.get(0).task_id_Tx!=null) {
                    CustomListViewValuesArr = GetImageFromDb(arrSalesTask.get(0).task_id_Tx);

                    count = CustomListViewValuesArr.size();
                }
                if(CustomListViewValuesArr!=null && CustomListViewValuesArr.size()>0)
                {

                }
                else {
                    TruckateTable();
                }

                GetFromShared();
            }

            //store data already filled
            if (arrSalesTask.size() > 0 && arrSalesTask != null) {

                setImageView();

                if (arrSalesTask.get(0).product != null && !arrSalesTask.get(0).product.equals(""))
                    product.setText(arrSalesTask.get(0).product);
                if (arrSalesTask.get(0).notes != null && !arrSalesTask.get(0).notes.equals(""))
                    MeetingNote.setText(arrSalesTask.get(0).notes);
                if (arrSalesTask.get(0).detail != null && !arrSalesTask.get(0).detail.equals(""))
                    Details.setText(arrSalesTask.get(0).detail);
                if (arrSalesTask.get(0).next_steps != null && !arrSalesTask.get(0).next_steps.equals(""))
                    NextSteps.setText(arrSalesTask.get(0).next_steps);
            }



        }
        return view;
    }

    private void setImageView() {

        CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();
        CustomListViewValuesArr = GetImageFromDb(arrayList.get(0).task_id_Tx);

        count=CustomListViewValuesArr.size();

        for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
            String path = CustomListViewValuesArr.get(i).getTaskId();
            if (path.equals(arrayList.get(0).task_id_Tx)) {
                try {
                    if (CustomListViewValuesArr.size() <= 2) {
                        photoGrid.setVisibility(View.GONE);
                        camaraImage1Layout.setVisibility(View.VISIBLE);
                        camaraImage2Layout.setVisibility(View.VISIBLE);

                        if (CustomListViewValuesArr.size() == 1) {
                            closeImageCam1.setVisibility(View.VISIBLE);
                            image1DI = CustomListViewValuesArr.get(i).getPhotoId();

                            Bitmap bm = null;
                            bm = BitmapFactory.decodeByteArray(CustomListViewValuesArr.get(i).getTakenPhoto(),
                                    0, CustomListViewValuesArr.get(i).getTakenPhoto().length);

                            camaraImage1.setImageBitmap(bm);
                        }
                        if (CustomListViewValuesArr.size() == 2) {
                            if (i == 0) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = CustomListViewValuesArr.get(i).getPhotoId();
                                //image2ID="";

                                Bitmap bm = null;
                                // bm = decodeSampledBitmapFromUri(itemList.get(i), 350, 220);
                                bm = BitmapFactory.decodeByteArray(CustomListViewValuesArr.get(i).getTakenPhoto(),
                                        0, CustomListViewValuesArr.get(i).getTakenPhoto().length);


                                camaraImage1.setImageBitmap(bm);
                            }

                            if (i == 1) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                image2ID = CustomListViewValuesArr.get(i).getPhotoId();
                                //image2ID="";

                                Bitmap bm = null;
                                // bm = decodeSampledBitmapFromUri(itemList.get(i), 350, 220);
                                bm = BitmapFactory.decodeByteArray(CustomListViewValuesArr.get(i).getTakenPhoto(),
                                        0, CustomListViewValuesArr.get(i).getTakenPhoto().length);

                                camaraImage2.setImageBitmap(bm);

                            }
                        }

                        if (CustomListViewValuesArr.size() > 2) {
                            photoGrid.setVisibility(View.VISIBLE);
                            camaraImage1Layout.setVisibility(View.GONE);
                            camaraImage2Layout.setVisibility(View.GONE);
                            SetListView();
                        }

                    } else {
                        photoGrid.setVisibility(View.VISIBLE);
                        camaraImage1Layout.setVisibility(View.GONE);
                        camaraImage2Layout.setVisibility(View.GONE);
                        SetListView();

                    }


                } catch (OutOfMemoryError e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                } catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Plaese Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                }

            }
        }


        System.out.println(".................CustomListViewValuesArr.size()--> " + CustomListViewValuesArr.size());

    }


    private ArrayList<PhotoSalesModel> GetImageFromDb(String task_id_tx) {
        sqlcon.open();
        ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
        tempArray = sqlcon.GetAllPhotosSalesTx(ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
        return tempArray;
    }

    private void SetListView() {

        CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();
        CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
        // photoGrid.removeAll();
        // for (int i = 0; i <CustomListViewValuesArr.size(); i++) {
        // String path = CustomListViewValuesArr.get(i).getTaskId();
        // if (path.equals(taskId)) {
        // photoGrid.add(CustomListViewValuesArr.get(i),this);
        // }
        // }

        System.out.println(".................CustomListViewValuesArr.size()--> " + CustomListViewValuesArr.size());
        adapter = new Photo_Sales_Grid_Adapter(getActivity(),
                CustomListViewValuesArr, new OnDeleteSalesImageClick() {

            public void onClickDelete(View v, int position,
                                      PhotoSalesModel photo) {
                // TODO Auto-generated method stub

                DeletePhotoFromDb(photo.getPhotoId(), position);

            }
        });

        photoGrid.setAdapter(adapter);

    }

    private void GetFromShared() {

        taskId = arrayList.get(0).task_id_Tx;
        issue = arrayList.get(0).task_name.toUpperCase() + " (" + arrayList.get(0).category_Name + ")";

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + toPersianNumber(taskId) + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        } else {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + taskId + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        }

        saveId = "img_" + taskId;
    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }


    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    @Override
    public void onClick(View v) {

        sqlcon.open();
        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);

        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            case R.id.camaraImage1:
                camOneSelected = true;
                camTwoSelected = false;
                add = false;
                // captureImage(); ,
                // if(camaraImage1.getDrawable() == null)

                if (closeImageCam1.getVisibility() != View.VISIBLE) {

                    if (count < 5)
                        ShowSelectImageDialog(getActivity());
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.only_five_allowed),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                break;

            case R.id.camaraImage2:
                camOneSelected = false;
                camTwoSelected = true;
                add = false;
                // captureImage();
                // if(camaraImage2.getDrawable() == null)
                if (closeImageCam2.getVisibility() != View.VISIBLE) {

                    if (count < 5)
                        ShowSelectImageDialog(getActivity());
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.only_five_allowed),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
                break;

            case R.id.addImageButton:
                camOneSelected = false;
                camTwoSelected = false;
                add = true;
                if (count < 5)
                    ShowSelectImageDialog(getActivity());
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.only_five_allowed),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;

            case R.id.doneButton:
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Done();
                break;

            case R.id.closeImageCam1:
                DeletePhoto(image1DI, true);
                break;

            case R.id.closeImageCam2:
                DeletePhoto(image2ID, false);
                break;

            default:
                break;
        }
    }

    private void Done() {

        if (count > 0) {
            if (product.getText().toString().length() == 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_product), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;

            } else if (MeetingNote.getText().toString().length() == 0) {

                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_meeting_notes), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            } else if (NextSteps.getText().toString().length() == 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.slect_next_steps), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;

            } else if (Details.getText().toString().length() == 0) {

                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_details), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                return;
            } else {

                String strProduct = product.getText().toString();
                String strNotes = MeetingNote.getText().toString();
                String strNextStep = NextSteps.getText().toString();
                String strDetails = Details.getText().toString();

                arrayList.get(0).product = strProduct;
                arrayList.get(0).notes = strNotes;
                arrayList.get(0).next_steps = strNextStep;
                arrayList.get(0).detail = strDetails;


                sqlcon.open();
                arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

                //store data already filled
                if (arrSalesTask.size() > 0 && arrSalesTask != null) {

                    if (arrSalesTask.get(0).task_id_Tx != null && !arrSalesTask.get(0).task_id_Tx.equals("")) {
                        if (!arrayList.get(0).product.equals("")) {
                            sqlcon.open();
                            sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_PRODUCT, arrayList.get(0).product, arrSalesTask.get(0).task_id_Tx);

                        }

                        if (!arrayList.get(0).notes.equals("")) {
                            sqlcon.open();
                            sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_MEETING_NOTE, arrayList.get(0).notes, arrSalesTask.get(0).task_id_Tx);
                        }

                        if (!arrayList.get(0).next_steps.equals("")) {
                            sqlcon.open();
                            sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_NEXT_STEP, arrayList.get(0).next_steps, arrSalesTask.get(0).task_id_Tx);
                        }

                        if (!arrayList.get(0).detail.equals("")) {
                            sqlcon.open();
                            sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_DETAIL, arrayList.get(0).detail, arrSalesTask.get(0).task_id_Tx);
                        }
                    }
                }


                Sales_Task_Final_Fragment writeReviewFragment = new Sales_Task_Final_Fragment(arrayList);
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        writeReviewFragment, true, true);
            }

        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_add_photo), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }


    }

    private void captureImage() {
//		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//		getActivity().startActivityForResult(intent,
//				CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 200);
            } else {
                cameraIntent();
//                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                fileUri = Uri.fromFile(fileName);
//                i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        } else {
//            Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//            fileUri = Uri.fromFile(fileName);
//            i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//            i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            cameraIntent();
        }
    }
    private void cameraIntent() {

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        } else {
            fileName = new File(getActivity().getFilesDir(), System.currentTimeMillis() + AppConstants.TEMP_PHOTO_FILE_NAME);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(fileName);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

                fileUri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        fileName);

            } else {
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    fileUri = Uri.fromFile(fileName);
                } else {
                    fileUri = InternalStorageContentProvider.CONTENT_URI;
                }

            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    fileUri);
            intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getActivity().startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
        } catch (Exception e) {

            Log.d("CAMERA", "cannot take picture", e);
        }

    }
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + saveId + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        getActivity();

        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            getActivity();
            if (resultCode == Activity.RESULT_OK) {
                previewCapturedImage(data);
            } else {
                getActivity();
                if (resultCode == Activity.RESULT_CANCELED) {

                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            "Sorry! Failed to capture image",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        } else if (resultCode != Activity.RESULT_CANCELED && data != null) {
            if (requestCode == SELECT_PICTURE) {
                try {

                    Uri selectedImageUri = data.getData();
                    String[] projection = {MediaStore.MediaColumns.DATA};
                    Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                            null);
                    int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    cursor.moveToFirst();

                    String selectedImagePath = cursor.getString(column_index);

                    Log.v("TTT", "selectedImagePath = " + selectedImagePath);

                    try {
                        fileName = new File(selectedImagePath);
                        ExifInterface exif = new ExifInterface(fileName.getPath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                        int angle = 0;

                        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                            angle = 90;
                        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                            angle = 180;
                        } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                            angle = 270;
                        }

                        Matrix mat = new Matrix();
                        mat.postRotate(angle);

                        Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                        bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth()/2, bmp.getHeight()/2, false);
                        Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);


                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);


                        byte[] byteArray = stream.toByteArray();

                        InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp, arrayList.get(0).task_id_Tx);

                        sqlcon.open();
                        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
                        System.out.println(".................count Galary--> " + count);
                        //String id="";

                        PhotoSalesModel tempValuesArr = new PhotoSalesModel();
                        tempValuesArr = GetLastPhotoFromDb();
                        //id=tempValuesArr.getPhotoId();

//                        InserToPhoto(taskId, byteArray, timeStamp);
//
//                        sqlcon.open();
//                        count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);
//                        System.out.println(".................count Galary--> " + count);
//                        //String id="";
//
//                        PhotoModel tempValuesArr = new PhotoModel();
//                        tempValuesArr = GetLastPhotoFromDb();
                        //id=tempValuesArr.getPhotoId();

                        if (count <= 2) {
                            photoGrid.setVisibility(View.GONE);
                            camaraImage1Layout.setVisibility(View.VISIBLE);
                            camaraImage2Layout.setVisibility(View.VISIBLE);

                            //camOneSelected = false;
                            //camTwoSelected=true;


                            if (camOneSelected) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = tempValuesArr.getPhotoId();
                                //image2ID="";
                                camaraImage1.setImageBitmap(reducedSizeBitmap);
                            }
                            if (camTwoSelected) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                //image1DI="";
                                image2ID = tempValuesArr.getPhotoId();
                                camaraImage2.setImageBitmap(reducedSizeBitmap);
                            }

                            if (add) {
                                if (count == 1) {
                                    closeImageCam1.setVisibility(View.VISIBLE);
                                    image1DI = tempValuesArr.getPhotoId();
                                    //image2ID="";
                                    camaraImage1.setImageBitmap(reducedSizeBitmap);
                                } else if (count == 2) {
                                    closeImageCam2.setVisibility(View.VISIBLE);
                                    //image1DI="";
                                    image2ID = tempValuesArr.getPhotoId();
                                    camaraImage2.setImageBitmap(reducedSizeBitmap);
                                }

                            }
                        } else {
                            photoGrid.setVisibility(View.VISIBLE);
                            camaraImage1Layout.setVisibility(View.GONE);
                            camaraImage2Layout.setVisibility(View.GONE);
                            SetListView();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.w("TAG", "-- Error in setting image");
                    } catch (OutOfMemoryError oom) {
                        oom.printStackTrace();
                        Log.w("TAG", "-- OOM Error in setting image");
                    }


                } catch (OutOfMemoryError e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Please Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                } catch (Exception e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Plaese Select valid image");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    e.printStackTrace();
                }
//                timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
//                        Locale.getDefault()).format(new Date());
//
//                Uri selectedImage = data.getData();
//                String[] filePath = {MediaStore.Images.Media.DATA};
//
//                Cursor c = getActivity().getContentResolver().query(
//                        selectedImage, filePath, null, null, null);
//                c.moveToFirst();
//                int columnIndex = c.getColumnIndex(filePath[0]);
//                String picturePath = c.getString(columnIndex);
//                c.close();
//
//                Bitmap scaleBitmap = (BitmapFactory.decodeFile(picturePath));
//                final Bitmap bitmap = Bitmap.createScaledBitmap(scaleBitmap,
//                        200, 200, false);
//                Log.d("path", picturePath + "");
//
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//                byte[] byteArray = stream.toByteArray();
//
//                InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp, arrayList.get(0).task_id_Tx);
//
//                sqlcon.open();
//                count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
//                System.out.println(".................count Galary--> " + count);
//                //String id="";
//
//                PhotoSalesModel tempValuesArr = new PhotoSalesModel();
//                tempValuesArr = GetLastPhotoFromDb();
//                //id=tempValuesArr.getPhotoId();
//
//                if (count <= 2) {
//                    photoGrid.setVisibility(View.GONE);
//                    camaraImage1Layout.setVisibility(View.VISIBLE);
//                    camaraImage2Layout.setVisibility(View.VISIBLE);
//
//                    //camOneSelected = false;
//                    //camTwoSelected=true;
//
//
//                    if (camOneSelected) {
//                        closeImageCam1.setVisibility(View.VISIBLE);
//                        image1DI = tempValuesArr.getPhotoId();
//                        //image2ID="";
//                        camaraImage1.setImageBitmap(bitmap);
//                    }
//                    if (camTwoSelected) {
//                        closeImageCam2.setVisibility(View.VISIBLE);
//                        //image1DI="";
//                        image2ID = tempValuesArr.getPhotoId();
//                        camaraImage2.setImageBitmap(bitmap);
//                    }
//
//                    if (add) {
////                        if (closeImageCam1.getVisibility() != View.VISIBLE) {
////                            closeImageCam1.setVisibility(View.VISIBLE);
////                            image1DI = tempValuesArr.getPhotoId();
////                            //image2ID="";
////                            camaraImage1.setImageBitmap(bitmap);
////                        } else if (closeImageCam2.getVisibility() != View.VISIBLE) {
////                            closeImageCam2.setVisibility(View.VISIBLE);
////                            //image1DI="";
////                            image2ID = tempValuesArr.getPhotoId();
////                            camaraImage2.setImageBitmap(bitmap);
////                        }
//
//                        if (count==1) {
//                            closeImageCam1.setVisibility(View.VISIBLE);
//                            image1DI = tempValuesArr.getPhotoId();
//                            //image2ID="";
//                            camaraImage1.setImageBitmap(bitmap);
//                        } else if (count==2) {
//                            closeImageCam2.setVisibility(View.VISIBLE);
//                            //image1DI="";
//                            image2ID = tempValuesArr.getPhotoId();
//                            camaraImage2.setImageBitmap(bitmap);
//                        }
//
//                    }
//                } else {
//                    photoGrid.setVisibility(View.VISIBLE);
//                    camaraImage1Layout.setVisibility(View.GONE);
//                    camaraImage2Layout.setVisibility(View.GONE);
//                    SetListView();
//
//                }

				/*
                 * profImage = ConstantFunction.BitMapToString(ConstantFunction
				 * .Shrinkmethod(picturePath, ivProfilePic.getWidth(),
				 * ivProfilePic.getHeight())); mask image
				 * ivProfilePic.setImageBitmap(bitmap);
				 */
            }
        }

    }

    private void previewCapturedImage(Intent data) {
        try {
            if (fileUri != null) {
                InputStream inputStream;
                try {
                    inputStream = getActivity().getContentResolver().openInputStream(
                            data.getData());
                    if (!fileName.exists()) {
                        new File(AppConstants.tempImagePath).mkdirs();
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(
                            fileName);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try {
                    Log.v("TTT", "imageToUploadUri = " + fileUri.toString());
//                    File fileName = new File(fileUri.toString().replace("file://", ""));
                    ExifInterface exif = new ExifInterface(fileName.getPath());
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                    int angle = 0;

                    if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                        angle = 90;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                        angle = 180;
                    } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                        angle = 270;
                    }

                    Matrix mat = new Matrix();
                    mat.postRotate(angle);

                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(fileName), null, null);
                    bmp = Bitmap.createScaledBitmap(bmp, bmp.getWidth()/2, bmp.getHeight()/2, false);
                    Bitmap reducedSizeBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
                    byte[] byteArray = stream.toByteArray();

                    InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp, arrayList.get(0).task_id_Tx);

                    sqlcon.open();
                    count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);


                    PhotoSalesModel tempValuesArr = new PhotoSalesModel();
                    tempValuesArr = GetLastPhotoFromDb();

                    System.out.println(".................count Cam--> " + count);

                    if (count <= 2) {
                        photoGrid.setVisibility(View.GONE);
                        camaraImage1Layout.setVisibility(View.VISIBLE);
                        camaraImage2Layout.setVisibility(View.VISIBLE);

                        //camOneSelected = false;
                        //camTwoSelected=true;

                        if (camOneSelected) {
                            closeImageCam1.setVisibility(View.VISIBLE);

                            image1DI = tempValuesArr.getPhotoId();
                            //image2ID="";
                            camaraImage1.setImageBitmap(reducedSizeBitmap);
                        }
                        if (camTwoSelected) {
                            closeImageCam2.setVisibility(View.VISIBLE);
                            //image1DI="";
                            image2ID = tempValuesArr.getPhotoId();
                            camaraImage2.setImageBitmap(reducedSizeBitmap);
                        }
                        if (add) {

                            if (count == 1) {
                                closeImageCam1.setVisibility(View.VISIBLE);
                                image1DI = tempValuesArr.getPhotoId();
                                //image2ID="";
                                camaraImage1.setImageBitmap(reducedSizeBitmap);
                            }
                            if (count == 2) {
                                closeImageCam2.setVisibility(View.VISIBLE);
                                //image1DI="";
                                image2ID = tempValuesArr.getPhotoId();
                                camaraImage2.setImageBitmap(reducedSizeBitmap);
                            }

                        }
                    } else {
                        photoGrid.setVisibility(View.VISIBLE);
                        camaraImage1Layout.setVisibility(View.GONE);
                        camaraImage2Layout.setVisibility(View.GONE);
                        SetListView();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                    Log.w("TAG", "-- Error in setting image");
                } catch (OutOfMemoryError oom) {
                    oom.printStackTrace();
                    Log.w("TAG", "-- OOM Error in setting image");
                }



            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    public void ShowSelectImageDialog(Context c) {

        final Context context = c;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_gallery);
        dialog.show();

        // dialog cancel button
        Button btnDialogCancel = (Button) dialog
                .findViewById(R.id.btnDialogCancel);
        btnDialogCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        // dialog Camera button
        Button btnCamera = (Button) dialog.findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isCamera = true;

                captureImage();
                /*
                 * String dir = Environment
				 * .getExternalStoragePublicDirectory(Environment
				 * .DIRECTORY_PICTURES) + "/movom/"; File newdir = new
				 * File(dir); newdir.mkdirs(); String file = dir + "movom.jpg";
				 * File newfile = new File(file); try { newfile.createNewFile();
				 * } catch (IOException e) { }
				 *
				 * Uri outputFileUri = Uri.fromFile(newfile); Intent intent =
				 * new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				 * intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
				 * getActivity().startActivityForResult(intent,
				 * CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
				 */
                dialog.dismiss();
            }
        });

        // dialog Gallery button
        Button btnGallery = (Button) dialog.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                isCamera=false;

//                Intent intent = new Intent(
//                        Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                getActivity().startActivityForResult(
//                        Intent.createChooser(intent, "Select File"), 2);
//

                isCamera = false;
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
                    } else {

                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        i.setType("image/*");
                        getActivity().startActivityForResult(i,
                                SELECT_PICTURE);
                    }
                } else {

                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    getActivity().startActivityForResult(i,
                            SELECT_PICTURE);
                }

                dialog.dismiss();

            }
        });
    }

    private void DeletePhoto(String photoId, boolean selectedImage1) {
        sqlcon.open();
        sqlcon.DeleteOnePhotoSales(photoId);

        sqlcon.open();
        count = sqlcon.GetRowCount(DbHelper.TABLE_PHOTO_TAKEN, taskId);

        if (selectedImage1) {
            closeImageCam1.setVisibility(View.GONE);

            camaraImage1
                    .setImageResource(R.drawable.inspection_icon_with_bg);
        } else {
            closeImageCam2.setVisibility(View.GONE);
            camaraImage2
                    .setImageResource(R.drawable.inspection_icon_with_bg);
        }

        if (closeImageCam1.getVisibility() != View.VISIBLE && closeImageCam2.getVisibility() != View.VISIBLE) {
            TruckateTable();
        }

    }

    public void TruckateTable() {
        sqlcon.open();
        sqlcon.DeleteAllux(DbHelper.TABLE_PHOTO_TAKEN_SALES, arrayList.get(0).task_id_Tx);
    }

    public void InserToPhoto(String taskId, byte[] image, String taken_time, String ux) {
        sqlcon.open();
        sqlcon.InsertToPhotoSales(taskId, image, taken_time, ux);
    }

    public ArrayList<PhotoSalesModel> GetPhotoFromDb(String taskId, String ux) {
        sqlcon.open();
        ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
        tempArray = sqlcon.GetAllPhotosSalesTx(taskId, ux);
        return tempArray;

    }

    public PhotoSalesModel GetLastPhotoFromDb() {
        sqlcon.open();
        PhotoSalesModel tempArray = new PhotoSalesModel();
        tempArray = sqlcon.GetLastPhotosSales();
        return tempArray;

    }

    public void DeletePhotoFromDb(String photoId, int position) {
        image1DI = "";
        image2ID = "";
        sqlcon.open();
        sqlcon.DeleteOnePhotoSales(photoId);
        CustomListViewValuesArr.remove(position);
        adapter.notifyDataSetChanged();
        sqlcon.open();
        count = sqlcon.GetRowCountSalesux(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
        System.out.println("................................count==> " + count);
        if (count <= 2) {
            ArrayList<PhotoSalesModel> tempPhotos = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"), arrayList.get(0).task_id_Tx);
            if (tempPhotos.size() == 2) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
                                .getTakenPhoto(), 0,
                        tempPhotos.get(0).getTakenPhoto().length);
                camaraImage1.setImageBitmap(bm);
                Bitmap bm1 = BitmapFactory.decodeByteArray(tempPhotos.get(1)
                                .getTakenPhoto(), 0,
                        tempPhotos.get(1).getTakenPhoto().length);
                camaraImage2.setImageBitmap(bm1);

                image1DI = tempPhotos.get(0).getPhotoId();
                image2ID = tempPhotos.get(1).getPhotoId();
            }

            if (tempPhotos.size() == 1) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
                                .getTakenPhoto(), 0,
                        tempPhotos.get(0).getTakenPhoto().length);
                camaraImage1.setImageBitmap(bm);
                image1DI = tempPhotos.get(0).getPhotoId();

                camaraImage2
                        .setImageResource(R.drawable.inspection_icon_with_bg);
            }

            if (tempPhotos.size() == 0) {

                photoGrid.setVisibility(View.GONE);
                camaraImage1Layout.setVisibility(View.VISIBLE);
                camaraImage2Layout.setVisibility(View.VISIBLE);

                camaraImage1
                        .setImageResource(R.drawable.inspection_icon_with_bg);
                camaraImage2
                        .setImageResource(R.drawable.inspection_icon_with_bg);
                image1DI = "";
                image2ID = "";
            }


        } else {
            photoGrid.setVisibility(View.VISIBLE);
            camaraImage1Layout.setVisibility(View.GONE);
            camaraImage2Layout.setVisibility(View.GONE);
            SetListView();
        }

        if (closeImageCam1.getVisibility() != View.VISIBLE && closeImageCam2.getVisibility() != View.VISIBLE) {
            TruckateTable();
        }

    }

    private void UpdateTable(String fieldName, String updateValue, String taskId) {
        sqlcon.open();
        sqlcon.UpdateSavedTable(fieldName, updateValue, taskId);
    }

    private void GetSavedItems(String taskId) {
        sqlcon.open();
        sqlcon.GetSavedItems(taskId);
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private void showNextStepPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        nextStepPopup = new PopupWindow(view);

        int width = NextSteps.getWidth();
        if (width > 0) {
            nextStepPopup.setWidth(width);
        } else {
            nextStepPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        nextStepPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        nextStepPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        nextStepPopup.setOutsideTouchable(true);
        nextStepPopup.setTouchable(true);
        nextStepPopup.setFocusable(true);

        NextStepDropDownAdapter nextStepAdapter = new NextStepDropDownAdapter(getActivity(), nextStepData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(nextStepAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NextSteps.setText(nextStepData.get(position).toString());
                nextStepPopup.dismiss();
            }
        });

        nextStepPopup.showAsDropDown(NextSteps, 0, 2);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                Log.v("TTT", "permission is dynamically granted");

                if (isCamera) {
                    cameraIntent();
//                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    File fileName = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "_POST_IMAGE.jpg");
//                    fileUri = Uri.fromFile(fileName);
//                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileName));
//                    i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION,
//                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                    getActivity().startActivityForResult(i, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    i.setType("image/*");
                    getActivity().startActivityForResult(i,
                            SELECT_PICTURE);
                }

                break;

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }

        }
    }

}
