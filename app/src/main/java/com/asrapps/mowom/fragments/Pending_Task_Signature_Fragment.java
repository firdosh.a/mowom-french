package com.asrapps.mowom.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.model.Login;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import static com.asrapps.mowom.constants.ConstantFunction.getstatus;

public class Pending_Task_Signature_Fragment extends BaseFragment implements
        OnClickListener {

    Paint paint;
    LinearLayout mContent;
    View view;
    Signature mSignature;

    LinearLayout clearSignButton, saveSignButton;

    //TextView txtHeaderUserName;
    ImageView headerUserImage, backButtonImage;

    String et_Id, taskId, issue;

    TextView pen_tsk_srt_id_header;
    boolean isSigned = false;

    ImageView imgOne, imgTwo, imgThree, imgFour,  imgOnesep,imgTwosep,imgThreesep;
    String selectedLanguage;
    LinearLayout llEng, llPersian;

    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.pending_task_signature_layout,
                    container, false);

			/*txtHeaderUserName = (TextView) view
                    .findViewById(R.id.txtHeaderUserName);*/

            llEng = (LinearLayout) view.findViewById(R.id.llEng);
            llPersian = (LinearLayout) view.findViewById(R.id.llPersian);

            imgOne = (ImageView) view.findViewById(R.id.imgOne);
            imgTwo = (ImageView) view.findViewById(R.id.imgTwo);
            imgThree = (ImageView) view.findViewById(R.id.imgThree);
            imgFour = (ImageView) view.findViewById(R.id.imgFour);
            imgOnesep=(ImageView)view.findViewById(R.id.imgOnesep);
            imgTwosep=(ImageView)view.findViewById(R.id.imgTwosep);
            imgThreesep=(ImageView)view.findViewById(R.id.imgThreesep);

            String is_report= getstatus(getActivity(), "is_report");
            String is_review= getstatus(getActivity(), "is_review");
            String is_signature= getstatus(getActivity(), "is_signature");

            int cntScreen=0;
            if(!is_report.equals("null") && is_report.equals("1"))
            {
                cntScreen++;
            }
            if(!is_review.equals("null") && is_review.equals("1"))
            {
                cntScreen++;
            } if(!is_signature.equals("null") && is_signature.equals("1"))
            {
                cntScreen++;
            }

            showFlowCounter(cntScreen);


            headerUserImage = (ImageView) view
                    .findViewById(R.id.headerUserImage);
            //			headerUserImage.setOnClickListener(this);

            backButtonImage = (ImageView) view
                    .findViewById(R.id.backButtonImage);
            backButtonImage.setOnClickListener(this);
            pen_tsk_srt_id_header = (TextView) view
                    .findViewById(R.id.pen_tsk_srt_id_header);

            clearSignButton = (LinearLayout) view
                    .findViewById(R.id.clearSignButton);
            clearSignButton.setOnClickListener(this);

            saveSignButton = (LinearLayout) view
                    .findViewById(R.id.saveSignButton);
            saveSignButton.setOnClickListener(this);

            internetConnection = new ConnectionDetector(getActivity());

            sqlcon = new SQLController(getActivity());

            GetFromShared();
            mContent = (LinearLayout) view.findViewById(R.id.mysignature);
            mSignature = new Signature(getActivity(), null);
            mContent.addView(mSignature);

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                llEng.setVisibility(View.GONE);
                llPersian.setVisibility(View.VISIBLE);

//				imgOne.setImageResource(R.drawable.pr_step1_unhover);
//				imgTwo.setImageResource(R.drawable.pr_step2_unhover);
//				imgThree.setImageResource(R.drawable.pr_step3_unhover);
//				imgFour.setImageResource(R.drawable.pr_step4_hover);
            } else {
                llEng.setVisibility(View.VISIBLE);
                llPersian.setVisibility(View.GONE);
            }

            if (internetConnection.isConnectingToInternet("")) {
                SetUserDetails();
            } else {
                new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
            }


        }
        return view;
    }

    private void showFlowCounter(int cntScreen) {

        imgOne.setVisibility(View.VISIBLE);
        imgTwo.setVisibility(View.VISIBLE);
        imgThree.setVisibility(View.VISIBLE);
        imgFour.setVisibility(View.VISIBLE);
        imgOnesep.setVisibility(View.VISIBLE);
        imgTwosep.setVisibility(View.VISIBLE);
        imgThreesep.setVisibility(View.VISIBLE);

        if(cntScreen==3)
        {
//            imgFour.setVisibility(View.GONE);
//            imgThreesep.setVisibility(View.GONE);
//
//            imgTwo.setImageResource(R.drawable.step_second_unhover);
//            imgThree.setImageResource(R.drawable.step_three_unhover);
//            imgFour.setImageResource(R.drawable.step_four_hover);

        }

        if(cntScreen==2)
        {
            imgFour.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);

            imgTwo.setImageResource(R.drawable.step_second_unhover);
            imgThree.setImageResource(R.drawable.step_three_hover);
            imgFour.setImageResource(R.drawable.step_four__unhover);

        }
        else if(cntScreen==1)
        {
            imgFour.setVisibility(View.GONE);
            imgThree.setVisibility(View.GONE);
            imgThreesep.setVisibility(View.GONE);
            imgTwosep.setVisibility(View.GONE);
            imgTwo.setImageResource(R.drawable.step_second_hover);
            imgThree.setImageResource(R.drawable.step_three_unhover);


        }
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));*/

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetFromShared() {

        taskId = ConstantFunction.getuser(getContext(), AppConstants.task_Id);
        et_Id = ConstantFunction.getuser(getContext(), AppConstants.et_id);
        issue = ConstantFunction.getuser(getContext(), AppConstants.issue);
        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + toPersianNumber(taskId) + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        } else {
            pen_tsk_srt_id_header.setText(getResources().getString(R.string.id) + taskId + ", "
                    + issue.toUpperCase(Locale.getDefault()));
        }
        // saveId = "img_" + taskId;
    }


    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;


            case R.id.clearSignButton:
                mSignature.clear();
                break;

            case R.id.saveSignButton:
                if (isSigned)
                    mSignature.save();
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.get_sign),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

            default:
                break;
        }

    }

    private void UpdateSignature(byte[] signature, String taskId) {
        sqlcon.open();
        sqlcon.UpdateSignature(signature, taskId);
    }

    private void GetSavedItems(String taskId) {
        sqlcon.open();
        sqlcon.GetSavedItems(taskId);
    }

    public class Signature extends View {
        static final float STROKE_WIDTH = 10f;
        static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        Paint paint = new Paint();
        Path path = new Path();

        float lastTouchX;
        float lastTouchY;
        final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void clear() {
            path.reset();
            invalidate();
            isSigned = false;
            // save.setEnabled(false);
        }

        public void save() {
            Bitmap returnedBitmap = Bitmap.createBitmap(mContent.getWidth(),
                    mContent.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(returnedBitmap);
            Drawable bgDrawable = mContent.getBackground();
            if (bgDrawable != null)
                bgDrawable.draw(canvas);
            else
                canvas.drawColor(Color.WHITE);
            mContent.draw(canvas);

            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            returnedBitmap.compress(Bitmap.CompressFormat.PNG, 25, bs);

            byte[] byteArray = bs.toByteArray();
            UpdateSignature(byteArray, taskId);
            GetSavedItems(taskId);


            Pending_Task_Final_Fragment finalFragment = new Pending_Task_Final_Fragment();
            mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                    finalFragment, true, true);

        }

        @Override
        protected void onDraw(Canvas canvas) {
            // TODO Auto-generated method stub
            canvas.drawPath(path, paint);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            // save.setEnabled(true);
            isSigned = true;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }

}
