package com.asrapps.mowom.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;


public class Report_Task_Details_Fragment extends BaseFragment {

    String  googleUrl;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;
    int id;
    PendingTask offlineSelectedTask;
    SharedPreferences sharedPref;
    String google_sheet_url, is_google_sheet;



    public void setgoogleUrl(String googleUrl) {
        this.googleUrl = googleUrl;
    }
    ImageView headerUserImage, backButtonImage;
    Button reportBtn;

    ConnectionDetector internetConnection;
    String selectedLanguage;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        view = inflater.inflate(R.layout.report_task_detail_layout,
                container, false);

        sqlcon = new SQLController(getActivity());

        sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);

        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);

        backButtonImage = (ImageView) view.findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.ClearAllPages();
                // mActivity.popFragments();
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new AppTab_Dashboard_Home(), true, true);
            }
        });

        internetConnection = new ConnectionDetector(getActivity());

        reportBtn = (Button) view.findViewById(R.id.reportBtn);
        reportBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (internetConnection
                        .isConnectingToInternet("")) {
                        if (googleUrl != null && googleUrl.length() > 0) {
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(googleUrl));
                            startActivity(i);
                        }

                }
            }
        });

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

        return view;
    }

    @Override
    public boolean onBackPressed() {
        mActivity.ClearAllPages();
        // mActivity.popFragments();
        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                new AppTab_Dashboard_Home(), true, true);

        return true;
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }


}
