package com.asrapps.mowom.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.AddSalesLocation;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.model.PhotoModel;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.asrapps.mowom.model.SalesTask;
import com.asrapps.mowom.model.SurveyTask;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;
import com.asrapps.utils.MowomLogFile;
import com.asrapps.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class AppTab_Dashboard_Home extends BaseFragment implements
        AsyncResponseListener, OnCancelListener, OnClickListener {

    TextView pending_task_count, rejected_task_count,
            completed_task_count, alert_task_count, special_task_count, txtNewTask;//txtHeaderUserName
    ImageView headerUserImage, pendingTaskImage, rejectedTaskImage,
            completedTaskImage, alertImage, specialImage, reportImage, AddTask1;
    private ProgressHUD mProgressHUD;
    ConnectionDetector internetConnection;

    String selectedLanguage;

    SQLController sqlcon;
    private ArrayList<Login> mLogin;

    ArrayList<PendingTask> allSyncData = new ArrayList<>();
    ArrayList<PendingTask> allSync = new ArrayList<>();
    ArrayList<SalesTask> allSales = new ArrayList<>();
    ArrayList<SurveyTask> allSurvey = new ArrayList<>();
    public ArrayList<AddSalesLocation> addSalesTask = new ArrayList<>();

    String et_id, task_list_id, task_list_issue, task_list_date_time, task_list_phone,
            task_list_address, task_detail_contactPersonName,
            task_detail_start_endTime, strTotalTime, strTaskReportTextView,
            strConsumerFeedBackTextView, strRatingValue, task_org_name;
    PendingTask pendingTask;
    String problem_reported;
    String diagnosis_done;
    String resolution;
    String notes_support;
    String deilvery_status;
    String notes_delevery;
    String startTime, endTime;
    Bitmap signature;
    String strSignature;
    private ArrayList<PhotoModel> CustomListViewValuesArr = new ArrayList<PhotoModel>();
    private ArrayList<PhotoSalesModel> CustomListViewValuesArrSurvey = new ArrayList<PhotoSalesModel>();
    private ArrayList<PhotoSalesModel> CustomListViewValuesArrSales = new ArrayList<PhotoSalesModel>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dashboard_layout, container,
                false);
        // mActivity.ClearAllPages();
        internetConnection = new ConnectionDetector(getActivity());

        sqlcon = new SQLController(getActivity());

		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/

        txtNewTask = (TextView) view.findViewById(R.id.txtNewTask);

        pending_task_count = (TextView) view
                .findViewById(R.id.pending_task_count);
        rejected_task_count = (TextView) view
                .findViewById(R.id.rejected_task_count);

        completed_task_count = (TextView) view
                .findViewById(R.id.completed_task_count);
        alert_task_count = (TextView) view.findViewById(R.id.alert_task_count);
        special_task_count = (TextView) view
                .findViewById(R.id.special_task_count);

        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
//		headerUserImage.setOnClickListener(this);

        rejectedTaskImage = (ImageView) view
                .findViewById(R.id.rejectedTaskImage);
        rejectedTaskImage.setOnClickListener(this);

        pendingTaskImage = (ImageView) view.findViewById(R.id.pendingTaskImage);
        pendingTaskImage.setOnClickListener(this);

        completedTaskImage = (ImageView) view
                .findViewById(R.id.completedTaskImage);
        completedTaskImage.setOnClickListener(this);

        specialImage = (ImageView) view.findViewById(R.id.specialImage);
        specialImage.setOnClickListener(this);

        reportImage = (ImageView) view.findViewById(R.id.reportImage);
        reportImage.setOnClickListener(this);

        AddTask1 = (ImageView) view.findViewById(R.id.AddTask1);
        AddTask1.setOnClickListener(this);

        if ((ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("sales") ||
                ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("survey"))) {
//&&            ConstantFunction.getstatus(getActivity(), "can_create_task").equalsIgnoreCase("yes")
            if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("survey")) {
                txtNewTask.setText(getActivity().getResources().getString(R.string.Add_Survey));
            }

            ((View) AddTask1.getParent()).setVisibility(View.GONE);
        } else {
            ((View) AddTask1.getParent()).setVisibility(View.GONE);
        }

        alertImage = (ImageView) view
                .findViewById(R.id.alertImage);
        alertImage.setOnClickListener(this);

        if (internetConnection
                .isConnectingToInternet("")) {

//            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//            SharedPreferences.Editor edit = sharedPref.edit();
//            edit.putString("task_id", "0");
//            edit.commit();

            SetUserDetails();

            SendLangData();
            synOperation();


        } else {
            sqlcon.open();

            pending_task_count.setText("");
            rejected_task_count.setText("");
            completed_task_count.setText("");
            alert_task_count.setText("");
            special_task_count.setText("");

            int count = sqlcon.getPendingListCount(ConstantFunction.getstatus(getActivity(), "UserId") + "", String.valueOf("1"));
            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

//            if (selectedLanguage.equalsIgnoreCase("pr")) {
////                pending_task_count.setText(toPersianNumber(String.valueOf(count)));
//                pending_task_count.setText(toPersianNumber(String.valueOf(count)));
//            } else {
////                pending_task_count.setText(String.valueOf(count));
//            }

//            int countSpecial = sqlcon.getSpecialListCount(ConstantFunction.getstatus(getActivity(), "UserId") + "", String.valueOf("2"));
//            if (selectedLanguage.equalsIgnoreCase("pr")) {
//                special_task_count.setText(toPersianNumber(String.valueOf(countSpecial)));
//            } else {
//                special_task_count.setText(String.valueOf(countSpecial));
//            }
        }


        return view;
    }

    private void SendLangData() {

        JSONObject requestObject = new JSONObject();
        try {

            String id = ConstantFunction.getuser(getActivity().getApplicationContext(), "UserId");
            requestObject.put("emp_id", id);
            String selectedLanguage = ConstantFunction.CheckLanguage(getActivity());
            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            requestObject.put("is_app_lang", selectedLanguage);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }


        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.AppLangChangeURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "APP Lang  _Activity :- " + "\n" + ConstantValues.DashBoardURL + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                Log.e("Response app lang==>", response);
                 if (requestURL.equals(ConstantValues.AppLangChangeURL) && !response.equalsIgnoreCase("")) {

                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

//                if (status.equalsIgnoreCase("success")) {
                        if (result_code.equalsIgnoreCase("1")) {

                        }else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        // ConstantFunction.showAlertDialog(LoginActivity.this,
                        // "Try again", false);
                    }
                }
            }
        };
        wsHelper.execute();
    }

    private void synOperation() {
        sqlcon.open();

        allSync = sqlcon.getPendingAllTask(ConstantFunction.getstatus(getActivity(), "UserId") + "");

        allSyncData = new ArrayList<>();


        for (int i = 0; i < allSync.size(); i++) {
            if (allSync.get(i).getStartTime() != null && !allSync.get(i).getStartTime().equals("null")) {
                allSyncData.add(allSync.get(i));
            }
        }

        if (allSyncData.size() > 0) {

            mProgressHUD = ProgressHUD.show(getActivity(),
                    getString(R.string.syncing), true, false, this);

            SyncAllData(0);

        } else {
            SurverOperation();
        }

    }

    public void GetSavedItems(String taskId) {
//		sqlcon.open();
//		PendingTask pendingTask = new PendingTask();
//		pendingTask = sqlcon.GetSavedItems(taskId);

        sqlcon.open();
        pendingTask = sqlcon.getPendingByIdTask(ConstantFunction.getstatus(getActivity(), "UserId") + "", taskId);

        task_org_name = pendingTask.getOrg_name().toUpperCase(
                Locale.getDefault());
        task_list_id = pendingTask.getTaskId();
        et_id = pendingTask.getEt_id();
        task_list_issue = pendingTask.getIssue();
        task_list_date_time = pendingTask.getDate_time();

        problem_reported = pendingTask.getProblem_reported();
        diagnosis_done = pendingTask.getDiagnosis_done();

        resolution = pendingTask.getResolution();

        notes_support = pendingTask.getNotes_support();

        deilvery_status = pendingTask.getDeilvery_status();

        notes_delevery = pendingTask.getNotes_delevery();

        StringBuilder t = new StringBuilder();
        try {
            task_list_date_time = WebServiceHelper.convert12to24(task_list_date_time);
            String st[] = task_list_date_time.split(" ");

            String stTime[] = st[1].split(":");
            String dtTime[] = st[0].split("/");

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                st[0] = st[0].replace(",", "").replace(" ", "");
                String dtStart = st[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SolarCalendar sc = new SolarCalendar(date);
                    String s = sc.date + "/" +
                            sc.month + "/" + sc.year;


                    Log.v("TTT", "ssssss = " + s);

                    t.append(toPersianNumber(s)).append(" ");
                    t.append(toPersianNumber(stTime[0]));
                    t.append(":");
                    t.append(toPersianNumber(stTime[1]));

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t.append(toPersianNumber(st[0])).append(" ");
                    t.append(toPersianNumber(stTime[0]));
                    t.append(":");
                    t.append(toPersianNumber(stTime[1]));
                    e.printStackTrace();
                }
//                task_list_date_time = toPersianNumber(WebServiceHelper.convert12to24(task_list_date_time));

            } else {

//                st[0] = st[0].replace(",", "").replace(" ", "");
//                String dtStart = st[0];
//                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                try {
//                    Date date = format.parse(dtStart);
//                    System.out.println(date);
//
//                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                    String s = dateformat.format(date);
//                    System.out.println("Current Date Time : " + s);
//                    Log.v("TTT", "ssssss = " + s);
//
//                    t.append(s).append(" ");
//                    t.append(stTime[0]);
//                    t.append(":");
//                    t.append(stTime[1]);
//
//                } catch (ParseException e) {
//                    // TODO Auto-generated catch block
//
//                    t.append(st[0]).append(" ");
//                    t.append(stTime[0]);
//                    t.append(":");
//                    t.append(stTime[1]);
//                    e.printStackTrace();
//                }

                task_list_date_time = task_list_date_time;


            }


//            if (st[2].equalsIgnoreCase("am")) {
//                t.append(" " + getResources().getString(R.string.am));
//            } else {
//                t.append(" " + getResources().getString(R.string.pm));
//            }
//
//
//            task_list_date_time = t.toString();
        } catch (Exception e) {


        }

        task_list_phone = pendingTask.getPhone();
        if(pendingTask
                .getClientAddress()!=null && pendingTask
                .getClientAddress().size()>0 && pendingTask
                .getClientAddress().get(0).location!=null) {
            task_list_address = pendingTask.getClientAddress().get(0).location;
        }
        else
        {
            task_list_address = "";
        }

        task_detail_contactPersonName = getString(R.string.contact_name)
                + " : " + pendingTask.getPersonName();


        StringBuilder t1 = new StringBuilder();
        StringBuilder t2 = new StringBuilder();


        startTime = pendingTask.getStartTime();

        endTime = pendingTask.getStopTime();


        String StartTime = ConstantFunction.GetSimpleTimeFormat24(startTime);
        String StopTime = ConstantFunction.GetSimpleTimeFormat24(endTime);


        try {


            Log.v("TTT", "startTime = " + StartTime);
            Log.v("TTT", "endTime = " + StopTime);

            String st[] = StartTime.split(" ");
            String et[] = StopTime.split(" ");

            String stTime[] = st[0].split(":");
            String etTime[] = et[0].split(":");

            selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                String dtStart = stTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SolarCalendar sc = new SolarCalendar(date);
                    String s = sc.date + "/" +
                            sc.month + "/" + sc.year;


                    Log.v("TTT", "ssssss = " + s);

                    t1.append(toPersianNumber(s));
                    t1.append(":");
                    t1.append(toPersianNumber(stTime[1]));

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t1.append(toPersianNumber(stTime[0]));
                    t1.append(":");
                    t1.append(toPersianNumber(stTime[1]));
                    e.printStackTrace();
                }


            } else {

                stTime[0] = stTime[0].replace(",", "").replace(" ", "");
                String dtStart = stTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                    String s = dateformat.format(date);
                    System.out.println("Current Date Time : " + s);

                    Log.v("TTT", "ssssss = " + s);

                    t1.append(s);
                    t1.append(":");
                    t1.append(stTime[1]);

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t1.append(stTime[0]);
                    t1.append(":");
                    t1.append(stTime[1]);
                    e.printStackTrace();
                }


            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                String dtStart = etTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SolarCalendar sc = new SolarCalendar(date);
                    String s = sc.date + "/" +
                            sc.month + "/" + sc.year;


                    Log.v("TTT", "ssssss = " + s);

                    t2.append(toPersianNumber(s));
                    t2.append(":");
                    t2.append(toPersianNumber(etTime[1]));

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t2.append(toPersianNumber(etTime[0]));
                    t2.append(":");
                    t2.append(toPersianNumber(etTime[1]));
                    e.printStackTrace();
                }


            } else {

                etTime[0] = etTime[0].replace(",", "").replace(" ", "");
                String dtStart = etTime[0];
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = format.parse(dtStart);
                    System.out.println(date);

                    SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
                    String s = dateformat.format(date);
                    System.out.println("Current Date Time : " + s);


                    Log.v("TTT", "ssssss = " + s);

                    t2.append(s);
                    t2.append(":");
                    t2.append(etTime[1]);

                } catch (ParseException e) {
                    // TODO Auto-generated catch block

                    t2.append(etTime[0]);
                    t2.append(":");
                    t2.append(etTime[1]);
                    e.printStackTrace();
                }


            }


//            if (st[1].equalsIgnoreCase("am")) {
//                t1.append(" " + getResources().getString(R.string.am));
//            } else {
//                t1.append(" " + getResources().getString(R.string.pm));
//            }
//
//            if (et[1].equalsIgnoreCase("am")) {
//                t2.append(" " + getResources().getString(R.string.am));
//            } else {
//                t2.append(" " + getResources().getString(R.string.pm));
//            }

            StartTime = t1.toString();
            StopTime = t2.toString();

        } catch (Exception e) {
            e.printStackTrace();

            StartTime = t1.toString();
            StopTime = t2.toString();
        }


//		startTime = pendingTask.getStartTime();
//		endTime = pendingTask.getStopTime();

        String formatedString = "Start : %s - Stop : %s";


        task_detail_start_endTime = getResources().getString(R.string.Start) + " " + StartTime + getResources().getString(R.string.stop) + " " + StopTime;

        strTotalTime = getString(R.string.total_time) + " "
                + pendingTask.getTotalTimeSpend();

        strTaskReportTextView = pendingTask.getTaskReport();

        strConsumerFeedBackTextView = pendingTask.getConsumerFeedback();

        strRatingValue = pendingTask.getRating();

        if(pendingTask.getSignatureImage()!=null && pendingTask.getSignatureImage().length>0) {
            signature = getImageView(pendingTask.getSignatureImage());
        }

        CustomListViewValuesArr = new ArrayList<PhotoModel>();
        CustomListViewValuesArr = GetPhotoFromDb(pendingTask.getTaskId());

    }

    public ArrayList<PhotoModel> GetPhotoFromDb(String taskId) {
        sqlcon.open();
        ArrayList<PhotoModel> tempArray = new ArrayList<PhotoModel>();
        tempArray = sqlcon.GetAllPhotos(taskId);
        return tempArray;

    }

    Bitmap getImageView(byte[] image) {
        Bitmap bm = null;
        bm = BitmapFactory.decodeByteArray(image, 0, image.length);
        try {
            strSignature = ConstantFunction.BitMapToString(bm);
        }
        catch (Exception e)
        {
            return null;
        }
        return bm;

    }

    private void SyncAllData(final int count) {

        GetSavedItems(allSyncData.get(count).getTaskId());


        JSONObject requestObject = new JSONObject();
        try {

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("emp_task_id", et_id);
            requestObject.put("taskid", task_list_id);
            requestObject.put("task_start_time", startTime);
            requestObject.put("task_endtime", endTime);
            requestObject.put("task_report", strTaskReportTextView);
            requestObject.put("task_feedback", strConsumerFeedBackTextView);
            requestObject.put("task_ratings", strRatingValue);
            if(strSignature!=null && strSignature.equals("null")) {
                requestObject.put("client_sign", strSignature + "");
            }
            else
            {
                requestObject.put("client_sign","");
            }


            if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("support")) {
                requestObject.put("problem_reported", problem_reported);
                requestObject.put("diagnosis_done", diagnosis_done);
                requestObject.put("resolution", resolution);
                requestObject.put("notes", notes_support);
            }

            if (ConstantFunction.getstatus(getActivity().getApplicationContext(), "department").equalsIgnoreCase("delivery")) {
                requestObject.put("deilvery_status", deilvery_status);
                requestObject.put("notes", notes_delevery);
            }

            String encodedString = "";
            for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
//                String strImage = GetStringFromBytes(CustomListViewValuesArr
//                        .get(i).getTakenPhoto());
                Bitmap reducedSizeBitmap = Utils.getBitmap(CustomListViewValuesArr
                                .get(i).getPhotoPath());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                reducedSizeBitmap.compress(Bitmap.CompressFormat.PNG, 25, stream);
                byte[] byteArray = stream.toByteArray();

                String strImage = GetStringFromBytes(byteArray);

                encodedString += new StringBuilder(String.valueOf(strImage))
                        .append(",").toString();
            }
            requestObject.put("imagecount", CustomListViewValuesArr.size());
            requestObject.put("task_images", encodedString);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        // Call web service
        final WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.UpdateTaskURL.trim());
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Dashboard_Home :- " + ConstantValues.UpdateTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                if (count < allSyncData.size()) {
                    mProgressHUD.dismiss();
                }

                Log.e("Response==>", response);
                MowomLogFile.writeToLog("\n" + "AppTab_Dashboard_Home :- " + ConstantValues.UpdateTaskURL + " -------------" + response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

//                        if (status.equalsIgnoreCase("success")) {
                        if (result_code.equalsIgnoreCase("1")) {
                            try {

                                Log.v("ImageView", "success uploaded");
                                Toast toast = Toast.makeText(getActivity(),
                                        getString(R.string.suc_uploaded),
                                        Toast.LENGTH_SHORT);

                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();

                                sqlcon.open();
                                sqlcon.taskDelete(task_list_id, et_id);

                                if (count + 1 < allSyncData.size() - 1) {
                                    SyncAllData(count + 1);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("17")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (result_code.equalsIgnoreCase("18")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_18));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
//							Toast toast = Toast.makeText(getActivity(),
//									getString(R.string.please_try_again),
//									Toast.LENGTH_SHORT);
//							toast.setGravity(Gravity.CENTER, 0, 0);
//							toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());


                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                        }

                    } catch (Exception e) {
//						Toast toast = Toast.makeText(getActivity(),
//								getString(R.string.please_try_again),
//								Toast.LENGTH_SHORT);
//						toast.setGravity(Gravity.CENTER, 0, 0);
//						toast.show();

                    }
                } else {

                }


                if (count == allSyncData.size() - 1) {
                    SurverOperation();
                }
            }
        };
        wsHelper.execute();

    }

    public String GetStringFromBytes(byte[] image) {
        String byteSyting = "";
        Bitmap bm = null;
        bm = BitmapFactory.decodeByteArray(image, 0, image.length);
        byteSyting = ConstantFunction.BitMapToString(bm);
        return byteSyting;
    }


    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
                "UserHeaderName"));
*/
        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

    private void GetDashBoard() {
        JSONObject requestObject = new JSONObject();

        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("id", id);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        try {
            mProgressHUD = ProgressHUD.show(getActivity(), getString(R.string.loading), true,
                    false, this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.DashBoardURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Dashboard_Home :- " + ConstantValues.DashBoardURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub
        try {
            mProgressHUD.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("Response==>", response);
        MowomLogFile.writeToLog("\n" + "AppTab_Dashboard_Home :- " + ConstantValues.DashBoardURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (result_code.equalsIgnoreCase("1")) {
                    try {

                        JSONObject job2 = job.getJSONObject("data");

                        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

                        if (selectedLanguage.equalsIgnoreCase("")) {
                            selectedLanguage = "en";
                        }

                        if (selectedLanguage.equalsIgnoreCase("pr")) {
                            pending_task_count.setText(toPersianNumber(job2.get("pending_tasks")
                                    .toString()));
                            rejected_task_count.setText(toPersianNumber(job2.get("rejected_tasks")
                                    .toString()));
                            completed_task_count.setText(toPersianNumber(job2
                                    .get("completed_tasks").toString()));
                            alert_task_count.setText(toPersianNumber(job2.get("alerts").toString()));
                            special_task_count.setText(toPersianNumber(job2.get("special_task")
                                    .toString()));
                        } else {
                            pending_task_count.setText(job2.get("pending_tasks")
                                    .toString());
                            rejected_task_count.setText(job2.get("rejected_tasks")
                                    .toString());
                            completed_task_count.setText(job2
                                    .get("completed_tasks").toString());
                            alert_task_count.setText(job2.get("alerts").toString());
                            special_task_count.setText(job2.get("special_task")
                                    .toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // GoDashBoard();

                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Login success", false);
                } else if (result_code.equalsIgnoreCase("17")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("19")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_19));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getActivity());


                                    startActivity(new Intent(getActivity(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
//                Toast toast = Toast.makeText(getActivity(),
//                        getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
                // ConstantFunction.showAlertDialog(LoginActivity.this,
                // "Try again", false);
            }
        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }
        try {
            if (mProgressHUD.isShowing())
                mProgressHUD.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void SalesOperation() {

        sqlcon.open();

        allSales = sqlcon.getSalesTaskAll();


        if (allSales.size() > 0) {

            sqlcon.open();
            addSalesTask = sqlcon.GetSalesLocation();


            sendDataSales(0);
        } else {
            GetDashBoard();
        }

    }


    private void SurverOperation() {

        sqlcon.open();
        allSurvey = sqlcon.getSurveyTaskAll();

        if (allSurvey.size() > 0) {
            sendSurveyData(0);
        } else {
            SalesOperation();
        }
    }

    private void sendDataSales(final int count) {

        JSONObject requestObject = new JSONObject();
        try {

            if (allSales.get(count).client_id.length() == 0) {
                requestObject.put("client_id", "0");
                requestObject.put("client_name", allSales.get(count).SALES_client_name_new + "");
                requestObject.put("client_address", allSales.get(count).SALES_client_address + "");
                requestObject.put("contact_person", allSales.get(count).SALES_contact_person + "");
                requestObject.put("tel_number", allSales.get(count).tel_number + "");
                requestObject.put("client_email", allSales.get(count).emp_id + "");
                requestObject.put("latitude", allSales.get(count).latitude + "");
                requestObject.put("longtitude", allSales.get(count).longtitude + "");
                requestObject.put("companyid", allSales.get(count).company_id + "");

            } else {
                requestObject.put("client_id", allSales.get(count).client_id + "");
            }
            requestObject.put("supervisor_id", allSales.get(count).supervisor_id + "");
            requestObject.put("category_id", allSales.get(count).category_id + "");

            requestObject.put("next_steps", allSales.get(count).next_steps + "");
            requestObject.put("notes", allSales.get(count).notes + "");
            requestObject.put("product", allSales.get(count).product + "");
            requestObject.put("detail", allSales.get(count).detail + "");
            requestObject.put("department", "sales");
            requestObject.put("emp_id", allSales.get(count).emp_id);

            requestObject.put("company_id", allSales.get(count).company_id);
            requestObject.put("task_name", allSales.get(count).task_name + "");
            requestObject.put("additional_instruction", allSales.get(count).additional_instruction + "");
            requestObject.put("tasktype", allSales.get(count).tasktype + "");

            requestObject.put("task_start_time", allSales.get(count).startTime + "");
            requestObject.put("task_endtime", allSales.get(count).endTime + "");

            String encodedString = "";

            String userId = ConstantFunction.getstatus(getActivity(), "UserId");
            CustomListViewValuesArrSales = GetPhotoFromDbSales(allSales.get(count).emp_id, allSales.get(count).task_id_Tx);

            for (int i = 0; i < CustomListViewValuesArrSales.size(); i++) {
                String strImage = GetStringFromBytes(CustomListViewValuesArrSales
                        .get(i).getTakenPhoto());
                encodedString += new StringBuilder(String.valueOf(strImage))
                        .append(",").toString();
            }

            requestObject.put("dept_imagecount", CustomListViewValuesArrSales.size());
            requestObject.put("dept_images", encodedString);

            //send location arrary to server of add sales task...........
            if (allSales != null && allSales.size() > 0 && allSales.get(0).task_id == null || allSales.get(0).task_id.equals("0")) {
                if (addSalesTask.size() > 0 && addSalesTask != null) {
                    try {
                        JSONArray arrayObj = new JSONArray();
                        for (int r = 0; r < addSalesTask.size(); r++) {

                            JSONObject obj = new JSONObject();

                            String uniqueId = addSalesTask.get(r).unique_id;
                            obj.put("lat", addSalesTask.get(r).latitude);
                            obj.put("long", addSalesTask.get(r).longitude);
                            arrayObj.put(obj);

                            sqlcon.open();
                            sqlcon.deleteSalesLocationByTransId(uniqueId);

                        }

                        requestObject.put("geopoints", arrayObj);

                        Log.v("Add sales", "arrayObj = " + arrayObj);

                    } catch (JSONException e) {
                        Log.d("Exception", e.toString());
                        e.printStackTrace();
                    }
                }
            }


        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

//		mProgressHUD = ProgressHUD.show(getActivity(),
//				getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.AddSalesENDTaskURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Dashboard_Home :- " + ConstantValues.AddSalesENDTaskURL + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {

                sqlcon.open();
                sqlcon.SalesTaskById(allSales.get(count));
                if (count + 1 < allSales.size()) {
                    sendDataSales(count + 1);

                } else {

//                    Log.v("salesData", " salesData successs uploaded");
//                    Toast toast = Toast.makeText(getActivity(),
//                            getString(R.string.suc_uploaded),
//                            Toast.LENGTH_SHORT);
//
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();

//					mActivity.ClearAllPages();
//					// mActivity.popFragments();
//					mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
//							new AppTab_Dashboard_Home(), true, true);
                }

                if (count == allSales.size() - 1) {
                    GetDashBoard();
                }
            }
        };
        wsHelper.execute();

    }


    public ArrayList<PhotoSalesModel> GetPhotoFromDbSales(String userid, String usertx) {
        sqlcon.open();
        ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
        tempArray = sqlcon.GetAllPhotosSalesTx(userid, usertx);
        return tempArray;

    }

    private void sendSurveyData(final int count) {

        JSONObject requestObject = new JSONObject();
        try {
            if (allSurvey.get(count).client_id.length() == 0 || allSurvey.get(count).client_id.equals("0")) {
                requestObject.put("client_name", allSurvey.get(count).SURVEY_client_name + "");
                requestObject.put("client_address", allSurvey.get(count).SURVEY_client_address + "");
                requestObject.put("phone_number", allSurvey.get(count).tel_number + "");
                requestObject.put("latitude", allSurvey.get(count).latitude + "");
                requestObject.put("longitude", allSurvey.get(count).longtitude + "");
                requestObject.put("client_id", "0");

            } else {
                requestObject.put("client_id", allSurvey.get(count).client_id + "");

            }
            requestObject.put("category_id", allSurvey.get(count).category_id + "");

            requestObject.put("emp_id", allSurvey.get(count).emp_id);
            requestObject.put("notes", allSurvey.get(count).notes);
            requestObject.put("supervisor_id", allSurvey.get(count).supervisor_id + "");

            String encodedString = "";

            String userId = ConstantFunction.getstatus(getActivity(), "UserId");
            CustomListViewValuesArrSurvey = GetPhotoFromDbSales(allSurvey.get(count).emp_id, allSurvey.get(count).userTx);

            for (int i = 0; i < CustomListViewValuesArrSurvey.size(); i++) {
                String strImage = GetStringFromBytes(CustomListViewValuesArrSurvey
                        .get(i).getTakenPhoto());
                encodedString += new StringBuilder(String.valueOf(strImage))
                        .append(",").toString();
            }

            requestObject.put("imagecount", CustomListViewValuesArr.size());
            requestObject.put("photo", encodedString);

            requestObject.put("company_id", allSurvey.get(count).company_id);


        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

//		mProgressHUD = ProgressHUD.show(getActivity(),
//				getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.SurveyURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "AppTab_Dashboard_Home :- " + ConstantValues.SurveyURL + " -------------" + requestObject.toString());

        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {

                sqlcon.open();
                sqlcon.SurveyTaskById(allSurvey.get(count));
                if (count + 1 < allSurvey.size()) {
                    sendSurveyData(count + 1);

                } else {

//                    Log.v("SurveyData", "successs uploaded");
//                    Toast toast = Toast.makeText(getActivity(),
//                            getString(R.string.suc_uploaded),
//                            Toast.LENGTH_SHORT);
//
//                    toast.setGravity(Gravity.CENTER, 0, 0);
//                    toast.show();

//					mActivity.ClearAllPages();
//					// mActivity.popFragments();
//					mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
//							new AppTab_Dashboard_Home(), true, true);
                }

                if (count == allSurvey.size() - 1) {
                    SalesOperation();
                }


            }
        };
        wsHelper.execute();

    }

    public ArrayList<PhotoSalesModel> GetPhotoFromDbSurvey(String taskId) {
        sqlcon.open();
        ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
        tempArray = sqlcon.GetAllPhotosSales(taskId);
        return tempArray;

    }


    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pendingTaskImage:
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new Pending_Task_List_Fragment(), true, true);
                break;

            case R.id.rejectedTaskImage:
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new Rejected_Task_Fragment(), true, true);
                break;

            case R.id.completedTaskImage:
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new Completed_Task_Fragment(), true, true);
                break;

            case R.id.alertImage:
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new Alert_Screen_List_Fragment(), true, true);
                break;
            case R.id.specialImage:
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new Special_Task_List_Fragment(), true, true);
                break;

            case R.id.reportImage:
                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                        new Report_page_Fragment(), true, true);
                break;

            case R.id.AddTask1:
                if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("sales")) {
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD, new Sales_page_create_Fragment(), true, true);
                } else if (ConstantFunction.getstatus(getActivity(), "department").equalsIgnoreCase("Survey")) {
                    mActivity.pushFragments(ConstantValues.TAB_DASHBOARD, new Survey_page_Fragment(), true, true);
                }
                break;
            case R.id.headerUserImage:
                ShowPopUp();
                break;


            default:
                break;
        }
        // TODO Auto-generated method stub
    }

    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                if (mLogin != null && mLogin.size() > 0) {
                    if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                        Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                        ConstantFunction.savestatus(getActivity(),
                                "UserId", mLogin.get(0).emp_id + "");
                        Log.v("userId", mLogin.get(0).emp_id);


                        Picasso.with(getActivity())
                                .load(uri)
                                .transform(new CircleTransform()).resize(100, 100)
                                .into(headerUserImage);
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }
    }
}
