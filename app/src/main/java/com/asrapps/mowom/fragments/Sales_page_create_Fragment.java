package com.asrapps.mowom.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.DbHelper;
import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.CategoryDropDownAdapter;
import com.asrapps.mowom.adapter.ClientDropDownAdapter;
import com.asrapps.mowom.adapter.ClientDropDownAdapterSurvey;
import com.asrapps.mowom.adapter.NextStepDropDownAdapter;
import com.asrapps.mowom.adapter.Photo_Sales_Grid_Adapter;
import com.asrapps.mowom.base.BaseFragment;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.CurrentLocation;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.dialog.Status_Update_Dialog_Activity;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.CategoryData;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.ClientData;
import com.asrapps.mowom.model.Login;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.asrapps.mowom.model.SalesTask;
import com.asrapps.mowom.view.PlacesAutoCompleteAdapter;
import com.asrapps.utils.MowomLogFile;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class Sales_page_create_Fragment extends BaseFragment implements
        OnClickListener, AsyncResponseListener, DialogInterface.OnCancelListener {

    private static final String DATEPICKER = "DatePickerDialog";

    ImageView headerUserImage, backButtonImage;

    ConnectionDetector internetConnection;
    Calendar cStart;
    private ArrayList<Login> mLogin;

    private ProgressHUD mProgressHUD;
    Photo_Sales_Grid_Adapter adapter;
    public ArrayList<PhotoSalesModel> CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();
    public ArrayList<ClientData> clientData = new ArrayList<ClientData>();
    public ArrayList<CategoryData> categoryData = new ArrayList<CategoryData>();

    public ArrayList<String> nextStepData = new ArrayList<String>();

    public ArrayList<String> taskTypeData = new ArrayList<String>();
    public ArrayList<SalesTask> arrSalesTask = new ArrayList<>();

    int count = 0;

    SharedPreferences sharedPref;

    String selectedLanguage = "";
    String validEmailString = ".~#^|$%*!@/()-'\":;,?{}=!$^';,?×÷<>{}€£¥₩%~`¤♡♥_|《》¡¿°•○●□■◇◆♧♣▲▼▶◀↑↓←→☆★▪:-);-):-D:-(:'(:";

    InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String blockCharacterSet = "~#^|$%*!@/()-'\":;,?{}=!$^';,?×÷<>{}€£¥₩%~`¤♡♥_|《》¡¿°•○●□■◇◆♧♣▲▼▶◀↑↓←→☆★▪:-);-):-D:-(:'(:O1234567890";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    //	Button addImageButton;
//	boolean camOneSelected,camTwoSelected,add;
//	Resources res;
//	ImageView camaraImage2, camaraImage1,closeImageCam1,closeImageCam2;
//	RelativeLayout camaraImage1Layout, camaraImage2Layout;
//	HorizontalListView photoGrid;
//	String image1DI,image2ID;
    SQLController sqlcon;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1888;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final String IMAGE_DIRECTORY_NAME = "Movom";
    static File mediaStorageDir;
    private Uri fileUri;
    static String timeStamp = "";
    Button doneButton;
    static String saveId = "";
    PopupWindow clientPopup, nextStepPopup, taskTypePopup, categoryPopup;

    AutoCompleteTextView clientNameEd;
    ClientDropDownAdapterSurvey clientDropDownAdapterSurvey;
    TextView selectCategoryEd, selectTypeEd;

    EditText contactPerson, TelPhone, TaskNameEd, AdditionalInstructionEd, emailEd;
    String category_Id, client_id = "", task_type;

    View view;

    CurrentLocation crntLocation;

    AutoCompleteTextView clientAddress;
    LinearLayout llCustomLayout;
    PlacesAutoCompleteAdapter placesAutoCompleteAdapter;

    String usertx = "";

    StringBuilder fullAddress;

    TextWatcher txtwt;

    private Context mContext;

    // code to resolve keyboard input lagging issue.
    private static final int KEYBOARD_TIMEOUT = 100;
    private Handler mKeyboardHandler = new Handler();
    private Runnable mKeyboadRunnable = new Runnable() {
        @Override
        public void run() {
            boolean isOnlyOneRecord = false;
            int i;
            for (i = 0; i < clientData.size(); i++) {
                if (clientData.get(i).clientName.equals(clientNameEd.getText().toString() + "")) {
                    if (!isOnlyOneRecord) {
                        isOnlyOneRecord = true;
                    } else {
                        isOnlyOneRecord = false;
                        break;
                    }

                    break;
                } else {
                    client_id = "";
                }

            }
            if (isOnlyOneRecord) {
//                if (clientData.get(i).address != null) {
                if (clientData.get(i).client_address != null && clientData.get(i).client_address.size() > 0) {

                    if (clientData.get(i).client_address.size() == 1) {
                        clientAddress.setVisibility(View.VISIBLE);
                        llCustomLayout.setVisibility(View.GONE);
                        if (!TextUtils.isEmpty(clientData.get(i).client_address.get(0).location)) {
                            clientAddress.setText(clientData.get(i).client_address.get(0).location);
                        }
//                        clientAddress.setText(clientData.get(i).address.toString());
                    } else if (clientData.get(i).client_address.size() > 1) {
                        clientAddress.setVisibility(View.GONE);
                        llCustomLayout.setVisibility(View.VISIBLE);
                        llCustomLayout.removeAllViews();
                        for (int r = 0; r < clientData.get(i).client_address.size(); r++) {
                            CheckBox cb1 = new CheckBox(mContext);
                            cb1.setTextSize(15);
                            cb1.setTextColor(Color.parseColor("#3073B0"));
                            cb1.setGravity(Gravity.LEFT);
                            cb1.setPadding(5, 5, 5, 5);

                            if (!TextUtils.isEmpty(clientData.get(i).client_address.get(r).location)) {
                                cb1.setText(clientData.get(i).client_address.get(r).location);
                            }
                            llCustomLayout.addView(cb1);
                        }
                    }
                } else {
                    llCustomLayout.setVisibility(View.GONE);
                    clientAddress.setVisibility(View.VISIBLE);
                    clientAddress.setText("");
                }
//                    clientAddress.setText(clientData.get(i).address.toString());
//                }

                if (clientData.get(i).contact_person != null) {
                    contactPerson.setText(clientData.get(i).contact_person.toString());
                } else {
                    contactPerson.setText("");
                }

                if (clientData.get(i).phone_number != null) {
                    TelPhone.setText(clientData.get(i).phone_number.toString());
                } else {
                    TelPhone.setText("");
                }

                if (clientData.get(i).clientemail != null) {
                    emailEd.setText(clientData.get(i).clientemail.toString());
                } else {
                    emailEd.setText("");
                }

                clientAddress.setEnabled(false);
                contactPerson.setEnabled(false);
                TelPhone.setEnabled(false);
                emailEd.setEnabled(false);

                client_id = clientData.get(i).clientId;

                clientAddress.setEnabled(false);
                TelPhone.setEnabled(false);
            } else {
                llCustomLayout.setVisibility(View.GONE);
                clientAddress.setVisibility(View.VISIBLE);
                clientAddress.setText("");
                TelPhone.setText("");
                contactPerson.setText("");
                emailEd.setText("");

                clientAddress.setEnabled(true);
                contactPerson.setEnabled(true);
                TelPhone.setEnabled(true);
                emailEd.setEnabled(true);

                if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                    if (fullAddress != null && fullAddress.length() > 0 && clientNameEd.getText().toString().length() > 0) {
                        clientAddress.setText(fullAddress + "");

                    } else {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(mContext, Locale.getDefault());

                        try {
                            if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                                addresses = geocoder.getFromLocation(ConstantValues.latitude, ConstantValues.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();


                                fullAddress = new StringBuilder();
                                if (address.length() > 0) {
                                    fullAddress.append(address);
                                }


                                if (city.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(city);
                                }

                                if (state.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(state);
                                }

                                if (country.length() > 0) {

                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(country);
                                }

                                if (clientNameEd.getText().toString().length() > 0) {
                                    clientAddress.setText(fullAddress + "");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                }
//
                client_id = "";
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());


        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

//		if (selectedLanguage.equalsIgnoreCase("pr"))
//		{
////			view = inflater.inflate(R.layout.sales_layout_pr, container, false);
//		}
//		else {

//		}

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = inflater.inflate(R.layout.sales_page_create_layout_pr, container, false);
        } else {
            view = inflater.inflate(R.layout.sales_page_create_layout, container, false);
        }


        sqlcon = new SQLController(getActivity());
        sharedPref = getActivity().getSharedPreferences("My_prefs", Context.MODE_PRIVATE);

//        usertx = ((long) Math.floor(Math.random() * 9000000000L) + 1000000000L) + "";

        crntLocation = new CurrentLocation(getActivity());
        crntLocation.GetCurrentLocation();


        internetConnection = new ConnectionDetector(getActivity());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

//                while (ConstantValues.latitude == 0.0 && ConstantValues.longitude == 0.0) {
                if (internetConnection
                        .isConnectingToInternet("")) {
                    if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                        Geocoder geocoder;
                        List<Address> addresses;
                        geocoder = new Geocoder(getActivity(), Locale.getDefault());

                        try {
                            if (ConstantValues.latitude != 0.0 && ConstantValues.longitude != 0.0) {
                                addresses = geocoder.getFromLocation(ConstantValues.latitude, ConstantValues.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String country = addresses.get(0).getCountryName();
                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();

                                fullAddress = new StringBuilder();
                                if (address.length() > 0) {
                                    fullAddress.append(address);
                                }


                                if (city.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(city);
                                }

                                if (state.length() > 0) {
                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(state);
                                }

                                if (country.length() > 0) {

                                    if (fullAddress.length() > 0) {

                                        fullAddress.append(" , ");
                                    }

                                    fullAddress.append(country);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                }
            }

//            }
        }, 2000);


        nextStepData = new ArrayList<>();
        nextStepData.add(getResources().getString(R.string.Followup));
        nextStepData.add(getResources().getString(R.string.SendInformation));
        nextStepData.add(getResources().getString(R.string.Contactnotavailable));
        nextStepData.add(getResources().getString(R.string.NotInterested));

        taskTypeData = new ArrayList<>();
        taskTypeData.add("Normal");
        taskTypeData.add("Special");

        selectCategoryEd = (TextView) view.findViewById(R.id.selectCategoryEd);
        headerUserImage = (ImageView) view.findViewById(R.id.headerUserImage);
        clientNameEd = (AutoCompleteTextView) view.findViewById(R.id.clientNameEd);

        clientNameEd.setFilters(new InputFilter[]{filter});

        clientAddress = (AutoCompleteTextView) view.findViewById(R.id.clientAddress);
        llCustomLayout = (LinearLayout) view.findViewById(R.id.llCustomLayout);

        placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.address_row_item);
        clientAddress.setAdapter(placesAutoCompleteAdapter);


        contactPerson = (EditText) view.findViewById(R.id.contactPerson);

        contactPerson.setFilters(new InputFilter[]{filter});

        TelPhone = (EditText) view.findViewById(R.id.TelPhone);
        emailEd = (EditText) view.findViewById(R.id.emailEd);


        //Add sales task if already started task
        sqlcon.open();
        arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

        //store data already filled
        if (arrSalesTask != null && arrSalesTask.size() > 0) {

            if (arrSalesTask.get(0).client_id != null) {
                clientNameEd.setText(arrSalesTask.get(0).SALES_client_name);
            } else
                clientNameEd.setText(arrSalesTask.get(0).SALES_client_name_new);
            if (arrSalesTask.get(0).SALES_client_address != null) {

                JSONArray arrClientAddress = null;
                try {
                    arrClientAddress = new JSONArray(arrSalesTask.get(0).SALES_client_address + "");
                    ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();
                    Gson gson = new Gson();
                    for (int r = 0; r < arrClientAddress.length(); r++) {
                        JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                        ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                        arrListClientAddress.add(address);
                    }
                    if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                        arrSalesTask.get(0).client_address = new ArrayList<ClientAddress>();
                        arrSalesTask.get(0).client_address.addAll(arrListClientAddress);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

//                if (clientData.get(i).client_address.size() == 1) {
//                    clientAddress.setVisibility(View.VISIBLE);
//                    llCustomLayout.setVisibility(View.GONE);
//                    clientAddress.setText(clientData.get(i).address.toString());
//                } else if (clientData.get(i).client_address.size() > 1) {
//                    clientAddress.setVisibility(View.GONE);
//                    llCustomLayout.setVisibility(View.VISIBLE);
//
//                    for (int r = 0; r < clientData.get(i).client_address.size(); r++) {
//                        CheckBox cb1 = new CheckBox(mContext);
//                        cb1.setTextSize(15);
//                        cb1.setTextColor(Color.parseColor("#000000"));
//                        cb1.setGravity(Gravity.CENTER);
//                        cb1.setPadding(5, 5, 5, 5);
//
//                        if (!TextUtils.isEmpty(clientData.get(i).client_address.get(r).location)) {
//                            cb1.setText(clientData.get(i).client_address.get(r).location);
//                        }
//                        llCustomLayout.addView(cb1);
//                    }
//                }
//
//            clientAddress.setText(arrSalesTask.get(0).SALES_client_address);
            if (arrSalesTask.get(0).SALES_contact_person != null)
                contactPerson.setText(arrSalesTask.get(0).SALES_contact_person);
            if (arrSalesTask.get(0).tel_number != null)
                TelPhone.setText(arrSalesTask.get(0).tel_number);
            if (arrSalesTask.get(0).client_email != null)
                emailEd.setText(arrSalesTask.get(0).client_email);
            if (arrSalesTask.get(0).task_id_Tx != null)
                usertx = arrSalesTask.get(0).task_id_Tx;
            else
                usertx = ((long) Math.floor(Math.random() * 9000000000L) + 1000000000L) + "";
        }

        if (usertx.equals("")) {
            usertx = ((long) Math.floor(Math.random() * 9000000000L) + 1000000000L) + "";
        }

        if (internetConnection
                .isConnectingToInternet("")) {
            //
            saveId = "img_" + ConstantFunction.getstatus(getActivity(), "UserId");

//			TruckateTable();
            SetUserDetails();
            getclientData();

        } else {
            sqlcon.open();
            clientData = sqlcon.getSalesClientAll();

            clientDropDownAdapterSurvey = new ClientDropDownAdapterSurvey(getActivity(), clientData);
            clientNameEd.setAdapter(clientDropDownAdapterSurvey);
            clientNameEd.setThreshold(0);
//            clientNameEd.showDropDown();

            sqlcon.open();
            categoryData = sqlcon.getCategoryAll();

            if (categoryData != null && categoryData.size() > 0) {
                selectCategoryEd.setText(categoryData.get(0).categoryName);
                category_Id = categoryData.get(0).categoryId;
            }

            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

//        selectClientButton= (Button) view.findViewById(R.id.selectClientButton);
//        selectClientButton.setVisibility(View.GONE);
//
//
//
//        selectClientButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addClientButton.setVisibility(View.VISIBLE);
//                selectClientButton.setVisibility(View.GONE);
//
//
//                clientNameNew.setVisibility(View.GONE);
//                clientNameEd.setVisibility(View.VISIBLE);
//
//                clientAddress.setEnabled(false);
//                contactPerson.setEnabled(false);
//                TelPhone.setEnabled(false);
//                emailEd.setEnabled(false);
//            }
//        });

//        addClientButton = (Button) view.findViewById(R.id.addClientButton);
//        addClientButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                addClientButton.setVisibility(View.GONE);
////                selectClientButton.setVisibility(View.VISIBLE);
//
//                client_id="";
////				if(clientData.get(position).clientId.equals("-1"))
////				{
////                clientNameNew.setVisibility(View.VISIBLE);
//                clientNameEd.setVisibility(View.GONE);
////				}
////				else
////				{
////					client_id=clientData.get(position).clientId;
////
////					llClientData.setVisibility(View.GONE);
////				}
//
//                clientAddress.setText("");
//                contactPerson.setText("");
//                TelPhone.setText("");
//                emailEd.setText("");
//                clientNameEd.setText("");
//
//                clientAddress.setEnabled(true);
//                contactPerson.setEnabled(true);
//                TelPhone.setEnabled(true);
//                emailEd.setEnabled(true);
//
//            }
//        });


        selectCategoryEd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                WebServiceHelper.hideKeyboard(getActivity(), v);

                showCategoryPopup();
            }
        });
        TaskNameEd = (EditText) view.findViewById(R.id.TaskNameEd);
        AdditionalInstructionEd = (EditText) view.findViewById(R.id.AdditionalInstructionEd);
        selectTypeEd = (TextView) view.findViewById(R.id.selectTypeEd);
        selectTypeEd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                WebServiceHelper.hideKeyboard(getActivity(), v);

                showTaskTypePopup();
            }
        });

		/*txtHeaderUserName = (TextView) view
                .findViewById(R.id.txtHeaderUserName);*/

//		llEng=(LinearLayout)view.findViewById(R.id.llEng);
//		llPersian=(LinearLayout)view.findViewById(R.id.llPersian);

//        clientNameEd.setVisibility(View.GONE);

        clientNameEd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                try {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < clientData.size(); i++) {
                    if (clientData.get(i).clientName.equals(clientNameEd.getText().toString() + "")) {

                        if (clientData.get(i).address != null) {
                            clientAddress.setText(clientData.get(i).address.toString());
                        } else {
                            llCustomLayout.setVisibility(View.GONE);
                            clientAddress.setVisibility(View.VISIBLE);
                            clientAddress.setText("");
                        }

                        if (clientData.get(i).contact_person != null) {
                            contactPerson.setText(clientData.get(i).contact_person.toString());
                        } else {
                            contactPerson.setText("");
                        }

                        if (clientData.get(i).phone_number != null) {
                            TelPhone.setText(clientData.get(i).phone_number.toString());
                        } else {
                            TelPhone.setText("");
                        }

                        if (clientData.get(i).clientemail != null) {
                            emailEd.setText(clientData.get(i).clientemail.toString());
                        } else {
                            emailEd.setText("");
                        }

                        clientAddress.setEnabled(false);
                        contactPerson.setEnabled(false);
                        TelPhone.setEnabled(false);
                        emailEd.setEnabled(false);

                        client_id = clientData.get(i).clientId;

                        clientAddress.setEnabled(false);
                        TelPhone.setEnabled(false);
                    }
                }


            }
        });

        txtwt = new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                Log.i("REACHES BEFORE", "YES");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Log.i("REACHES AFTER", "YES");
                mKeyboardHandler.removeCallbacks(mKeyboadRunnable);
                mKeyboardHandler.postDelayed(mKeyboadRunnable, KEYBOARD_TIMEOUT);

            }
        };
        clientNameEd.addTextChangedListener(txtwt);


//			headerUserImage.setOnClickListener(this);

        backButtonImage = (ImageView) view
                .findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);

//		addImageButton = (Button) view.findViewById(R.id.addImageButton);
//		addImageButton.setOnClickListener(this);

        doneButton = (Button) view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(this);
        // addImageButton.setEnabled(false);


//		camaraImage1 = (ImageView) view.findViewById(R.id.camaraImage1);
//		camaraImage1.setOnClickListener(this);
//
//		camaraImage2 = (ImageView) view.findViewById(R.id.camaraImage2);
//		camaraImage2.setOnClickListener(this);
//
//		closeImageCam1= (ImageView) view.findViewById(R.id.closeImageCam1);
//		closeImageCam1.setOnClickListener(this);
//
//		closeImageCam2= (ImageView) view.findViewById(R.id.closeImageCam2);
//		closeImageCam2.setOnClickListener(this);
//
//		closeImageCam1.setVisibility(View.GONE);
//		closeImageCam2.setVisibility(View.GONE);
//
//		camaraImage1Layout = (RelativeLayout) view
//				.findViewById(R.id.camaraImage1Layout);
//		camaraImage2Layout = (RelativeLayout) view
//				.findViewById(R.id.camaraImage2Layout);
//
//		res = getResources();
//		photoGrid = (HorizontalListView) view.findViewById(R.id.photoGrid);
//		photoGrid.setVisibility(View.GONE);

        return view;
    }


    private void showClientPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        clientPopup = new PopupWindow(view);

        int width = clientNameEd.getWidth();
        if (width > 0) {
            clientPopup.setWidth(width);
        } else {
            clientPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        clientPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        clientPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        clientPopup.setOutsideTouchable(true);
        clientPopup.setTouchable(true);
        clientPopup.setFocusable(true);

        ClientDropDownAdapter clientAdapter = new ClientDropDownAdapter(getActivity(), clientData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(clientAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clientNameEd.setText(clientData.get(position).clientName);

//				if(clientData.get(position).clientId.equals("-1"))
//				{
//					llClientData.setVisibility(View.VISIBLE);
//				}
//				else
//				{
                client_id = clientData.get(position).clientId;
//
//                clientNameNew.setVisibility(View.GONE);
                clientNameEd.setVisibility(View.VISIBLE);
//				}

//				clientNameNew=(EditText)view.findViewById(R.id.clientNameNew);
                if (clientData.get(position).address != null) {
                    clientAddress.setText(clientData.get(position).address.toString());
                }

                if (clientData.get(position).contact_person != null) {
                    contactPerson.setText(clientData.get(position).contact_person.toString());
                }

                if (clientData.get(position).phone_number != null) {
                    TelPhone.setText(clientData.get(position).phone_number.toString());
                }

                if (clientData.get(position).clientemail != null) {
                    emailEd.setText(clientData.get(position).clientemail.toString());
                }
                clientAddress.setEnabled(false);
                contactPerson.setEnabled(false);
                TelPhone.setEnabled(false);
                emailEd.setEnabled(false);


                clientPopup.dismiss();
            }
        });

        clientPopup.showAsDropDown(clientNameEd, 0, 2);
    }


//	private void showNextStepPopup() {
//
//		LayoutInflater layoutInflater = (LayoutInflater)
//				getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
//		View view = layoutInflater.inflate(R.layout.dropdown_popup, null);
//
//		nextStepPopup = new PopupWindow(view);
//
//		int width = NextSteps.getWidth();
//		if (width > 0) {
//			nextStepPopup.setWidth(width);
//		} else {
//			nextStepPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
//		}
//		nextStepPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//		nextStepPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
//		nextStepPopup.setOutsideTouchable(true);
//		nextStepPopup.setTouchable(true);
//		nextStepPopup.setFocusable(true);
//
//		NextStepDropDownAdapter nextStepAdapter = new NextStepDropDownAdapter(getActivity(), nextStepData);
//
//		final ListView ageList = (ListView) view.findViewById(R.id.listView);
//		ageList.setAdapter(nextStepAdapter);
//
//		ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				NextSteps.setText(nextStepData.get(position).toString());
//				nextStepPopup.dismiss();
//			}
//		});
//
//		nextStepPopup.showAsDropDown(NextSteps, 0, 2);
//	}

    private void showTaskTypePopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        taskTypePopup = new PopupWindow(view);

        int width = selectTypeEd.getWidth();
        if (width > 0) {
            taskTypePopup.setWidth(width);
        } else {
            taskTypePopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        taskTypePopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        taskTypePopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        taskTypePopup.setOutsideTouchable(true);
        taskTypePopup.setTouchable(true);
        taskTypePopup.setFocusable(true);

        NextStepDropDownAdapter nextStepAdapter = new NextStepDropDownAdapter(getActivity(), taskTypeData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(nextStepAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectTypeEd.setText(taskTypeData.get(position).toString());

                if (position == 0) {
                    task_type = "1";
                } else {
                    task_type = "2";
                }
                taskTypePopup.dismiss();
            }
        });

        taskTypePopup.showAsDropDown(selectTypeEd, 0, 2);
    }

    private void showCategoryPopup() {

        LayoutInflater layoutInflater = (LayoutInflater)
                getActivity().getBaseContext().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.dropdown_popup, null);

        categoryPopup = new PopupWindow(view);

        int width = selectCategoryEd.getWidth();
        if (width > 0) {
            categoryPopup.setWidth(width);
        } else {
            categoryPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        }
        categoryPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        categoryPopup.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        categoryPopup.setOutsideTouchable(true);
        categoryPopup.setTouchable(true);
        categoryPopup.setFocusable(true);

        CategoryDropDownAdapter nextStepAdapter = new CategoryDropDownAdapter(getActivity(), categoryData);

        final ListView ageList = (ListView) view.findViewById(R.id.listView);
        ageList.setAdapter(nextStepAdapter);

        ageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectCategoryEd.setText(categoryData.get(position).categoryName.toString());
                category_Id = categoryData.get(position).categoryId;
                categoryPopup.dismiss();
            }
        });

        categoryPopup.showAsDropDown(selectCategoryEd, 0, 2);
    }

    private void SetUserDetails() {
        /*
         * txtHeaderUserName.setText(ConstantFunction.getuser(getActivity(),
		 * "UserHeaderName"));
		 */

        String userImage = ConstantFunction.getuser(getActivity(), "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(getActivity())
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(getActivity(), "UserId")).execute();
        }

    }

//	private void SetListView() {
//
//		CustomListViewValuesArr = new ArrayList<PhotoSalesModel>();
//		CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));
//		// photoGrid.removeAll();
//		// for (int i = 0; i <CustomListViewValuesArr.size(); i++) {
//		// String path = CustomListViewValuesArr.get(i).getTaskId();
//		// if (path.equals(taskId)) {
//		// photoGrid.add(CustomListViewValuesArr.get(i),this);
//		// }
//		// }
//
//		System.out.println(".................CustomListViewValuesArr.size()--> "+CustomListViewValuesArr.size());
//		adapter = new Photo_Sales_Grid_Adapter(getActivity(),
//				CustomListViewValuesArr, new OnDeleteSalesImageClick() {
//
//			public void onClickDelete(View v, int position,
//									  PhotoSalesModel photo) {
//				// TODO Auto-generated method stub
//
//				DeletePhotoFromDb(photo.getPhotoId(), position);
//
//			}
//		});
//
//		photoGrid.setAdapter(adapter);
//
//	}

//	public void DeletePhotoFromDb(String photoId, int position) {
//		image1DI="";
//		image2ID="";
//		sqlcon.open();
//		sqlcon.DeleteOnePhotoSales(photoId);
//		CustomListViewValuesArr.remove(position);
//		adapter.notifyDataSetChanged();
//		sqlcon.open();
//		count = sqlcon.GetRowCountSales(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"));
//		System.out.println("................................count==> "+count);
//		if (count <= 2) {
//			ArrayList<PhotoSalesModel> tempPhotos = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));
//			if (tempPhotos.size() == 2) {
//
//				photoGrid.setVisibility(View.GONE);
//				camaraImage1Layout.setVisibility(View.VISIBLE);
//				camaraImage2Layout.setVisibility(View.VISIBLE);
//
//				Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
//								.getTakenPhoto(), 0,
//						tempPhotos.get(0).getTakenPhoto().length);
//				camaraImage1.setImageBitmap(bm);
//				Bitmap bm1 = BitmapFactory.decodeByteArray(tempPhotos.get(1)
//								.getTakenPhoto(), 0,
//						tempPhotos.get(1).getTakenPhoto().length);
//				camaraImage2.setImageBitmap(bm1);
//
//				image1DI=tempPhotos.get(0).getPhotoId();
//				image2ID=tempPhotos.get(1).getPhotoId();
//			}
//
//			if (tempPhotos.size() == 1) {
//
//				photoGrid.setVisibility(View.GONE);
//				camaraImage1Layout.setVisibility(View.VISIBLE);
//				camaraImage2Layout.setVisibility(View.VISIBLE);
//
//				Bitmap bm = BitmapFactory.decodeByteArray(tempPhotos.get(0)
//								.getTakenPhoto(), 0,
//						tempPhotos.get(0).getTakenPhoto().length);
//				camaraImage1.setImageBitmap(bm);
//				image1DI=tempPhotos.get(0).getPhotoId();
//
//				camaraImage2
//						.setImageResource(R.drawable.inspection_icon_with_bg);
//			}
//
//			if (tempPhotos.size() == 0) {
//
//				photoGrid.setVisibility(View.GONE);
//				camaraImage1Layout.setVisibility(View.VISIBLE);
//				camaraImage2Layout.setVisibility(View.VISIBLE);
//
//				camaraImage1
//						.setImageResource(R.drawable.inspection_icon_with_bg);
//				camaraImage2
//						.setImageResource(R.drawable.inspection_icon_with_bg);
//				image1DI="";
//				image2ID="";
//			}
//
//
//		}
//		else {
//			photoGrid.setVisibility(View.VISIBLE);
//			camaraImage1Layout.setVisibility(View.GONE);
//			camaraImage2Layout.setVisibility(View.GONE);
//			SetListView();
//		}
//
//		if(closeImageCam1.getVisibility() != View.VISIBLE&&closeImageCam2.getVisibility() != View.VISIBLE){
//			TruckateTable();
//		}
//
//	}
//	public void TruckateTable() {
//		sqlcon.open();
//		sqlcon.DeleteAll(DbHelper.TABLE_PHOTO_TAKEN_SALES);
//	}


//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		getActivity();
//
//		if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
//			getActivity();
//			if (resultCode == Activity.RESULT_OK) {
//				previewCapturedImage();
//			}
//
//			else {
//				getActivity();
//				if (resultCode == Activity.RESULT_CANCELED) {
//
//				} else {
//					Toast toast = Toast.makeText(getActivity(),
//							"Sorry! Failed to capture image",
//							Toast.LENGTH_SHORT);
//					toast.setGravity(Gravity.CENTER, 0, 0);
//					toast.show();
//				}
//			}
//		}
//
//		else if (resultCode != Activity.RESULT_CANCELED && data != null) {
//			if (requestCode == 2) {
//
//				timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
//						Locale.getDefault()).format(new Date());
//
//				Uri selectedImage = data.getData();
//				String[] filePath = { MediaStore.Images.Media.DATA };
//
//				Cursor c = getActivity().getContentResolver().query(
//						selectedImage, filePath, null, null, null);
//				c.moveToFirst();
//				int columnIndex = c.getColumnIndex(filePath[0]);
//				String picturePath = c.getString(columnIndex);
//				c.close();
//
//				Bitmap scaleBitmap = (BitmapFactory.decodeFile(picturePath));
//				final Bitmap bitmap = Bitmap.createScaledBitmap(scaleBitmap,
//						200, 200, false);
//				Log.d("path", picturePath + "");
//
//				ByteArrayOutputStream stream = new ByteArrayOutputStream();
//				bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//				byte[] byteArray = stream.toByteArray();
//
//				InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp);
//
//				sqlcon.open();
//				count = sqlcon.GetRowCountSales(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"));
//				System.out.println(".................count Galary--> "+count);
//				//String id="";
//
//				PhotoSalesModel tempValuesArr = new PhotoSalesModel();
//				tempValuesArr = GetLastPhotoFromDb();
//				//id=tempValuesArr.getPhotoId();
//
//				if (count<= 2) {
//					photoGrid.setVisibility(View.GONE);
//					camaraImage1Layout.setVisibility(View.VISIBLE);
//					camaraImage2Layout.setVisibility(View.VISIBLE);
//
//					//camOneSelected = false;
//					//camTwoSelected=true;
//
//
//
//
//					if(camOneSelected){
//						closeImageCam1.setVisibility(View.VISIBLE);
//						image1DI=tempValuesArr.getPhotoId();
//						//image2ID="";
//						camaraImage1.setImageBitmap(bitmap);
//					}
//					if(camTwoSelected){
//						closeImageCam2.setVisibility(View.VISIBLE);
//						//image1DI="";
//						image2ID=tempValuesArr.getPhotoId();
//						camaraImage2.setImageBitmap(bitmap);
//					}
//
//					if(add){
//						if(closeImageCam1.getVisibility() != View.VISIBLE){
//							closeImageCam1.setVisibility(View.VISIBLE);
//							image1DI=tempValuesArr.getPhotoId();
//							//image2ID="";
//							camaraImage1.setImageBitmap(bitmap);
//						}
//
//						else if(closeImageCam2.getVisibility() != View.VISIBLE){
//							closeImageCam2.setVisibility(View.VISIBLE);
//							//image1DI="";
//							image2ID=tempValuesArr.getPhotoId();
//							camaraImage2.setImageBitmap(bitmap);
//						}
//					}
//				}
//
//				else  {
//					photoGrid.setVisibility(View.VISIBLE);
//					camaraImage1Layout.setVisibility(View.GONE);
//					camaraImage2Layout.setVisibility(View.GONE);
//					SetListView();
//
//				}
//
//				/*
//				 * profImage = ConstantFunction.BitMapToString(ConstantFunction
//				 * .Shrinkmethod(picturePath, ivProfilePic.getWidth(),
//				 * ivProfilePic.getHeight())); mask image
//				 * ivProfilePic.setImageBitmap(bitmap);
//				 */
//			}
//		}
//
//	}
//	public PhotoSalesModel GetLastPhotoFromDb() {
//		sqlcon.open();
//		PhotoSalesModel tempArray = new PhotoSalesModel();
//		tempArray = sqlcon.GetLastPhotosSales();
//		return tempArray;
//
//	}
//
//
//	private void previewCapturedImage() {
//		try {
//
//			// bimatp factory
//			BitmapFactory.Options options = new BitmapFactory.Options();
//
//			// downsizing image as it throws OutOfMemory Exception for larger
//			// images
//			options.inSampleSize = 8;
//
//			final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
//					options);
//			ByteArrayOutputStream stream = new ByteArrayOutputStream();
//			bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//			byte[] byteArray = stream.toByteArray();
//
//			InserToPhoto(ConstantFunction.getstatus(getActivity(), "UserId"), byteArray, timeStamp);
//
//			sqlcon.open();
//			count = sqlcon.GetRowCountSales(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"));
//
//
//			PhotoSalesModel tempValuesArr = new PhotoSalesModel();
//			tempValuesArr = GetLastPhotoFromDb();
//			System.out.println(".................count Cam--> "+count);
//
//			if (count<= 2) {
//				photoGrid.setVisibility(View.GONE);
//				camaraImage1Layout.setVisibility(View.VISIBLE);
//				camaraImage2Layout.setVisibility(View.VISIBLE);
//
//				//camOneSelected = false;
//				//camTwoSelected=true;
//
//				if(camOneSelected){
//					closeImageCam1.setVisibility(View.VISIBLE);
//
//					image1DI=tempValuesArr.getPhotoId();
//					//image2ID="";
//					camaraImage1.setImageBitmap(bitmap);
//				}
//				if(camTwoSelected){
//					closeImageCam2.setVisibility(View.VISIBLE);
//					//image1DI="";
//					image2ID=tempValuesArr.getPhotoId();
//					camaraImage2.setImageBitmap(bitmap);
//				}
//				if(add){
//					if(closeImageCam1.getVisibility() == View.VISIBLE){
//						closeImageCam1.setVisibility(View.VISIBLE);
//						image1DI=tempValuesArr.getPhotoId();
//						//image2ID="";
//						camaraImage1.setImageBitmap(bitmap);
//					}
//					if(closeImageCam2.getVisibility() == View.VISIBLE){
//						closeImageCam2.setVisibility(View.VISIBLE);
//						//image1DI="";
//						image2ID=tempValuesArr.getPhotoId();
//						camaraImage2.setImageBitmap(bitmap);
//					}
//				}
//			}
//
//			else {
//				photoGrid.setVisibility(View.VISIBLE);
//				camaraImage1Layout.setVisibility(View.GONE);
//				camaraImage2Layout.setVisibility(View.GONE);
//				SetListView();
//			}
//
//			/*
//			if (count<= 2) {
//				photoGrid.setVisibility(View.GONE);
//				camaraImage1Layout.setVisibility(View.VISIBLE);
//				camaraImage2Layout.setVisibility(View.VISIBLE);
//
//				if(cam1Selected)
//				camaraImage1.setImageBitmap(bitmap);
//				else
//					camaraImage2.setImageBitmap(bitmap);
//			}
//*/
//
//		} catch (NullPointerException e) {
//			e.printStackTrace();
//		}
//	}
//	public void InserToPhoto(String taskId, byte[] image, String taken_time) {
//		sqlcon.open();
//		sqlcon.InsertToPhotoSales(taskId, image, taken_time);
//	}
//
//	public ArrayList<PhotoSalesModel> GetPhotoFromDb(String taskId) {
//		sqlcon.open();
//		ArrayList<PhotoSalesModel> tempArray = new ArrayList<PhotoSalesModel>();
//		tempArray = sqlcon.GetAllPhotosSales(taskId);
//		return tempArray;
//
//	}
//
//	private void DeletePhoto(String photoId, boolean selectedImage1){
//		sqlcon.open();
//		sqlcon.DeleteOnePhotoSales(photoId);
//
//		sqlcon.open();
//		count = sqlcon.GetRowCountSales(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"));
//
//		if(selectedImage1){
//			closeImageCam1.setVisibility(View.GONE);
//
//			camaraImage1
//					.setImageResource(R.drawable.inspection_icon_with_bg);
//		}
//		else{
//			closeImageCam2.setVisibility(View.GONE);
//			camaraImage2
//					.setImageResource(R.drawable.inspection_icon_with_bg);
//		}
//
//		if(closeImageCam1.getVisibility() != View.VISIBLE&&closeImageCam2.getVisibility() != View.VISIBLE){
//			TruckateTable();
//		}
//
//	}


    @Override
    public void onClick(View v) {

//        sqlcon.open();
//        count = sqlcon.GetRowCountSales(DbHelper.TABLE_PHOTO_TAKEN_SALES, ConstantFunction.getstatus(getActivity(), "UserId"));

        switch (v.getId()) {

            case R.id.headerUserImage:
                ShowPopUp();
                break;

            case R.id.backButtonImage:
                mActivity.popFragments();
                break;

//			case R.id.camaraImage1:
//				camOneSelected = true;
//				camTwoSelected=false;
//				add=false;
//				// captureImage(); ,
//				// if(camaraImage1.getDrawable() == null)
//
//				if(closeImageCam1.getVisibility() != View.VISIBLE) {
//
//					if (count < 5)
//						ShowSelectImageDialog(getActivity());
//					else {
//						Toast toast = Toast.makeText(getActivity(),
//								getString(R.string.only_five_allowed),
//								Toast.LENGTH_SHORT);
//						toast.setGravity(Gravity.CENTER, 0, 0);
//						toast.show();
//					}
//				}
//				break;
//
//			case R.id.camaraImage2:
//				camOneSelected = false;
//				camTwoSelected=true;
//				add=false;
//				// captureImage();
//				// if(camaraImage2.getDrawable() == null)
//				if(closeImageCam2.getVisibility() != View.VISIBLE)	{
//
//					if (count < 5)
//						ShowSelectImageDialog(getActivity());
//					else {
//						Toast toast = Toast.makeText(getActivity(),
//								getString(R.string.only_five_allowed),
//								Toast.LENGTH_SHORT);
//						toast.setGravity(Gravity.CENTER, 0, 0);
//						toast.show();
//					}
//				}
//				break;
//
//			case R.id.addImageButton:
//				camOneSelected = false;
//				camTwoSelected=false;
//				add=true;
//				if (count < 5)
//					ShowSelectImageDialog(getActivity());
//				else {
//					Toast toast = Toast.makeText(getActivity(),
//							getString(R.string.only_five_allowed),
//							Toast.LENGTH_SHORT);
//					toast.setGravity(Gravity.CENTER, 0, 0);
//					toast.show();
//				}
//				break;

            case R.id.doneButton:


                String task_id = ConstantFunction.getuser(getActivity(), AppConstants.task_Id);
                String sales_Task_id = ConstantFunction.getuser(getActivity(), "sales_Task");


                if (!sales_Task_id.equals(task_id) && !TextUtils.isEmpty(task_id) && !task_id.equals("0"))// && sharedPref.getBoolean("is_start_task", true))
                {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.err_start_task));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                                mActivity.ClearAllPages();
                                // mActivity.popFragments();
                                mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                        new AppTab_Dashboard_Home(), true, true);

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                } else {

//                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putBoolean("is_start_task", true);
//                    editor.commit();

                    try {
                        View view = getActivity().getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    sendSalesData();
                }
                break;

//			case R.id.closeImageCam1:
//				DeletePhoto(image1DI, true);
//				break;
//
//			case R.id.closeImageCam2:
//				DeletePhoto(image2ID, false);
//				break;

            default:
                break;
        }
    }

    //	public void ShowSelectImageDialog(Context c) {
//
//		final Context context = c;
//		final Dialog dialog = new Dialog(context);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.getWindow().setBackgroundDrawable(
//				new ColorDrawable(Color.TRANSPARENT));
//
//		// Include dialog.xml file
//		dialog.setContentView(R.layout.dialog_gallery);
//		dialog.show();
//
//		// dialog cancel button
//		Button btnDialogCancel = (Button) dialog
//				.findViewById(R.id.btnDialogCancel);
//		btnDialogCancel.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				dialog.dismiss();
//			}
//		});
//
//		// dialog Camera button
//		Button btnCamera = (Button) dialog.findViewById(R.id.btnCamera);
//		btnCamera.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				captureImage();
//				/*
//				 * String dir = Environment
//				 * .getExternalStoragePublicDirectory(Environment
//				 * .DIRECTORY_PICTURES) + "/movom/"; File newdir = new
//				 * File(dir); newdir.mkdirs(); String file = dir + "movom.jpg";
//				 * File newfile = new File(file); try { newfile.createNewFile();
//				 * } catch (IOException e) { }
//				 *
//				 * Uri outputFileUri = Uri.fromFile(newfile); Intent intent =
//				 * new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//				 * intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
//				 * getActivity().startActivityForResult(intent,
//				 * CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
//				 */
//				dialog.dismiss();
//			}
//		});
//
//		// dialog Gallery button
//		Button btnGallery = (Button) dialog.findViewById(R.id.btnGallery);
//		btnGallery.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(
//						Intent.ACTION_PICK,
//						MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//				getActivity().startActivityForResult(
//						Intent.createChooser(intent, "Select File"), 2);
//				dialog.dismiss();
//			}
//		});
//	}
//
//	private void captureImage() {
//		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//		fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
//
//		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//
//		getActivity().startActivityForResult(intent,
//				CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
//	}
//	public Uri getOutputMediaFileUri(int type) {
//		return Uri.fromFile(getOutputMediaFile(type));
//	}
//
//	private static File getOutputMediaFile(int type) {
//
//		mediaStorageDir = new File(
//				Environment
//						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//				IMAGE_DIRECTORY_NAME);
//
//		// Create the storage directory if it does not exist
//		if (!mediaStorageDir.exists()) {
//			if (!mediaStorageDir.mkdirs()) {
//				Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
//						+ IMAGE_DIRECTORY_NAME + " directory");
//				return null;
//			}
//		}
//
//		// Create a media file name
//		timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
//				.format(new Date());
//		File mediaFile;
//		if (type == MEDIA_TYPE_IMAGE) {
//			mediaFile = new File(mediaStorageDir.getPath() + File.separator
//					+ "IMG_" + timeStamp + saveId + ".jpg");
//		} else {
//			return null;
//		}
//
//		return mediaFile;
//	}
//
    private void ShowPopUp() {

        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();

        dialog.setCancelable(true);
        dialog.show(fragTransaction, "Dialog");

        // Show popup
        // GetService(ConstantValues.RejectTaskURL);
    }

    //
//
    private void getclientData() {


        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        JSONObject requestObject = new JSONObject();
        try {

            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }


        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ListOfClient);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_page_create_Fragment :- " + ConstantValues.ListOfClient + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                mProgressHUD.dismiss();
                Log.e("Response==>", response);

                getcategoryData();

                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {

                                JSONArray job2 = job.getJSONArray("data");

                                clientData = new ArrayList<>();
                                for (int i = 0; i < job2.length(); i++) {

                                    ClientData obj = new ClientData();
                                    obj.clientId = ((JSONObject) job2.get(i)).get("clientid").toString();
                                    obj.clientName = ((JSONObject) job2.get(i)).get("client_name").toString();

                                    if (((JSONObject) job2.get(i)).has("client_address")) {
                                        JSONArray arrClientAddress = ((JSONObject) job2.get(i)).getJSONArray("client_address");
                                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                                        Gson gson = new Gson();
                                        for (int r = 0; r < arrClientAddress.length(); r++) {
                                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                                            arrListClientAddress.add(address);
                                        }

                                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                                            obj.client_address = new ArrayList<>();
                                            obj.client_address.addAll(arrListClientAddress);

                                            obj.address = ((JSONObject) job2.get(i)).getJSONArray("client_address").toString();
                                        }
                                    }

//                                    if (((JSONObject) job2.get(i)).has("address"))
//                                        obj.address = ((JSONObject) job2.get(i)).get("address").toString();
                                    obj.contact_person = ((JSONObject) job2.get(i)).get("contact_person").toString();
                                    obj.clientemail = ((JSONObject) job2.get(i)).get("clientemail").toString();
                                    obj.phone_number = ((JSONObject) job2.get(i)).get("phone_number").toString();

                                    clientData.add(obj);

                                    sqlcon.open();
                                    sqlcon.addSalesClientTask(obj);

                                }

                                clientDropDownAdapterSurvey = new ClientDropDownAdapterSurvey(getActivity(), clientData);
                                clientNameEd.setAdapter(clientDropDownAdapterSurvey);
                                clientNameEd.setThreshold(0);
//                                clientNameEd.showDropDown();

//								ClientData obj=new ClientData();
//								obj.clientId= "-1";
//								obj.clientName = "Add New";
//								obj.address="";
//								obj.contact_person = "";
//								obj.clientemail="";
//								obj.phone_number ="";
//
//
//								clientData.add(obj);

//								sqlcon.open();
//								sqlcon.addSalesClientTask(obj);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());


                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                } else {

                }

            }
        };
        wsHelper.execute();
    }

    private void getcategoryData() {


        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        JSONObject requestObject = new JSONObject();
        try {

            requestObject.put("company_id", ConstantFunction.getstatus(getActivity(), "companyid"));

            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }


        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ListOfCCategory);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_page_create_Fragment :- " + ConstantValues.ListOfCCategory + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                mProgressHUD.dismiss();
                Log.e("Response==>", response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {

                                JSONArray job2 = job.getJSONArray("data");

                                categoryData = new ArrayList<>();
                                for (int i = 0; i < job2.length(); i++) {

                                    CategoryData obj = new CategoryData();
                                    obj.categoryId = ((JSONObject) job2.get(i)).get("id").toString();
                                    obj.categoryName = ((JSONObject) job2.get(i)).get("name").toString();

                                    categoryData.add(obj);

                                    sqlcon.open();
                                    sqlcon.addCategory(obj);


                                    if (categoryData != null && categoryData.size() > 0) {
                                        selectCategoryEd.setText(categoryData.get(0).categoryName);
                                        category_Id = categoryData.get(0).categoryId;
                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again),
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());


                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                    }
                } else {

                }

            }
        };
        wsHelper.execute();
    }


    private void sendSalesData() {
        if (client_id.length() == 0) {

            if (clientNameEd.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_name),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (clientAddress.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_address),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (contactPerson.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_contact_person),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (TelPhone.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_tele),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (TelPhone.getText().toString().length() < 10 || TelPhone.getText().toString().length() > 16) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_tele_valid),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (emailEd.getText().toString().length() <= 0) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_email),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            } else if (!WebServiceHelper.isEmailValid(emailEd.getText().toString().trim()) || validEmailString.contains(emailEd.getText().toString().charAt(0) + "")) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.enter_client_email_format),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                return;

            }
//            else if (validEmailString.contains(emailEd.getText().toString().charAt(0) + "")) {
//                Toast toast = Toast.makeText(getActivity(),
//                        getString(R.string.enter_client_valid_email_format),
//                        Toast.LENGTH_SHORT);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//                return;
//            }

        }
//		else if (product.getText().toString().length()<=0) {
//			Toast toast = Toast.makeText(getActivity(),
//					getString(R.string.enter_product),
//					Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//
//			return;
//
//		}else if (MeetingNote.getText().toString().length()<=0) {
//			Toast toast = Toast.makeText(getActivity(),
//					getString(R.string.enter_meeting_notes),
//					Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//
//			return;
//
//		}
//		else if (NextSteps.getText().toString().length()<=0) {
//			Toast toast = Toast.makeText(getActivity(),
//					getString(R.string.slect_next_steps),
//					Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//
//			return;
//
//		}else if (Details.getText().toString().length()<=0) {
//			Toast toast = Toast.makeText(getActivity(),
//					getString(R.string.enter_details),
//					Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//
//			return;
//
//		}
       /* else if (selectCategoryEd.getText().toString().length() <= 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.err_select_cat),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;

        }
        else if (TaskNameEd.getText().toString().length() <= 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.err_task_name),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;

        } else if (selectTypeEd.getText().toString().length() <= 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.err_select_type),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;

        } else if (AdditionalInstructionEd.getText().toString().length() <= 0) {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.err_additional_information),
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;

        } */

        ArrayList<SalesTask> arrayList = new ArrayList();

        SalesTask model = new SalesTask();

        model.emp_id = ConstantFunction.getstatus(getActivity(), "UserId");

        model.task_id_Tx = usertx;

        model.client_id = client_id + "";
        model.supervisor_id = ConstantFunction.getstatus(getActivity(), "supervisor_id");

        model.company_id = ConstantFunction.getstatus(getActivity(), "companyid");

        if (client_id.length() > 0) {
            model.SALES_client_name = clientNameEd.getText().toString();
        } else {
            model.SALES_client_name_new = clientNameEd.getText().toString();
        }

        model.SALES_client_address = clientAddress.getText().toString();
        model.SALES_contact_person = contactPerson.getText().toString();
        model.tel_number = TelPhone.getText().toString();
        model.client_email = emailEd.getText().toString();
        model.category_id = category_Id + "";

        for (int i = 0; i < categoryData.size(); i++) {
            if (category_Id.equals(categoryData.get(i).categoryId)) {
                model.category_Name = categoryData.get(i).categoryName + "";
            }
        }

        model.task_name = TaskNameEd.getText().toString();
        model.tasktype = "1";
        model.additional_instruction = AdditionalInstructionEd.getText().toString();

        model.latitude = ConstantValues.latitude + "";
        model.longtitude = ConstantValues.longitude + "";


//				obj.longtitude=ConstantValues.longitude+"";

        arrayList.add(model);

//            Bundle bundle = new Bundle();
//            bundle.putParcelableArrayList("arr", (ArrayList<? extends Parcelable>) arrayList);
//            new Sales_page_create_Fragment().setArguments(bundle);

        //   mActivity.pushFragments(ConstantValues.TAB_DASHBOARD, new Sales_page_start_job_fragment(), true, true);

//            FragmentManager manager =getActivity(). getSupportFragmentManager();
//            FragmentTransaction ft = manager.beginTransaction();
//            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
//            ft.replace(R.id.realtabcontent, new Sales_page_start_job_fragment(arrayList));
//            ft.commit();


        ConstantFunction.savestatus(getActivity(),
                "is_sales_task", "1");

        ConstantFunction.savestatus(getActivity(),
                "sales_task_id", model.task_id_Tx + "");

        sqlcon.open();
        arrSalesTask = sqlcon.GetAddTaskDetail(ConstantFunction.getstatus(getActivity(), "UserId"));

        //store data already filled
        if (arrSalesTask.size() > 0 && arrSalesTask != null) {

            if (arrSalesTask.get(0).task_id_Tx != null && model.task_id_Tx != null) {
                if (arrSalesTask.get(0).task_id_Tx == model.task_id_Tx) {
                    sqlcon.open();
                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_NAME, model.SALES_client_name, model.task_id_Tx);
                    sqlcon.open();
                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_ADDRESS, model.SALES_client_address, model.task_id_Tx);
                    sqlcon.open();
                    sqlcon.UpdateSalesData(DbHelper.SALES_CONTACT_NAME, model.SALES_contact_person, model.task_id_Tx);
                    sqlcon.open();
                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_PHONE, model.tel_number, model.task_id_Tx);
                    sqlcon.open();
                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_EMAIL, model.client_email, model.task_id_Tx);
//                    sqlcon.open();
//                    sqlcon.UpdateSalesData(DbHelper.SALES_CLIENT_TASK_ID_TX, model.task_id_Tx, model.task_id_Tx);
                }
            }
        } else {
            sqlcon.open();
            sqlcon.insertAddSalesTask(model);
        }


        sendStatusforTask(getStartTime());

        Sales_page_start_job_fragment detailFragment = new Sales_page_start_job_fragment(arrayList);
        // detailFragment.setTaskId(pendingTask.getTaskId());
        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                detailFragment, true, true);

        //insert add task detail in database



        /*if (count > 0) {
        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.please_add_photo), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

            return;
        }*/

        //


//		if (internetConnection
//				.isConnectingToInternet("")) {
//
//			sendDataSales();
//		}
//		else
//		{
//			sqlcon.open();
//			SalesTask obj=new SalesTask();
//
//			if (clientNameEd.getText().toString().equalsIgnoreCase("Add New")) {
//
//				obj.SALES_client_name_new="0";
//
//
//				obj.SALES_client_name=clientNameNew.getText().toString() + "";
//				obj.SALES_client_address=clientAddress.getText().toString() + "";
//				obj.SALES_contact_person= contactPerson.getText().toString() + "";
//				obj.tel_number= TelPhone.getText().toString() + "";
//				obj.client_email= emailEd.getText().toString() + "";
//				obj.latitude = ConstantValues.latitude+"";
//				obj.longtitude=ConstantValues.longitude+"";
//
//
//			} else {
//				obj.SALES_client_name_new="1";
//				obj.client_id= client_id + "";
//
//			}
//
//			obj.category_id=category_Id + "";
//
////			obj.next_steps= NextSteps.getText().toString() + "";
////			obj.notes=MeetingNote.getText().toString()+"";
////			obj.product= product.getText().toString()+"";
////			obj.detail=Details.getText().toString()+"";
//			obj.department=ConstantFunction.getstatus(getActivity(), "department");
//			obj.emp_id= ConstantFunction.getstatus(getActivity(), "UserId");
//
//			obj.company_id= ConstantFunction.getstatus(getActivity(),"companyid");
//			obj.category_id=category_Id+"";
//			obj.task_name= TaskNameEd.getText().toString()+"";
//			obj.additional_instruction=AdditionalInstructionEd.getText().toString()+"";
//			obj.tasktype=task_type+"";
//
////			String encodedString = "";
////
////			String userId=ConstantFunction.getstatus(getActivity(), "UserId");
////			CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));
////
////			for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
////				String strImage = GetStringFromBytes(CustomListViewValuesArr
////						.get(i).getTakenPhoto());
////				encodedString += new StringBuilder(String.valueOf(strImage))
////						.append(",").toString();
////			}
////
////			requestObject.put("dept_imagecount", CustomListViewValuesArr.size());
////			requestObject.put("dept_images",encodedString);
//
//			sqlcon.addSalesTask(obj);
//
//			Toast toast = Toast.makeText(getActivity(),
//					getString(R.string.suc_uploaded),
//					Toast.LENGTH_SHORT);
//
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//
//			mActivity.ClearAllPages();
//			// mActivity.popFragments();
//			mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
//					new AppTab_Dashboard_Home(), true, true);
//
//
//		}

        //if task is started or busy

    }

    public String getStartTime() {
        cStart = Calendar.getInstance();
        String taskStartTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
                Locale.getDefault());
        taskStartTime = sdf.format(cStart.getTime());
        return taskStartTime;
    }


    private void sendStatusforTask(String date) {


        SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        SharedPreferences.Editor editor1 = prefs1.edit();
        editor1.putString("currentTime", date + "");
        editor1.commit();


        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("emp_work_status", "1");
            requestObject.put("start_date_time", date + "");

        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }
        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ListOfChangeStatus);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Sales_page_create_Fragment :- " + ConstantValues.ListOfChangeStatus + " -------------" + requestObject.toString());
        wsHelper.delegate = new AsyncResponseListener() {
            @Override
            public void processFinish(String requestURL, String response) {
                // TODO Auto-generated method stub

                mProgressHUD.dismiss();
                Log.e("Response==>", response);

                if (!response.equalsIgnoreCase("")) {
                    MowomLogFile.writeToLog("\n" + "Sales_page_create_Fragment :- " + ConstantValues.ListOfChangeStatus + " -------------" + response);
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {

                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
//                            Toast toast = Toast.makeText(getActivity(),
//                                    getString(R.string.please_try_again),
//                                    Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();

                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
//                            Toast toast = Toast.makeText(getActivity(),
//                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//                            toast.setGravity(Gravity.CENTER, 0, 0);
//                            toast.show();
                        }

                    } catch (Exception e) {
//                        Toast toast = Toast.makeText(getActivity(),
//                                getString(R.string.please_try_again),
//                                Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();

                    }
                } else {

                }

            }
        };
        wsHelper.execute();
    }

//	private void sendDataSales() {
//
//		JSONObject requestObject = new JSONObject();
//		try {
//
//			if (clientNameEd.getText().toString().equalsIgnoreCase("Add New")) {
//				requestObject.put("client_name", clientNameNew.getText().toString() + "");
//				requestObject.put("client_address", clientAddress.getText().toString() + "");
//				requestObject.put("contact_person", contactPerson.getText().toString() + "");
//				requestObject.put("tel_number", TelPhone.getText().toString() + "");
//				requestObject.put("client_email", emailEd.getText().toString() + "");
//				requestObject.put("latitude",ConstantValues.latitude+"");
//				requestObject.put("longtitude",ConstantValues.longitude+"");
//
//
//			} else {
//				requestObject.put("client_id", client_id + "");
//
//			}
//
//			requestObject.put("category_id", category_Id + "");
//
////			requestObject.put("next_steps", NextSteps.getText().toString() + "");
////			requestObject.put("notes",MeetingNote.getText().toString()+"");
////			requestObject.put("product", product.getText().toString()+"");
////			requestObject.put("detail",Details.getText().toString()+"");
//			requestObject.put("department",ConstantFunction.getstatus(getActivity(), "department"));
//			requestObject.put("emp_id", ConstantFunction.getstatus(getActivity(), "UserId"));
//
//			requestObject.put("company_id", ConstantFunction.getstatus(getActivity(),"companyid"));
//			requestObject.put("category_id",category_Id+"");
//			requestObject.put("task_name", TaskNameEd.getText().toString()+"");
//			requestObject.put("additional_instruction",AdditionalInstructionEd.getText().toString()+"");
//			requestObject.put("tasktype",task_type+"");
//
//
//
//
//			String encodedString = "";
//
//			String userId=ConstantFunction.getstatus(getActivity(), "UserId");
//			CustomListViewValuesArr = GetPhotoFromDb(ConstantFunction.getstatus(getActivity(), "UserId"));
//
//			for (int i = 0; i < CustomListViewValuesArr.size(); i++) {
//				String strImage = GetStringFromBytes(CustomListViewValuesArr
//						.get(i).getTakenPhoto());
//				encodedString += new StringBuilder(String.valueOf(strImage))
//						.append(",").toString();
//			}
//
//			requestObject.put("dept_imagecount", CustomListViewValuesArr.size());
//			requestObject.put("dept_images",encodedString);
//
//		} catch (JSONException e) {
//			Log.d("TTT","Exception while signing in"+ e.toString());
//			e.printStackTrace();
//		}
//
//		mProgressHUD = ProgressHUD.show(getActivity(),
//				getString(R.string.loading), true, false, this);
//
//		// Call web service
//		WebServiceHelper wsHelper = new WebServiceHelper(
//				ConstantValues.AddSalesTaskURL);
//		wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
//				"application/json");
//		wsHelper.setRequestBody(requestObject.toString());
//		wsHelper.delegate = this;
//		wsHelper.execute();
//
//	}


//	public String GetStringFromBytes(byte[] image) {
//		String byteSyting = "";
//		Bitmap bm = null;
//		bm = BitmapFactory.decodeByteArray(image, 0, image.length);
//		byteSyting = ConstantFunction.BitMapToString(bm);
//		return byteSyting;
//	}

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        if (!requestURL.equalsIgnoreCase(ConstantValues.ListOfClient))
            MowomLogFile.writeToLog("\n" + "Sales_page_create_Fragment :- " + ConstantValues.ListOfClient + " -------------" + response);
        if (!requestURL.equalsIgnoreCase(ConstantValues.ListOfCCategory))
            MowomLogFile.writeToLog("\n" + "Sales_page_create_Fragment :- " + ConstantValues.ListOfCCategory + " -------------" + response);

        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (status.equalsIgnoreCase("success")) {
                    try {

                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.suc_uploaded),
                                Toast.LENGTH_SHORT);

                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();

                        mActivity.ClearAllPages();
                        // mActivity.popFragments();
                        mActivity.pushFragments(ConstantValues.TAB_DASHBOARD,
                                new AppTab_Dashboard_Home(), true, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (status.equalsIgnoreCase("failed")) {
                    Toast toast = Toast.makeText(getActivity(),
                            getString(R.string.please_try_again),
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();

                }
            } catch (Exception e) {
                Toast toast = Toast.makeText(getActivity(),
                        getString(R.string.please_try_again),
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }
        } else {

        }

    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }
//
//
//	private static String[] engNumbers = new String[]{ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//	public static String toEngNumber(String text) {
//		if (text.length() == 0) {
//			return "";
//		}
//		StringBuilder out = new StringBuilder();
//		int length = text.length();
//		for (int i = 0; i < length; i++) {
//			String c = text.charAt(i)+"";
//
//			if (c.equals("۰")) {
//				int number = 0;
//				out.append(engNumbers[number]);
//			} else if (c.equals("۱")) {
//				int number = 1;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۲")) {
//				int number = 2;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۳")) {
//				int number = 3;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۴")) {
//				int number = 4;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۵")) {
//				int number = 5;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۶")) {
//				int number = 6;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۷")) {
//				int number = 7;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۸")) {
//				int number = 8;
//				out.append(engNumbers[number]);
//			}  else if (c.equals("۹")) {
//				int number = 9;
//				out.append(engNumbers[number]);
//			} else {
//				out.append(c);
//			}
//
//
//		}
//		return out.toString()+"";
//	}
//
//	private void ShowToast() {
//		Toast toast = Toast.makeText(getActivity(),
//				getString(R.string.please_check_date), Toast.LENGTH_SHORT);
//		toast.setGravity(Gravity.CENTER, 0, 0);
//		toast.show();
//	}
//
//	private void ShowPopUp() {
//
//		FragmentManager fragManager = getActivity().getSupportFragmentManager();
//		FragmentTransaction fragTransaction = fragManager.beginTransaction();
//		Status_Update_Dialog_Activity dialog = new Status_Update_Dialog_Activity();
//
//		dialog.setCancelable(true);
//		dialog.show(fragTransaction, "Dialog");
//
//		// Show popup
//		// GetService(ConstantValues.RejectTaskURL);
//	}


    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(getActivity(),
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);

                    Picasso.with(getActivity())
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }

        }
    }
}

