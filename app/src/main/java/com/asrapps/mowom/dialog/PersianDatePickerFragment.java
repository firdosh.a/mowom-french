package com.asrapps.mowom.dialog;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PersianDatePickerFragment extends DialogFragment {

	OnDateSetListener ondateSet;

	public PersianDatePickerFragment() {
	}

	public void setCallBack(OnDateSetListener ondate) {
		ondateSet = ondate;
	}

	private int year, month, day;

	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
		year = args.getInt("year");
		month = args.getInt("month");
		day = args.getInt("day");
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
//		return new DatePickerDialog(getActivity(), ondateSet, year, month, day);

		DatePickerDialog dpd = new DatePickerDialog(getActivity(), ondateSet, year, month, day);

		// Create a TextView programmatically.
		TextView tv = new TextView(getActivity());

		// Create a TextView programmatically
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT, // Width of TextView
				RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
		tv.setLayoutParams(lp);
		tv.setPadding(10, 10, 10, 10);
		tv.setGravity(Gravity.CENTER);
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);
		tv.setText("Select Date");
		tv.setTextColor(Color.parseColor("#ff0000"));
		tv.setBackgroundColor(Color.parseColor("#FFD2DAA7"));

		// Set the newly created TextView as a custom tile of DatePickerDialog
		//dpd.setCustomTitle(tv);

		// Or you can simply set a tile for DatePickerDialog
            /*
                setTitle(CharSequence title)
                    Set the title text for this dialog's window.
            */
		dpd.setTitle("Select Date"); // Uncomment this line to activate it

		// Return the DatePickerDialog
		return  dpd;
	}

}
