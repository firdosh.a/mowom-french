package com.asrapps.mowom.dialog;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.asrapps.mowom.R;
import com.asrapps.mowom.adapter.PhotosPopUpAdapter;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.model.PhotoModel;

import java.util.ArrayList;

public class PhotoPopUpDialog extends DialogFragment implements OnClickListener {
	private View view;
	private ViewPager photoPager;
	private ImageButton iBtnBack, iBtnNext;
	private ImageView ivClose;

	ArrayList<PhotoModel> jsonArr = new ArrayList<PhotoModel>();
	private int position;

	String selectedLanguage;

	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		getDialog().requestWindowFeature(STYLE_NO_TITLE);
		getDialog().getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));

		view = inflater.inflate(R.layout.dialog_photo_popup, container, false);

		photoPager = (ViewPager) view.findViewById(R.id.photoPager);

		iBtnBack = (ImageButton) view.findViewById(R.id.iBtnBack);
		iBtnNext = (ImageButton) view.findViewById(R.id.iBtnNext);

		ivClose = (ImageView) view.findViewById(R.id.ivClose);
		ivClose.setOnClickListener(this);

		selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

		if (selectedLanguage.equalsIgnoreCase("")){
			selectedLanguage="en";
		}

		if (selectedLanguage.equalsIgnoreCase("pr"))
		{
			RelativeLayout.LayoutParams params11 =(RelativeLayout.LayoutParams)ivClose.getLayoutParams();
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
				params11.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			}
			params11.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			ivClose.setLayoutParams(params11);
		}



		PhotosPopUpAdapter adapter = new PhotosPopUpAdapter(getActivity(),
				iBtnBack, iBtnNext, photoPager, jsonArr, position);
		photoPager.setAdapter(adapter);

		return view;
	}

	public void setPhotoData(ArrayList<PhotoModel> jsonArr, int position) {
		this.jsonArr = jsonArr;
		this.position = position;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivClose:
			getDialog().dismiss();
			break;

		default:
			break;
		}
	}
}