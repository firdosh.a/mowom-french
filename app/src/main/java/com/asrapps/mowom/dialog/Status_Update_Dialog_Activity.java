package com.asrapps.mowom.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.utils.MowomLogFile;

import org.json.JSONException;
import org.json.JSONObject;

public class Status_Update_Dialog_Activity extends DialogFragment implements
        OnClickListener, AsyncResponseListener, OnCancelListener,
        OnCheckedChangeListener {

    ImageView closeImage;
    private RadioGroup radioActive, radioInActive;
    private RadioButton radioStatusButton;
    Button dialogSubmitButton;
    View view;
    RadioButton activeBtn, absentBtn,
            sickBtn, holydayBtn;

    ConnectionDetector internetConnection;
    private ProgressHUD mProgressHUD;

    String active;


    String selectedLanguage;

    TextView rdactive, rdabsent, rdsick, rdholyday;

    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        view = inflater.inflate(R.layout.update_status_dialog_layout,
                container, false);


        rdactive = (TextView) view.findViewById(R.id.rdactive);
        rdabsent = (TextView) view.findViewById(R.id.rdabsent);
        rdsick = (TextView) view.findViewById(R.id.rdsick);
        rdholyday = (TextView) view.findViewById(R.id.rdholyday);


        closeImage = (ImageView) view.findViewById(R.id.closeImage);
        closeImage.setOnClickListener(this);

        radioActive = (RadioGroup) view.findViewById(R.id.radioActive);
        radioActive.setOnCheckedChangeListener(this);

        radioInActive = (RadioGroup) view.findViewById(R.id.radioInActive);
        radioInActive.setOnCheckedChangeListener(this);


        activeBtn = (RadioButton) view.findViewById(R.id.activeBtn);
        activeBtn.setOnClickListener(this);

        absentBtn = (RadioButton) view.findViewById(R.id.absentBtn);
        absentBtn.setOnClickListener(this);

        sickBtn = (RadioButton) view.findViewById(R.id.sickBtn);
        sickBtn.setOnClickListener(this);

        holydayBtn = (RadioButton) view.findViewById(R.id.holydayBtn);
        holydayBtn.setOnClickListener(this);

        dialogSubmitButton = (Button) view
                .findViewById(R.id.dialogSubmitButton);
        dialogSubmitButton.setOnClickListener(this);
        SetSelected();

        selectedLanguage = ConstantFunction.CheckLanguage(getActivity());

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {

            ((TextView) view.findViewById(R.id.textView3)).setGravity(Gravity.RIGHT);
            ((TextView) view.findViewById(R.id.textView4)).setGravity(Gravity.RIGHT);


            RelativeLayout.LayoutParams params11 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params11.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params11.addRule(RelativeLayout.CENTER_VERTICAL);
            activeBtn.setLayoutParams(params11);
            absentBtn.setLayoutParams(params11);
            sickBtn.setLayoutParams(params11);
            holydayBtn.setLayoutParams(params11);

            RelativeLayout.LayoutParams params111 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params111.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params111.addRule(RelativeLayout.CENTER_VERTICAL);
            params111.addRule(RelativeLayout.LEFT_OF, R.id.absentBtn);
            rdabsent.setLayoutParams(params111);
            rdabsent.setGravity(Gravity.RIGHT);

            RelativeLayout.LayoutParams params112 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params112.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params112.addRule(RelativeLayout.CENTER_VERTICAL);
            params112.addRule(RelativeLayout.LEFT_OF, R.id.sickBtn);
            rdsick.setLayoutParams(params112);
            rdsick.setGravity(Gravity.RIGHT);


            RelativeLayout.LayoutParams params113 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params113.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params113.addRule(RelativeLayout.CENTER_VERTICAL);
            params113.addRule(RelativeLayout.LEFT_OF, R.id.holydayBtn);
            rdholyday.setLayoutParams(params113);
            rdholyday.setGravity(Gravity.RIGHT);

            RelativeLayout.LayoutParams params114 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params114.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params114.addRule(RelativeLayout.CENTER_VERTICAL);
            params114.addRule(RelativeLayout.LEFT_OF, R.id.activeBtn);
            rdactive.setLayoutParams(params114);
            rdactive.setGravity(Gravity.RIGHT);


//			Drawable drawable = getResources().getDrawable(android.R.drawable.btn_radio);
//			drawable.setBounds(0, 0, 57, 72);
//
//			activeBtn.setCompoundDrawables(null, null, drawable, null);
//			activeBtn.setGravity(Gravity.CENTER_VERTICAL);
//			activeBtn.setButtonDrawable(android.R.color.transparent);
//			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//				//noinspection deprecation
//				activeBtn.setBackgroundDrawable(null);
//			} else {
//				activeBtn.setBackground(null);
//			}
//
//			absentBtn.setCompoundDrawables(null, null, drawable, null);
//			absentBtn.setGravity(Gravity.CENTER_VERTICAL);
//			absentBtn.setButtonDrawable(android.R.color.transparent);
//			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//				//noinspection deprecation
//				absentBtn.setBackgroundDrawable(null);
//			} else {
//				absentBtn.setBackground(null);
//			}
//
//
//			sickBtn.setCompoundDrawables(null, null, drawable, null);
//			sickBtn.setGravity(Gravity.CENTER_VERTICAL);
//			sickBtn.setButtonDrawable(android.R.color.transparent);
//			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//				//noinspection deprecation
//				sickBtn.setBackgroundDrawable(null);
//			} else {
//				sickBtn.setBackground(null);
//			}
//
//
//			holydayBtn.setCompoundDrawables(null, null, drawable, null);
//			holydayBtn.setGravity(Gravity.CENTER_VERTICAL);
//			holydayBtn.setButtonDrawable(android.R.color.transparent);
//			if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//				//noinspection deprecation
//				holydayBtn.setBackgroundDrawable(null);
//			} else {
//				holydayBtn.setBackground(null);
//			}


        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        // safety check
        if (getDialog() == null)
            return;

        int width = WindowManager.LayoutParams.MATCH_PARENT;
        int height = WindowManager.LayoutParams.MATCH_PARENT;

        getDialog().getWindow().setLayout(width, height);
    }

    //UpdateStatusURL
    private void UpdateStatus(String url, String reason, String active) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(getActivity(), "UserId");
            requestObject.put("emp_id", id);
            requestObject.put("reason", reason);
            requestObject.put("active", active);

        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(url);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Status_Update_Dialog_Activity :- " + ConstantValues.UpdateStatusURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // TODO Auto-generated method stub
        mProgressHUD = ProgressHUD.show(getActivity(),
                getString(R.string.loading), true, false, this);

        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {

        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
        Log.e("Response==>", response);
        if (!response.equalsIgnoreCase("")) {

            if (requestURL.equalsIgnoreCase(ConstantValues.RejectTaskURL)) {
                MowomLogFile.writeToLog("\n" + "Status_Update_Dialog_Activity :- " + ConstantValues.RejectTaskURL + " -------------" + response);
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject job = new JSONObject(response);
                        String status = job.getString("status");
                        String result_code = job.getString("code");

                        if (status.equalsIgnoreCase("success")) {
                            try {
                                getDialog().dismiss();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (result_code.equalsIgnoreCase("17")) {

                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(getString(R.string.code_17));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();

                            }
                        } else if (status.equalsIgnoreCase("failed")) {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again),
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } else if (result_code.equalsIgnoreCase("400")) {
                            if (status.equalsIgnoreCase("User does not Exist.")) {
                                try {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    alertDialogBuilder.setMessage(getString(R.string.err_400));
                                    alertDialogBuilder.setCancelable(false);

                                    alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int arg1) {
                                            dialog.dismiss();

                                            ConstantFunction.ChangeLang(getActivity().getApplicationContext(), "en");
                                            ConstantFunction.LoadLocale(getActivity());

                                            startActivity(new Intent(getActivity(), LoginActivity.class));

                                        }
                                    });

                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else if (result_code.equalsIgnoreCase("401")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                alertDialogBuilder.setMessage(status + "");
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast toast = Toast.makeText(getActivity(),
                                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }


                    } catch (Exception e) {
                        Toast toast = Toast.makeText(getActivity(),
                                getString(R.string.please_try_again),
                                Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }
                }
            }

        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        if (checkedId == R.id.activeBtn) {
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (checkedId == R.id.absentBtn) {
            if (activeBtn.isChecked())
                activeBtn.setChecked(false);
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (checkedId == R.id.sickBtn) {
            if (activeBtn.isChecked())
                activeBtn.setChecked(false);
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (checkedId == R.id.holydayBtn) {
            if (activeBtn.isChecked())
                activeBtn.setChecked(false);
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
        }

        switch (checkedId) {
            case R.id.activeBtn:

                radioInActive.clearCheck();
                break;

            case R.id.absentBtn:

                radioActive.clearCheck();
                break;

            case R.id.sickBtn:

                radioActive.clearCheck();
                break;

            case R.id.holydayBtn:

                radioActive.clearCheck();
                break;

            default:
                break;
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        if (v.getId() == R.id.activeBtn) {
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (v.getId() == R.id.absentBtn) {
            if (activeBtn.isChecked())
                activeBtn.setChecked(false);
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (v.getId() == R.id.sickBtn) {
            if (activeBtn.isChecked())
                activeBtn.setChecked(false);
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
            if (holydayBtn.isChecked())
                holydayBtn.setChecked(false);
        }

        if (v.getId() == R.id.holydayBtn) {
            if (activeBtn.isChecked())
                activeBtn.setChecked(false);
            if (sickBtn.isChecked())
                sickBtn.setChecked(false);
            if (absentBtn.isChecked())
                absentBtn.setChecked(false);
        }


        switch (v.getId()) {
            case R.id.closeImage:
                getDialog().dismiss();
                break;

            case R.id.dialogSubmitButton:

                internetConnection = new ConnectionDetector(getActivity());
                if (internetConnection
                        .isConnectingToInternet(getString(R.string.check_connection))) {

                    RadioSelected();
                }

                break;

		/*case R.id.activeBtn:
            System.out.println("......................activeBtn");
			//reasonTextBox.setEnabled(false);
			absentBtn.setSelected(false);
			sickBtn.setSelected(false);
			holydayBtn.setSelected(false);
			break;

		case R.id.absentBtn:
			System.out.println("......................absentBtn");
			//reasonTextBox.setEnabled(false);
			activeBtn.setSelected(false);
			break;

		case R.id.sickBtn:
			System.out.println("......................sickBtn");
			//reasonTextBox.setEnabled(false);
			activeBtn.setSelected(false);
			break;

		case R.id.holydayBtn:
			System.out.println("......................holydayBtn");
			//reasonTextBox.setEnabled(true);
			activeBtn.setSelected(false);
			break;*/


            default:
                break;
        }
    }

    private void RadioSelected() {
        int selectedActive = radioActive.getCheckedRadioButtonId();
        int selectedInactive = radioInActive.getCheckedRadioButtonId();


        int realCheck = selectedActive == -1 ? selectedInactive : selectedActive;

        String reason = "";
        if (realCheck != -1) {

            radioStatusButton = (RadioButton) view.findViewById(realCheck);
            reason = radioStatusButton.getText().toString();

            if (reason.equalsIgnoreCase(getString(R.string.active))) {
                active = "1";
                reason = "";
            } else {
                active = "2";
            }

            UpdateStatus(ConstantValues.UpdateStatusURL, reason, active);

            ConstantFunction.savestatus(getActivity(),
                    "userstatus", reason);
            getDialog().dismiss();

        } else {
            Toast toast = Toast.makeText(getActivity(),
                    getString(R.string.select_status), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    private void SetSelected() {
        String reason = ConstantFunction.getuser(getActivity(), "userstatus");
        if (reason.equalsIgnoreCase(""))
            activeBtn.setChecked(true);

        if (reason.equalsIgnoreCase(getString(R.string.absent)))
            absentBtn.setChecked(true);

        if (reason.equalsIgnoreCase(getString(R.string.sick)))
            sickBtn.setChecked(true);

        if (reason.equalsIgnoreCase(getString(R.string.holyday)))
            holydayBtn.setChecked(true);

    }


}
