package com.asrapps.mowom.view;

/**
 * Created by qtm-kalpesh on 17/9/16.
 */
import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Show autocomplete view for list of Business Details(Image, Business Name) in add new appointment Screen
 */
public class BusinessCustomAutoCompleteView extends AutoCompleteTextView {

    /**
     * @category Default Constructor
     */
    public BusinessCustomAutoCompleteView(Context context) {
        super(context);
    }

    /**
     * @category Parameterized Constructor
     */
    public BusinessCustomAutoCompleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BusinessCustomAutoCompleteView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * this is how to disable AutoCompleteTextView filter after filter
     */
    @Override
    protected void performFiltering(final CharSequence text, final int keyCode) {
        String filterText = "";
        super.performFiltering(filterText, keyCode);
    }
}
