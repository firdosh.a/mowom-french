package com.asrapps.mowom.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.asrapps.mowom.R;
import com.asrapps.mowom.dialog.PhotoPopUpDialog1;
import com.asrapps.mowom.model.PhotoSalesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HorizontalLayoutScrollListView2 extends LinearLayout {

	Context myContext;
	ArrayList<PhotoSalesModel> itemList = new ArrayList<PhotoSalesModel>();

	public HorizontalLayoutScrollListView2(Context context) {
		super(context);
		myContext = context;
	}

	public HorizontalLayoutScrollListView2(Context context, AttributeSet attrs) {
		super(context, attrs);
		myContext = context;
	}

	public HorizontalLayoutScrollListView2(Context context, AttributeSet attrs,
										   int defStyle) {
		super(context, attrs, defStyle);
		myContext = context;
	}
	
	public void removeAll(){
		removeAllViews();
	}
	
	public void RemoveItem(int index){
		removeViewAt(index);
	}
	
	

	public void add(PhotoSalesModel path, Fragment myActivity) {
		int newIdx = itemList.size();
		itemList.add(path);
		addView(getImageView(newIdx, myActivity));
	
	}
	
	public View AddView(final int i,Fragment myActivity){
		System.out.println("................................ I am Here 1.................");
		RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.image_viewlayout);
		LayoutInflater inflater = 
		              (LayoutInflater)myActivity.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		
		View menuLayout = inflater.inflate(R.layout.image_view_child, mainLayout, true);
		ImageView imageView = (ImageView)menuLayout.findViewById(R.id.taken_images);

		System.out.println("................................ I am Here 2.................");
		Bitmap bm = null;
		if (i < itemList.size()) {
			// bm = decodeSampledBitmapFromUri(itemList.get(i), 350, 220);
			bm = BitmapFactory.decodeByteArray(itemList.get(i).getTakenPhoto(),
					0, itemList.get(i).getTakenPhoto().length);
		}

		System.out.println("................................ I am Here 3.................");
		imageView.setLayoutParams(new LayoutParams(350, 220));
		imageView.setAdjustViewBounds(true);
		imageView.setPadding(5, 5, 5, 5);
		imageView.setImageBitmap(bm);
		

		System.out.println("................................ I am Here 4.................");
		ImageView closeImage = (ImageView)menuLayout.findViewById(R.id.closeButton);
		closeImage.setImageResource(R.drawable.dialog_close_icon);

		
		

		System.out.println("................................ I am Here 5.................");
		return menuLayout;
	}

	ImageView getImageView(final int i, final Fragment myActivity) {
		Bitmap bm = null;
		if (i < itemList.size()) {
			// bm = decodeSampledBitmapFromUri(itemList.get(i), 350, 220);
			bm = BitmapFactory.decodeByteArray(itemList.get(i).getTakenPhoto(),
					0, itemList.get(i).getTakenPhoto().length);



		}
		
		

		ImageView imageView = new ImageView(myContext);
		imageView.setLayoutParams(new LayoutParams(350, 220));
		imageView.setAdjustViewBounds(true);
		imageView.setPadding(5, 5, 5, 5);
		imageView.setImageBitmap(bm);
		
		
		/*
		 * imageView.setOnClickListener(new OnClickListener(){
		 * 
		 * @Override public void onClick(View v) { Toast.makeText(myContext,
		 * "Clicked - " + itemList.get(i).getPhotoId(),
		 * Toast.LENGTH_LONG).show();
		 * 
		 * 
		 * FragmentManager fragManager = myActivity.getChildFragmentManager();
		 * 
		 * PhotoPopUpDialog dialog = new PhotoPopUpDialog();
		 * dialog.setCancelable(true); dialog.setPhotoData(null, 1);
		 * dialog.show(fragManager, "PhotoDialog");
		 * 
		 * }});
		 */

		return imageView;
	}

	public void addFromService(PhotoSalesModel path, Fragment myActivity) {
		int newIdx = itemList.size();
		itemList.add(path);
		addView(getImageFromService(newIdx, myActivity));
	}

	ImageView getImageFromService(final int i, final Fragment myActivity) {

		ImageView imageView = new ImageView(myContext);
		imageView.setLayoutParams(new LayoutParams(350, 220));
		imageView.setAdjustViewBounds(true);
		imageView.setPadding(5, 5, 5, 5);

		Picasso.with(myActivity.getActivity()).load(itemList.get(i).getPhoto())
				.placeholder(R.drawable.loading)
				.error(R.drawable.icon_no_preview).into(imageView);

		// imageView.setImageBitmap(bm);

		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				FragmentManager fragManager = myActivity
						.getChildFragmentManager();

				PhotoPopUpDialog1 dialog = new PhotoPopUpDialog1();
				dialog.setCancelable(true);
				dialog.setPhotoData(itemList, i);
				dialog.show(fragManager, "PhotoDialog");

			}
		});

		return imageView;
	}

	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		bm = BitmapFactory.decodeFile(path, options);

		return bm;
	}

	public int calculateInSampleSize(

	BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}
		}

		return inSampleSize;
	}

}
