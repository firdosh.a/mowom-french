package com.asrapps.mowom.view;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Auto complete view for list of address in Business address
 *
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
	
		Context mContext;
	    public ArrayList<String> resultList;
	    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
		private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
		private static final String OUT_JSON = "/json";
		private static final String API_KEY = "AIzaSyDRWohd0Tg24-1feE01F_llbMc9QfXcSZw";

		/**
		 * @category Constructor
		 * @param context Activity Context
		 * @param textViewResourceId view resource id for List Item
		 *
		 */
	    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
	        super(context, textViewResourceId);
	        mContext=context;
	    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		TextView textView = (TextView) super.getView(position, convertView, parent);
		textView.setText(resultList.get(position).toString());

		return textView;
	}

	/**
	     * total count of items in listview
	     *
	     */
	    @Override
	    public int getCount() {
	    	try {
	    		return resultList.size();
			} catch (Exception e) {
				return 0;
			}
	        
	    }

	    /**
	     * fetch item with specific position from collection 
	     *
	     */
	    @Override
	    public String getItem(int index) {
	    	try {
	    		 return resultList.get(index);
			} catch (Exception e) {
				 return "";
			}
	       
	    }

	    /**
	     * Fiter list of items from collection based on user inputs address
	     *
	     */
	    @Override
	    public Filter getFilter() {
	        Filter filter = new Filter() {
	            @Override
	            protected FilterResults performFiltering(CharSequence constraint) {
	                FilterResults filterResults = new FilterResults();
	                if (constraint != null) {
	                    // Retrieve the autocomplete results.
	                    resultList = autocomplete(constraint.toString());

	                    // Assign the data to the FilterResults
	                    filterResults.values = resultList;
	                    filterResults.count = resultList.size();
	                }
	                return filterResults;
	            }

	            @Override
	            protected void publishResults(CharSequence constraint, FilterResults results) {
	                if (results != null && results.count > 0) {
	                    notifyDataSetChanged();
	                }
	                else {
	                    notifyDataSetInvalidated();
	                }
	            }};
	        return filter;
	    }
	    
	    /**
	     * prepare Autocomplete view based on user input address
	     *
	     */
	    private ArrayList<String> autocomplete(String input) {
		    ArrayList<String> resultList = null;

		    HttpURLConnection conn = null;
		    StringBuilder jsonResults = new StringBuilder();
		    try {
		        StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
		        sb.append("?key=" + API_KEY);
		        sb.append("&sensor=false");
		        sb.append("&input=" + URLEncoder.encode(input, "utf8"));

		        URL url = new URL(sb.toString());

				Log.v("TTT","sb = "+sb.toString());


		        conn = (HttpURLConnection) url.openConnection();
		        InputStreamReader in = new InputStreamReader(conn.getInputStream());

		        // Load the results into a StringBuilder
		        int read;
		        char[] buff = new char[1024];
		        while ((read = in.read(buff)) != -1) {
		            jsonResults.append(buff, 0, read);
		        }
		        Log.v("TTT","jsonResults = "+jsonResults);
		    } catch (MalformedURLException e) {
		        Log.e("TTT", "Error processing Places API URL", e);

		        return resultList;
		    } catch (IOException e) {
		        Log.e("TTT", "Error connecting to Places API", e);
		        
		        return resultList;
		    } finally {
		        if (conn != null) {
		            conn.disconnect();
		        }
		    }

		    try {
		        // Create a JSON object hierarchy from the results
		        JSONObject jsonObj = new JSONObject(jsonResults.toString());
		        JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

		        // Extract the Place descriptions from the results
		        resultList = new ArrayList<String>(predsJsonArray.length());
		        for (int i = 0; i < predsJsonArray.length(); i++) {
		            resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
		        }
		    } catch (JSONException e) {
		        Log.e("TTT", "Cannot process JSON results", e);
		        
		    }

		    return resultList;
		}
	}
