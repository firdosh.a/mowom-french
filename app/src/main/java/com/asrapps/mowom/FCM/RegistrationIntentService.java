package com.asrapps.mowom.FCM;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.asrapps.mowom.R;
import com.asrapps.mowom.base.AppMainTabActivity;
import com.asrapps.mowom.broadcost.AlarmNotiBroadcastReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;


/**
 * Created by qtm-kalpesh on 7/10/16.
 */
public class RegistrationIntentService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static final int MESSAGE_NOTIFICATION_ID = 435345;
    Context context;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.toString());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.toString());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getSound());
        }

//        Map<String, String> data = remoteMessage.getData();
//        String from = remoteMessage.getFrom();
//        Notification notification = remoteMessage.getNotification();
//        createNotification(notification);

        sendNotification(remoteMessage);

    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(RemoteMessage messageBody) {

        Context context = getBaseContext();

        Log.v("TTT", "notification message = " + messageBody.getNotification().getSound());
        Intent intent = new Intent(context, AppMainTabActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent,
                0);

        Uri defaultSoundUri = Uri.parse("android.resource://com.asrapps.mowom/" + R.raw.mowom_noti_sound);
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Notification notificationBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle("Mowom")
                    .setContentText(messageBody.getNotification().getBody())
                    .setSmallIcon(R.drawable.alert_logo_icon)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri)
                    .build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(Math.abs(new Random().nextInt()), notificationBuilder);
        } else {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.alert_logo_icon);
            Notification notificationBuilder = new NotificationCompat.Builder(context)
                    .setContentTitle("Mowom")
                    .setContentText(messageBody.getNotification().getBody())
                    .setSmallIcon(R.drawable.alert_logo_icon)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setLargeIcon(bm)
//                    .setSound(defaultSoundUri)
                    .build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(Math.abs(new Random().nextInt()), notificationBuilder);

//

        }

        Intent intentAlarm = new Intent(context, AlarmNotiBroadcastReceiver.class);
            PendingIntent pendingIntentAlarm = PendingIntent.getBroadcast(
                    context, Math.abs(new Random().nextInt()), intentAlarm, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                    +(2000), pendingIntentAlarm);

    }

    // Creates notification based on title and body received
    private void createNotification(Notification notification) {

//        Intent intent = new Intent(this, LoginActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setContentTitle(notification.getTitle())
//                .setContentText(notification.toString())
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(context);
//        NotificationManager mNotificationManager = (NotificationManager) context
//                .getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());
    }

}