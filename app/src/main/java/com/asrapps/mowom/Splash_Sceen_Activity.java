package com.asrapps.mowom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.asrapps.mowom.base.AppMainTabActivity;
import com.asrapps.mowom.broadcost.MowomService;
import com.asrapps.mowom.broadcost.UpdaterService;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.SessionManager;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.gson.Gson;

import org.json.JSONObject;

//import com.parse.ParseAnalytics;
//import com.parse.ParseInstallation;
//import com.parse.ParseUser;

public class Splash_Sceen_Activity extends Activity implements
        AsyncResponseListener, OnCancelListener, ConnectionCallbacks, OnConnectionFailedListener {

    ConnectionDetector internetConnection;

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        ConstantFunction.LoadLocale(this);
        internetConnection = new ConnectionDetector(this);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        UpdaterService.inProcess = preferences.getBoolean("inProcess", false);
        UpdaterService.firstTime = preferences.getBoolean("firstTime", false);
        UpdaterService.currentTime = preferences.getString("currentTime", "");
        Log.d("UpdaterService", "splash UpdaterService Created currentTime " + UpdaterService.currentTime + " inProcess = " + UpdaterService.inProcess);

        LocationManager manager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps(this);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (internetConnection.isConnectingToInternet(""))
//                        if (internetConnection.isConnectingToInternet(getString(R.string.check_connection)))
                        CheckSession();
                    else {
                        if (SessionManager.getLoginDetail(Splash_Sceen_Activity.this) != null) {
                            Gson gson = new Gson();
                            Login loginData = gson.fromJson(SessionManager.getLoginDetail(Splash_Sceen_Activity.this), Login.class);

                            if (loginData != null) {
//                                startActivity(new Intent(Splash_Sceen_Activity.this, LogoScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                startActivity(new Intent(Splash_Sceen_Activity.this, AppMainTabActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                overridePendingTransition(0, 0);
                                finish();
                            }
                        } else {
                            ConstantFunction.ChangeLang(getApplicationContext(), "en");
                            ConstantFunction.LoadLocale(Splash_Sceen_Activity.this);

                            startActivity(new Intent(Splash_Sceen_Activity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        }

                    }

                }
            }, ConstantValues.SPLASH_TIME_OUT);
        }
    }

    // Start the service
    public void startNewService() {

//		if(ConstantFunction.getstatus(this, "privacy").equals("false")){
//			startService(new Intent(this, MowomService.class));
//		}
    }

    // Stop the service
    public void stopNewService() {

        stopService(new Intent(this, MowomService.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_layout);

		/*buildGoogleApiClient();
        if (checkPlayServices()) {
			// Building the GoogleApi client				
			test();
		}*/

    }


    private void CheckSession() {
        String id = ConstantFunction.getuser(this, "UserId");
        if (!id.equalsIgnoreCase("")) {

//			ParseAnalytics.trackAppOpenedInBackground(getIntent());
//			ParseInstallation.getCurrentInstallation().saveInBackground();
//			if (ParseUser.getCurrentUser() == null) {
//	            ParseUser.enableAutomaticUser();
//	     }


            Intent intent = new Intent(Splash_Sceen_Activity.this,
                    AppMainTabActivity.class);
//            Intent intent = new Intent(Splash_Sceen_Activity.this,
//                    LogoScreenActivity.class);
            startActivity(intent);
            startNewService();
            finish();
        } else {

            ConstantFunction.ChangeLang(getApplicationContext(), "en");
            ConstantFunction.LoadLocale(Splash_Sceen_Activity.this);


            Intent intent = new Intent(Splash_Sceen_Activity.this,
                    LoginActivity.class);
            startActivity(intent);
            finish();
        }
        // requestObject.put("id", id);
    }

    public void buildAlertMessageNoGps(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(mActivity.getString(R.string.gps_enable))
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                mActivity
                                        .startActivity(new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                // turnGPSOn();
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                dialog.cancel();
                                mActivity.finish();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        // mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        // TODO Auto-generated method stub

        // mProgressHUD.dismiss();
        Log.e("Response==>", response);

        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");

                if (status.equalsIgnoreCase("success")) {
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // GoDashBoard();

                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Login success", false);
                } else {
                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Invalid credentials", false);
                }
            } catch (Exception e) {
                // ConstantFunction.showAlertDialog(LoginActivity.this,
                // "Try again", false);
            }
        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }

    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub

    }
}
