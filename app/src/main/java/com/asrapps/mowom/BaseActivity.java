package com.asrapps.mowom;

import android.app.Activity;
import android.os.Bundle;

import com.asrapps.mowom.constants.SessionManager;

/**
 * Created by richa on 28/10/16.
 */

public class BaseActivity extends Activity {

    public SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(this);

    }
}
