package com.asrapps.mowom.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.model.Report;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Report_Page_Adapter extends BaseAdapter {

    Context context;
    Activity activity;
    Report report = null;
    private List<Report> reportList;
    String selectedLanguage = "";

    public Report_Page_Adapter(Activity activity,
                               List<Report> strApiResult, String selectedLanguage) {
        this.activity = activity;
        this.context = activity;
        this.reportList = strApiResult;
        this.selectedLanguage = selectedLanguage;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return reportList.size();
    }

    @Override
    public Object getItem(int position) {
        return reportList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        ViewHolder holder;

        if (view == null) {
            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("en") || selectedLanguage.equalsIgnoreCase("fr")) {
                view = LayoutInflater.from(context).inflate(
                        R.layout.report_layout_child_view, parent, false);
            } else {

                view = LayoutInflater.from(context).inflate(
                        R.layout.report_layout_child_view_persian, parent, false);
            }

            holder = new ViewHolder();
            //,,,,,,
            holder.report_date_time = (TextView) view
                    .findViewById(R.id.report_date_time);
            holder.report_task_id = (TextView) view
                    .findViewById(R.id.report_task_id);
            holder.report_client_name = (TextView) view
                    .findViewById(R.id.report_client_name);
            holder.report_time_spend = (TextView) view
                    .findViewById(R.id.report_time_spend);


            holder.report_parent_layout = (LinearLayout) view
                    .findViewById(R.id.report_parent_layout);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {
            selectedLanguage = ConstantFunction.CheckLanguage(activity);

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }
            report = reportList.get(position);
            if (position % 2 == 0)
                holder.report_parent_layout.setBackgroundColor(Color
                        .parseColor("#EDEDED"));
            else
                holder.report_parent_layout.setBackgroundColor(Color
                        .parseColor("#F9F9F9"));
//            holder.report_date_time.setText(report.getDateTime());

            try {
                String task_list_date_time = ConstantFunction.GetDateFormat1(report.getDateTime());

                if (selectedLanguage.equalsIgnoreCase("pr")) {
                    StringBuilder t = new StringBuilder();
                    try {
//                    2016-08-21 12:53:09
                        task_list_date_time=WebServiceHelper.convert12to24(task_list_date_time);

                        String st[] = task_list_date_time.split(" ");

                        String stTime[] = st[1].split(":");
                        String dtTime[] = st[0].split("/");

                        String selectedLanguage;
                        selectedLanguage = ConstantFunction.CheckLanguage(activity);

                        if (selectedLanguage.equalsIgnoreCase("")) {
                            selectedLanguage = "en";
                        }

                        if (selectedLanguage.equalsIgnoreCase("pr")) {
                            st[0]=st[0].replace(",","");
                            String dtStart = st[0];

                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            try {
                                Date date = format.parse(dtStart);
                                System.out.println(date);

                                SolarCalendar sc = new SolarCalendar(date);
                                String s= sc.date  + "/" +
                                        sc.month + "/" + sc.year;


                                Log.v("TTT","ssssss = "+s);

                                t.append(toPersianNumber(s)).append(" ");
                                t.append(toPersianNumber(stTime[0]));
                                t.append(":");
                                t.append(toPersianNumber(stTime[1]));

                            } catch (ParseException e) {
                                // TODO Auto-generated catch block

                                t.append(toPersianNumber(st[0])).append(" ");
                                t.append(toPersianNumber(stTime[0]));
                                t.append(":");
                                t.append(toPersianNumber(stTime[1]));

                                e.printStackTrace();
                            }


                        } else {
                            t.append(st[0]).append(" ");
                            t.append(stTime[0]);
                            t.append(":");
                            t.append(stTime[1]);

                        }

//                        try {
//                            if (st[2].equalsIgnoreCase("am")) {
//                                t.append(" " + activity.getResources().getString(R.string.am));
//                            } else {
//                                t.append(" " + activity.getResources().getString(R.string.pm));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }


                        task_list_date_time = t.toString();

                        holder.report_date_time.setText(task_list_date_time);

                    } catch (Exception e) {
                        holder.report_date_time.setText(task_list_date_time);
                        e.printStackTrace();
                    }
//                    holder.report_date_time.setText(toPersianNumber(WebServiceHelper.convert12to24(task_list_date_time)));


                } else {
//                    StringBuilder t = new StringBuilder();
//                    try {
////                    2016-08-21 12:53:09
//                        String st[] = task_list_date_time.split(" ");
//
//                        String stTime[] = st[1].split(":");
//                        String dtTime[] = st[0].split("/");
//
//                            st[0]=st[0].replace(",","");
//                            String dtStart = st[0];
//
//                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                            try {
//                                Date date = format.parse(dtStart);
//                                System.out.println(date);
//
//                                SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                                String s = dateformat.format(date);
//                                System.out.println("Current Date Time : " + s);
//
//                                Log.v("TTT","ssssss = "+s);
//
//                                t.append(s).append(" ");
//                                t.append(stTime[0]);
//                                t.append(":");
//                                t.append(stTime[1]);
//
//                            } catch (ParseException e) {
//                                // TODO Auto-generated catch block
//
//                                t.append(st[0]).append(" ");
//                                t.append(stTime[0]);
//                                t.append(":");
//                                t.append(stTime[1]);
//
//                                e.printStackTrace();
//                            }
//
//
//
//                        try {
//                            if (st[2].equalsIgnoreCase("am")) {
//                                t.append(" " + activity.getResources().getString(R.string.am));
//                            } else {
//                                t.append(" " + activity.getResources().getString(R.string.pm));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//
//                        task_list_date_time = t.toString();
//
//                        holder.report_date_time.setText(task_list_date_time);
//
//                    } catch (Exception e) {
//                        holder.report_date_time.setText(task_list_date_time);
//                        e.printStackTrace();
//                    }

                    holder.report_date_time.setText(WebServiceHelper.convert12to24(task_list_date_time));
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();

            }


            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.report_task_id.setText(toPersianNumber(report.getJobId())+"#");
            } else {
                holder.report_task_id.setText("#"+report.getJobId());
            }

            holder.report_client_name.setText(report
                    .getClientName().toUpperCase(Locale.getDefault()));


            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.report_time_spend.setText(toPersianNumber(report.getTimeSpend()));
            } else {
                holder.report_time_spend.setText(report.getTimeSpend());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private class ViewHolder {
        TextView report_date_time, report_task_id, report_client_name, report_time_spend;

        LinearLayout report_parent_layout;
    }
}