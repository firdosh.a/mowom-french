package com.asrapps.mowom.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.MyLeadingMarginSpan2;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.OnDoneClick;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Completed_Task_List_Adapter extends BaseAdapter {

    Context context;
    Activity activity;
    PendingTask completedTask = null;
    OnDoneClick mListener;
    private List<PendingTask> pentingTaskList;
    int tempPosition;
    String selectedLanguage;
    int id;

    public Completed_Task_List_Adapter(Activity activity,
                                       List<PendingTask> strApiResult, OnDoneClick listener) {
        this.activity = activity;
        this.context = activity;
        this.pentingTaskList = strApiResult;
        mListener = listener;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return pentingTaskList.size();
    }

    @Override
    public Object getItem(int position) {
        return pentingTaskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        ViewHolder holder;

//		if (view == null) {

        selectedLanguage = ConstantFunction.CheckLanguage(activity);

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            view = LayoutInflater.from(context).inflate(
                    R.layout.completed_task_list_child_item_pr, parent, false);
        } else {
            view = LayoutInflater.from(context).inflate(
                    R.layout.completed_task_list_child_item, parent, false);
        }

        holder = new ViewHolder();

        holder.llRate = (LinearLayout) view.findViewById(R.id.llRate);

        holder.idLayout1 = (LinearLayout) view.findViewById(R.id.idLayout1);
        holder.label_comma = (TextView) view
                .findViewById(R.id.label_comma);

        holder.overlapping_issue_all = (TextView) view.findViewById(R.id.overlapping_issue_all);

        holder.llLeadMargin = (LinearLayout) view.findViewById(R.id.llLeadMargin);

        holder.lable_id = (TextView) view.findViewById(R.id.lable_id);
        holder.completed_parent_layout = (RelativeLayout) view
                .findViewById(R.id.completed_parent_layout);
        holder.completed_task_list_id = (TextView) view
                .findViewById(R.id.completed_task_list_id);
        holder.completed_task_list_issue = (TextView) view
                .findViewById(R.id.completed_task_list_issue);
        holder.completed_task_list_date_time = (TextView) view
                .findViewById(R.id.completed_task_list_date_time);
        holder.completed_task_list_phone = (TextView) view
                .findViewById(R.id.completed_task_list_phone);
        holder.completed_task_list_address = (TextView) view
                .findViewById(R.id.completed_task_list_address);
        holder.completed_task_org_name = (TextView) view
                .findViewById(R.id.completed_task_org_name);
        holder.ratingValue = (TextView) view.findViewById(R.id.ratingValue);
        holder.star_image_1 = (ImageView) view
                .findViewById(R.id.star_image_1);
        holder.star_image_2 = (ImageView) view
                .findViewById(R.id.star_image_2);
        holder.star_image_3 = (ImageView) view
                .findViewById(R.id.star_image_3);
        holder.star_image_4 = (ImageView) view
                .findViewById(R.id.star_image_4);
        holder.star_image_5 = (ImageView) view
                .findViewById(R.id.star_image_5);

//			view.setTag(holder);
//		} else {
//			holder = (ViewHolder) view.getTag();
//		}

        try {
            tempPosition = position;
            completedTask = pentingTaskList.get(position);
            if (position % 2 == 0)
                holder.completed_parent_layout.setBackgroundColor(Color
                        .parseColor("#FCFFF4"));//#FCFFF4
            else
                holder.completed_parent_layout.setBackgroundColor(Color
                        .parseColor("#F4F7EB"));//#F4F7EB

            if (completedTask.getTasktype().equalsIgnoreCase("2"))
                holder.completed_parent_layout.setBackgroundColor(Color
                        .parseColor("#F9EDFF"));//#F9EDFF

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.completed_task_list_id.setText(toPersianNumber(completedTask.getTaskId()));

                Drawable DICON = view.getResources().getDrawable(R.drawable.id_icon);
                int leftMargin = DICON.getIntrinsicWidth() + holder.lable_id.getText().length() + id + 80;
//            Set the icon in R.id.icon
                SpannableString issueString = new SpannableString(completedTask.getIssue().toUpperCase(Locale.getDefault()));
//            Expose the indent for the first rows
                issueString.setSpan(new MyLeadingMarginSpan2(1, 0), 0, issueString.length(), 0);

                holder.completed_task_list_issue.setText(issueString);

                holder.overlapping_issue_all.setText(activity.getResources().getString(R.string.id) + " " + toPersianNumber(completedTask.getTaskId()) + " " + activity.getResources().getString(R.string.comma) + " " + completedTask.getIssue().toUpperCase(Locale.getDefault()));

            } else {
                holder.completed_task_list_id.setText(completedTask.getTaskId());

                SpannableString issueString = new SpannableString(completedTask.getIssue().toUpperCase(Locale.getDefault()));
                holder.completed_task_list_issue.setText(issueString);
                holder.overlapping_issue_all.setText(activity.getResources().getString(R.string.id) + " " + completedTask.getTaskId() + " " + activity.getResources().getString(R.string.comma) + " " + completedTask.getIssue().toUpperCase(Locale.getDefault()));

            }

            String task_list_date_time = completedTask
                    .getDate_time();
            if (selectedLanguage.equalsIgnoreCase("pr")) {

                task_list_date_time = WebServiceHelper.convert12to24(task_list_date_time);

                StringBuilder t = new StringBuilder();
                try {

                    String st[] = task_list_date_time.split(" ");

                    String stTime[] = st[1].split(":");
                    String dtTime[] = st[0].split("/");

                    String selectedLanguage;
                    selectedLanguage = ConstantFunction.CheckLanguage(activity);

                    if (selectedLanguage.equalsIgnoreCase("")) {
                        selectedLanguage = "en";
                    }

                    if (selectedLanguage.equalsIgnoreCase("pr")) {

                        st[0] = st[0].replace(",", "");
                        String dtStart = st[0];
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = format.parse(dtStart);
                            System.out.println(date);

                            SolarCalendar sc = new SolarCalendar(date);
                            String s = sc.date + "/" +
                                    sc.month + "/" + sc.year;

                            Log.v("TTT", "ssssss = " + s);

                            t.append(toPersianNumber(s)).append(" ");
                            t.append(toPersianNumber(stTime[0]));
                            t.append(":");
                            t.append(toPersianNumber(stTime[1]));

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block

                            t.append(toPersianNumber(st[0])).append(" ");
                            t.append(toPersianNumber(stTime[0]));
                            t.append(":");
                            t.append(toPersianNumber(stTime[1]));
                            e.printStackTrace();
                        }


                    } else {
                        t.append(st[0]).append(" ");
                        t.append(stTime[0]);
                        t.append(":");
                        t.append(stTime[1]);

                    }


//					try {
//						if (st[2].equalsIgnoreCase("am")) {
//							t.append(" " + activity.getResources().getString(R.string.am));
//						} else {
//							t.append(" " + activity.getResources().getString(R.string.pm));
//						}
//					}
//					catch (Exception e)
//					{
//						e.printStackTrace();
//					}


                    task_list_date_time = t.toString();
                } catch (Exception e) {


                }

                holder.completed_task_list_date_time.setText(toPersianNumber(task_list_date_time));

            } else {
//				StringBuilder t = new StringBuilder();
//				try {
//
//					String st[] = task_list_date_time.split(" ");
//
//					String stTime[] = st[1].split(":");
//					String dtTime[] = st[0].split("/");
//
//					String selectedLanguage;
//					selectedLanguage = ConstantFunction.CheckLanguage(activity);
//
//					if (selectedLanguage.equalsIgnoreCase("")) {
//						selectedLanguage = "en";
//					}
//
//
//						st[0]=st[0].replace(",","");
//						String dtStart = st[0];
//						SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//						try {
//							Date date = format.parse(dtStart);
//							System.out.println(date);
//
//							SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//							String s = dateformat.format(date);
//							System.out.println("Current Date Time : " + s);
//
//							Log.v("TTT","ssssss = "+s);
//
//							t.append(s).append(" ");
//							t.append(stTime[0]);
//							t.append(":");
//							t.append(stTime[1]);
//
//						} catch (ParseException e) {
//							// TODO Auto-generated catch block
//
//							t.append(st[0]).append(" ");
//							t.append(stTime[0]);
//							t.append(":");
//							t.append(stTime[1]);
//							e.printStackTrace();
//						}
//
//
//
//
//					try {
//						if (st[2].equalsIgnoreCase("am")) {
//							t.append(" " + activity.getResources().getString(R.string.am));
//						} else {
//							t.append(" " + activity.getResources().getString(R.string.pm));
//						}
//					}
//					catch (Exception e)
//					{
//						e.printStackTrace();
//					}
//
//
//					task_list_date_time = t.toString();
//				} catch (Exception e) {
//
//
//				}

                holder.completed_task_list_date_time.setText(WebServiceHelper.convert12to24(task_list_date_time));

            }

            String selectedLanguage;
            selectedLanguage = ConstantFunction.CheckLanguage(activity);

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.completed_task_list_phone.setText(toPersianNumber(completedTask.getPhone()));
            } else {
                holder.completed_task_list_phone.setText(completedTask.getPhone());
            }


            if(completedTask
                    .getClientAddress()!=null && completedTask
                    .getClientAddress().size()>0 && completedTask
                    .getClientAddress().get(0).location!=null) {
                holder.completed_task_list_address.setText(completedTask
                        .getClientAddress().get(0).location);
            }
            else
            {
                holder.completed_task_list_address.setText("");
            }
            holder.completed_task_org_name.setText(completedTask.getOrg_name().toUpperCase(Locale.getDefault()));

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.ratingValue.setText(toPersianNumber(completedTask.getRating()));
            } else {
                holder.ratingValue.setText(completedTask.getRating());
            }

            double rating = Double.parseDouble(completedTask.getRating());

            view.setTag(position);
            view.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mListener.onClickView(v, Integer.parseInt(v.getTag() + ""),
                            pentingTaskList.get(Integer.parseInt(v.getTag() + "")));
                }
            });

            holder.star_image_1.setImageResource(R.drawable.empty_star_icon);
            holder.star_image_2.setImageResource(R.drawable.empty_star_icon);
            holder.star_image_3.setImageResource(R.drawable.empty_star_icon);
            holder.star_image_4.setImageResource(R.drawable.empty_star_icon);
            holder.star_image_5.setImageResource(R.drawable.empty_star_icon);

            if (rating > 0 && rating <= 0.5) {
                holder.star_image_1.setImageResource(R.drawable.half_star_icon);
            }
            if (rating > 0.5 && rating <= 1.0) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
            }
            if (rating > 1.0 && rating <= 1.5) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.half_star_icon);
            }
            if (rating > 1.5 && rating <= 2.0) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
            }
            if (rating > 2.0 && rating <= 2.5) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
                holder.star_image_3.setImageResource(R.drawable.half_star_icon);
            }
            if (rating > 2.5 && rating <= 3.0) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
                holder.star_image_3.setImageResource(R.drawable.full_star_icon);
            }
            if (rating > 3.0 && rating <= 3.5) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
                holder.star_image_3.setImageResource(R.drawable.full_star_icon);
                holder.star_image_4.setImageResource(R.drawable.half_star_icon);
            }
            if (rating > 3.5 && rating <= 4.0) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
                holder.star_image_3.setImageResource(R.drawable.full_star_icon);
                holder.star_image_4.setImageResource(R.drawable.full_star_icon);
            }
            if (rating > 4.0 && rating <= 4.5) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
                holder.star_image_3.setImageResource(R.drawable.full_star_icon);
                holder.star_image_4.setImageResource(R.drawable.full_star_icon);
                holder.star_image_5.setImageResource(R.drawable.half_star_icon);
            }
            if (rating > 4.5 && rating <= 5.0) {
                holder.star_image_1.setImageResource(R.drawable.full_star_icon);
                holder.star_image_2.setImageResource(R.drawable.full_star_icon);
                holder.star_image_3.setImageResource(R.drawable.full_star_icon);
                holder.star_image_4.setImageResource(R.drawable.full_star_icon);
                holder.star_image_5.setImageResource(R.drawable.full_star_icon);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (completedTask != null &&
                completedTask.getProduct() != null && completedTask.getProduct().toString().length() > 0 &&
//                completedTask.getAdditional_instruction() != null && completedTask.getAdditional_instruction().toString().length() > 0 &&
                completedTask.getNext_steps() != null && completedTask.getNext_steps().toString().length() > 0 &&
                completedTask.getDetail() != null && completedTask.getDetail().toString().length() > 0) {

            holder.llRate.setVisibility(View.GONE);
        } else {

            if(completedTask.getRating()!=null && completedTask.getRating().equals("0.0"))
            {
                holder.llRate.setVisibility(View.GONE);
            }
            else {
                holder.llRate.setVisibility(View.VISIBLE);
            }
        }


        return view;
    }


    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private class ViewHolder {
        TextView completed_task_list_id, completed_task_list_issue,
                completed_task_list_date_time, completed_task_list_phone,
                completed_task_list_address, completed_task_org_name,
                ratingValue, lable_id, label_comma, overlapping_issue_all;

        ImageView star_image_1, star_image_2, star_image_3, star_image_4,
                star_image_5;

        RelativeLayout completed_parent_layout;
        LinearLayout idLayout1, llRate, llLeadMargin;
    }

}
