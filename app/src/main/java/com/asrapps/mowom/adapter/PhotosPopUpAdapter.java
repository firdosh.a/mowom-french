package com.asrapps.mowom.adapter;

import java.util.ArrayList;


import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.asrapps.mowom.R;
import com.asrapps.mowom.model.PhotoModel;
import com.squareup.picasso.Picasso;

public class PhotosPopUpAdapter extends PagerAdapter {
	// Declare Variables
	private Context context;
	private LayoutInflater inflater;
	private ImageButton iBtnBack, iBtnNext;
	private ViewPager photoPager;

	ArrayList<PhotoModel> jsonArr = new ArrayList<PhotoModel>();
	private int imgPosition;

	private boolean isFirstLoad;

	public PhotosPopUpAdapter(FragmentActivity activity, ImageButton iBtnBack,
			ImageButton iBtnNext, ViewPager photoPager, ArrayList<PhotoModel> jsonArr,
			int imgPosition) {
		this.context = activity;
		this.iBtnBack = iBtnBack;
		this.iBtnNext = iBtnNext;
		this.photoPager = photoPager;

		isFirstLoad = true;

		this.jsonArr = jsonArr;
		this.imgPosition = imgPosition;
	}

	@Override
	public int getCount() {
		return jsonArr.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(final ViewGroup container, final int position) {

		// Declare Variables
		ImageView ivPhoto;

		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View itemView = inflater.inflate(R.layout.adapter_list_images,
				container, false);

		// Locate the ImageView in viewpager_item.xml
		ivPhoto = (ImageView) itemView.findViewById(R.id.ivOrgPhotos);

		try {
			String imagePath;

			if (isFirstLoad) {
				photoPager.setCurrentItem(imgPosition);
				imagePath = jsonArr.get(imgPosition).getPhoto();
				isFirstLoad = false;
			} else {
				imagePath = jsonArr.get(position).getPhoto();
			}

			final String strImage = imagePath;

			Picasso.with(context).load(strImage)
					.error(R.drawable.icon_no_preview).into(ivPhoto);

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Add viewpager_item.xml to ViewPager
		((ViewPager) container).addView(itemView);

		iBtnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				photoPager.setCurrentItem(photoPager.getCurrentItem() - 1);
			}
		});

		iBtnNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				photoPager.setCurrentItem(photoPager.getCurrentItem() + 1);
			}
		});

		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((RelativeLayout) object);

	}
}
