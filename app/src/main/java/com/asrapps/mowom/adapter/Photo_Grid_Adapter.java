package com.asrapps.mowom.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.interfaces.OnDeleteImageClick;
import com.asrapps.mowom.model.PhotoModel;
import com.asrapps.utils.Utils;

import java.util.List;

import static com.asrapps.mowom.R.id.photo_imageView;

public class Photo_Grid_Adapter extends BaseAdapter{

	Context context;
	Activity activity;
	PhotoModel pendingTask=null;
	private List<PhotoModel> pentingTaskList;
	int tempPosition;
	OnDeleteImageClick mListener;

	public Photo_Grid_Adapter(Activity activity,
			List<PhotoModel> strApiResult, OnDeleteImageClick listener) {
		this.activity = activity;
		this.context = activity;
		this.pentingTaskList = strApiResult;
		mListener = listener;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pentingTaskList.size();
	}

	@Override
	public Object getItem(int position) {
		return pentingTaskList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		
		ViewHolder holder;

		if (view == null) {
			view = LayoutInflater.from(context).inflate(
					R.layout.photo_layout_tab_item, parent, false);

			holder = new ViewHolder();

			
			holder.photo_imageView = (ImageView) view
					.findViewById(photo_imageView);
			holder.closeImage= (ImageView) view
					.findViewById(R.id.closeImage);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {
			Bitmap bm = null;
			tempPosition=position;
		    pendingTask = pentingTaskList.get(position);
		   
//		    bm = BitmapFactory.decodeByteArray(pendingTask.getTakenPhoto(),
//					0, pendingTask.getTakenPhoto().length);
			bm= Utils.getBitmap(pendingTask.getPhotoPath());

			try {
				int targetWidth = holder.photo_imageView.getWidth();

				DisplayMetrics metrics = new DisplayMetrics();
				((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);

				int height = metrics.heightPixels;
				targetWidth = metrics.widthPixels/2;

				double aspectRatio = (double) bm.getHeight() / (double) bm.getWidth();
				int targetHeight = (int) (targetWidth * aspectRatio);
				if (targetHeight > 0 && targetWidth > 0) {
					Bitmap result = Bitmap.createScaledBitmap(bm, targetWidth, targetHeight, false);
					if (result != bm) {
						// Same bitmap is returned if sizes are the same
						bm.recycle();
					}
//                                        llNewsImage.getLayoutParams().height = (int) (targetHeight);
					holder.photo_imageView.setImageBitmap(result);
				} else {
					holder.photo_imageView.setImageBitmap(bm);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();

				holder.photo_imageView.setImageBitmap(bm);
			}


		    holder.closeImage.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onClickDelete(v, position,
							pentingTaskList.get(position));
				}
			});
		    
		    /*Picasso.with(activity)
			.load(BitmapFactory.decodeByteArray(pendingTask.getTakenPhoto(),
					0, pendingTask.getTakenPhoto().length))
			.error(R.drawable.icon_no_preview).into(holder.photo_imageView);*/
		    			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}

	private class ViewHolder {
		
		ImageView photo_imageView;
		ImageView closeImage;

	}

}
