package com.asrapps.mowom.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.MyLeadingMarginSpan2;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.model.PendingTask;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Rejected_Task_List_Adapter extends BaseAdapter {
    Context context;
    Activity activity;
    PendingTask pendingTask = null;
    private List<PendingTask> pentingTaskList;
    String selectedLanguage;

    int id;

    public Rejected_Task_List_Adapter(Activity activity,
                                      List<PendingTask> strApiResult) {
        this.activity = activity;
        this.context = activity;
        this.pentingTaskList = strApiResult;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return pentingTaskList.size();
    }

    @Override
    public Object getItem(int position) {
        return pentingTaskList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        ViewHolder holder;

        if (view == null) {
            selectedLanguage = ConstantFunction.CheckLanguage(activity);

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = LayoutInflater.from(context).inflate(
                        R.layout.rejected_task_list_child_item_pr, parent, false);
            } else {
                view = LayoutInflater.from(context).inflate(
                        R.layout.rejected_task_list_child_item, parent, false);
            }


            holder = new ViewHolder();

            holder.llLeadMargin = (LinearLayout) view.findViewById(R.id.llLeadMargin);

            holder.rejected_task_list_id = (TextView) view
                    .findViewById(R.id.rejected_task_list_id);
            holder.idLayout1 = (LinearLayout) view.findViewById(R.id.idLayout1);
            holder.label_comma = (TextView) view
                    .findViewById(R.id.label_comma);

            holder.overlapping_issue_all = (TextView) view.findViewById(R.id.overlapping_issue_all);

            holder.rejected_task_list_issue = (TextView) view
                    .findViewById(R.id.rejected_task_list_issue);
            holder.rejected_task_list_date_time = (TextView) view
                    .findViewById(R.id.rejected_task_list_date_time);
            holder.rejected_task_list_phone = (TextView) view
                    .findViewById(R.id.rejected_task_list_phone);
            holder.rejected_task_list_address = (TextView) view
                    .findViewById(R.id.rejected_task_list_address);
            holder.rejected_task_org_name = (TextView) view
                    .findViewById(R.id.rejected_task_org_name);
            holder.rejected_task_org_ph_num = (TextView) view
                    .findViewById(R.id.reason_textView);
            holder.lable_id = (TextView) view.findViewById(R.id.lable_id);

            holder.rejected_parent_layout = (RelativeLayout) view
                    .findViewById(R.id.rejected_parent_layout);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {

            pendingTask = pentingTaskList.get(position);
            if (position % 2 == 0)
                holder.rejected_parent_layout.setBackgroundColor(Color
                        .parseColor("#FCFCFC"));
            else
                holder.rejected_parent_layout.setBackgroundColor(Color
                        .parseColor("#FFF2F2"));

            selectedLanguage = ConstantFunction.CheckLanguage(activity);

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            id = pendingTask.getTaskId().length();
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.rejected_task_list_id.setText(toPersianNumber(pendingTask.getTaskId()));

                Drawable DICON = view.getResources().getDrawable(R.drawable.id_icon);
                int leftMargin = DICON.getIntrinsicWidth() + holder.lable_id.getText().length() + id + 80;
//            Set the icon in R.id.icon
                SpannableString issueString = new SpannableString(pendingTask.getIssue().toUpperCase(Locale.getDefault()));
//            Expose the indent for the first rows
                issueString.setSpan(new MyLeadingMarginSpan2(1, 0), 0, issueString.length(), 0);

                holder.rejected_task_list_issue.setText(issueString);

                holder.overlapping_issue_all.setText(activity.getResources().getString(R.string.id) + " " + toPersianNumber(pendingTask.getTaskId()) + " " + activity.getResources().getString(R.string.comma) + " " + pendingTask.getIssue().toUpperCase(Locale.getDefault()));

            } else {
                holder.rejected_task_list_id.setText(pendingTask.getTaskId());
                SpannableString issueString = new SpannableString(pendingTask.getIssue().toUpperCase(Locale.getDefault()));
                holder.rejected_task_list_issue.setText(issueString);
                holder.overlapping_issue_all.setText(activity.getResources().getString(R.string.id) + " " + pendingTask.getTaskId() + " " + activity.getResources().getString(R.string.comma) + " " + pendingTask.getIssue().toUpperCase(Locale.getDefault()));

            }

            String task_list_date_time = pendingTask
                    .getDate_time();

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                StringBuilder t = new StringBuilder();
                try {
                    task_list_date_time = WebServiceHelper.convert12to24(task_list_date_time);

                    String st[] = task_list_date_time.split(" ");

                    String stTime[] = st[1].split(":");
                    String dtTime[] = st[0].split("/");

                    String selectedLanguage;
                    selectedLanguage = ConstantFunction.CheckLanguage(activity);

                    if (selectedLanguage.equalsIgnoreCase("")) {
                        selectedLanguage = "en";
                    }

                    if (selectedLanguage.equalsIgnoreCase("pr")) {

                        st[0] = st[0].replace(",", "");
                        String dtStart = st[0];
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = format.parse(dtStart);
                            System.out.println(date);

                            SolarCalendar sc = new SolarCalendar(date);
                            String s = sc.date + "/" +
                                    sc.month + "/" + sc.year;

                            Log.v("TTT", "ssssss = " + s);

                            t.append(toPersianNumber(s)).append(" ");
                            t.append(toPersianNumber(stTime[0]));
                            t.append(":");
                            t.append(toPersianNumber(stTime[1]));

                        } catch (ParseException e) {
                            // TODO Auto-generated catch block

                            t.append(toPersianNumber(st[0])).append(" ");
                            t.append(toPersianNumber(stTime[0]));
                            t.append(":");
                            t.append(toPersianNumber(stTime[1]));
                            e.printStackTrace();
                        }

                    } else {
                        t.append(st[0]).append(" ");
                        t.append(stTime[0]);
                        t.append(":");
                        t.append(stTime[1]);

                    }

//                    try {
//                        if (st[2].equalsIgnoreCase("am")) {
//                            t.append(" " + activity.getResources().getString(R.string.am));
//                        } else {
//                            t.append(" " + activity.getResources().getString(R.string.pm));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//
//                    }


                    task_list_date_time = t.toString();

                    holder.rejected_task_list_date_time.setText(task_list_date_time);

                } catch (Exception e) {
                    holder.rejected_task_list_date_time.setText(task_list_date_time);
                    e.printStackTrace();
                }

//                holder.rejected_task_list_date_time.setText(toPersianNumber(WebServiceHelper.convert12to24(task_list_date_time)));

            } else {
//                StringBuilder t = new StringBuilder();
//                try {
//
//                    String st[] = task_list_date_time.split(" ");
//
//                    String stTime[] = st[1].split(":");
//                    String dtTime[] = st[0].split("/");
//
//                    String selectedLanguage;
//                    selectedLanguage = ConstantFunction.CheckLanguage(activity);
//
//                    if (selectedLanguage.equalsIgnoreCase("")) {
//                        selectedLanguage = "en";
//                    }
//
//
//                        st[0]=st[0].replace(",","");
//                        String dtStart = st[0];
//                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                        try {
//                            Date date = format.parse(dtStart);
//                            System.out.println(date);
//
//                            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                            String s = dateformat.format(date);
//                            System.out.println("Current Date Time : " + s);
//
//                            Log.v("TTT","ssssss = "+s);
//
//                            t.append(s).append(" ");
//                            t.append(stTime[0]);
//                            t.append(":");
//                            t.append(stTime[1]);
//
//                        } catch (ParseException e) {
//                            // TODO Auto-generated catch block
//
//                            t.append(st[0]).append(" ");
//                            t.append(stTime[0]);
//                            t.append(":");
//                            t.append(stTime[1]);
//                            e.printStackTrace();
//                        }
//
//
//                    try {
//                        if (st[2].equalsIgnoreCase("am")) {
//                            t.append(" " + activity.getResources().getString(R.string.am));
//                        } else {
//                            t.append(" " + activity.getResources().getString(R.string.pm));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//
//                    }
//
//
//                    task_list_date_time = t.toString();
//
//                    holder.rejected_task_list_date_time.setText(task_list_date_time);
//
//                } catch (Exception e) {
//                    holder.rejected_task_list_date_time.setText(task_list_date_time);
//                    e.printStackTrace();
//                }
                holder.rejected_task_list_date_time.setText(WebServiceHelper.convert12to24(task_list_date_time));


            }


            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.rejected_task_list_phone.setText(toPersianNumber(pendingTask.getPhone()));
            } else {
                holder.rejected_task_list_phone.setText(pendingTask.getPhone());

            }

            if(pendingTask
                    .getClientAddress()!=null && pendingTask
                    .getClientAddress().size()>0 && pendingTask
                    .getClientAddress().get(0).location!=null) {
                holder.rejected_task_list_address.setText(pendingTask.getClientAddress().get(0).location);
            }
            else
            {
                holder.rejected_task_list_address.setText("");
            }
            holder.rejected_task_org_name.setText(pendingTask.getOrg_name().toUpperCase(Locale.getDefault()));
            holder.rejected_task_org_ph_num.setText(pendingTask.getReason() + " - " + activity.getResources().getString(R.string.reason));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private class ViewHolder {
        TextView rejected_task_list_id, rejected_task_list_issue,
                rejected_task_list_date_time, rejected_task_list_phone,
                rejected_task_list_address, rejected_task_org_name,
                rejected_task_org_ph_num, lable_id, label_comma, overlapping_issue_all;

        RelativeLayout rejected_parent_layout;

        LinearLayout idLayout1, llLeadMargin;
    }
}
