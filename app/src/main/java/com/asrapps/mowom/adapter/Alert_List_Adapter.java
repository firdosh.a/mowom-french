package com.asrapps.mowom.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.flow.MyLeadingMarginSpan2;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.OnDoneClick;
import com.asrapps.mowom.model.Alerts;
import com.asrapps.mowom.view.persiandatepicker.SolarCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import com.asrapps.mowom.constants.FlowTextHelper;
//import com.asrapps.mowom.constants.MyLeadingMarginSpan2;

public class Alert_List_Adapter extends BaseAdapter {

    Context context;
    Activity activity;
    Alerts alertTask = null;
    OnDoneClick mListener;
    private List<Alerts> alertList;
    //int tempPosition;
    int id;

    String selectedLanguage;

    public Alert_List_Adapter(Activity activity,
                              List<Alerts> strApiResult, OnDoneClick listener) {
        this.activity = activity;
        this.context = activity;
        this.alertList = strApiResult;
        mListener = listener;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return alertList.size();
    }

    @Override
    public Object getItem(int position) {
        return alertList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        ViewHolder holder;

        if (view == null) {

            selectedLanguage = ConstantFunction.CheckLanguage(activity);

            if (selectedLanguage.equalsIgnoreCase("")) {
                selectedLanguage = "en";
            }

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                view = LayoutInflater.from(context).inflate(
                        R.layout.alert_screen_list_child_item_pr, parent, false);
            } else {
                view = LayoutInflater.from(context).inflate(
                        R.layout.alert_screen_list_child_item, parent, false);
            }

            holder = new ViewHolder();

            holder.lable_id = (TextView) view.findViewById(R.id.lable_id);
            holder.idLayout1 = (LinearLayout) view.findViewById(R.id.idLayout1);
            holder.llLeadMargin = (LinearLayout) view.findViewById(R.id.llLeadMargin);
            holder.label_comma = (TextView) view.findViewById(R.id.label_comma);
            holder.overlapping_issue_all = (TextView) view.findViewById(R.id.overlapping_issue_all);

            holder.alert_list_id = (TextView) view
                    .findViewById(R.id.alert_list_id);
            holder.alert_list_issue = (TextView) view
                    .findViewById(R.id.alert_list_issue);
            holder.alert_list_date_time = (TextView) view
                    .findViewById(R.id.alert_list_date_time);
            holder.alert_list_address = (TextView) view
                    .findViewById(R.id.alert_list_address);
            holder.alert_org_name = (TextView) view
                    .findViewById(R.id.alert_org_name);
            holder.phoneNumber = (TextView) view.findViewById(R.id.alert_org_ph_num);

            holder.acceptMissionButton = (LinearLayout) view.findViewById(R.id.acceptMissionButton);
            holder.rejectMissionButton = (LinearLayout) view.findViewById(R.id.rejectMissionButton);


            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        try {

            alertTask = alertList.get(position);

            id = alertTask.getTask_id().length();
            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.alert_list_id.setText(toPersianNumber(alertTask.getTask_id()));

                Drawable DICON = view.getResources().getDrawable(R.drawable.id_icon);
                int leftMargin = DICON.getIntrinsicWidth() + holder.lable_id.getText().length() + id + 90;
//            Set the icon in R.id.icon
                SpannableString issueString = new SpannableString(alertTask.getTask_name().toUpperCase(Locale.getDefault()));
//            Expose the indent for the first rows
                issueString.setSpan(new MyLeadingMarginSpan2(1, 0), 0, issueString.length(), 0);

                holder.alert_list_issue.setText(issueString);

                holder.overlapping_issue_all.setText(activity.getResources().getString(R.string.id) + " " + toPersianNumber(alertTask.getTask_id()) + " " + activity.getResources().getString(R.string.comma) + " " + alertTask.getTask_name().toUpperCase(Locale.getDefault()));

            } else {

                holder.alert_list_id.setText(alertTask.getTask_id());

                SpannableString issueString = new SpannableString(alertTask.getTask_name().toUpperCase(Locale.getDefault()));
                holder.alert_list_issue.setText(issueString);
                holder.overlapping_issue_all.setText(activity.getResources().getString(R.string.id) + " " + alertTask.getTask_id() + " " + activity.getResources().getString(R.string.comma) + " " + alertTask.getTask_name().toUpperCase(Locale.getDefault()));

//                Display display = activity.getWindowManager().getDefaultDisplay();
//                FlowTextHelper.tryFlowText(alertTask.getTask_name().toUpperCase(Locale.getDefault()), holder.llLeadMargin, holder.alert_list_issue, display, 5);

//                WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
//                Display display =  wm.getDefaultDisplay();
//                if (thumbnail_view != null && valueTV != null) {
//                    FlowTextHelper.tryFlowText(issueString,  holder.alert_list_issue, holder.alert_list_issue, display);
//                }

//                holder.idLayout1.measure(0, 0);
//                holder.lable_id.measure(0, 0);
//                holder.alert_list_id.measure(0, 0);
//                holder.label_comma.measure(0, 0);

//                int leftMargin = holder.lable_id.getMeasuredWidth() + holder.alert_list_id.getMeasuredWidth() + holder.label_comma.getMeasuredWidth() + 10;
//
//            Set the icon in R.id.icon
//                SpannableString issueString = new SpannableString(alertTask.getTask_name().toUpperCase(Locale.getDefault()));
//            Expose the indent for the first rows
//                issueString.setSpan(new MyLeadingMarginSpan2(1, leftMargin), 0, issueString.length(), 0);

//
            }

//            holder.alert_list_issue.setText(alertTask.getTask_name().toUpperCase(Locale.getDefault()));

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                StringBuilder t = new StringBuilder();
                String dt = WebServiceHelper.convert12to24(alertTask.getTask_starttime());

                try {

                    String st[] = dt.split(" ");

                    String stTime[] = st[1].split(":");
                    String dtTime[] = st[0].split("/");

                    selectedLanguage = ConstantFunction.CheckLanguage(activity);

                    if (selectedLanguage.equalsIgnoreCase("")) {
                        selectedLanguage = "en";
                    }

                    if (selectedLanguage.equalsIgnoreCase("pr")) {

                        st[0] = st[0].replace(",", "");
                        String dtStart = st[0];
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = format.parse(dtStart);

                            System.out.println(date);

                            SolarCalendar sc = new SolarCalendar(date);
                            String s = sc.date + "/" +
                                    sc.month + "/" + sc.year;

                            Log.v("TTT", "TTT = " + s);

                            t.append(toPersianNumber(s)).append(" ");
                            t.append(toPersianNumber(stTime[0]));
                            t.append(":");
                            t.append(toPersianNumber(stTime[1]));

                        } catch (ParseException e) {
                            e.printStackTrace();
                            // TODO Auto-generated catch block

                            t.append(toPersianNumber(st[0])).append(" ");
                            t.append(toPersianNumber(stTime[0]));
                            t.append(":");
                            t.append(toPersianNumber(stTime[1]));

                            e.printStackTrace();
                        }


                    } else {

                    }

//                    try {
//
//                        if (st[2].equalsIgnoreCase("am")) {
//                            t.append(" " + activity.getResources().getString(R.string.am));
//                        } else {
//                            t.append(" " + activity.getResources().getString(R.string.pm));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                    holder.alert_list_date_time.setText(t.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    holder.alert_list_date_time.setText(toPersianNumber(dt));

                }
            } else {
//                StringBuilder t = new StringBuilder();
//                try {
//                    String st[] = alertTask.getTask_starttime().split(" ");
//
//                    String stTime[] = st[1].split(":");
//                    String dtTime[] = st[0].split("/");
//
//                    st[0] = st[0].replace(",", "");
//                    String dtStart = st[0];
//                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//                    try {
//                        Date date = format.parse(dtStart);
//
//                        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
//                        String s = dateformat.format(date);
//                        System.out.println("Current Date Time : " + s);
//
//                        Log.v("TTT", "TTT = " + s);
//
//                        t.append(s).append(" ");
//                        t.append(stTime[0]);
//                        t.append(":");
//                        t.append(stTime[1]);
//
//                    } catch (ParseException e) {
//                        // TODO Auto-generated catch block
//
//                        t.append(st[0]).append(" ");
//                        t.append(stTime[0]);
//                        t.append(":");
//                        t.append(stTime[1]);
//
//                        e.printStackTrace();
//                    }
//
//
//                    try {
//
//                        if (st[2].equalsIgnoreCase("am")) {
//                            t.append(" " + activity.getResources().getString(R.string.am));
//                        } else {
//                            t.append(" " + activity.getResources().getString(R.string.pm));
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    holder.alert_list_date_time.setText(t.toString());
//                } catch (Exception e) {
//                    holder.alert_list_date_time.setText(alertTask.getTask_starttime());
//
//                }

                holder.alert_list_date_time.setText(WebServiceHelper.convert12to24(alertTask.getTask_starttime()));

            }


            if (alertTask
                    .getClient_address() != null && alertTask
                    .getClient_address().size() > 0 && alertTask
                    .getClient_address().get(0).location != null) {
                holder.alert_list_address.setText(alertTask
                        .getClient_address().get(0).location);
            } else {
                holder.alert_list_address.setText("");
            }

//            holder.alert_list_address.setText(alertTask.getAddress());
            holder.alert_org_name.setText(alertTask.getClient_name().toUpperCase(Locale.getDefault()));

            if (selectedLanguage.equalsIgnoreCase("pr")) {
                holder.phoneNumber.setText(toPersianNumber(alertTask.getContact_number()));
            } else {
                holder.phoneNumber.setText(alertTask.getContact_number());
            }

            holder.acceptMissionButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub


                    mListener.onAcceptClick(v, position,
                            alertList.get(position));
                }
            });

            holder.rejectMissionButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mListener.onRejectClick(v, position,
                            alertList.get(position));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }


    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private class ViewHolder {
        TextView alert_list_id, alert_list_issue, alert_list_date_time,
                alert_list_address, alert_org_name, phoneNumber, lable_id, label_comma, overlapping_issue_all;
        LinearLayout acceptMissionButton, rejectMissionButton;
        LinearLayout idLayout1, llLeadMargin;
    }
}
