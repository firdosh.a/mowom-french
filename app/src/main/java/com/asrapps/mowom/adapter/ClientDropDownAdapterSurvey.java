package com.asrapps.mowom.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.model.ClientData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qtm-kalpesh on 4/12/15.
 */
public class ClientDropDownAdapterSurvey extends BaseAdapter implements Filterable {

    private final Context context;
    private List<ClientData> rowItem;
    private List<ClientData> rowItemall;
    String selectedLanguage = "";
//    private Filter myFilter;

    public ClientDropDownAdapterSurvey(Context context, List<ClientData> rowItem) {
        this.context = context;
        this.rowItem = rowItem;
        this.rowItemall = rowItem;
        selectedLanguage = ConstantFunction.CheckLanguage(this.context);

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

//        myFilter = createFilter();

    }

//    @Override
//    public Filter getFilter() {
//        return myFilter;
//    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if (constraint.length() > 0) {

                    // Retrieve the autocomplete results.

                    String prefixString = constraint.toString().toLowerCase();

                    final ArrayList<ClientData> newValues = new ArrayList<ClientData>();

                    for (ClientData Word : rowItemall) {
                        String valueText = Word.clientName.toString().toLowerCase();

                        if (valueText.startsWith(prefixString))
                            newValues.add(Word);
                    }

                    rowItem=new ArrayList<>();
                    rowItem.addAll(newValues);

                    // Assign the data to the FilterResults
                    filterResults.values = rowItem;
                    filterResults.count = rowItem.size();

                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }

//                if (results != null && results.count > 0) {
//                    addAll(((ArrayList<ClientData>) results.values));
//                    notifyDataSetChanged();
//                } else {
//                    notifyDataSetInvalidated();
//                }

            }};
        return filter;
    }


//    private Filter createFilter() {
//        return new Filter() {
//            @Override
//            protected Filter.FilterResults performFiltering(CharSequence constraint) {
//                Filter.FilterResults filterResults = new Filter.FilterResults();
//
//                if (constraint.length() > 0) {
//
//                    notifyDataSetChanged();
//                    // Retrieve the autocomplete results.
//
//                    String PrefixString = constraint.toString().toLowerCase();
//
//                    final ArrayList<ClientData> NewValues = new ArrayList<ClientData>();
//
//                    for (ClientData Word : rowItem) {
//                        String ValueText = Word.clientName.toLowerCase();
//
//                        if (ValueText.startsWith(PrefixString))
//                            NewValues.add(Word);
//                    }
//
//                    // Assign the data to the FilterResults
//                    filterResults.values = NewValues;
//                    filterResults.count = NewValues.size();
//
//                }
//
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
//                if (results != null && results.count > 0) {
//                    addAll(((ArrayList<ClientData>) results.values));
//                    notifyDataSetChanged();
//                } else {
//                    notifyDataSetInvalidated();
//                }
//            }
//        };
//    }

    private void addAll(ArrayList<ClientData> values) {
        if (values != null && values.size() > 0) {
            rowItem = values;
            notifyDataSetChanged();
        }
    }

    private class ViewHolder {
        TextView txtTitleName;
    }

    @Override
    public int getCount() {
        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItem.get(position).clientName;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.v("BaseAdapter", "getView");
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (selectedLanguage.equalsIgnoreCase("pr")) {

                convertView = mInflater.inflate(R.layout.dropdown_list_item_pr, null);
            } else {
                convertView = mInflater.inflate(R.layout.dropdown_list_item, null);
            }

            holder = new ViewHolder();
            holder.txtTitleName = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTitleName.setText(rowItem.get(position).clientName);

        return convertView;
    }
}
