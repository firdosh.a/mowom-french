package com.asrapps.mowom.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.asrapps.mowom.R;
import com.asrapps.mowom.interfaces.OnDeleteSalesImageClick;
import com.asrapps.mowom.model.PhotoSalesModel;

import java.util.ArrayList;
import java.util.List;

public class Photo_Sales_Grid_Adapter extends BaseAdapter{

	Context context;
	Activity activity;
	PhotoSalesModel pendingTask=null;
	private List<PhotoSalesModel> pentingTaskList;
	int tempPosition;
	OnDeleteSalesImageClick mListener;

	public Photo_Sales_Grid_Adapter(Activity activity,
									ArrayList<PhotoSalesModel> strApiResult, OnDeleteSalesImageClick listener) {
		this.activity = activity;
		this.context = activity;
		this.pentingTaskList = strApiResult;
		mListener = listener;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pentingTaskList.size();
	}

	@Override
	public Object getItem(int position) {
		return pentingTaskList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		
		ViewHolder holder;

		if (view == null) {
			view = LayoutInflater.from(context).inflate(
					R.layout.photo_layout_tab_item, parent, false);

			holder = new ViewHolder();

			
			holder.photo_imageView = (ImageView) view
					.findViewById(R.id.photo_imageView);
			holder.closeImage= (ImageView) view
					.findViewById(R.id.closeImage);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {
			Bitmap bm = null;
			tempPosition=position;
		    pendingTask = pentingTaskList.get(position);
		   
		    bm = BitmapFactory.decodeByteArray(pendingTask.getTakenPhoto(),
					0, pendingTask.getTakenPhoto().length);
		    
		    holder.photo_imageView.setImageBitmap(bm);
		    holder.closeImage.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onClickDelete(v, position,
							pentingTaskList.get(position));
				}
			});
		    
		    /*Picasso.with(activity)
			.load(BitmapFactory.decodeByteArray(pendingTask.getTakenPhoto(),
					0, pendingTask.getTakenPhoto().length))
			.error(R.drawable.icon_no_preview).into(holder.photo_imageView);*/
		    			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}

	private class ViewHolder {
		
		ImageView photo_imageView;
		ImageView closeImage;

	}

}
