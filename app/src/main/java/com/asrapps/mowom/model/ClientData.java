package com.asrapps.mowom.model;

import java.util.ArrayList;

/**
 * Created by qtm-kalpesh on 7/9/16.
 */
public class ClientData {

    public String clientId;
    public String clientName;
    public String address;
    public String contact_person;
    public String clientemail;
    public String phone_number;
    public ArrayList<ClientAddress> client_address;

}
