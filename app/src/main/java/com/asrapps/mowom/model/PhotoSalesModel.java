package com.asrapps.mowom.model;

public class PhotoSalesModel {

	private String photo;
	
	private byte[] takenPhoto;
	private String photoId;
	private String takenDate;
	private String userId;

	public byte[] getTakenPhoto() {
		return takenPhoto;
	}

	public void setTakenPhoto(byte[] takenPhoto) {
		this.takenPhoto = takenPhoto;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public String getTakenDate() {
		return takenDate;
	}

	public void setTakenDate(String takenDate) {
		this.takenDate = takenDate;
	}

	public String getTaskId() {
		return userId;
	}

	public void setTaskId(String taskId) {
		this.userId = taskId;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
