package com.asrapps.mowom.model;

/**
 * Created by richa on 28/2/17.
 */

public class ClientAddress {
    public String location;
    public String client_location_id;
    public String latitude;
    public String longitude;
}
