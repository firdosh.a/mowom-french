package com.asrapps.mowom.model;

/**
 * Created by richa on 7/11/16.
 */

public class AddSalesLocation {

    public String unique_id;
    public String emp_id;
    public String trans_id;
    public String latitude;
    public String longitude;
    public String et_Id;

}
