package com.asrapps.mowom.model;

/**
 * Created by richa on 28/10/16.
 */

public class Login {

    public String comp_id;
    public String emp_id;
    public String emp_name;
    public String password;
    public String emp_email;
    public String phone;

    public String is_report;
    public String is_review;
    public String is_signature;

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmp_email() {
        return emp_email;
    }

    public void setEmp_email(String emp_email) {
        this.emp_email = emp_email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(String profileimage) {
        this.profileimage = profileimage;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCan_create_task() {
        return can_create_task;
    }

    public void setCan_create_task(String can_create_task) {
        this.can_create_task = can_create_task;
    }

    public String getCompanyid() {
        return companyid;
    }

    public void setCompanyid(String companyid) {
        this.companyid = companyid;
    }

    public String getSupervisor_id() {
        return supervisor_id;
    }

    public void setSupervisor_id(String supervisor_id) {
        this.supervisor_id = supervisor_id;
    }

    public String getClient_logo() {
        return client_logo;
    }

    public void setClient_logo(String client_logo) {
        this.client_logo = client_logo;
    }

    public String profileimage;
    public String profileImageSdCard;
    public String department;
    public String can_create_task;
    public String companyid;
    public String supervisor_id;
    public String client_logo;

}
