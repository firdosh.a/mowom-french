package com.asrapps.mowom.model;

import com.asrapps.mowom.constants.ConstantFunction;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PendingTask {

    private String taskId;
    private String issue;
    private String date_time;
    private String phone;
    private String address;
    private String org_name;
    private String org_ph_num;
    private double latitude;
    private double longitude;

    private String time;
    private String personName;
    private String supervisorName;
    private PendingTask pendingTask;
    private String tasktype;
    private String additionalInfo;


    private String startTime;
    private String stopTime;
    private String totalTimeSpend;
    private String taskReport;
    private String consumerFeedback;
    private String rating;
    private byte[] signatureImage;
    private String signatureString;

    private String reason;

    private String problem_reported;
    private String diagnosis_done;
    private String resolution;
    private String notes_support;


    private String deilvery_status;
    private String notes_delevery;


    private String product;
    private String detail;
    private String next_steps;
    private String additional_instruction;
    private boolean isStartTask;

    private String is_recurring;
    private String recurring_type;
    private String recurring_end_date;
    private ArrayList<ClientAddress> client_address;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String duration;

    public String is_google_sheet;
    public String google_sheet_url;


    public ArrayList<ClientAddress> getClientAddress() {
        return client_address;
    }

    public void setClientAddress(ArrayList<ClientAddress> clientAddress) {
        this.client_address = clientAddress;
    }


    public String getEt_id() {
        return et_id;
    }

    public void setEt_id(String et_id) {
        this.et_id = et_id;
    }

    private String et_id;

    public String getis_recurring() {
        return is_recurring;
    }

    public void setIs_recurring(String is_recurring) {
        this.is_recurring = is_recurring;
    }

    public String getRecurring_type() {
        return recurring_type;
    }

    public void setRecurring_type(String recurring_type) {
        this.recurring_type = recurring_type;
    }

    public String getRecurring_end_date() {
        return recurring_end_date;
    }

    public void setRecurring_end_date(String recurring_end_date) {
        this.recurring_end_date = recurring_end_date;
    }


    public String getProblem_reported() {
        return problem_reported;
    }

    public void setProblem_reported(String problem_reported) {
        this.problem_reported = problem_reported;
    }

    public String getDiagnosis_done() {
        return diagnosis_done;
    }

    public void setDiagnosis_done(String diagnosis_done) {
        this.diagnosis_done = diagnosis_done;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getNotes_support() {
        return notes_support;
    }

    public void setNotes_support(String notes_support) {
        this.notes_support = notes_support;
    }

    public String getDeilvery_status() {
        return deilvery_status;
    }

    public void setDeilvery_status(String deilvery_status) {
        this.deilvery_status = deilvery_status;
    }

    public String getNotes_delevery() {
        return notes_delevery;
    }

    public void setNotes_delevery(String notes_delevery) {
        this.notes_delevery = notes_delevery;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getTasktype() {
        return tasktype;
    }

    public void setTasktype(String tasktype) {
        this.tasktype = tasktype;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public String getTotalTimeSpend() {
        return totalTimeSpend;
    }

    public void setTotalTimeSpend(String totalTimeSpend) {
        this.totalTimeSpend = totalTimeSpend;
    }

    public String getTaskReport() {
        return taskReport;
    }

    public String getIs_google_sheet() {
        return is_google_sheet;
    }

    public void setIs_google_sheet(String is_google_sheet) {
        this.is_google_sheet = is_google_sheet;
    }

    public String getGoogle_sheet_url() {
        return google_sheet_url;
    }

    public void setGoogle_sheet_url(String google_sheet_url) {
        this.google_sheet_url = google_sheet_url;
    }

    public void setTaskReport(String taskReport) {
        this.taskReport = taskReport;
    }

    public String getConsumerFeedback() {
        return consumerFeedback;
    }

    public void setConsumerFeedback(String consumerFeedback) {
        this.consumerFeedback = consumerFeedback;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public byte[] getSignatureImage() {
        return signatureImage;
    }

    public void setSignatureImage(byte[] signatureImage) {
        this.signatureImage = signatureImage;
    }

    public String getSignatureString() {
        return signatureString;
    }

    public void setSignatureString(String signatureString) {
        this.signatureString = signatureString;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public PendingTask getPendingTask() {
        return pendingTask;
    }

    public void setPendingTask(PendingTask pendingTask) {
        this.pendingTask = pendingTask;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    boolean isSuccess;
    ArrayList<PendingTask> pendingListTasks;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public ArrayList<PendingTask> getPendingListTasks() {
        return pendingListTasks;
    }

    public void setPendingListTasks(ArrayList<PendingTask> pendingListTasks) {
        this.pendingListTasks = pendingListTasks;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String id) {
        this.taskId = id;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getOrg_ph_num() {
        return org_ph_num;
    }

    public void setOrg_ph_num(String org_ph_num) {
        this.org_ph_num = org_ph_num;
    }

    public PendingTask() {

    }

    public PendingTask(String pendingTaskObj) {
        String status = "";
        JSONObject job = null;
        try {
            job = new JSONObject(pendingTaskObj);
            status = job.getString("status");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (status.equalsIgnoreCase("success")) {
            try {
                isSuccess = true;
                pendingListTasks = new ArrayList<PendingTask>();
                JSONArray jArray = job.getJSONArray("data");

                for (int i = 0; i < jArray.length(); i++) {
                    PendingTask tasks = new PendingTask();
                    JSONObject jObject = jArray.getJSONObject(i);

                    if(jObject.has("et_id") && jObject.get("et_id")!=null) {
                        tasks.et_id = jObject.get("et_id").toString();
                    }
                    tasks.taskId = jObject.get("task_id").toString();
                    tasks.issue = jObject.get("task_name").toString();
                    tasks.date_time = ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
                    tasks.phone = jObject.get("contact_number").toString();
                    if(jObject.has("contact_person_name")) {
                        tasks.personName = jObject.get("contact_person_name").toString();
                    }
                    if(jObject.has("duration")) {
                        tasks.duration = jObject.get("duration").toString();
                    }
                    if(jObject.has("is_google_sheet")) {
                        tasks.is_google_sheet = jObject.get("is_google_sheet").toString();
                    }if(jObject.has("google_sheet_url")) {
                        tasks.google_sheet_url = jObject.get("google_sheet_url").toString();
                    }

//                    tasks.address = jObject.get("address").toString();
                    if(jObject.has("client_address")) {
                        JSONArray arrClientAddress = jObject.getJSONArray("client_address");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }

                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            tasks.client_address = new ArrayList<>();
                            tasks.client_address.addAll(arrListClientAddress);

                            tasks.address = jObject.getJSONArray("client_address").toString();
                        }

                    }
                    tasks.org_name = jObject.get("client_name").toString();
                    tasks.org_ph_num = jObject.get("contact_number").toString();

                    tasks.is_recurring = jObject.get("is_recurring").toString();
                    tasks.recurring_type = jObject.get("recurring_type").toString();
                    tasks.recurring_end_date = jObject.get("recurring_end_date").toString();

                    if (jObject.get("latitude") != null && !jObject.get("latitude").toString().equals("null")) {
                        tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
                    } else {
                    }
                    if (jObject.get("longtitude") != null && !jObject.get("longtitude").toString().equals("null")) {
                        tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    } else {

                    }

//					tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
//					tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    pendingListTasks.add(tasks);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (status.equalsIgnoreCase("failed")) {

            isSuccess = false;
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Invalid credentials", false);
        }
    }

    public PendingTask(String pendingTaskObj, String dummy) {
        String status = "";
        JSONObject job = null;
        try {
            job = new JSONObject(pendingTaskObj);
            status = job.getString("status");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (status.equalsIgnoreCase("success")) {
            try {
                isSuccess = true;
                pendingListTasks = new ArrayList<PendingTask>();

                JSONObject jObj = job.getJSONObject("data");
                String supervisor = jObj.get("spname").toString();
                JSONArray jArray = jObj.getJSONArray("alldetails");

                for (int i = 0; i < jArray.length(); i++) {
                    PendingTask tasks = new PendingTask();
                    JSONObject jObject = jArray.getJSONObject(i);

                    if(jObject.has("et_id") && jObject.get("et_id")!=null) {
                        tasks.et_id = jObject.get("et_id").toString();
                    }
                    tasks.taskId = jObject.get("task_id").toString();
                    tasks.issue = jObject.get("task_name").toString();
                    tasks.date_time = ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
                    tasks.time = ConstantFunction.GetTimeFormat(jObject.get("task_starttime").toString());
                    tasks.phone = jObject.get("contact_number").toString();
                    if(jObject.has("contact_person_name")) {
                        tasks.personName = jObject.get("contact_person_name").toString();
                    }
                    tasks.supervisorName = supervisor;
                    if(jObject.has("duration")) {
                        tasks.duration = jObject.get("duration").toString();
                    }
                    if(jObject.has("is_google_sheet")) {
                        tasks.is_google_sheet = jObject.get("is_google_sheet").toString();
                    }if(jObject.has("google_sheet_url")) {
                        tasks.google_sheet_url = jObject.get("google_sheet_url").toString();
                    }
//                    tasks.address = jObject.get("address").toString();
                    if(jObject.has("client_address")) {
                        JSONArray arrClientAddress = jObject.getJSONArray("client_address");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }

                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            tasks.client_address = new ArrayList<>();
                            tasks.client_address.addAll(arrListClientAddress);

                            tasks.address = jObject.getJSONArray("client_address").toString();
                        }

                    }

                    tasks.org_name = jObject.get("client_name").toString();
                    tasks.org_ph_num = jObject.get("contact_number").toString();

                    if (jObject.get("latitude") != null && !jObject.get("latitude").toString().equals("null")) {
                        tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
                    } else {
                    }
                    if (jObject.get("longtitude") != null && !jObject.get("longtitude").toString().equals("null")) {
                        tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    } else {

                    }

//					tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
//					tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    tasks.additionalInfo = jObject.get("additional_instruction").toString();
                    this.pendingTask = tasks;
                    pendingListTasks.add(tasks);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (status.equalsIgnoreCase("failed")) {

            isSuccess = false;
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Invalid credentials", false);
        }
    }


    public PendingTask(String pendingTaskObj, String dummy, String CompletedTask) {
        String status = "";
        JSONObject job = null;
        try {
            job = new JSONObject(pendingTaskObj);
            status = job.getString("status");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (status.equalsIgnoreCase("success")) {
            try {
                isSuccess = true;
                pendingListTasks = new ArrayList<PendingTask>();
                JSONArray jArray = job.getJSONArray("data");

                for (int i = 0; i < jArray.length(); i++) {
                    PendingTask tasks = new PendingTask();
                    JSONObject jObject = jArray.getJSONObject(i);

                    if(jObject.has("et_id") && jObject.get("et_id")!=null) {
                        tasks.et_id = jObject.get("et_id").toString();
                    }
                    tasks.taskId = jObject.get("task_id").toString();
                    tasks.date_time = ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
                    tasks.rating = jObject.get("rating").toString();
                    tasks.issue = jObject.get("task_name").toString();
                    tasks.tasktype = jObject.get("tasktype").toString();

                    tasks.org_name = jObject.get("client_name").toString();
//                    tasks.address = jObject.get("address").toString();
                    if(jObject.has("contact_person_name")) {
                        tasks.personName = jObject.get("contact_person_name").toString();
                    }
                    tasks.phone = jObject.get("contact_person").toString();

                    if(jObject.has("duration")) {
                        tasks.duration = jObject.get("duration").toString();
                    }
                    if(jObject.has("is_google_sheet")) {
                        tasks.is_google_sheet = jObject.get("is_google_sheet").toString();
                    }if(jObject.has("google_sheet_url")) {
                        tasks.google_sheet_url = jObject.get("google_sheet_url").toString();
                    }
                    if(jObject.has("client_address")) {
                        JSONArray arrClientAddress = jObject.getJSONArray("client_address");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }

                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            tasks.client_address = new ArrayList<>();
                            tasks.client_address.addAll(arrListClientAddress);

                            tasks.address = jObject.getJSONArray("client_address").toString();
                        }

                    }

                    if (jObject.get("latitude") != null && !jObject.get("latitude").toString().equals("null")) {
                        tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
                    } else {
                    }
                    if (jObject.get("longtitude") != null && !jObject.get("longtitude").toString().equals("null")) {
                        tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    } else {

                    }
                    if (jObject.get("product") != null && !jObject.get("product").toString().equals("null")) {
                        tasks.product = jObject.get("product").toString();
                    }

                    if (jObject.get("detail") != null && !jObject.get("detail").toString().equals("null")) {
                        tasks.detail = jObject.get("detail").toString();
                    }

                    if (jObject.get("next_steps") != null && !jObject.get("next_steps").toString().equals("null")) {
                        tasks.next_steps = jObject.get("next_steps").toString();
                    }

                    if (jObject.get("additional_instruction") != null && !jObject.get("additional_instruction").toString().equals("null")) {
                        tasks.additional_instruction = jObject.get("additional_instruction").toString();
                    }


                    this.pendingTask = tasks;
                    pendingListTasks.add(tasks);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (status.equalsIgnoreCase("failed")) {

            isSuccess = false;
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Invalid credentials", false);
        }
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAdditional_instruction() {
        return additional_instruction;
    }

    public void setAdditional_instruction(String additional_instruction) {
        this.additional_instruction = additional_instruction;
    }

    public String getNext_steps() {
        return next_steps;
    }

    public void setNext_steps(String next_steps) {
        this.next_steps = next_steps;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public PendingTask(String pendingTaskObj, String dummy, boolean RejectedTask) {
        String status = "";
        JSONObject job = null;
        try {
            job = new JSONObject(pendingTaskObj);
            status = job.getString("status");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (status.equalsIgnoreCase("success")) {
            try {
                isSuccess = true;
                pendingListTasks = new ArrayList<PendingTask>();
                JSONArray jArray = job.getJSONArray("data");

                for (int i = 0; i < jArray.length(); i++) {
                    PendingTask tasks = new PendingTask();
                    JSONObject jObject = jArray.getJSONObject(i);

                    if(jObject.has("et_id") && jObject.get("et_id")!=null) {
                        tasks.et_id = jObject.get("et_id").toString();
                    }
                    tasks.taskId = jObject.get("task_id").toString();
                    tasks.date_time = ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
                    tasks.issue = jObject.get("task_name").toString();
                    tasks.reason = jObject.get("rejected_task_reason").toString();
                    tasks.rating = jObject.get("rating").toString();
                    tasks.org_name = jObject.get("client_name").toString();
//                    tasks.address = jObject.get("address").toString();
                    if(jObject.has("contact_person_name")) {
                        tasks.personName = jObject.get("contact_person_name").toString();
                    }
                    tasks.phone = jObject.get("contact_number").toString();
                    if(jObject.has("is_google_sheet")) {
                        tasks.is_google_sheet = jObject.get("is_google_sheet").toString();
                    }if(jObject.has("google_sheet_url")) {
                        tasks.google_sheet_url = jObject.get("google_sheet_url").toString();
                    }
                    if(jObject.has("duration")) {
                        tasks.duration = jObject.get("duration").toString();
                    }

                    if(jObject.has("client_address")) {
                        JSONArray arrClientAddress = jObject.getJSONArray("client_address");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }

                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            tasks.client_address = new ArrayList<>();
                            tasks.client_address.addAll(arrListClientAddress);

                            tasks.address = jObject.getJSONArray("client_address").toString();
                        }

                    }

                    if (jObject.get("latitude") != null && !jObject.get("latitude").toString().equals("null")) {
                        tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
                    } else {
                    }
                    if (jObject.get("longtitude") != null && !jObject.get("longtitude").toString().equals("null")) {
                        tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    } else {

                    }

//					tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
//					tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    this.pendingTask = tasks;
                    pendingListTasks.add(tasks);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (status.equalsIgnoreCase("failed")) {

            isSuccess = false;
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Invalid credentials", false);
        }
    }

    public PendingTask(String pendingTaskObj, boolean Special) {
        String status = "";
        JSONObject job = null;
        try {
            job = new JSONObject(pendingTaskObj);
            status = job.getString("status");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (status.equalsIgnoreCase("success")) {
            try {
                isSuccess = true;
                pendingListTasks = new ArrayList<PendingTask>();
                JSONArray jArray = job.getJSONArray("data");

                for (int i = 0; i < jArray.length(); i++) {
                    PendingTask tasks = new PendingTask();
                    JSONObject jObject = jArray.getJSONObject(i);

                    if(jObject.has("et_id") && jObject.get("et_id")!=null) {
                        tasks.et_id = jObject.get("et_id").toString();
                    }
                    tasks.taskId = jObject.get("task_id").toString();
                    tasks.issue = jObject.get("task_name").toString();
                    tasks.date_time = ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
                    tasks.org_name = jObject.get("client_name").toString();
//                    tasks.address = jObject.get("address").toString();
                    if(jObject.has("contact_person_name")) {
                        tasks.personName = jObject.get("contact_person_name").toString();
                    }
                    tasks.org_ph_num = jObject.get("contact_number").toString();
                    if(jObject.has("duration")) {

                        tasks.duration = jObject.get("duration").toString();
                    }
                    if(jObject.has("is_google_sheet")) {
                        tasks.is_google_sheet = jObject.get("is_google_sheet").toString();
                    }if(jObject.has("google_sheet_url")) {
                        tasks.google_sheet_url = jObject.get("google_sheet_url").toString();
                    }
                    tasks.is_recurring = jObject.get("is_recurring").toString();
                    tasks.recurring_type = jObject.get("recurring_type").toString();
                    tasks.recurring_end_date = jObject.get("recurring_end_date").toString();

                    if(jObject.has("client_address")) {
                        JSONArray arrClientAddress = jObject.getJSONArray("client_address");
                        ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                        Gson gson = new Gson();
                        for (int r = 0; r < arrClientAddress.length(); r++) {
                            JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                            ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                            arrListClientAddress.add(address);
                        }

                        if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                            tasks.client_address = new ArrayList<>();
                            tasks.client_address.addAll(arrListClientAddress);

                            tasks.address = jObject.getJSONArray("client_address").toString();
                        }

                    }

                    if (jObject.get("latitude") != null && !jObject.get("latitude").toString().equals("null")) {
                        tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
                    } else {
                    }
                    if (jObject.get("longtitude") != null && !jObject.get("longtitude").toString().equals("null")) {
                        tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());
                    } else {

                    }

//					tasks.latitude = Double.parseDouble(jObject.get("latitude").toString());
//					tasks.longitude = Double.parseDouble(jObject.get("longtitude").toString());

					/*tasks.date_time = ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
                    tasks.time = ConstantFunction.GetTimeFormat(jObject.get("task_starttime").toString());
					tasks.supervisorName = jObject.get("supervisor_name").toString();
					tasks.org_ph_num = jObject.get("contact_number").toString();*/


                    this.pendingTask = tasks;
                    pendingListTasks.add(tasks);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (status.equalsIgnoreCase("failed")) {

            isSuccess = false;
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Invalid credentials", false);
        }
    }


}
