package com.asrapps.mowom.model;

public class PhotoModel {

	private String photo;
	
//	private byte[] takenPhoto;
	private String photoId;
	private String takenDate;
	private String taskId;
	private String photoPath;

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

//	public byte[] getTakenPhoto() {
//		return takenPhoto;
//	}
//
//	public void setTakenPhoto(byte[] takenPhoto) {
//		this.takenPhoto = takenPhoto;
//	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public String getTakenDate() {
		return takenDate;
	}

	public void setTakenDate(String takenDate) {
		this.takenDate = takenDate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
