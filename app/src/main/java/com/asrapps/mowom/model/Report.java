package com.asrapps.mowom.model;

import com.asrapps.mowom.constants.ConstantFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Report {

	private String dateTime;
	private String jobId;
	private String clientName;
	private String timeSpend;
	private String totalHours;
	private boolean isSuccess;
	private String taskId;

	private ArrayList<Report> reportList;
	private Report report;
	
	private long totalSecs=0;
	
	private long hours;
	private long minutes;
	private long seconds;
	

	public long getTotalSecs() {
		return totalSecs;
	}

	public void setTotalSecs(long totalSecs) {
		this.totalSecs = totalSecs;
	}

	public long getHours() {
		return hours;
	}

	public void setHours(long hours) {
		this.hours = hours;
	}

	public long getMinutes() {
		return minutes;
	}

	public void setMinutes(long minutes) {
		this.minutes = minutes;
	}

	public long getSeconds() {
		return seconds;
	}

	public void setSeconds(long seconds) {
		this.seconds = seconds;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public ArrayList<Report> getReportList() {
		return reportList;
	}

	public void setReportList(ArrayList<Report> reportList) {
		this.reportList = reportList;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getTimeSpend() {
		return timeSpend;
	}

	public void setTimeSpend(String timeSpend) {
		this.timeSpend = timeSpend;
	}

	public String getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(String totalHours) {
		this.totalHours = totalHours;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Report() {

	}

	public Report(String reportObj) {
		String status = "";
		JSONObject job = null;
		try {
			job = new JSONObject(reportObj);
			status = job.getString("status");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		if (status.equalsIgnoreCase("success")) {
			try {
				isSuccess = true;
				reportList = new ArrayList<Report>();
				JSONArray jArray = job.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					Report report = new Report();
					JSONObject jObject = jArray.getJSONObject(i);

					report.dateTime = ConstantFunction.ChangeDateFormat(
							"yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy - HH:mm",
							jObject.get("createdat").toString());
					report.jobId =  jObject.get("task_id").toString();
					report.clientName = jObject.get("client_name").toString();
					report.timeSpend = jObject.get("work_hours").toString();
					this.totalSecs+=GetSeconds(report.timeSpend);

					String work_hour=jObject.get("work_hours").toString();
					String[] timeArray=work_hour.split(":");

					if(timeArray!=null && timeArray.length>1)
					{
						report.timeSpend=timeArray[0]+":"+timeArray[1];
					}

//					report.timeSpend=ConstantFunction.ChangeDateFormat(
//							"HH:mm:ss", "HH:mm",
//							jObject.get("work_hours").toString());
					report.report = report;
					reportList.add(report);
				}
				if(totalSecs>0){
					this.hours = totalSecs / 3600;
					this.minutes = (totalSecs % 3600) / 60;
					this.seconds = totalSecs % 60;
					
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (status.equalsIgnoreCase("failed")) {

			isSuccess = false;
			// ConstantFunction.showAlertDialog(LoginActivity.this,
			// "Invalid credentials", false);
		}
	}

	private long GetSeconds(String time){
		String[] parts = time.split(":");
		long totSec=0;
		int hour=Integer.parseInt(parts[0]);
		int min=Integer.parseInt(parts[1]);
		int sec=Integer.parseInt(parts[2]);
		
		totSec=(hour*3600)+(min*60)+sec;
		
		return totSec;
	}
}
