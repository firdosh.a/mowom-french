package com.asrapps.mowom.model;


import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CompletedTask {

    private String google_sheet_url;
    private String is_google_sheet;
    private String et_id;
    private String comp_id;
    private String emp_id;
    private String emp_name;
    private String loc_id;
    private String task_id;
    private String et_duedate;
    private String et_hours;
    private String et_mins;
    private String et_status;
    private String task_starttime;
    private String task_endtime;
    private String seenby;
    private String seen;
    private String report;
    private String feedback;
    private String status;
    private String rejected_task_reason;
    private String rating;
    private String createdat;
    private String screenshot;
    private String supervisor_id;
    private String clientsign;
    private String sup_name;
    private String contact_name;
    private String address;
    private String contactnumber;
    private String latitude;
    private String longtitude;
    private String client_name;
    private String task_name;
    private ArrayList<PhotoModel> listOfImages;
    private boolean isSuccess;

    private String product;
    private String detail;
    private String next_steps;
    private String additional_instruction;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String duration;

    public String getGoogle_sheet_url() {
        return google_sheet_url;
    }

    public void setGoogle_sheet_url(String google_sheet_url) {
        this.google_sheet_url = google_sheet_url;
    }

    public String getIs_google_sheet() {
        return is_google_sheet;
    }

    public void setIs_google_sheet(String is_google_sheet) {
        this.is_google_sheet = is_google_sheet;
    }

    public ArrayList<ClientAddress> getClient_address() {
        return client_address;
    }

    public void setClient_address(ArrayList<ClientAddress> client_address) {
        this.client_address = client_address;
    }

    private ArrayList<ClientAddress> client_address;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAdditional_instruction() {
        return additional_instruction;
    }

    public void setAdditional_instruction(String additional_instruction) {
        this.additional_instruction = additional_instruction;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getNext_steps() {
        return next_steps;
    }

    public void setNext_steps(String next_steps) {
        this.next_steps = next_steps;
    }

    public ArrayList<PhotoModel> getListOfImages() {
        return listOfImages;
    }

    public void setListOfImages(ArrayList<PhotoModel> listOfImages) {
        this.listOfImages = listOfImages;
    }

    public String getSup_name() {
        return sup_name;
    }

    public void setSup_name(String sup_name) {
        this.sup_name = sup_name;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }


    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getEt_id() {
        return et_id;
    }

    public void setEt_id(String et_id) {
        this.et_id = et_id;
    }

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getEmp_name() {
        return emp_name;
    }

    public void setEmp_name(String emp_name) {
        this.emp_name = emp_name;
    }

    public String getLoc_id() {
        return loc_id;
    }

    public void setLoc_id(String loc_id) {
        this.loc_id = loc_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getEt_duedate() {
        return et_duedate;
    }

    public void setEt_duedate(String et_duedate) {
        this.et_duedate = et_duedate;
    }

    public String getEt_hours() {
        return et_hours;
    }

    public void setEt_hours(String et_hours) {
        this.et_hours = et_hours;
    }

    public String getEt_mins() {
        return et_mins;
    }

    public void setEt_mins(String et_mins) {
        this.et_mins = et_mins;
    }

    public String getEt_status() {
        return et_status;
    }

    public void setEt_status(String et_status) {
        this.et_status = et_status;
    }

    public String getTask_starttime() {
        return task_starttime;
    }

    public void setTask_starttime(String task_starttime) {
        this.task_starttime = task_starttime;
    }

    public String getTask_endtime() {
        return task_endtime;
    }

    public void setTask_endtime(String task_endtime) {
        this.task_endtime = task_endtime;
    }

    public String getSeenby() {
        return seenby;
    }

    public void setSeenby(String seenby) {
        this.seenby = seenby;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejected_task_reason() {
        return rejected_task_reason;
    }

    public void setRejected_task_reason(String rejected_task_reason) {
        this.rejected_task_reason = rejected_task_reason;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(String screenshot) {
        this.screenshot = screenshot;
    }

    public String getSupervisor_id() {
        return supervisor_id;
    }

    public void setSupervisor_id(String supervisor_id) {
        this.supervisor_id = supervisor_id;
    }

    public String getClientsign() {
        return clientsign;
    }

    public void setClientsign(String clientsign) {
        this.clientsign = clientsign;
    }


    public CompletedTask(String completedTaskObj) {
        String status = "";
        JSONObject job = null;
        try {
            job = new JSONObject(completedTaskObj);
            status = job.getString("status");
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (status.equalsIgnoreCase("success")) {
            try {
                isSuccess = true;

                JSONObject jObj = job.getJSONObject("data");
                JSONArray jArray = jObj.getJSONArray("alldetails");

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jObject = jArray.getJSONObject(i);
                    String supervisor = jObject.get("sup_name").toString();
                    this.et_id = jObject.get("et_id").toString();
                    this.comp_id = jObject.get("comp_id").toString();
                    this.emp_id = jObject.get("emp_id").toString();
                    this.emp_name = jObject.get("emp_name").toString();
                    this.loc_id = jObject.get("loc_id").toString();
                    this.task_id = jObject.get("task_id").toString();
                    this.et_duedate = jObject.get("et_duedate").toString();
                    this.et_hours = jObject.get("et_hours").toString();
                    this.et_mins = jObject.get("et_mins").toString();
                    this.et_status = jObject.get("et_status").toString();
                    this.task_starttime = jObject.get("task_starttime").toString();
                    this.task_endtime = jObject.get("task_endtime").toString();
                    this.seenby = jObject.get("seenby").toString();
                    this.seen = jObject.get("seen").toString();
                    this.report = jObject.get("report").toString();
                    this.feedback = jObject.get("feedback").toString();
                    this.status = jObject.get("status").toString();
                    this.rejected_task_reason = jObject.get("task_id").toString();
                    this.rating = jObject.get("rating").toString();
                    this.createdat = jObject.get("createdat").toString();
                    this.screenshot = jObject.get("screenshot").toString();
                    this.supervisor_id = jObject.get("supervisor_id").toString();
                    this.clientsign = jObject.get("clientsign").toString();
                    //System.out.println("this.clientsign==> "+this.clientsign);
                    this.sup_name = supervisor;//jObject.get("supervisor").toString();
                    this.contact_name = jObject.get("contact_name").toString();
//                    this.address = jObject.get("address").toString();
                    this.contactnumber = jObject.get("contactnumber").toString();
                    if(jObject.has("duration")) {
                        this.duration = jObject.get("duration").toString();
                    }
                    if(jObject.has("is_google_sheet")) {
                        this.is_google_sheet = jObject.get("is_google_sheet").toString();
                    }if(jObject.has("google_sheet_url")) {
                        this.google_sheet_url = jObject.get("google_sheet_url").toString();
                    }
                    JSONArray arrClientAddress = jObject.getJSONArray("client_address");
                    ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

                    Gson gson = new Gson();
                    for (int r = 0; r < arrClientAddress.length(); r++) {
                        JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
                        ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
                        arrListClientAddress.add(address);
                    }

                    if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
                        this.client_address = new ArrayList<>();
                        this.client_address.addAll(arrListClientAddress);

                        this.address = jObject.getJSONArray("client_address").toString();
                    }

                    if (jObject.get("latitude") != null && !jObject.get("latitude").toString().equals("null")) {
                        this.latitude = jObject.get("latitude").toString();
                    }

                    if (jObject.get("longtitude") != null && !jObject.get("longtitude").toString().equals("null")) {
                        this.longtitude = jObject.get("longtitude").toString();
                    }

                    this.client_name = jObject.get("client_name").toString();
                    this.task_name = jObject.get("task_name").toString();

                    if (jObject.get("product") != null && jObject.get("product").toString().length() > 0) {

                        this.product = jObject.get("product").toString();

                        this.listOfImages = new ArrayList<PhotoModel>();

                        this.screenshot = jObject.get("dept_images").toString();

                        if(screenshot.length()>0) {
                            String[] images = screenshot.split(",");
                            for (int img = 0; img < images.length; img++) {
                                PhotoModel photos = new PhotoModel();
                                photos.setPhoto(images[img]);
                                this.listOfImages.add(photos);
                            }
                        }

                    } else {
                        this.listOfImages = new ArrayList<PhotoModel>();
                        if(screenshot.length()>0) {
                            String[] images = screenshot.split(",");
                            for (int img = 0; img < images.length; img++) {
                                PhotoModel photos = new PhotoModel();
                                photos.setPhoto(images[img]);
                                this.listOfImages.add(photos);
                            }
                        }
                    }

                    if (jObject.get("detail") != null && jObject.get("detail").toString().length() > 0) {


                        this.detail = jObject.get("detail").toString();
                    }

                    if (jObject.get("next_steps") != null && jObject.get("next_steps").toString().length() > 0) {

                        this.next_steps = jObject.get("next_steps").toString();
                    }

                    if (jObject.get("additional_instruction") != null && jObject.get("additional_instruction").toString().length() > 0) {

                        this.additional_instruction = jObject.get("additional_instruction").toString();
                    }

                    if (jObject.get("notes") != null && jObject.get("notes").toString().length() > 0) {

                        this.additional_instruction = jObject.get("notes").toString();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (status.equalsIgnoreCase("failed")) {

            isSuccess = false;
        }
    }

}
