package com.asrapps.mowom.model;

import com.asrapps.mowom.constants.ConstantFunction;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Alerts {

	private String  task_id;
	private String  task_starttime;
	private String  task_name;
	private String client_name;
	private String address;
	private String contact_person_name;
	private String contact_number;
	private double latitude;
	private double longtitude;

	public ArrayList<ClientAddress> getClient_address() {
		return client_address;
	}

	public void setClient_address(ArrayList<ClientAddress> client_address) {
		this.client_address = client_address;
	}

	private ArrayList<ClientAddress> client_address;
	
	private String supervisor_name;
	private Alerts alertsObj;
	
	private boolean isSuccess;
	private ArrayList<Alerts> alertList;
	private String tasktype;
	
	

	
	
	
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public ArrayList<Alerts> getAlertList() {
		return alertList;
	}
	public void setAlertList(ArrayList<Alerts> alertList) {
		this.alertList = alertList;
	}
	public Alerts getAlertsObj() {
		return alertsObj;
	}
	public void setAlertsObj(Alerts alertsObj) {
		this.alertsObj = alertsObj;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getTask_starttime() {
		return task_starttime;
	}
	public void setTask_starttime(String task_starttime) {
		this.task_starttime = task_starttime;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContact_person_name() {
		return contact_person_name;
	}
	public void setContact_person_name(String contact_person_name) {
		this.contact_person_name = contact_person_name;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongtitude() {
		return longtitude;
	}
	public void setLongtitude(double longtitude) {
		this.longtitude = longtitude;
	}
	public String getSupervisor_name() {
		return supervisor_name;
	}
	public void setSupervisor_name(String supervisor_name) {
		this.supervisor_name = supervisor_name;
	}
	
	public Alerts(){
		
	}
	
	public Alerts(String alertsListObj) {
		String status = "";
		JSONObject job = null;
		try {
			job = new JSONObject(alertsListObj);
			status = job.getString("status");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		
		if (status.equalsIgnoreCase("success")) {
			try {
				isSuccess = true;
				alertList = new ArrayList<Alerts>();
				JSONArray jArray = job.getJSONArray("data");

				for (int i = 0; i < jArray.length(); i++) {
					Alerts alerts = new Alerts();
					JSONObject jObject = jArray.getJSONObject(i);
					alerts.task_id= jObject.get("task_id").toString();
					alerts.tasktype= jObject.get("tasktype").toString();
					
					alerts.task_starttime= ConstantFunction.GetDateFormat(jObject.get("task_starttime").toString());
					alerts.task_name= jObject.get("task_name").toString();
					alerts.client_name= jObject.get("client_name").toString();
//					alerts.address= jObject.get("address").toString();
					alerts.contact_person_name= jObject.get("contact_person_name").toString();
					alerts.contact_number= jObject.get("contact_number").toString();
//
					if(jObject.has("client_address")){
						JSONArray arrClientAddress = jObject.getJSONArray("client_address");
						ArrayList<ClientAddress> arrListClientAddress = new ArrayList<>();

						Gson gson = new Gson();
						for (int r = 0; r < arrClientAddress.length(); r++) {
							JSONObject objClientAddress = arrClientAddress.getJSONObject(r);
							ClientAddress address = gson.fromJson(objClientAddress.toString(), ClientAddress.class);
							arrListClientAddress.add(address);
						}

						if (arrListClientAddress != null && arrListClientAddress.size() > 0) {
							alerts.client_address = new ArrayList<>();
							alerts.client_address.addAll(arrListClientAddress);

							alerts.address = jObject.getJSONArray("client_address").toString();
						}
					}
					if(jObject.get("latitude")!=null && !jObject.get("latitude").toString().equals("null")) {
						alerts.latitude = Double.parseDouble(jObject.get("latitude").toString());
					}
					else
					{
					}
					if(jObject.get("longtitude")!=null && !jObject.get("longtitude").toString().equals("null")) {
						alerts.longtitude = Double.parseDouble(jObject.get("longtitude").toString());
					}
					else
					{

					}

//					alerts.latitude= Double.parseDouble(jObject.get("latitude").toString());
//					alerts.longtitude= Double.parseDouble(jObject.get("longtitude").toString());
					alerts.supervisor_name= jObject.get("supervisor_name").toString();
					alerts.alertsObj=alerts;
					alertList.add(alerts);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (status.equalsIgnoreCase("failed")) {

			isSuccess = false;
			// ConstantFunction.showAlertDialog(LoginActivity.this,
			// "Invalid credentials", false);
		}
	}
	public String getTasktype() {
		return tasktype;
	}
	public void setTasktype(String tasktype) {
		this.tasktype = tasktype;
	}
	
	
}
