package com.asrapps.mowom.model;

import java.util.ArrayList;

/**
 * Created by qtm-kalpesh on 16/9/16.
 */
public class SalesTask{

    public String task_id;
    public String task_id_Tx;
    public String SALES_client_name_new;
    public String SALES_client_name;
    public String SALES_client_address;
    public ArrayList<ClientAddress> client_address;
    public String SALES_contact_person;
    public String tel_number;
    public String client_email;
    public String latitude;
    public String longtitude;
    public String client_id;
    public String category_id;
    public String next_steps;
    public String notes;
    public String product;
    public String detail;
    public String department;
    public String emp_id;
    public String company_id;
    public String task_name;
    public String additional_instruction;
    public String tasktype;

    // new params for sales_Task
    public String startTime="";
    public String endTime="";
    public String totalTimeTaken="";
    public String category_Name="";
    public String supervisor_id="";

//    public SalesTask(Parcel in) {
//        task_id = in.readString();
//        SALES_client_name_new = in.readString();
//        SALES_client_name = in.readString();
//        SALES_client_address = in.readString();
//        SALES_contact_person = in.readString();
//        tel_number = in.readString();
//        client_email = in.readString();
//        latitude = in.readString();
//        longtitude = in.readString();
//        client_id = in.readString();
//        category_id = in.readString();
//        next_steps = in.readString();
//        notes = in.readString();
//        product = in.readString();
//        detail = in.readString();
//        department = in.readString();
//        emp_id = in.readString();
//        company_id = in.readString();
//        task_name = in.readString();
//        additional_instruction = in.readString();
//        tasktype = in.readString();
//        startTime = in.readString();
//        endTime = in.readString();
//        category = in.readString();
//    }
//
//    public static final Creator<SalesTask> CREATOR = new Creator<SalesTask>() {
//        @Override
//        public SalesTask createFromParcel(Parcel in) {
//            return new SalesTask(in);
//        }
//
//        @Override
//        public SalesTask[] newArray(int size) {
//            return new SalesTask[size];
//        }
//    };
//
//    public SalesTask() {
//
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(task_id);
//        dest.writeString(SALES_client_name_new);
//        dest.writeString(SALES_client_name);
//        dest.writeString(SALES_client_address);
//        dest.writeString(SALES_contact_person);
//        dest.writeString(tel_number);
//        dest.writeString(client_email);
//        dest.writeString(latitude);
//        dest.writeString(longtitude);
//        dest.writeString(client_id);
//        dest.writeString(category_id);
//        dest.writeString(next_steps);
//        dest.writeString(notes);
//        dest.writeString(product);
//        dest.writeString(detail);
//        dest.writeString(department);
//        dest.writeString(emp_id);
//        dest.writeString(company_id);
//        dest.writeString(task_name);
//        dest.writeString(additional_instruction);
//        dest.writeString(tasktype);
//        dest.writeString(startTime);
//        dest.writeString(endTime);
//        dest.writeString(category);
//    }
}