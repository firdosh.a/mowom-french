package com.asrapps.mowom.interfaces;

public interface AsyncResponseListener {

	void processFinish(String requestURL, String response);

}
