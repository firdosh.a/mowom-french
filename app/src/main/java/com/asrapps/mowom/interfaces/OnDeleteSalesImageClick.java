package com.asrapps.mowom.interfaces;

import android.view.View;

import com.asrapps.mowom.model.PhotoSalesModel;

public interface OnDeleteSalesImageClick {
	void onClickDelete(View v, int position, PhotoSalesModel photo);
}
