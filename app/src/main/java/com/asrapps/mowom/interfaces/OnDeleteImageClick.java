package com.asrapps.mowom.interfaces;

import android.view.View;

import com.asrapps.mowom.model.PhotoModel;

public interface OnDeleteImageClick {
	void onClickDelete(View v, int position,PhotoModel photo);
}
