package com.asrapps.mowom.interfaces;

public interface EditNameDialogListener {
	void onFinishEditDialog(String inputText);
}
