package com.asrapps.mowom.interfaces;

import android.view.View;

import com.asrapps.mowom.model.Alerts;
import com.asrapps.mowom.model.ClientAddress;
import com.asrapps.mowom.model.PendingTask;

import java.util.ArrayList;

public interface OnDoneClick {

	void onClickMap(View v, int position,ArrayList<ClientAddress> client_address);
	void onClickCall(View v, int position,String phoneNumber);
	void onClickMenu(View v, int position,PendingTask pendingTask);
	void onClickView(View v, int position,PendingTask pendingTask);
	void onAcceptClick(View v, int position,Alerts alerts);
	void onRejectClick(View v, int position,Alerts alerts);
	}



