package com.asrapps.mowom.interfaces;

import android.view.View;

import com.asrapps.mowom.model.PendingTask;

public interface OnPhotoClick {
	void onClickView(View v, int position,PendingTask pendingTask);
}
