package com.asrapps.mowom.helper;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.RequestBody;
//import okhttp3.Response;

public class WebServiceHelper extends AsyncTask<String, Void, String> {


	public static boolean isEmailValid(CharSequence email) {
		return !TextUtils.isEmpty(email)
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static String convert12to24(String date24)
	{
		try {
			date24 = date24.replace(",", "");


			SimpleDateFormat readFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

			SimpleDateFormat writeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Date date = null;
			try {
				date = readFormat.parse(date24);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			if (date != null) {
				String formattedDate = writeFormat.format(date);

				return formattedDate;
			} else {
				return date24;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			date24 = date24.replace(",", "");
			return date24;

		}
	}

	private Request.Builder requestBuilder = null;
	private String requestURL = null;
	
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	//private static final MediaType OCTET_STREAM = MediaType.parse("application/octet-stream");
	private static final MediaType JAVA_SERIALIZED_OBJECT = MediaType.parse("application/x-java-serialized-object");
	
	OkHttpClient client = new OkHttpClient();
	public AsyncResponseListener delegate = null;
	
	public WebServiceHelper(String requestURL){
		this.requestURL = requestURL;
		this.requestBuilder = new Request.Builder().url(requestURL);
	}

	public static void hideKeyboard(Context c, View view) {
		if (view instanceof EditText
				|| view instanceof AutoCompleteTextView) {
			InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}


	protected String doInBackground(String... params) {
		try {				
			Request request = requestBuilder.build();
			
//			System.out.println("Request URL: " + request.urlString());
			System.out.println("Auth Token: " + request.header(ConstantValues.kWSHeaderAuthToken));
			
			if(request.body() != null){
				System.out.println("Data Type: " + request.body().contentType().toString());
			}
			
//			client.setConnectTimeout(60, TimeUnit.SECONDS);
			Response response = client.newCall(request).execute();
			//System.out.println("response ==>"+response.body().string());
			return response.body().string();       
		}
		catch (Exception e) {
			
			System.out.println("Connection Exception:" + e.getLocalizedMessage());
			
			//Create the JSON for response
			JSONObject errorResponse = new JSONObject();
		    try {
		    	//Creating JSON object for response_status key
		    	JSONObject response_status = new JSONObject();
		    	response_status.put("response_code", 1);
		    	response_status.put("response_message", "Failed");
		    	
		    	//Creating JSON object for response_data key
		    	JSONObject response_data = new JSONObject();
		    	response_data.put("error", "Unable to complete the request due to network connection error. Please try again.");
		    	
		    	errorResponse.put("response_status", response_status);
		    	errorResponse.put("response_data", response_data);
		    	
			} catch (JSONException JSONexp) {
				JSONexp.printStackTrace();
			}
		    
		    return errorResponse.toString();
	    }
	}

	protected void onPostExecute(String responseString) {
	    // TODO: check this.exception 
	    // TODO: do something with the feed
		System.out.println("Response: " + responseString);
		
		delegate.processFinish(this.requestURL, responseString);
	}
	
	public void addHeader(String name, String value){
		this.requestBuilder.addHeader(name, value);
	}
	
	public void setRequestBody(String requestData){
		System.out.println("Request Body: " + requestData);
		this.requestBuilder.post(RequestBody.create(JSON, requestData));
	}
	
	public void setRequestBody(byte[] requestData){
		System.out.println("Request Body: " + requestData.toString());
		this.requestBuilder.post(RequestBody.create(JAVA_SERIALIZED_OBJECT, requestData));
	}

	
}
