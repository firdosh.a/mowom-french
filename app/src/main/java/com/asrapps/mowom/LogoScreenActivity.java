package com.asrapps.mowom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.asrapps.mowom.base.AppMainTabActivity;
import com.asrapps.mowom.constants.ConstantFunction;
import com.squareup.picasso.Picasso;

/**
 * Created by richa on 18/10/16.
 */

public class LogoScreenActivity extends Activity {

    private ImageView ivLoadLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_screen);

        ivLoadLogo = (ImageView) findViewById(R.id.ivLoadLogo);

        String logo_url = ConstantFunction.getstatus(LogoScreenActivity.this, "client_logo");
        Picasso.with(getApplicationContext()).load(logo_url).into(ivLoadLogo);

        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(3 * 1000);

                    // After 5 seconds redirect to another intent
                    Intent intent = new Intent(LogoScreenActivity.this, AppMainTabActivity.class);
                    startActivity(intent);
                    finish();

                } catch (Exception e) {

                }
            }
        };

        // start thread
        background.start();
    }
}
