package com.asrapps.mowom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.helper.CircleTransform;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.Login;
import com.asrapps.utils.MowomLogFile;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;


public class Dashboard_Activity extends Activity implements
        AsyncResponseListener, OnCancelListener {

    TextView pending_task_count, rejected_task_count,
            completed_task_count, alert_task_count, special_task_count;//txtHeaderUserName
    ImageView headerUserImage;
    private ProgressHUD mProgressHUD;

    String selectedLanguage;
    ConnectionDetector internetConnection;
    SQLController sqlcon;
    private ArrayList<Login> mLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout);

        internetConnection = new ConnectionDetector(Dashboard_Activity.this);
        sqlcon = new SQLController(Dashboard_Activity.this);

		/*txtHeaderUserName = (TextView) findViewById(R.id.txtHeaderUserName);*/
        pending_task_count = (TextView) findViewById(R.id.pending_task_count);
        rejected_task_count = (TextView) findViewById(R.id.rejected_task_count);

        completed_task_count = (TextView) findViewById(R.id.completed_task_count);
        alert_task_count = (TextView) findViewById(R.id.alert_task_count);
        special_task_count = (TextView) findViewById(R.id.special_task_count);
        headerUserImage = (ImageView) findViewById(R.id.headerUserImage);

//        SetUserDetails();
//        GetDashBoard();

        if (internetConnection.isConnectingToInternet("")) {
            SetUserDetails();
            GetDashBoard();
        } else {
            new ProfileTask(ConstantFunction.getuser(Dashboard_Activity.this, "UserId")).execute();
        }
    }

    private void SetUserDetails() {
        /*txtHeaderUserName.setText(ConstantFunction.getuser(this,
                "UserHeaderName"));*/
        String userImage = ConstantFunction.getuser(Dashboard_Activity.this, "client_logo");
        if (!userImage.equalsIgnoreCase("")) {
            Picasso.with(this)
                    .load(userImage)
                    .transform(new CircleTransform()).resize(100, 100)
                    .error(R.drawable.icon_no_preview).into(headerUserImage);
        } else {
            new ProfileTask(ConstantFunction.getuser(Dashboard_Activity.this, "UserId")).execute();
        }
    }

    private void GetDashBoard() {

        JSONObject requestObject = new JSONObject();
        try {
            String id = ConstantFunction.getuser(this, "UserId");
            requestObject.put("id", id);
        } catch (JSONException e) {
            Log.d("TTT", "Exception while signing in" + e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(this, "Loading...", true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.DashBoardURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Dashboard_Activity :- " + "\n" + ConstantValues.DashBoardURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }



    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        // TODO Auto-generated method stub

        if(mProgressHUD!=null) {
            mProgressHUD.dismiss();
        }

        Log.e("Response app lang==>", response);
        MowomLogFile.writeToLog("\n" + "Dashboard_Activity :- " + "\n" + ConstantValues.DashBoardURL + " -------------" + response);
        if (requestURL.equals(ConstantValues.DashBoardURL) && !response.equalsIgnoreCase("")) {
            Log.e("Response dashboard==>", response);
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

//                if (status.equalsIgnoreCase("success")) {
                if (result_code.equalsIgnoreCase("1")) {
                    try {

                        JSONObject job2 = job.getJSONObject("data");


                        selectedLanguage = ConstantFunction.CheckLanguage(Dashboard_Activity.this);

                        if (selectedLanguage.equalsIgnoreCase("")) {
                            selectedLanguage = "en";
                        }

                        if (selectedLanguage.equalsIgnoreCase("pr")) {
                            pending_task_count.setText(toPersianNumber(job2.get("pending_tasks")
                                    .toString()));
                            rejected_task_count.setText(toPersianNumber(job2.get("rejected_tasks")
                                    .toString()));
                            completed_task_count.setText(toPersianNumber(job2
                                    .get("completed_tasks").toString()));
                            alert_task_count.setText(toPersianNumber(job2.get("alerts").toString()));
                            special_task_count.setText(toPersianNumber(job2.get("special_jobs")
                                    .toString()));
                        } else {
                            pending_task_count.setText(job2.get("pending_tasks")
                                    .toString());
                            rejected_task_count.setText(job2.get("rejected_tasks")
                                    .toString());
                            completed_task_count.setText(job2
                                    .get("completed_tasks").toString());
                            alert_task_count.setText(job2.get("alerts").toString());
                            special_task_count.setText(job2.get("special_jobs")
                                    .toString());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // GoDashBoard();

                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Login success", false);
                } else if (result_code.equalsIgnoreCase("17")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard_Activity.this);
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("19")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard_Activity.this);
                        alertDialogBuilder.setMessage(getString(R.string.code_19));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (result_code.equalsIgnoreCase("400")) {
                    if (status.equalsIgnoreCase("User does not Exist.")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard_Activity.this);
                            alertDialogBuilder.setMessage(getString(R.string.err_400));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                    ConstantFunction.ChangeLang(getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(Dashboard_Activity.this);

                                    startActivity(new Intent(Dashboard_Activity.this, LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (result_code.equalsIgnoreCase("401")) {
                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard_Activity.this);
                        alertDialogBuilder.setMessage(status + "");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else {
                    Toast toast = Toast.makeText(Dashboard_Activity.this,
                            getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

            } catch (Exception e) {
                Toast toast = Toast.makeText(Dashboard_Activity.this,
                        getString(R.string.please_try_again), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                // ConstantFunction.showAlertDialog(LoginActivity.this,
                // "Try again", false);
            }
        }else {
            Toast toast = Toast.makeText(Dashboard_Activity.this,
                    getString(R.string.please_try_again), Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "Login failed", false);
        }
    }

    private static String[] persianNumbers = new String[]{"۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"};

    public static String toPersianNumber(String text) {
        if (text.length() == 0) {
            return "";
        }
        StringBuilder out = new StringBuilder();
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if ('0' <= c && c <= '9') {
                int number = Integer.parseInt(String.valueOf(c));
                out.append(persianNumbers[number]);
            } else if (c == '٫') {
                out.append('،');
            } else {
                out.append(c);
            }


        }
        return out.toString() + "";
    }

    private class ProfileTask extends AsyncTask<Void, Void, Void> {
        private String uId;
        private Bitmap myBitmap = null;

        public ProfileTask(String userId) {
            this.uId = userId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                sqlcon.open();
                mLogin = sqlcon.GetUserLoginByUserId(uId);
                Log.v("profile successfully", mLogin.size() + "");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mLogin != null && mLogin.size() > 0) {
                if (!mLogin.get(0).client_logo.equals("") && mLogin.get(0).client_logo != null) {
                    Uri uri = Uri.fromFile(new File(mLogin.get(0).client_logo));

                    ConstantFunction.savestatus(Dashboard_Activity.this,
                            "UserId", mLogin.get(0).emp_id + "");
                    Log.v("userId", mLogin.get(0).emp_id);


                    Picasso.with(Dashboard_Activity.this)
                            .load(uri)
                            .transform(new CircleTransform()).resize(100, 100)
                            .into(headerUserImage);
                }
            }
        }
    }
}
