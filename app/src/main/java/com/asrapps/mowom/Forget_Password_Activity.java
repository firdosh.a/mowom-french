package com.asrapps.mowom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.constants.ProgressHUD;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.utils.MowomLogFile;

import org.json.JSONException;
import org.json.JSONObject;

public class Forget_Password_Activity extends Activity implements
        OnClickListener, AsyncResponseListener, OnCancelListener {

    private Button ForgotPwdsubmitBtn;
    private ImageView backButtonImage;
    private TextView ForgotPwdLabel;
    private EditText ForgotPwd_email_txtBox;

    private ProgressHUD mProgressHUD;
    ConnectionDetector internetConnection;
    String selectedLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forget_password_layout);
        ForgotPwdsubmitBtn = (Button) findViewById(R.id.ForgotPwdsubmitBtn);
        ForgotPwdsubmitBtn.setOnClickListener(this);
        backButtonImage = (ImageView) findViewById(R.id.backButtonImage);
        backButtonImage.setOnClickListener(this);
        ForgotPwdLabel = (TextView) findViewById(R.id.ForgotPwdLabel);
        ForgotPwdLabel.setVisibility(View.GONE);
        ForgotPwd_email_txtBox = (EditText) findViewById(R.id.ForgotPwd_email_txtBox);
        selectedLanguage = ConstantFunction.CheckLanguage(Forget_Password_Activity.this);

        if (selectedLanguage.equalsIgnoreCase("")) {
            selectedLanguage = "en";
        }

        if (selectedLanguage.equalsIgnoreCase("pr")) {
            Drawable imgUser = getResources().getDrawable(R.drawable.email_id_icon);
            ForgotPwd_email_txtBox.setCompoundDrawablesWithIntrinsicBounds(null, null, imgUser, null);

            ForgotPwd_email_txtBox.setGravity(Gravity.RIGHT);
            ForgotPwdLabel.setGravity(Gravity.RIGHT);
        }

        internetConnection = new ConnectionDetector(this);
//		Test();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ForgotPwdsubmitBtn:
                if (internetConnection
                        .isConnectingToInternet(getString(R.string.check_connection)))
                    Submit();
                break;

            case R.id.backButtonImage:
                finish();
                break;

            default:
                break;
        }
    }

    private void Submit() {
        String enteredEmail = ForgotPwd_email_txtBox.getText().toString();
        if (!enteredEmail.equalsIgnoreCase("")) {
            if (ConstantFunction.isEmailValid(enteredEmail))
                ForGetPassword(enteredEmail);
            else {
                Toast toast = Toast.makeText(this, "Please Enter Valid Email Id",
                        Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        } else {
            Toast toast = Toast.makeText(this, "Please Enter the Email Id",
                    Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    private void Test() {

        JSONObject requestObject = new JSONObject();
        try {

            requestObject.put("team", "1");
            requestObject.put("sports", "7,5,8,9");


        } catch (JSONException e) {
            Log.d("Exception while sending in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(this, getString(R.string.loading), true, false, this);
        WebServiceHelper wsHelper = new WebServiceHelper(
                "http://unitygames2015.com/admin/web/v1/top/stories/0");
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Forget_Password_Activity :- " + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }


    private void ForGetPassword(String enteredEmail) {

        JSONObject requestObject = new JSONObject();
        try {

            requestObject.put("email", enteredEmail);
        } catch (JSONException e) {
            Log.d("Exception while sending in", e.toString());
            e.printStackTrace();
        }

        mProgressHUD = ProgressHUD.show(this, getString(R.string.loading), true, false, this);
        WebServiceHelper wsHelper = new WebServiceHelper(
                ConstantValues.ForgetPasswordURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFile.writeToLog("\n" + "Forget_Password_Activity :- " + ConstantValues.ForgetPasswordURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // TODO Auto-generated method stub
        mProgressHUD.dismiss();
    }

    @Override
    public void processFinish(String requestURL, String response) {

        mProgressHUD.dismiss();
        MowomLogFile.writeToLog("\n" + "Forget_Password_Activity :- " + ConstantValues.ForgetPasswordURL + " -------------" + response);
        if (!response.equalsIgnoreCase("")) {
            try {
                JSONObject job = new JSONObject(response);
                String status = job.getString("status");
                String result_code = job.getString("code");

                if (status.equalsIgnoreCase("success")) {
                    try {

                        // JSONObject job2 = job.getJSONObject("data");
                        ForgotPwdLabel.setVisibility(View.VISIBLE);
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Forget_Password_Activity.this);
                            alertDialogBuilder.setMessage(getString(R.string.password_will_send));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();
                                    GoHome();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Login success", false);
                } else if (result_code.equalsIgnoreCase("17")) {

                    try {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Forget_Password_Activity.this);
                        alertDialogBuilder.setMessage(getString(R.string.code_17));
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();

                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                } else if (status.equalsIgnoreCase("Failed")) {
                    String reason = job.getString("reason");
                    Toast toast = Toast.makeText(this,
                            reason + "\nPlease Try Again Later!!..",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            } catch (Exception e) {
                Toast toast = Toast.makeText(this, "Try again", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

            }
        } else {
            // ConstantFunction.showAlertDialog(LoginActivity.this,
            // "failed", false);
        }

    }

    private void GoHome() {
        ConstantFunction.ChangeLang(getApplicationContext(), "en");
        ConstantFunction.LoadLocale(Forget_Password_Activity.this);

        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
