package com.asrapps.mowom.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.asrapps.mowom.R;
import com.asrapps.mowom.broadcost.MowomService;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.fragments.AppTab_Dashboard_Home;
import com.asrapps.mowom.fragments.AppTab_Logout_Home;
import com.asrapps.mowom.fragments.AppTab_Profile_Home;
import com.asrapps.mowom.fragments.AppTab_Settings_Home;
import com.asrapps.mowom.view.AndroidBug5497Workaround;

import java.util.HashMap;
import java.util.Stack;

public class AppMainTabActivity extends FragmentActivity {

    Activity activity;
    /* Your Tab host */
    private TabHost mTabHost;

    private static final int REQUEST_LOCATION = 113;

    /* A HashMap of stacks, where we use tab identifier as keys.. */
    private HashMap<String, Stack<Fragment>> mStacks;

    /* Save current tabs identifier in this.. */
    private String mCurrentTab;
    boolean doubleBackToExitPressedOnce = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page_tab_layout);

        AndroidBug5497Workaround.assistActivity(this);

		/*
         * Navigation stacks for each tab gets created.. tab identifier is used
		 * as key to get respective stack for each tab
		 */
        mStacks = new HashMap<String, Stack<Fragment>>();
        mStacks.put(ConstantValues.TAB_DASHBOARD, new Stack<Fragment>());
        mStacks.put(ConstantValues.TAB_PROFILE, new Stack<Fragment>());
        mStacks.put(ConstantValues.TAB_SETTINGS, new Stack<Fragment>());
        mStacks.put(ConstantValues.TAB_LOGOUT, new Stack<Fragment>());

        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setOnTabChangedListener(listener);

        mTabHost.setup();
        activity = (Activity) this;
        initializeTabs();

        mTabHost.getTabWidget().getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ClearAllPages();
                mCurrentTab = ConstantValues.TAB_DASHBOARD;

                mTabHost.setCurrentTab(0);

                pushFragments(ConstantValues.TAB_DASHBOARD, new AppTab_Dashboard_Home(), false,
                        true);

                //ClearPageUptoOne();
            }
        });

        mTabHost.getTabWidget().getChildAt(0).setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View v) {
                //Toast.makeText(getApplicationContext(), "long click", Toast.LENGTH_SHORT).show();

                ClearAllPages();

                mCurrentTab = ConstantValues.TAB_DASHBOARD;

                mTabHost.setCurrentTab(0);
                pushFragments(ConstantValues.TAB_DASHBOARD, new AppTab_Dashboard_Home(), false,
                        true);
//                ClearPageUptoOne();
                return true;
            }
        });

        if (ConstantFunction.getstatus(activity, "privacy").equals("true")) {
//            startService(new Intent(this, MowomService.class));
            //ConstantFunction.savestatus(this, "privacy", "false");

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION);
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(AppMainTabActivity.this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_LOCATION);

                    // MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                } else {
                    startService(new Intent(AppMainTabActivity.this, MowomService.class));
                }
            } else {
                startService(new Intent(AppMainTabActivity.this, MowomService.class));
            }
            //ConstantFunction.savestatus(this, "privacy", "false");
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_LOCATION: {
                startService(new Intent(AppMainTabActivity.this, MowomService.class));
            }
        }
    }

    @SuppressLint("InflateParams")
    @SuppressWarnings("deprecation")
    private View createTabView(final int id, String tabText) {
        View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
        imageView.setImageDrawable(getResources().getDrawable(id));
        TextView text = (TextView) view.findViewById(R.id.tab_text);
        text.setText(tabText);
        return view;
    }

    public void initializeTabs() {
        /* Setup your tab icons and content views.. Nothing special in this.. */
        TabHost.TabSpec spec = mTabHost
                .newTabSpec(ConstantValues.TAB_DASHBOARD);

        mTabHost.setCurrentTab(-3);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.tab_dashboard_state_btn, getString(R.string.tab_dashboard).toString()));
        mTabHost.addTab(spec);

        spec = mTabHost.newTabSpec(ConstantValues.TAB_PROFILE);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.tab_profile_state_btn,
                getString(R.string.tab_profile).toString()));
        mTabHost.addTab(spec);

        spec = mTabHost.newTabSpec(ConstantValues.TAB_SETTINGS);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.tab_settings_state_btn,
                getString(R.string.tab_setting).toString()));
        mTabHost.addTab(spec);

        spec = mTabHost.newTabSpec(ConstantValues.TAB_LOGOUT);
        spec.setContent(new TabHost.TabContentFactory() {
            public View createTabContent(String tag) {
                return findViewById(R.id.realtabcontent);
            }
        });
        spec.setIndicator(createTabView(R.drawable.tab_logout_state_btn,
                getString(R.string.tab_logout).toString()));
        mTabHost.addTab(spec);


    }


    /* Comes here when user switch tab, or we do programmatically */
    TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
        public void onTabChanged(String tabId) {
            /* Set current tab.. */
            mCurrentTab = tabId;

            Log.v("TTT", "tabId = " + tabId);

//            if (mStacks.get(tabId).size() == 0) {
                /*
                 * First time this tab is selected. So add first fragment of
				 * that tab. Dont need animation, so that argument is false. We
				 * are adding a new fragment which is not present in stack. So
				 * add to stack is true.
				 */
            try {
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (tabId.equals(ConstantValues.TAB_DASHBOARD)) {

                ClearAllPages();

                pushFragments(tabId, new AppTab_Dashboard_Home(), false,
                        true);
            } else if (tabId.equals(ConstantValues.TAB_PROFILE)) {
                pushFragments(tabId, new AppTab_Profile_Home(), false, true);
            } else if (tabId.equals(ConstantValues.TAB_SETTINGS)) {
                pushFragments(tabId, new AppTab_Settings_Home(), false,
                        true);
            } else if (tabId.equals(ConstantValues.TAB_LOGOUT)) {
                //ConstantFunction.Logout(activity);
                pushFragments(tabId, new AppTab_Logout_Home(),
                        false, true);
            }
//            }
//
//        else {
//                /*
//				 * We are switching tabs, and target tab is already has atleast
//				 * one fragment. No need of animation, no need of stack pushing.
//				 * Just show the target fragment
//				 */
//                pushFragments(tabId, mStacks.get(tabId).lastElement(), false,
//                        false);
//            }
        }
    };

    /*
     * Might be useful if we want to switch tab programmatically, from inside
     * any of the fragment.
     */
    public void setCurrentTab(int val) {
        mTabHost.setCurrentTab(val);
    }

    /*
     * To add fragment to a tab. tag -> Tab identifier fragment -> Fragment to
     * show, in tab identified by tag shouldAnimate -> should animate
     * transaction. false when we switch tabs, or adding first fragment to a tab
     * true when when we are pushing more fragment into navigation stack.
     * shouldAdd -> Should add to fragment navigation stack (mStacks.get(tag)).
     * false when we are switching tabs (except for the first time) true in all
     * other cases.
     */
    public void pushFragments(String tag, Fragment fragment,
                              boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }

    public void Replace(String tag, Fragment fragment
    ) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        //ft.replace(R.id.realtabcontent, fragment);

        ft.detach(fragment);
        ft.replace(R.id.realtabcontent, fragment);
        ft.attach(fragment);
        ft.commit();
    }

    public void popFragments() {
        Fragment fragment;
        if (mStacks.get(mCurrentTab).size() - 2 >= 0) {
            fragment = mStacks.get(mCurrentTab).elementAt(
                    mStacks.get(mCurrentTab).size() - 2);
            /*
         * Select the second last fragment in current tab's stack.. which will
		 * be shown after the fragment transaction given below
		 */

		/* pop current fragment from stack.. */
            mStacks.get(mCurrentTab).pop();

		/*
         * We have the target fragment in hand.. Just show it.. Show a standard
		 * navigation animation
		 */
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
            ft.replace(R.id.realtabcontent, fragment);
            ft.commit();
        }


    }

    @Override
    public void onBackPressed() {
        if (((BaseFragment) mStacks.get(mCurrentTab).lastElement())
                .onBackPressed() == false) {
			/*
			 * top fragment in current tab doesn't handles back press, we can do
			 * our thing, which is
			 * 
			 * if current tab has only one fragment in stack, ie first fragment
			 * is showing for this tab. finish the activity else pop to previous
			 * fragment in stack for the same tab
			 */
            if (mStacks.get(mCurrentTab).size() == 1) {

                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }


                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);

                //super.onBackPressed(); // or call finish..

            } else {
                System.out.println(".........................I am in ELSE");
                popFragments();
            }
        } else {
            // do nothing.. fragment already handled back button press.
        }
    }

    /*
     * Imagine if you wanted to get an image selected using ImagePicker intent
     * to the fragment. Ofcourse I could have created a public function in that
     * fragment, and called it from the activity. But couldn't resist myself.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mStacks.get(mCurrentTab).size() == 0) {
            return;
        }

		/* Now current fragment on screen gets onActivityResult callback.. */
        mStacks.get(mCurrentTab).lastElement()
                .onActivityResult(requestCode, resultCode, data);
    }

    public void ClearAllPages() {
        mStacks.get(mCurrentTab).clear();
    }

    public void ClearPageUptoOne() {
        int count = mStacks.get(mCurrentTab).size();

        for (int i = 1; i < count; i++)
            popFragments();


    }

//	Handler handler = new Handler();
//	private void PostLocation(){
//
//		if(ConstantFunction.getstatus(activity, "privacy").equals("true")){
//
//			final HttpClient httpClient = new DefaultHttpClient();
//			double latitude = 0;
//			double longitude = 0;
//			GPSTracker gps = new GPSTracker(AppMainTabActivity.this);
//
//			// check if GPS enabled
//			if(gps.canGetLocation()){
//
//				latitude = gps.getLatitude();
//				longitude = gps.getLongitude();
//
//				//\n is for new line
//				Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
//			}else{
//				// can't get location
//				// GPS or Network is not enabled
//				// Ask user to enable GPS/network in settings
//				gps.showSettingsAlert();
//			}
//
//			List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
//			nameValuePair.add(new BasicNameValuePair("emo_id", ConstantFunction.getuser(AppMainTabActivity.this, "UserId")));
//			nameValuePair.add(new BasicNameValuePair("lat", ""+latitude));
//			nameValuePair.add(new BasicNameValuePair("lang", ""+longitude));
//
//			final HttpPost httpPost = new HttpPost("http://mowom.eu/app/webapi/update_current_location.php");
//
//			//Encoding POST data
//			try {
//				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
//			} catch (UnsupportedEncodingException e) 
//			{
//				e.printStackTrace();
//			}
//
//			new Thread(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						HttpResponse response = httpClient.execute(httpPost);
//						// write response to log
//						Log.d("Http Post Response:", response.toString());
//					} catch (ClientProtocolException e) {
//						// Log exception
//						e.printStackTrace();
//					} catch (IOException e) {
//						// Log exception
//						e.printStackTrace();
//					} 
//				}
//			}).start();
//
//			handler.postDelayed(new Runnable() {
//				@Override
//				public void run() {
//					PostLocation();
//				}
//			}, 5000);
//		}
//	}
}
