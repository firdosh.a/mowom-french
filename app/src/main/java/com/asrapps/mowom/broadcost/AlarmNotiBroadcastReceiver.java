package com.asrapps.mowom.broadcost;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;

import com.asrapps.mowom.R;

/**
 * Created by mital on 11/5/17.
 */

public class AlarmNotiBroadcastReceiver extends BroadcastReceiver {
    MediaPlayer mp;
    @Override
    public void onReceive(Context context, Intent intent) {
        android.util.Log.v("TTT","alarm");
        mp=MediaPlayer.create(context, R.raw.mowom_noti_sound   );
        mp.start();

    }
}