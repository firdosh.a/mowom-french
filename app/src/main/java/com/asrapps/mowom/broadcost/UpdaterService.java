package com.asrapps.mowom.broadcost;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class UpdaterService extends Service {
    Updater updater;
    BroadcastReceiver broadcaster;
    public static boolean inProcess = false;
    public static boolean firstTime = true;
    Intent intent;
    static final public String BROADCAST_ACTION = "com.asrapps.mowom.broadcost";
    public static String currentTime = "";

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        updater = new Updater();
        Log.d("UpdaterService", "Created inProcess " + inProcess);
        //showMSg("Created");
//        if (inProcess) {
//            currentTime = updater.getStartTime();
//            Log.d("UpdaterService", "UpdaterService Created currentTime " + currentTime + " inProcess = " + inProcess);
//            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//            SharedPreferences.Editor editor = prefs.edit();
//            editor.putString("currentTime", UpdaterService.currentTime + "");
//            editor.commit();
//
//        } else {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            UpdaterService.currentTime = preferences.getString("currentTime", "");

            Log.d("UpdaterService", "UpdaterService Created currentTime " + currentTime + " inProcess = " + inProcess);

            if (currentTime.length() == 0) {
                currentTime = updater.getStartTime();
            }
//            currentTime = updater.getResumeStartTime();
//        }

        intent = new Intent(BROADCAST_ACTION);
    }

    @Override
    public synchronized int onStartCommand(Intent intent, int flags, int startId) {

        if (!updater.isRunning()) {
            updater.start();
            Log.d("UpdaterService", "Started");
            //showMSg("Started");
            updater.isRunning = true;
//            UpdaterService.inProcess = false;
//            UpdaterService.firstTime = false;
        }

//		return super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }


    @Override
    public synchronized void onDestroy() {

        super.onDestroy();
        Log.v("UpdaterService", "Update service Destroy");

        if (updater.isRunning) {
            updater.interrupt();
            Log.d("UpdaterService", "Destroyed");
            //showMSg("Destroyed");
            updater.isRunning = false;
            Log.v("UpdaterService", "startedTimer in background");
            updater = null;
        }

    }

    class Updater extends Thread {

        public String getTime() {
            String taskStartTime = "";
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss",
                    Locale.getDefault());
            taskStartTime = sdf.format(c.getTime());
            return taskStartTime;
        }

        public String getStartTime() {
            String taskStartTime = "";
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                    Locale.getDefault());
            taskStartTime = sdf.format(c.getTime());
            return taskStartTime;
        }

        public String getResumeStartTime() {
            String taskStartTime = "";
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS",
                    Locale.getDefault());
            taskStartTime = sdf.format(c.getTime());
            return taskStartTime;
        }


        public boolean isRunning = false;
        public long DELAY = 1000;

        // int i = 0;

        @Override
        public void run() {
            super.run();
            try {
                isRunning = true;
                while (isRunning) {
                    Log.d("UpdaterService", "Running...");

                    // sendbroadcast
                    try {
                        sendResult(getTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        Thread.sleep(DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        isRunning = false;
                    }
                } // while end
            } // run end

            catch (Exception ex) {
                Log.i("UpdaterService==> ", ex.getMessage());
            }
        }

        public boolean isRunning() {
            return this.isRunning;
        }

    }

    public void sendResult(String message) {
        System.out.println("counter==> " + message);
        System.out.println("currentTime==> " + currentTime);
        intent.putExtra("counter", message);
        intent.putExtra("time", currentTime);
        sendBroadcast(intent);
    }

	/*public void showMSg(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}*/

}
