package com.asrapps.mowom.broadcost;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.asrapps.dbhelper.SQLController;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.R;
import com.asrapps.mowom.base.AppMainTabActivity;
import com.asrapps.mowom.constants.AppConstants;
import com.asrapps.mowom.constants.ConnectionDetector;
import com.asrapps.mowom.constants.ConstantFunction;
import com.asrapps.mowom.constants.ConstantValues;
import com.asrapps.mowom.helper.WebServiceHelper;
import com.asrapps.mowom.interfaces.AsyncResponseListener;
import com.asrapps.mowom.model.AddSalesLocation;
import com.asrapps.mowom.model.LocationOffline;
import com.asrapps.mowom.model.LocationOfflineGroup;
import com.asrapps.utils.MowomLogFileLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.asrapps.mowom.constants.AppConstants.et_id;
import static com.asrapps.mowom.constants.ConstantFunction.getuser;
import static com.asrapps.mowom.constants.ConstantValues.BetteryStatusURL;
import static com.asrapps.mowom.constants.ConstantValues.OFFLINELOCATIONURL;
import static com.asrapps.mowom.constants.ConstantValues.TASK_ROUTE_LOCATION;
import static com.asrapps.mowom.constants.ConstantValues.UpdateLocURL;

public class MowomService extends Service implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener, AsyncResponseListener, OnCancelListener {

    private GoogleApiClient mGoogleApiClient;

    ArrayList<LocationOffline> allLoaction = new ArrayList<>();

    ConnectionDetector internetConnection;
    SQLController sqlcon;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 10000;

    private Handler customHandler = new Handler();
    private Handler customHandlerBettery = new Handler();

    private LocationRequest mLocationRequest;
    SharedPreferences sharedPref;
    private static int UPDATE_INTERVAL = 3000;
    private static int BATTERY_INTERVAL = 1000 * 60 * 20;
    private static int FATEST_INTERVAL = 3000;
    private static int DISPLACEMENT = 3;

//    private static int UPDATE_INTERVAL = 30000; // 10 sec
//    private static int BATTERY_INTERVAL = 1000 * 60 * 20; // 10 sec
//    private static int FATEST_INTERVAL = 10000; // 10 sec
//    private static int DISPLACEMENT = 100; // 100 meters


    private Location mLastLocation;
    BatteryLevelReceiver receiver;
    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

    @Override
    public IBinder onBind(Intent arg0) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPref = getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
        StartTheProcess();
        Log.v("task_id_location", "service Started");
        return START_STICKY;
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            Log.v("task_id_location", "Permission issue");

            return;
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();

            Log.e("Offline", "latitude = " + latitude);
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            MowomLogFileLocation.writeToLog("\n" + "MowomService :-" + currentDateTimeString + "=====" + "-------------Offline latitude = " + latitude);
            Log.e("Offline", "longitude = " + longitude);
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            MowomLogFileLocation.writeToLog("\n" + "MowomService :-" + currentDateTimeString + "=====" + " -------------Offline longitude = " + longitude);

            if (ConstantFunction.getstatus(getApplicationContext(), "is_sales_task") != null && ConstantFunction.getstatus(getApplicationContext(), "is_sales_task").equals("1")) {
                if (ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id) != null && ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id) == "0") {
                    AddSalesLocation loc = new AddSalesLocation();
                    loc.emp_id = ConstantFunction.getuser(getApplicationContext(), "UserId");
                    loc.trans_id = ConstantFunction.getuser(getApplicationContext(), "sales_task_id");
                    loc.latitude = String.valueOf(mLastLocation.getLatitude());
                    loc.longitude = String.valueOf(mLastLocation.getLongitude());

                    sqlcon.open();
                    sqlcon.addSalesLoactionOffline(loc);
                }
            }

            if (internetConnection
                    .isConnectingToInternet("")) {

                Log.e("Offline", "internet ON");

                if(!AppConstants.task_Id.equals("0") && !AppConstants.task_Id.equalsIgnoreCase("taskid")) {
                    sqlcon.open();
                    allLoaction = sqlcon.GetLocationOffline(ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id));

                    if (allLoaction.size() == 0) {
                        Log.e("Offline", "internet ON and DB size 0");
                        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                        MowomLogFileLocation.writeToLog("\n" + "MowomService :-" + currentDateTimeString + "=====" + " -------------internet ON and DB size 0 ");
                        String task_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id);
                        SendToServer(mLastLocation);
                        Log.v("task_id_location", "SendToServer " + latitude);
                        Log.v("task_id_location", "SendToServer " + longitude);
                    } else {
                        Log.e("Offline", "internet ON and Db have record = " + allLoaction.size());
                        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                        MowomLogFileLocation.writeToLog("\n" + "MowomService :-RP$$$$$$$$--------------- " + currentDateTimeString + "=====" + "-------------Offline internet ON and Db have record = " + allLoaction.size() + "");

                        String task_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id);
                        Log.e("Offline", "task_id = " + task_id);

                        if (task_id != null && !task_id.equals("") && !String.valueOf(mLastLocation.getLatitude()).equals("") && !String.valueOf(mLastLocation.getLongitude()).equals("")) {
                            LocationOffline loc = new LocationOffline();
                            loc.emp_id = ConstantFunction.getuser(getApplicationContext(), "UserId");
                            loc.task_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id);
                            loc.latitude = String.valueOf(mLastLocation.getLatitude());
                            loc.longitude = String.valueOf(mLastLocation.getLongitude());
                            loc.et_id = ConstantFunction.getuser(getApplicationContext(), et_id);

                            sqlcon.open();
                            sqlcon.addLoactionOffline(loc);
                        }

                        //sync offline location
                        sqlcon.open();
                        allLoaction = sqlcon.GetLocationOffline(ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id));
                        SyncOfflineLocation(allLoaction);
                    }
                }
                else
                {
                    //send all offline task location
                    sqlcon.open();
                    ArrayList<LocationOfflineGroup> allSync = sqlcon.GetLocationOfflineAll();
                    if(allSync.size()>0) {
                        for (int pp = 0; pp < allSync.size(); pp++) {
                            if (allSync.get(pp).lstLoc.size() > 0) {
                                SyncOfflineLocation(allSync.get(pp).lstLoc);
                            }
                        }
                    }

                    if(!ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id).equals("0") || !ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id).equalsIgnoreCase("taskid")) {
                        SendToServer(mLastLocation);
                    }


                }
//              SendToServer(mLastLocation);

            } else {

                Log.e("Offline", "Internet OFF");

                Log.v("latitude offline  RP$$$$$$$$---------------", "" + mLastLocation.getLatitude());
                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                MowomLogFileLocation.writeToLog("\n" + "MowomService :-  RP$$$$$$$$---------------" + currentDateTimeString + "=====" + " -------------latitude offline = " + mLastLocation.getLatitude() + "");
                Log.v("longitude offline", "" + mLastLocation.getLongitude());
                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                MowomLogFileLocation.writeToLog("\n" + "MowomService :-  RP$$$$$$$$---------------" + currentDateTimeString + "=====" + " -------------longitude offline = " + mLastLocation.getLongitude() + "");
                String task_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id);

                Log.e("Offline RP$$$$$$$$--------------- ", "task_id = " + task_id);
                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                MowomLogFileLocation.writeToLog("\n" + "MowomService :-  RP$$$$$$$$---------------" + currentDateTimeString + "=====" + " -------------offline task_id = " + task_id + "");

                if (task_id != null && !task_id.equals("")) {
                    LocationOffline loc = new LocationOffline();
                    loc.emp_id = ConstantFunction.getuser(getApplicationContext(), "UserId");
                    loc.task_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id);
                    loc.latitude = String.valueOf(mLastLocation.getLatitude());
                    loc.longitude = String.valueOf(mLastLocation.getLongitude());
                    loc.et_id = ConstantFunction.getuser(getApplicationContext(), et_id);

                    sqlcon.open();
                    sqlcon.addLoactionOffline(loc);
                }
            }


            //Toast.makeText(getApplicationContext(), "latitude==> \n\n"+latitude+""+longitude, Toast.LENGTH_SHORT).show();
            //lblLocation.setText(latitude + ", " + longitude);

        } else {
            // Toast.makeText(getApplicationContext(), "Couldn't get the location. Make sure location is enabled on the device, ",
            // + Toast.LENGTH_SHORT).show();

            //lblLocation.setText("(Couldn't get the location. Make sure location is enabled on the device)");
        }
    }


    private void SyncOfflineLocation(ArrayList<LocationOffline> allLoaction) {

        Log.e("Offline RP$$$$$$$$---------------", "OFFLINELOCATIONURL request");

        JSONObject requestObject = new JSONObject();
        try {
            String id = getuser(getApplicationContext(), "UserId");
            String task_id="0";
            String et_id="0";

            if(allLoaction.size()>0) {
                 task_id = allLoaction.get(0).task_id;
                 et_id = allLoaction.get(0).et_id;
            }


            requestObject.put("emp_id", id);
            requestObject.put("task_id", task_id);
            requestObject.put("emp_task_id", et_id);

            JSONArray arrayObj = new JSONArray();
            for (int r = 0; r < allLoaction.size(); r++) {

                JSONObject obj = new JSONObject();

                obj.put("lat", allLoaction.get(r).latitude);
                obj.put("long", allLoaction.get(r).longitude);
                arrayObj.put(obj);

            }

            requestObject.put("geopoints", arrayObj);

            Log.v("offline", "arrayObj = " + arrayObj);


        } catch (JSONException e) {
            Log.d("Exception", e.toString());
            e.printStackTrace();
        }

        WebServiceHelper wsHelper = new WebServiceHelper(
                OFFLINELOCATIONURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");

        Log.e("Offline", " offline req requestObject.toString() = " + requestObject.toString());
        wsHelper.setRequestBody(requestObject.toString());
        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "offline RP$$$$$$$$--------------- req requestObject MowomService :- " + currentDateTimeString + "=====" + OFFLINELOCATIONURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();


        try {
            sqlcon.open();
            sqlcon.deleteOfflineLocation(ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void displayBettryStatus() {
        if (ConstantValues.mProgressStatus != null) {
            if (internetConnection
                    .isConnectingToInternet("")) {

                BetteryStatusSendToServer(ConstantValues.mProgressStatus);

//
            }
        }
    }

    private void BetteryStatusSendToServer(String mProgressStatus) {

        JSONObject requestObject = new JSONObject();
        try {
            String id = getuser(getApplicationContext(), "UserId");

            requestObject.put("emp_id", id);
            requestObject.put("battery_last_status", ConstantValues.mProgressStatus + "%");

        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        WebServiceHelper wsHelper = new WebServiceHelper(
                BetteryStatusURL);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + BetteryStatusURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();

    }


    private boolean checkPlayServices() {
        try {
            int resultCode = GooglePlayServicesUtil
                    .isGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.SUCCESS) {
                if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                    GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) getApplicationContext(),
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "This device is not supported.", Toast.LENGTH_LONG)
                            .show();
                    //finish();
                }
                return false;
            }
        } catch (Exception e) {

        }
        return true;

    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
    }

    @Override
    public void onCreate() {

        internetConnection = new ConnectionDetector(getApplicationContext());
        sqlcon = new SQLController(getApplicationContext());

        //    Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG)
        //          .show();
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();

            createLocationRequest();
        }

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        receiver = new BatteryLevelReceiver();
        registerReceiver(receiver, filter);

        displayBettryStatus();

    }

    @Override
    public void onStart(Intent intent, int startId) {
        // For time consuming an long tasks you can launch a new thread here...
        //Toast.makeText(this, " Service Started", Toast.LENGTH_LONG).show();
        //    sharedPref = getSharedPreferences("My_prefs", Context.MODE_PRIVATE);
        //    StartTheProcess();
    }


    @Override
    public void onConnectionFailed(ConnectionResult arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onConnected(Bundle arg0) {
        // TODO Auto-generated method stub
        //displayLocation();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        mLastLocation = location;
        //displayLocation();
        /*Toast.makeText(getApplicationContext(), "Location changed!",
                Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onDestroy() {
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        StopGetingLocation();
        unregisterReceiver(receiver);
        StopBetteryStatus();

        startService(new Intent(this, MowomService.class));

    }

    private void BackgroundProcess() {
        customHandler.postDelayed(updateTimerThread, 10000);
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            //getLocation();
            if (mGoogleApiClient.isConnected()) {
                //startLocationUpdates();

                Log.v("task_id_location", "update location1");
                displayLocation();
            }
            /*Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT)
                    .show();*/
            customHandler.postDelayed(this, 10000);
        }
    };

    private Runnable updateTimerThreadBettrey = new Runnable() {

        public void run() {
            displayBettryStatus();

            if(ConstantValues.mProgressStatus!=null && ConstantValues.mProgressStatus.length()>0) {
                customHandlerBettery.postDelayed(this, BATTERY_INTERVAL);
            }
            else
            {
                customHandlerBettery.postDelayed(this, 1000);
            }
        }
    };

    private void StartTheProcess() {
        BackgroundProcess();
        BackGroundProcessBettery();
    }

    private void BackGroundProcessBettery() {

        if(ConstantValues.mProgressStatus.length()==0)
        {
            customHandlerBettery.postDelayed(updateTimerThreadBettrey, 1000);
        }
        else
        {
            customHandlerBettery.postDelayed(updateTimerThreadBettrey, BATTERY_INTERVAL);
        }
    }

    private void StopGetingLocation() {
        customHandler.removeCallbacks(updateTimerThread);
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void StopBetteryStatus() {
        customHandlerBettery.removeCallbacks(updateTimerThreadBettrey);
    }

    @Override
    public void onCancel(DialogInterface arg0) {

    }


    @Override
    public void processFinish(String requestURL, String response) {
        // TODO Auto-generated method stub

        //mProgressHUD.dismiss();
        Log.e("Response==>", response);
        if (requestURL.equalsIgnoreCase(OFFLINELOCATIONURL))
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- " + currentDateTimeString + "=====" + OFFLINELOCATIONURL + " -------------" + response);
        if (requestURL.equalsIgnoreCase(UpdateLocURL))
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + UpdateLocURL + " -------------" + response);
        if (requestURL.equalsIgnoreCase(ConstantValues.BetteryStatusURL))
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + ConstantValues.BetteryStatusURL + " -------------" + response);
        if (requestURL.equalsIgnoreCase(ConstantValues.TASK_ROUTE_LOCATION))
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + ConstantValues.TASK_ROUTE_LOCATION + " -------------" + response);

        if (requestURL.equalsIgnoreCase(OFFLINELOCATIONURL)) {
            currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
            MowomLogFileLocation.writeToLog("\n" + "MowomService :- RP$$$$$$$$--------------- " + currentDateTimeString + "=====" + ConstantValues.OFFLINELOCATIONURL + " -------------" + response);
            Log.e("Offline", " OFFLINELOCATIONURL response = " + response);
            if (!response.equalsIgnoreCase("")) {
                try {

                    JSONObject job = new JSONObject(response.toString());
                    String status = job.getString("status");
                    String result_code = job.getString("code");

//                    if (status.equalsIgnoreCase("success")) {
                    if (result_code.equalsIgnoreCase("1")) {
                        try {
                            sqlcon.open();
                            sqlcon.deleteOfflineLocation(ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("18")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                            alertDialogBuilder.setMessage(getString(R.string.code_18));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();
                                    ConstantFunction.ChangeLang(getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getApplicationContext());

                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();
                                        ConstantFunction.ChangeLang(getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getApplicationContext());

                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
//                        Toast toast = Toast.makeText(getApplicationContext(),
//                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            if (requestURL.equalsIgnoreCase(ConstantValues.TASK_ROUTE_LOCATION)) {
                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + TASK_ROUTE_LOCATION + " -------------" + response);
                Log.e("Offline", " TASK_ROUTE_LOCATION response = " + response);
            }
            if (requestURL.equalsIgnoreCase(UpdateLocURL)) {
                currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + UpdateLocURL + " -------------" + response);
                Log.e("Offline", " UpdateLocURL response = " + response);
            }

            if (!response.equalsIgnoreCase("")) {
                try {
                    JSONObject job = new JSONObject(response);
                    String status = job.getString("status");
                    String result_code = job.getString("code");

//                    if (status.equalsIgnoreCase("success")) {
                    if (result_code.equalsIgnoreCase("1")) {
                        try {
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // GoDashBoard();
                        // ConstantFunction.showAlertDialog(LoginActivity.this,
                        // "Login success", false);
                    } else if (result_code.equalsIgnoreCase("17")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                            alertDialogBuilder.setMessage(getString(R.string.code_17));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("18")) {

                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                            alertDialogBuilder.setMessage(getString(R.string.code_18));
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();
                                    ConstantFunction.ChangeLang(getApplicationContext(), "en");
                                    ConstantFunction.LoadLocale(getApplicationContext());

                                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    } else if (result_code.equalsIgnoreCase("400")) {
                        if (status.equalsIgnoreCase("User does not Exist.")) {
                            try {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                                alertDialogBuilder.setMessage(getString(R.string.err_400));
                                alertDialogBuilder.setCancelable(false);

                                alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();
                                        ConstantFunction.ChangeLang(getApplicationContext(), "en");
                                        ConstantFunction.LoadLocale(getApplicationContext());

                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                                    }
                                });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else if (result_code.equalsIgnoreCase("401")) {
                        try {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
                            alertDialogBuilder.setMessage(status + "");
                            alertDialogBuilder.setCancelable(false);

                            alertDialogBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int arg1) {
                                    dialog.dismiss();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
//                        Toast toast = Toast.makeText(getApplicationContext(),
//                                getString(R.string.please_try_again), Toast.LENGTH_SHORT);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//                        toast.show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    // ConstantFunction.showAlertDialog(LoginActivity.this,
                    // "Try again", false);
                }
            } else {
                // ConstantFunction.showAlertDialog(LoginActivity.this,
                // "Login failed", false);
            }
        }


    }

    private void SendToServer(Location location) {

        Log.v("Offline", "TASK_ROUTE_LOCATION request");

        JSONObject requestObject = new JSONObject();
        try {
            String id = getuser(getApplicationContext(), "UserId");

            requestObject.put("emp_id", id);
            requestObject.put("lat", String.valueOf(location.getLatitude()));
            requestObject.put("lang", String.valueOf(location.getLongitude()));

        } catch (JSONException e) {
            Log.d("Exception", e.toString());
            e.printStackTrace();
        }

        //mProgressHUD = ProgressHUD.show(mcontext, "Loading...", true, false, this);

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(
                UpdateLocURL.trim());
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + UpdateLocURL + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();

        updateLocation(mLastLocation);
    }

    private void updateLocation(Location location) {
        Log.v("task_id_location", "updateLocation");


//        Toast.makeText(getApplicationContext(),"location = "+location.getLongitude()+","+location.getLatitude(),Toast.LENGTH_SHORT).show();


        Log.d("updateLocation", "updateLocation");
        JSONObject requestObject = new JSONObject();
        try {
            String id = getuser(getApplicationContext(), "UserId");

            requestObject.put("emp_id", id);
            requestObject.put("lat", String.valueOf(location.getLatitude()));
            requestObject.put("lang", String.valueOf(location.getLongitude()));


            String task_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.task_Id);
            String et_id = ConstantFunction.getuser(getApplicationContext(), AppConstants.et_id);
            String task_id1 = String.valueOf(sharedPref.getString("task_id", "0"));
            Log.v("Task_id", task_id + " " + task_id1);

            requestObject.put("emp_task_id", et_id);
            requestObject.put("task_id", task_id);
            Log.v("task_id_location", task_id);

        } catch (JSONException e) {
            Log.d("Exception while signing in", e.toString());
            e.printStackTrace();
        }

        // Call web service
        WebServiceHelper wsHelper = new WebServiceHelper(TASK_ROUTE_LOCATION);
        wsHelper.addHeader(ConstantValues.kWSHeaderContentType,
                "application/json");
        wsHelper.setRequestBody(requestObject.toString());
        MowomLogFileLocation.writeToLog("\n" + "MowomService :- " + currentDateTimeString + "=====" + TASK_ROUTE_LOCATION + " -------------" + requestObject.toString());
        wsHelper.delegate = this;
        wsHelper.execute();

        Log.d("updateLocation", requestObject.toString());
    }

    // Initialize a new BroadcastReceiver instance


}