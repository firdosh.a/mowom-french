package com.asrapps.mowom.constants.flow;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.text.Layout;
import android.text.style.LeadingMarginSpan;

/**
 * Created by qtm-kalpesh on 16/7/16.
 */
public class MyLeadingMarginSpan2 implements LeadingMarginSpan.LeadingMarginSpan2 {
    private int margin;
    private int lines;
    private boolean wasDrawCalled = false;
    private int drawLineCount = 0;

    public MyLeadingMarginSpan2(int lines, int margin) {
        this.margin = margin;
        this.lines = lines;
    }

    @Override
    public int getLeadingMargin(boolean first) {
        boolean isFirstMargin = first;
        // a different algorithm for api 21+
        if (Build.VERSION.SDK_INT >= 21) {
            this.drawLineCount = this.wasDrawCalled ? this.drawLineCount + 1 : 0;
            this.wasDrawCalled = false;
            isFirstMargin = this.drawLineCount <= this.lines;
        }

        return isFirstMargin ? this.margin : 0;
    }

    /**
     * Renders the leading margin.  This is called before the margin has been
     * adjusted by the value returned by {@link #getLeadingMargin(boolean)}.
     *
     * @param c        the canvas
     * @param p        the paint. The this should be left unchanged on exit.
     * @param x        the current position of the margin
     * @param dir      the base direction of the paragraph; if negative, the margin
     *                 is to the right of the text, otherwise it is to the left.
     * @param top      the top of the line
     * @param baseline the baseline of the line
     * @param bottom   the bottom of the line
     * @param text     the text
     * @param start    the start of the line
     * @param end      the end of the line
     * @param first    true if this is the first line of its paragraph
     * @param layout   the layout containing this line
     */
    @Override
    public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {
        this.wasDrawCalled = true;
    }


    @Override
    public int getLeadingMarginLineCount() {
        return this.lines;
    }
}