package com.asrapps.mowom.constants;

import android.content.Context;
import android.net.ConnectivityManager;
import android.view.Gravity;
import android.widget.Toast;

public class ConnectionDetector {

	private Context _context;

	public ConnectionDetector(Context context){
		this._context = context;
	}

	public boolean isConnectingToInternet(String msg){

		try {
			ConnectivityManager cm = (ConnectivityManager) _context
					.getSystemService(Context.CONNECTIVITY_SERVICE);

			if (cm != null && cm.getActiveNetworkInfo() != null
					&& cm.getActiveNetworkInfo().isConnected())
				return true;
			else {

				if(msg.length()>0) {
					Toast toast = Toast.makeText(_context,
							msg,
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();

			if(msg.length()>0) {
				Toast toast = Toast.makeText(_context,
						msg,
						Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
			return false;
		}
//		ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
//		if (connectivity != null)
//		{
//			NetworkInfo[] info = connectivity.getAllNetworkInfo();
//			if (info != null)
//				for (int i = 0; i < info.length; i++)
//					if (info[i].getState() == NetworkInfo.State.CONNECTED)
//					{
//						return true;
//					}
//		}
//		if(msg.length()>0) {
//			Toast toast = Toast.makeText(_context,
//					msg,
//					Toast.LENGTH_SHORT);
//			toast.setGravity(Gravity.CENTER, 0, 0);
//			toast.show();
//		}
//
//
//		return false;
	}
}