package com.asrapps.mowom.constants;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Patterns;
import android.view.Gravity;
import android.widget.Toast;

import com.asrapps.mowom.R;
import com.asrapps.mowom.LoginActivity;
import com.asrapps.mowom.broadcost.MowomService;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConstantFunction {

    public static final boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static void savestatus(Context ctx, String tag, String value) {
        try {

            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(ctx);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(tag, value);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getstatus(Context ctx, String tag) {
        try {

//			SharedPreferences sharedPreferences = PreferenceManager
//					.getDefaultSharedPreferences(ctx);
//			SharedPreferences.Editor editor = sharedPreferences.edit();
//			editor.putString(tag, value);
//			editor.commit();
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(ctx);
            String status = sharedPreferences.getString(tag, "");
            return status;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /* get value from shared pref */
    public static String getuser(Context context, String tag) {
        try {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            String user = sharedPreferences.getString(tag, "");
            return user;
        } catch (Exception e) {
            return "";
        }
    }

    public static void Logout(final Activity ctx) {

        SessionManager.putLoginDetail(null, ctx);

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(ctx.getString(R.string.do_you_want_logout));
        builder.setPositiveButton(ctx.getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                ConstantFunction.savestatus(ctx, "UserId", "");
                ConstantFunction.savestatus(ctx, "UserHeaderName", "");

                ConstantFunction.savestatus(ctx, "UserName", "");

                ConstantFunction.savestatus(ctx, "UserPhone", "");
                ConstantFunction.savestatus(ctx, "UserEmail", "");

                ConstantFunction.savestatus(ctx, "UserImage", "");
                ctx.stopService(new Intent(ctx, MowomService.class));

                ConstantFunction.ChangeLang(ctx, "en");
                ConstantFunction.LoadLocale(ctx);

                Intent intent = new Intent(ctx, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                ctx.startActivity(intent);
                ctx.finish();
            }

        });
        builder.setNegativeButton(ctx.getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub
            }
        });

        builder.show();
    }

    public static void buildAlertMessageNoGps(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setMessage(
                mActivity.getString(R.string.gps_enable))
                .setCancelable(false)
                .setPositiveButton(mActivity.getString(R.string.yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog,
                                                final int id) {
                                mActivity
                                        .startActivity(new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                .setNegativeButton(mActivity.getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @SuppressLint("SimpleDateFormat")
    public static String GetDateFormat(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy, hh:mm a");
        return outFormat.format(dtIn);
    }

    @SuppressLint("SimpleDateFormat")
    public static String GetDateFormat1(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "dd/MM/yyyy - HH:mm");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy, hh:mm a");
        return outFormat.format(dtIn);
    }


    @SuppressLint("SimpleDateFormat")
    public static String GetSimpleTimeFormat(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss.SSS");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("hh:mm a");
        return outFormat.format(dtIn);
    }

    @SuppressLint("SimpleDateFormat")
    public static String GetSimpleTimeFormat24(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss.SSS");
            if (dateString != null)
                dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
        return outFormat.format(dtIn);
    }

    @SuppressLint("SimpleDateFormat")
    public static String GetTimeFormat(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("hh:mm a");
        return outFormat.format(dtIn);
    }

    @SuppressLint("SimpleDateFormat")
    public static String GetTimeFormat24(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
        return outFormat.format(dtIn);
    }

    @SuppressLint("SimpleDateFormat")
    public static String GetTime24(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "yyyy-MM-dd hh:mm a");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
        return outFormat.format(dtIn);
    }


    @SuppressLint("SimpleDateFormat")
    public static String GetDateAndTime(String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    "dd-MM-yyyy");
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return outFormat.format(dtIn);
    }

    public static String ChangeDateFormat(String currentFormat, String requestedFormat, String dateString) {
        Date dtIn = null;
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat(
                    currentFormat, Locale.getDefault());
            dtIn = (Date) inFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat(requestedFormat, Locale.getDefault());
        return outFormat.format(dtIn);
    }

    public static Date StringToDate(String format, String date) {

        DateFormat df = new SimpleDateFormat(format, Locale.ENGLISH);
        Date startDate = null;
        try {
            startDate = df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public static String DateToString(String format, Date date) {
        SimpleDateFormat inFormat = new SimpleDateFormat(
                format, Locale.getDefault());
        String reportDate = inFormat.format(date);
        return reportDate;
        //dtIn = (Date) inFormat.parse(date);
    }

    /*
     * Reduce image size
     */
    public static Bitmap Shrinkmethod(String file, int width, int height) {
        BitmapFactory.Options bitopt = new BitmapFactory.Options();
        bitopt.inJustDecodeBounds = true;
        Bitmap bit = BitmapFactory.decodeFile(file, bitopt);

        int h = (int) Math.ceil(bitopt.outHeight / (float) height);
        int w = (int) Math.ceil(bitopt.outWidth / (float) width);

        if (h > 1 || w > 1) {
            if (h > w) {
                bitopt.inSampleSize = h;

            } else {
                bitopt.inSampleSize = w;
            }
        }
        bitopt.inJustDecodeBounds = false;
        bit = BitmapFactory.decodeFile(file, bitopt);
        return bit;
    }

    // Convert Bitmap to string
    public static String BitMapToString(Bitmap bitmap) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 25, baos);
            byte[] b = baos.toByteArray();
            String temp = Base64.encodeToString(b, Base64.DEFAULT);
            return temp;
        }
        catch (OutOfMemoryError e)
        {
            e.printStackTrace();

            return  BitMapToString(Bitmap.createScaledBitmap(bitmap,bitmap.getWidth()/2, bitmap.getHeight()/2, false));
        }
    }

    public static String CheckLanguage(Context c) {
        String strlanguage = "";
        String langPref = "Language";
        SharedPreferences prefs = c.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        strlanguage = prefs.getString(langPref, "");
        return strlanguage;
    }

    public static void LoadLocale(Context c) {
        String langPref = "Language";
        SharedPreferences prefs = c.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        ConstantValues.strlanguage = prefs.getString(langPref, "");
        ChangeLang(c, ConstantValues.strlanguage);
    }

    public static void ChangeLang(Context ctx, String lang) {
        Locale myLocale;
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        SaveLocale(ctx, lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        ctx.getResources().updateConfiguration(config, ctx.getResources().getDisplayMetrics());

    }

    public static void SaveLocale(Context ctx, String lang) {
        String langPref = "Language";
        SharedPreferences prefs = ctx.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }

    public static String getLocale(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        return prefs.getString("Language", "en");
    }

    public static void Restart(final Context ctx, String lang) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(ctx.getString(R.string.need_re_start));
        builder.setCancelable(false);
        builder.setPositiveButton(ctx.getString(R.string.yes), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {


                Toast toast = Toast.makeText(ctx.getApplicationContext(), ctx.getString(R.string.already_active), Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        RestartTheApp(ctx, 2);
                    }
                }, 1000);


            }

        });
        builder.setNegativeButton(ctx.getString(R.string.no), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub
            }
        });
        builder.show();
    }

    public static void RestartTheApp(Context context, int delay) {
        if (delay == 0) {
            delay = 1;
        }
        //Log.e("", "restarting app");
        Intent restartIntent = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName());
        PendingIntent intent = PendingIntent.getActivity(
                context, 0,
                restartIntent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
        System.exit(2);
    }

    public static Bitmap getScaledBitmapLessThan720Width(String imagePath) {
        BitmapFactory.Options optionsSizes = new BitmapFactory.Options();
        optionsSizes.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imagePath, optionsSizes);

        int actualWidth = optionsSizes.outWidth;
        int actualHeight = optionsSizes.outHeight;

        // max Height and width values of the compressed image is taken as
        // 816x612

        if (actualHeight == 0) {
            actualHeight = 1;
        }

        float maxHeight = 720.0f;
        float maxWidth = 508.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the
        // image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = calculateInSampleSize(optionsSizes, actualWidth,
                actualHeight);

        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
