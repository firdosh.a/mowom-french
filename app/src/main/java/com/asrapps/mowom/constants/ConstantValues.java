package com.asrapps.mowom.constants;

public class ConstantValues {

    public static final String TAB_DASHBOARD = "tab_dashboard_identifier";
    public static final String TAB_PROFILE = "tab_profile_identifier";
    public static final String TAB_SETTINGS = "tab_settings_identifier";
    public static final String TAB_LOGOUT = "tab_logout_identifier";
    public static double latitude = 0.0;
    public static double longitude = 0.0;

    public static boolean isStartRunning = false;

    public static int SPLASH_TIME_OUT = 2000;
    public static final String kWSHeaderAuthToken = "auth_token";
    public static final String kWSHeaderContentType = "Content-Type";

    public static String strlanguage = "";

    // URL
//    public static String URL = "http://neerukaar.ir/app/webapi/";

    //    before 16 feb
//    public static String URL = "http://mowom.eu/app/webapi/";
    //change 16 feb
    public static String URL = "http://mowom.eu/dev1/webapi/";


    // ImageConstant URL
    public static final String ImageURL = "http://mowom.eu/dev1/public/uploads/employees/";
    //"http://betaprojects.in/mowom/images/supervisor/"; http://mowom.eu/images/taskimages/
    public static final String taskImageURL = "http://mowom.eu/dev1/images/taskimages/";
    public static final String taskImageSalesURL = "http://mowom.eu/dev1/images/salesimages/";
    //"http://betaprojects.in/mowom/images/taskimages/";

    public static final String LoginURL = URL + "login.php";
    public static final String DashBoardURL = URL + "dashboard.php";
    public static final String AppLangChangeURL = URL + "app_lang_change.php";
    public static final String ForgetPasswordURL = URL + "forgot.php";
    public static final String PendingTaskListURL = URL
            + "pending_task_listing.php";
    public static final String PendingTaskDetailsURL = URL
            + "pending_task_details.php";
    public static final String ChangeProfile = URL + "change_profile.php";
    public static final String GetProfile = URL + "profile.php";
    public static final String RejectedTaskURL = URL + "rejected_tasks.php";
    public static final String CompletedTaskURL = URL + "completed_tasks.php";
    public static final String UpdateTaskURL = URL + "update_taskreport.php";
    public static final String AddSalesTaskURL = URL + "add_sales.php";
    public static final String AddSalesENDTaskURL = URL + "end_sales_task.php";
    public static final String SurveyURL = URL + "add_survey.php";

    public static final String ListOfClient = URL + "get_clients.php";
    public static final String ListOfSurveyClient = URL + "survey_client_list.php";
    public static final String ListOfCCategory = URL + "categorylist.php";

    public static final String CompletedTaskDetailURL = URL + "completedtaskfulldetails.php";
    public static final String AlertScreenURL = URL + "new_taskdetails.php";
    public static final String AcceptMissionURL = URL + "accept_mission.php";
    public static final String RejectTaskURL = URL + "rejecttask.php";
    public static final String SpecialTaskURL = URL + "specialjoblist.php";
    public static final String ReportURL = URL + "task_report.php";
    public static final String UpdateLocURL = URL + "update_current_location.php";
    public static final String UpdateStatusURL = URL + "get_employee_status.php";
    public static final String StartTaskURL = URL + "start_task.php";
    public static final String TASK_ROUTE_LOCATION = URL + "taskroutelocation.php";
    public static final String BetteryStatusURL = URL + "battery_status.php";
    public static final String ListOfChangeStatus = URL + "emp_change_work_status.php";
    public static final String LogOutURL = URL + "logout.php";
    public static final String OFFLINELOCATIONURL = URL + "offlinepoints.php";
    public static final String SalesTaskStartURL = URL + "start_sales_task.php";

    public static final String DeviceURL = URL + "login.php";

    public static String task_org_name = "";
    public static String task_list_id = "";
    public static String task_list_issue = "";
    public static String task_list_date_time = "";
    public static String task_list_phone = "";
    public static String task_list_address = "";
    public static String task_detail_contactPersonName = "";
    public static String deviceToken = "";
    public static String mProgressStatus = "";

    public static final String PREFS_IS_LOGGEND_IN = "is_logged_in";

    public static final String PREFS_LOGIN_USER = "LoggedUserDetail";
    public static final String PREFS_LOGIN_DETAIL = "UserDetail";

    public static final String PREFS_DEVICE_ID = "Registration_ID";


}
