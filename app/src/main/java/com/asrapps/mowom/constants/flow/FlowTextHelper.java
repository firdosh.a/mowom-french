package com.asrapps.mowom.constants.flow;

import android.text.SpannableString;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by qtm-kalpesh on 16/7/16.
 */
public class FlowTextHelper {
    private static boolean mNewClassAvailable;

    /* class initialization fails when this throws an exception */
    static {
        try {
            Class.forName("android.text.style.LeadingMarginSpan$LeadingMarginSpan2");
            mNewClassAvailable = true;
        } catch (Exception ex) {
            mNewClassAvailable = false;
        }
    }

    public static void tryFlowText(String text, View thumbnailView, TextView messageView, Display display, int addPadding) {
        // There is nothing I can do for older versions, so just return
        if (!mNewClassAvailable) return;
        thumbnailView.measure(0, 0);
        messageView.measure(0, 0);

        Log.v("TTT", "thumbnailView = " + thumbnailView.getMeasuredHeight() + " " + thumbnailView.getMeasuredHeight());
        Log.v("TTT", "messageView = " + messageView.getMeasuredHeight() + " " + messageView.getMeasuredHeight());
        Log.v("TTT", "display = " + display.getWidth() + " " + display.getHeight());


        // Get height and width of the image and height of the text line
        thumbnailView.measure(display.getWidth(), display.getHeight());
        int height = thumbnailView.getMeasuredHeight();
        int width = thumbnailView.getMeasuredWidth() + addPadding;
        messageView.measure(width, height); //to allow getTotalPaddingTop
        int padding = messageView.getTotalPaddingTop();
        float textLineHeight = messageView.getPaint().getTextSize();

        // Set the span according to the number of lines and width of the image
        int lines = (int) Math.round((height - padding) / textLineHeight);

        Log.v("TTT", "textLineHeight = " + textLineHeight);
        Log.v("TTT", "lines = " + lines);

        SpannableString ss = new SpannableString(text);
        //For an html text you can use this line: SpannableStringBuilder ss = (SpannableStringBuilder)Html.fromHtml(text);
        ss.setSpan(new MyLeadingMarginSpan2(1, width), 0, ss.length(), 0);
        messageView.setText(ss);

        // Align the text with the image by removing the rule that the text is to the right of the image
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) messageView.getLayoutParams();
        int[] rules = params.getRules();
        rules[RelativeLayout.RIGHT_OF] = 0;
    }


}

