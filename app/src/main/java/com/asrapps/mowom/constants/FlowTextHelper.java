package com.asrapps.mowom.constants;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by richa on 28/11/16.
 */

public class FlowTextHelper {
    private static boolean mNewClassAvailable;

    /* class initialization fails when this throws an exception */
    static {
        try {
            Class.forName("android.text.style.LeadingMarginSpan$LeadingMarginSpan2");
            mNewClassAvailable = true;
        } catch (Exception ex) {
            mNewClassAvailable = false;
        }
    }

    public static void tryFlowText(String text, View thumbnailView, View thumbnailView1, View thumbnailView2, View thumbnailView3, TextView messageView, Display display, int addPadding) {
        // There is nothing I can do for older versions, so just return
        if (!mNewClassAvailable) return;

        thumbnailView.measure(0, 0);
        thumbnailView1.measure(0, 0);
        thumbnailView2.measure(0, 0);
        thumbnailView3.measure(0, 0);
        messageView.measure(0, 0);

        Log.v("Nerrukar Application", "thumbnailView = " + thumbnailView.getMeasuredHeight() + " " + thumbnailView.getMeasuredHeight());
        Log.v("Nerrukar Application", "messageView = " + messageView.getMeasuredHeight() + " " + messageView.getMeasuredHeight());
        Log.v("Nerrukar Application", "display = " + display.getWidth() + " " + display.getHeight());


        // Get height and width of the image and height of the text line
        thumbnailView.measure(display.getWidth(), display.getHeight());
        thumbnailView1.measure(display.getWidth(), display.getHeight());
        thumbnailView2.measure(display.getWidth(), display.getHeight());
        thumbnailView3.measure(display.getWidth(), display.getHeight());
        int height = thumbnailView.getMeasuredHeight() + thumbnailView1.getMeasuredHeight() + thumbnailView2.getMeasuredHeight() + thumbnailView3.getMeasuredHeight();
        int width = thumbnailView.getMeasuredWidth() + thumbnailView1.getMeasuredHeight() + thumbnailView2.getMeasuredHeight() + thumbnailView3.getMeasuredHeight() + addPadding;
        messageView.measure(width, height); //to allow getTotalPaddingTop
        int padding = messageView.getTotalPaddingTop();
        float textLineHeight = messageView.getPaint().getTextSize();

        // Set the span according to the number of lines and width of the image
        int lines = (int) Math.round((height - padding) / textLineHeight);

        Log.v("Nerrukar Application", "textLineHeight = " + textLineHeight);
        Log.v("Nerrukar Application", "lines = " + lines);

        //SpannableString ss = new SpannableString(text);
        //For an html text you can use this line:
        if (!text.equals("")) {
            SpannableStringBuilder ss = (SpannableStringBuilder) Html.fromHtml(String.valueOf("text abc test 123 in android overlaping solution find"));
            ss.setSpan(new MyLeadingMarginSpan2(1, width), 0, ss.length(), 0);
            messageView.setText(ss);
            messageView.setMovementMethod(LinkMovementMethod.getInstance()); // links

            // Align the text with the image by removing the rule that the text is to the right of the image
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) messageView.getLayoutParams();
            int[] rules = params.getRules();
            rules[RelativeLayout.RIGHT_OF] = 0;
        }
    }
}
