package com.asrapps.mowom.constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by richa on 28/10/16.
 */

public class SessionManager {

    private SharedPreferences mPrefs;

    public SessionManager(Context mContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public static void putLoginDetail(String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ConstantValues.PREFS_LOGIN_DETAIL, value);
        editor.commit();
    }

    public static String getLoginDetail(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(ConstantValues.PREFS_LOGIN_DETAIL, null);
    }
}
