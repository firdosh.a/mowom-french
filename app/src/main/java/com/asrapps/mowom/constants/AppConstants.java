package com.asrapps.mowom.constants;

import android.os.Environment;

import java.io.File;

public class AppConstants {
	public static final String appName = "Mowom";
	public static final String task_Id="taskId";
	public static final String et_id="et_Id";
	public static final String issue="issue";
	public static final String TEMP_PHOTO_FILE_NAME = "_POST_IMAGE.jpg";
	public static String tempImagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + appName + File.separator;

}
